<?php get_header(); ?>

<div class="page-header row">

	<div class="medium-5 columns">
		<h1>Case Studies</h1>
		<!--<span class="search-results-count"><?php echo get_the_date('Y-m-d'); ?></span>-->
	</div>

	<?php get_template_part('template-parts/page-header-search'); ?>

</div><!-- page-header -->




<div class="main-content no-right-column" data-equalizer="main-content" data-equalize-on="medium">



		<div class="main-content-main" data-equalizer-watch="main-content">

					<div class="main-content-main--breadcrumbs" data-equalizer-watch="main-content-headers">
							
							<?php
							if ( function_exists('yoast_breadcrumb') ) {
							yoast_breadcrumb('
							<p id="breadcrumbs">','</p>
							');
							}
							?>

						<!--<div class="sort-results">
							<span>Relevance</span> &nbsp; | &nbsp; <a href="#">Newest First</a>
						</div>-->

					</div>

		<div class="entry-content padded">

		
		<?php
		while ( have_posts() ) : the_post();

						 the_content(); ?>

						<?php get_template_part('layout'); ?>

		<?php the_post_navigation();

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				#comments_template();
			endif;

		endwhile; // End of the loop.
		?>

		</div>

		</div>


  		<div class="sidebar-left" data-equalizer-watch="main-content">

            <div class="sidebar--header" data-equalizer-watch="main-content-headers">
                <h3>Industries</h3> <svg class="icon icon-details"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-details"></use></svg>
            </div>

            <div class="padded">

                <?php
                # we need to get a list of all industries from the options page

                # reset choices
                $field['industries'] = array();
                        
                # get the textarea value from options page without any formatting
                $industries = get_field('industry', 'option', false);
                    
                # explode the value so that each line is a new array piece
                $industries = explode("\n", $industries);
                    
                # remove any unwanted white space
                $industries = array_map('trim', $industries);
                $url = get_site_url();

                # loop through array and add to field 'choices'
                if( is_array($industries) ) :
                    foreach( $industries as $industry ) :
                        if($industry === '- Select Industry -') continue; #skip first item      
                        #$industry2 = str_replace("&","&amp;",$industry);       
				
				

						$find = [" ", "&amp;"];
						$replace   = ["-", "_"];
						$cleaned_industry = str_replace($find, $replace, $industry);
					
                       	echo '<a href="' . $url . '/industries/?industry='.$cleaned_industry.'">'.$industry.'</a>';
                            
                    endforeach;
                endif; ?>

            </div>

	</div><!--sidebar-left -->




</div>



<?php get_footer();
