<?php
#require_once('../../../../wp-load.php');
#$post_id = '88780';	#clevertouch
#$post_id = '21345';	#hilti

	#get the employer
	$graduate_review_employer = get_field('graduate_review_employer', $post_id );	
	#echo '<p>graduate_review_employer: '.$graduate_review_employer.'</p>';


	$graduate_reviews = get_posts(array(
		'post_type'		=> 'graduate_reviews',	
		'post_status'	=> 'publish',
		'posts_per_page'=> '-1',
		'meta_query' => array(
			array(
				'key' 	=> 'graduate_review_employer',
				'value' => $graduate_review_employer,
				'compare' => '='
			)
		)	
	));
	#create our arrays
	$career_progression = array();
	$colleagues = array();
	$company_culture = array();
	$environmental_awareness = array();
	$responsibility = array();
	$training = array();
	$enjoyment = array();
	$work_life_balance = array();
	$compensation_benefits = array();

	foreach($graduate_reviews as $review) : 

		#pass the results into the array
		$career_progression[] = get_field( 'career_progression', $review->ID );
		$colleagues[] = get_field( 'colleagues', $review->ID );
		$company_culture[] = get_field( 'company_culture', $review->ID );
		$environmental_awareness[] = get_field( 'environmental_awareness', $review->ID );
		$responsibility[] = get_field( 'responsibility', $review->ID );
		$training[] = get_field( 'training', $review->ID );
		$enjoyment[] = get_field( 'enjoyment', $review->ID );
		$work_life_balance[] = get_field( 'work_life_balance', $review->ID );
		$compensation_benefits[] = get_field( 'compensation_benefits', $review->ID );

	endforeach;	

	$graduate_career_progression_total = array_sum($career_progression);
	#echo '<p>graduate_career_progression_total: '.$graduate_career_progression_total.'</p>';

	$graduate_colleagues_total = array_sum($colleagues);
	#echo '<p>graduate_colleagues_total: '.$graduate_colleagues_total.'</p>';

	$graduate_company_culture_total = array_sum($company_culture);
	#echo '<p>graduate_company_culture_total: '.$graduate_company_culture_total.'</p>';

	$graduate_environmental_awareness_total = array_sum($environmental_awareness);
	#echo '<p>graduate_environmental_awareness_total: '.$graduate_environmental_awareness_total.'</p>';

	$graduate_responsibility_total = array_sum($responsibility);
	#echo '<p>graduate_responsibility_total: '.$graduate_responsibility_total.'</p>';

	$graduate_training_total = array_sum($training);
	#echo '<p>graduate_training_total: '.$graduate_training_total.'</p>';

	$graduate_enjoyment_total = array_sum($enjoyment);
	#echo '<p>graduate_enjoyment_total: '.$graduate_enjoyment_total.'</p>';

	$graduate_work_life_balance_total = array_sum($work_life_balance);
	#echo '<p>graduate_work_life_balance_total: '.$graduate_work_life_balance_total.'</p>';

	$graduate_compensation_benefits_total = array_sum($compensation_benefits);
	#echo '<p>graduate_compensation_benefits_total: '.$graduate_compensation_benefits_total.'</p>';

	wp_reset_query();
			

	#get the total number of graduate reviews
	$graduate_reviews = new WP_Query( array( 
		'numberposts'	=> -1,
		'post_type'		=> 'graduate_reviews',
		'post_status'	=> 'publish',
		'meta_key'		=> 'graduate_review_employer', 
		'meta_value'	=> $graduate_review_employer 
	));
	$total_graduate_reviews = $graduate_reviews->found_posts;
	
	#get the total number of apprentice reviews
	$apprentice_reviews = new WP_Query( array( 
		'numberposts'	=> -1,
		'post_type'		=> 'apprentice_reviews',
		'post_status'	=> 'publish',
		'meta_key'		=> 'apprentice_review_employer', 
		'meta_value'	=> $graduate_review_employer 
	));
	$total_apprentice_reviews = $apprentice_reviews->found_posts;
	
	#get a combined total of review submissions
	$average_total_reviews = ($total_graduate_reviews + $total_apprentice_reviews);			
	
	#pass the employer name to get the user/company ID
	$company = get_users(
		array(
			'meta_query' => array(
				array(
					'key' => 'company_name',
					'value' => $graduate_review_employer,
					'compare' => '=='
				)
			)
		)
	);	
	foreach ( $company as $user ) {
		#now update the user/company meta
		
		update_user_meta( $user->ID, 'graduate_career_progression', $graduate_career_progression_total );
		update_user_meta( $user->ID, 'graduate_colleagues', $graduate_colleagues_total );
		update_user_meta( $user->ID, 'graduate_company_culture', $graduate_company_culture_total );
		update_user_meta( $user->ID, 'graduate_environmental_awareness', $graduate_environmental_awareness_total );
		update_user_meta( $user->ID, 'graduate_responsibility', $graduate_responsibility_total );
		update_user_meta( $user->ID, 'graduate_training', $graduate_training_total );
		update_user_meta( $user->ID, 'graduate_enjoyment', $graduate_enjoyment_total );
		update_user_meta( $user->ID, 'graduate_work_life_balance', $graduate_work_life_balance_total );
		update_user_meta( $user->ID, 'graduate_compensation_benefits', $graduate_compensation_benefits_total );		
		update_user_meta( $user->ID, 'graduate_total_review', $total_graduate_reviews );
		update_user_meta( $user->ID, 'average_total_reviews', $average_total_reviews );
		
	}		