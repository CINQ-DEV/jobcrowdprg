<?php
#require_once('../../../../wp-load.php');
#$user->ID = '419';	
	
	#get the total number of reviews
	$average_total_reviews = get_field('average_total_reviews', 'user_'.$user->ID );
#echo '<p>average_total_reviews: '.$average_total_reviews.'<br>';
	
	###
	# calculate all of the averages
	###
	$graduate_career_progression = get_field('graduate_career_progression', 'user_'.$user->ID );
	$apprentice_career_progression = get_field('apprentice_career_progression', 'user_'.$user->ID );
	if ($graduate_career_progression > 0 && $apprentice_career_progression == 0 ){
		$average_career_progression = (($graduate_career_progression)/$average_total_reviews);
		$career_progression = round(($average_career_progression),1); #round(($average_career_progression/20),2);
	} elseif ($graduate_career_progression == 0 && $apprentice_career_progression > 0 ){
		$average_career_progression = (($apprentice_career_progression)/$average_total_reviews);
		$career_progression = round(($average_career_progression),1); #round(($average_career_progression/20),2);
	} else {
		$average_career_progression = (($graduate_career_progression + $apprentice_career_progression)/$average_total_reviews);
		$career_progression = round(($average_career_progression),1); #round(($average_career_progression/20),2);		
	}

#echo '<p>graduate_career_progression: '.$graduate_career_progression.'<br>';
#echo 'apprentice_career_progression: '.$apprentice_career_progression.'<br>';
#echo 'career_progression: '.$career_progression.'</p>';


	$graduate_colleagues = get_field('graduate_colleagues', 'user_'.$user->ID );
	$apprentice_colleagues = get_field('apprentice_colleagues', 'user_'.$user->ID );
	if ($graduate_colleagues > 0 && $apprentice_colleagues == 0 ){
		$average_colleagues = (($graduate_colleagues)/$average_total_reviews);
		$colleagues = round(($average_colleagues),1);
	} elseif ($graduate_colleagues == 0 && $apprentice_colleagues > 0 ){
		$average_colleagues = (($apprentice_colleagues)/$average_total_reviews);
		$colleagues = round(($average_colleagues),1);
	} else {
		$average_colleagues = (($graduate_colleagues + $apprentice_colleagues)/$average_total_reviews);
		$colleagues = round(($average_colleagues),1);
	}

#echo '<p>graduate_colleagues: '.$graduate_colleagues.'<br>';
#echo 'apprentice_colleagues: '.$apprentice_colleagues.'<br>';
#echo 'colleagues: '.$colleagues.'</p>';
						   
	$graduate_company_culture = get_field('graduate_company_culture', 'user_'.$user->ID );
	$apprentice_company_culture = get_field('apprentice_company_culture', 'user_'.$user->ID );
	if ($graduate_company_culture > 0 && $apprentice_company_culture == 0 ){
		$average_company_culture = (($graduate_company_culture)/$average_total_reviews);
		$company_culture = round(($average_company_culture),1);	
	} elseif ($graduate_company_culture == 0 && $apprentice_company_culture > 0 ){
		$average_company_culture = (($graduate_company_culture + $apprentice_company_culture)/$average_total_reviews);
		$company_culture = round(($average_company_culture),1);	
	} else {
		$average_company_culture = (($graduate_company_culture + $apprentice_company_culture)/$average_total_reviews);
		$company_culture = round(($average_company_culture),1);	
	}

#echo '<p>graduate_company_culture: '.$graduate_company_culture.'<br>';
#echo 'apprentice_company_culture: '.$apprentice_company_culture.'<br>';
#echo 'company_culture: '.$company_culture.'</p>';
								
	$graduate_environmental_awareness = get_field('graduate_environmental_awareness', 'user_'.$user->ID );
	$apprentice_environmental_awareness = get_field('apprentice_environmental_awareness', 'user_'.$user->ID );
	if ($graduate_environmental_awareness > 0 && $apprentice_environmental_awareness == 0 ){
		$average_environmental_awareness = (($graduate_environmental_awareness )/$average_total_reviews);	
		$environmental_awareness = round(($average_environmental_awareness),1);						
	} elseif ($graduate_environmental_awareness == 0 && $apprentice_environmental_awareness > 0 ){
		$average_environmental_awareness = (($apprentice_environmental_awareness)/$average_total_reviews);	
		$environmental_awareness = round(($average_environmental_awareness),1);						
	} else {
		$average_environmental_awareness = (($graduate_environmental_awareness + $apprentice_environmental_awareness)/$average_total_reviews);	
		$environmental_awareness = round(($average_environmental_awareness),1);						
	}

#echo '<p>graduate_environmental_awareness: '.$graduate_environmental_awareness.'<br>';
#echo 'apprentice_environmental_awareness: '.$apprentice_environmental_awareness.'<br>';
#echo 'environmental_awareness: '.$environmental_awareness.'</p>';
	
	$graduate_responsibility = get_field('graduate_responsibility', 'user_'.$user->ID );
	$apprentice_responsibility = get_field('apprentice_responsibility', 'user_'.$user->ID );
	if ($graduate_responsibility > 0 && $apprentice_responsibility == 0 ){
		$average_responsibility = (($graduate_responsibility )/$average_total_reviews);
		$responsibility = round(($average_responsibility),1);
	} elseif ($graduate_responsibility == 0 && $apprentice_responsibility > 0 ){
		$average_responsibility = (($apprentice_responsibility)/$average_total_reviews);
		$responsibility = round(($average_responsibility),1);
	} else {
		$average_responsibility = (($graduate_responsibility + $apprentice_responsibility)/$average_total_reviews);
		$responsibility = round(($average_responsibility),1);
	}

#echo '<p>graduate_responsibility: '.$graduate_responsibility.'<br>';
#echo 'apprentice_responsibility: '.$apprentice_responsibility.'<br>';
#echo 'responsibility: '.$responsibility.'</p>';
		
	$graduate_training = get_field('graduate_training', 'user_'.$user->ID );
	$apprentice_training = get_field('apprentice_training', 'user_'.$user->ID );
	if ($graduate_training > 0 && $apprentice_training == 0 ){
		$average_training = (($graduate_training)/$average_total_reviews);
		$training = round(($average_training),1);
	} elseif ($graduate_training == 0 && $apprentice_training > 0 ){
		$average_training = (($apprentice_training)/$average_total_reviews);
		$training = round(($average_training),1);
	} else {
		$average_training = (($graduate_training + $apprentice_training)/$average_total_reviews);
		$training = round(($average_training),1);
	}

#echo '<p>graduate_training: '.$graduate_training.'<br>';
#echo 'apprentice_training: '.$apprentice_training.'<br>';
#echo 'training: '.$training.'</p>';
						 
	$graduate_enjoyment = get_field('graduate_enjoyment', 'user_'.$user->ID );
	$apprentice_enjoyment = get_field('apprentice_enjoyment', 'user_'.$user->ID );
	if ($graduate_enjoyment > 0 && $apprentice_enjoyment == 0 ){
		$average_enjoyment = (($graduate_enjoyment)/$average_total_reviews);	
		$enjoyment = round(($average_enjoyment),1);					   
	} elseif ($graduate_enjoyment == 0 && $apprentice_enjoyment > 0 ){
		$average_enjoyment = (($apprentice_enjoyment)/$average_total_reviews);	
		$enjoyment = round(($average_enjoyment),1);					   
	} else {
		$average_enjoyment = (($graduate_enjoyment + $apprentice_enjoyment)/$average_total_reviews);	
		$enjoyment = round(($average_enjoyment),1);					   
	}

#echo '<p>graduate_enjoyment: '.$graduate_enjoyment.'<br>';
#echo 'apprentice_enjoyment: '.$apprentice_enjoyment.'<br>';
#echo 'enjoyment: '.$enjoyment.'</p>';
	
	$graduate_work_life_balance = get_field('graduate_work_life_balance', 'user_'.$user->ID );
	$apprentice_work_life_balance = get_field('apprentice_work_life_balance', 'user_'.$user->ID );
	if ($graduate_work_life_balance > 0 && $apprentice_work_life_balance == 0 ){
		$average_work_life_balance = (($graduate_work_life_balance)/$average_total_reviews);
		$work_life_balance = round(($average_work_life_balance),1);
	} elseif ($graduate_work_life_balance == 0 && $apprentice_work_life_balance > 0 ){
		$average_work_life_balance = (($apprentice_work_life_balance)/$average_total_reviews);
		$work_life_balance = round(($average_work_life_balance),1);
	} else {
		$average_work_life_balance = (($graduate_work_life_balance + $apprentice_work_life_balance)/$average_total_reviews);
		$work_life_balance = round(($average_work_life_balance),1);
	}

#echo '<p>graduate_work_life_balance: '.$graduate_work_life_balance.'<br>';
#echo 'apprentice_work_life_balance: '.$apprentice_work_life_balance.'<br>';
#echo 'work_life_balance: '.$work_life_balance.'</p>';

	$graduate_compensation_benefits = get_field('graduate_compensation_benefits', 'user_'.$user->ID );
	$apprentice_compensation_benefits = get_field('apprentice_compensation_benefits', 'user_'.$user->ID );
	if ($graduate_compensation_benefits > 0 && $apprentice_compensation_benefits == 0 ){
		$average_compensation_benefits = (($graduate_compensation_benefits)/$average_total_reviews);
		$compensation_benefits = round(($average_compensation_benefits),1);
	} elseif ($graduate_compensation_benefits == 0 && $apprentice_compensation_benefits > 0 ){
		$average_compensation_benefits = (($apprentice_compensation_benefits)/$average_total_reviews);
		$compensation_benefits = round(($average_compensation_benefits),1);
	} else {
		$average_compensation_benefits = (($graduate_compensation_benefits + $apprentice_compensation_benefits)/$average_total_reviews);
		$compensation_benefits = round(($average_compensation_benefits),1);
	}

#echo '<p>graduate_compensation_benefits: '.$graduate_compensation_benefits.'<br>';
#echo 'apprentice_compensation_benefits: '.$apprentice_compensation_benefits.'<br>';
#echo 'compensation_benefits: '.$compensation_benefits.'</p>';
	
	$overall_rating = ($career_progression + $colleagues + $company_culture + $environmental_awareness + $responsibility + $training + $enjoyment + $work_life_balance + $compensation_benefits);
	#round to nearest whole number. Calculate the MEAN average
	$rating = round(($overall_rating/9),1); 
	#$rating = round($overall_rating/9); 
#echo 'rating: '.$rating.'</p>';
		
	#update all the averages	

	update_user_meta( $user->ID, 'average_career_progression', $career_progression );
	update_user_meta( $user->ID, 'average_colleagues', $colleagues );
	update_user_meta( $user->ID, 'average_company_culture', $company_culture );
	update_user_meta( $user->ID, 'average_environmental_awareness', $environmental_awareness );
	update_user_meta( $user->ID, 'average_responsibility', $responsibility );
	update_user_meta( $user->ID, 'average_training', $training );
	update_user_meta( $user->ID, 'average_enjoyment', $enjoyment );
	update_user_meta( $user->ID, 'average_work_life_balance', $work_life_balance );
	update_user_meta( $user->ID, 'average_compensation_benefits', $compensation_benefits );
	update_user_meta( $user->ID, 'overall_rating', $rating );	
