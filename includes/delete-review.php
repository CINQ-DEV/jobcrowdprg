<?php
# Our include
require_once('../../../../wp-load.php');

$nonce = $_REQUEST['_wpnonce'];

if ( ! wp_verify_nonce( $nonce, 'prg-delete-review-nonce' ) ) :
	
	#nonce doesn't match or exist so terminate script!
	die( "<h1>Security Check!</h1><p>You're trying to access this page directly. If you believe you're seeing this page in error, please contact the administrator!</p>" ); 

else:

	$selector = 'field_595cec87bddf0';
	$row = $_GET['row'];
	$user_id = 'user_'.get_current_user_id();
	
	#echo 'User: '.$user_id.'<br>'; #debug
	#echo 'Row: '.$row.'<br>';		#debug
	
	delete_row( $selector, $row, $user_id );
	
	#$redirect = $_SERVER['HTTP_REFERER'].'&importSuccess=true';
	$redirect = $_SERVER['HTTP_REFERER'];
	#echo $redirect; #debug
	
	header('Location: '.$redirect); die; 

endif;
?>