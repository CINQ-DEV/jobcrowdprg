<?php
# Our include
require_once('../../../../wp-load.php');

$nonce = $_REQUEST['_wpnonce'];

if ( ! wp_verify_nonce( $nonce, 'prg-import-recipients-nonce' ) ) :
	
	#nonce doesn't match or exist so terminate script!
	die( "<h1>Security Check!</h1><p>You're trying to access this page directly. If you believe you're seeing this page in error, please contact the administrator!</p>" ); 

else:

	$post_id = $_GET['aid'];
	$file = get_field('csv', $post_id);
	$skip_first_row = get_field('skip_first_row', $post_id);
	#echo '<p>Post ID: '.$post_id.'</p>'; #debug
	#echo '<p>File: '.$file.'</p>'; #debug
	
	$survey_type = get_field('survey_type',$post_id);
	if ($survey_type == 'Graduate') :
		$survey = 'student';
	else:
		$survey = 'apprentice';
	endif;
	
	if( $file ):
		
		$csvFile = fopen($file, 'r');	
		
		#skip first line if it has column headings
		if ($skip_first_row == 1){
			fgetcsv($csvFile);
		}
					
		#parse data from csv file line by line
		$x = 0; #set counter
		while(($line = fgetcsv($csvFile)) !== FALSE){	
		
			$x++; #increment counter	
			
			#we need to create the user accounts on import
				
			#lets construct the user login
			$initial = substr($line[0], 0, 1);
			$user_login = $initial.$line[1];
				
			#we can also auto generate a password
			$password = wp_generate_password( 12, false ); 
				
			#set up the user meta
			$userdata = array(
				'first_name'	=> $line[0],
				'last_name'		=> $line[1],	
				'user_email'	=> $line[2],
				'user_login'	=> $user_login,	
				'role'			=> $survey,
				'user_pass'		=> $password
			);
				
			$user_id[$i] = wp_insert_user( $userdata ) ;
				
			# On success
			if ( ! is_wp_error( $user_id[$i] ) ) {
				$user_status =  "User created";
				#wp_mail( $email_address, 'Welcome!', 'Your Password: ' . $password );
				#wp_mail( $line[2], 'Welcome!', 'Your Password: ' . $password );
			} else {
				$user_status =  "User exists";	
			}			
			
			$row = array(
				
				'field_5947dbd28c36e'	=> $line[0],		#forename
				'field_5947dbd78c36f'	=> $line[1],		#surname
				'field_5947dbdc8c370'	=> $line[2],		#email address
				'field_5996cd0d95b6c' 	=> '',				#partially completed
				'field_5947dbe48c371' 	=> '',				#completed				
				'field_5996a05bc0d7d'	=> $post_id.'-'.$x,	#import_id		
				'field_59e0b97b6788b' 	=> $user_status		#user_status
				
			);
			
			$i = add_row('field_5947dbbd8c36d', $row, $post_id);
			
					
		}
					
		#close opened csv file
		fclose($csvFile);
		
	
	endif; #endif $file
	
#redirect 

$redirect = $_SERVER['HTTP_REFERER'].'&importSuccess=true';
#echo $redirect; #debug

header('Location: '.$redirect); die; 
	


endif; #endif $nonce
?>