<?php
#require_once('../../../../wp-load.php');
#$post_id = '88780';	

	#get the employer
	$apprentice_review_employer = get_field('apprentice_review_employer', $post_id );	
	#echo '<p>graduate_review_employer: '.apprentice_review_employer.'</p>';


	$graduate_reviews = get_posts(array(
		'post_type'		=> 'apprentice_reviews',
		'post_status'	=> 'publish',
		'posts_per_page'=> '-1',
		'meta_query' => array(
			array(
				'key' 	=> 'apprentice_review_employer',
				'value' => $apprentice_review_employer,
				'compare' => '='
			)
		)	
	));
	#create our arrays
	$career_progression = array();
	$colleagues = array();
	$company_culture = array();
	$environmental_awareness = array();
	$responsibility = array();
	$training = array();
	$enjoyment = array();
	$work_life_balance = array();
	$compensation_benefits = array();

	foreach($graduate_reviews as $review) : 

		#pass the results into the array
		$career_progression[] = get_field( 'career_progression', $review->ID );
		$colleagues[] = get_field( 'colleagues', $review->ID );
		$company_culture[] = get_field( 'company_culture', $review->ID );
		$environmental_awareness[] = get_field( 'environmental_awareness', $review->ID );
		$responsibility[] = get_field( 'responsibility', $review->ID );
		$training[] = get_field( 'training', $review->ID );
		$enjoyment[] = get_field( 'enjoyment', $review->ID );
		$work_life_balance[] = get_field( 'work_life_balance', $review->ID );
		$compensation_benefits[] = get_field( 'compensation_benefits', $review->ID );

	endforeach;	

	$apprentice_career_progression_total = array_sum($career_progression);
	#echo '<p>apprentice_career_progression_total: '.$apprentice_career_progression_total.'</p>';

	$apprentice_colleagues_total = array_sum($colleagues);
	#echo '<p>apprentice_colleagues_total: '.$apprentice_colleagues_total.'</p>';

	$apprentice_company_culture_total = array_sum($company_culture);
	#echo '<p>apprentice_company_culture_total: '.$apprentice_company_culture_total.'</p>';

	$apprentice_environmental_awareness_total = array_sum($environmental_awareness);
	#echo '<p>apprentice_environmental_awareness_total: '.$apprentice_environmental_awareness_total.'</p>';

	$apprentice_responsibility_total = array_sum($responsibility);
	#echo '<p>apprentice_responsibility_total: '.$apprentice_responsibility_total.'</p>';

	$apprentice_training_total = array_sum($training);
	#echo '<p>apprentice_training_total: '.$apprentice_training_total.'</p>';

	$apprentice_enjoyment_total = array_sum($enjoyment);
	#echo '<p>apprentice_enjoyment_total: '.$apprentice_enjoyment_total.'</p>';

	$apprentice_work_life_balance_total = array_sum($work_life_balance);
	#echo '<p>apprentice_work_life_balance_total: '.$apprentice_work_life_balance_total.'</p>';

	$apprentice_compensation_benefits_total = array_sum($compensation_benefits);
	#echo '<p>apprentice_compensation_benefits_total: '.$apprentice_compensation_benefits_total.'</p>';

	wp_reset_query();
			

	#get the total number of graduate reviews
	$graduate_reviews = new WP_Query( array( 
		'numberposts'	=> -1,
		'post_type'		=> 'graduate_reviews',
		'post_status'	=> 'publish',
		'meta_key'		=> 'graduate_review_employer', 
		'meta_value'	=> $apprentice_review_employer 
	));
	$total_graduate_reviews = $graduate_reviews->found_posts;
	
	#get the total number of apprentice reviews
	$apprentice_reviews = new WP_Query( array( 
		'numberposts'	=> -1,
		'post_type'		=> 'apprentice_reviews',
		'post_status'	=> 'publish',
		'meta_key'		=> 'apprentice_review_employer', 
		'meta_value'	=> $apprentice_review_employer 
	));
	$total_apprentice_reviews = $apprentice_reviews->found_posts;
	
	#get a combined total of review submissions
	$average_total_reviews = ($total_graduate_reviews + $total_apprentice_reviews);	
	
	#pass the employer name to get the user/company ID
	$company = get_users(
		array(
			'meta_query' => array(
				array(
					'key' => 'company_name',
					'value' => $apprentice_review_employer,
					'compare' => '=='
				)
			)
		)
	);	
	foreach ( $company as $user ) {
		#now update the user/company meta
		
		update_user_meta( $user->ID, 'apprentice_career_progression', $apprentice_career_progression_total );
		update_user_meta( $user->ID, 'apprentice_colleagues', $apprentice_colleagues_total );
		update_user_meta( $user->ID, 'apprentice_company_culture', $apprentice_company_culture_total );
		update_user_meta( $user->ID, 'apprentice_environmental_awareness', $apprentice_environmental_awareness_total );
		update_user_meta( $user->ID, 'apprentice_responsibility', $apprentice_responsibility_total );
		update_user_meta( $user->ID, 'apprentice_training', $apprentice_training_total );
		update_user_meta( $user->ID, 'apprentice_enjoyment', $apprentice_enjoyment_total );
		update_user_meta( $user->ID, 'apprentice_work_life_balance', $apprentice_work_life_balance_total );
		update_user_meta( $user->ID, 'apprentice_compensation_benefits', $apprentice_compensation_benefits_total );	
		update_user_meta( $user->ID, 'apprentice_total_review', $total_apprentice_reviews );
		update_user_meta( $user->ID, 'average_total_reviews', $average_total_reviews );
		
	}		