<?php
# Our include
require_once('../../../../wp-load.php');

$nonce = $_REQUEST['_wpnonce'];

if ( ! wp_verify_nonce( $nonce, 'prg-delete-review-nonce' ) ) :
	
	#nonce doesn't match or exist so terminate script!
	die( "<h1>Security Check!</h1><p>You're trying to access this page directly. If you believe you're seeing this page in error, please contact the administrator!</p>" ); 

else:

	$selector = 'field_595cec87bddf0';
	$rid = $_GET['review_id'];
	$uid = 'user_'.get_current_user_id();
	
	#echo 'User: '.$uid.'<br>'; #debug
	#echo 'Review ID: '.$rid.'<br>';		#debug
	
	if( have_rows('stored_reviews', $uid) ):

		$i = 1;

		while ( have_rows('stored_reviews', $uid) ) : the_row();

		$review_id = get_sub_field('review_id');
		$review_title = get_sub_field('review_title');
		$count = count(get_sub_field('review_id'));
		
		echo '<p>ID: '.$review_id.' - Title: '.$review_title.'</p>';
		
		#get the row id if the review id selected mis equal to the loop
		if ($rid==$review_id):

			delete_row( $selector, $i, $uid );

		endif;

		echo $i;
		$i++;

		endwhile;

	endif;
	
		
	

	$redirect = $_SERVER['HTTP_REFERER'];
	#echo $redirect; #debug
	
	header('Location: '.$redirect); die; 

endif;
?>