jQuery(".add-review").click(function(){								 

	var rid = jQuery(this).attr("rid");
	var uid = jQuery(this).attr("uid");
	var title = jQuery(this).attr("title");
		
	//alert("Review ID="+rid);
	//alert("User ID="+uid);	
	//alert("Title="+title);
	
	jQuery.ajax({

		url : ajax_url,                 // Use our localized variable that holds the AJAX URL
		type: 'POST',                   // Declare our ajax submission method ( GET or POST )
		data: {
			action  : 'prgStoreReview',	// AJAX POST Action
			'rid': rid,  
			'uid': uid,       
			'title': title						
		},               

		cache		: false,
		success		: function(data) {
			//alert(this.data);
			console.log("success!");
			
			jQuery('#msg').html(data).fadeIn('slow');
			jQuery('#msg').html("<p><i class='fa fa-check' aria-hidden='true'></i> Review successfully saved to your account!</p>").fadeIn('slow') //also show a success message 
			jQuery('#msg').delay(5000).fadeOut('slow');	
			jQuery(".add-review").html("");
			
		},
		error		: function(xhr, status, error) {
			var err = eval("(" + xhr.responseText + ")");
			alert(err.Message);
		}
	});
});