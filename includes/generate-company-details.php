<?php
# Our include
require_once('../../../../wp-load.php');

$nonce = $_REQUEST['_wpnonce'];

if ( ! wp_verify_nonce( $nonce, 'prg-company-details-nonce' ) ) :
	
	#nonce doesn't match or exist so terminate script!
	die( "<h1>Security Check!</h1><p>You're trying to access this page directly. If you believe you're seeing this page in error, please contact the administrator!</p>" ); 

else:

	$file = 'company_details_'.date("d-m-Y_H-i",time());

	#setup download headers
	header("Content-Type: text/csv");
	#header("Content-Disposition: attachment; filename=User_Sample.csv");
	header('Content-Disposition: attachement; filename="'.$file.'.csv"');
	# Disable caching
	header("Cache-Control: no-cache, no-store, must-revalidate"); # HTTP 1.1
	header("Pragma: no-cache"); # HTTP 1.0
	header("Expires: 0"); # Proxies

	date_default_timezone_set("Europe/London");



$companies = array(
	'role__in'     	=> array('customer', 'subscriber'),
	'role__not_in' 	=> array('administrator'),
	'exclude'      	=> array(1),
	'number' 		=> -1,
	'orderby' 		=> 'meta_value',
	'meta_key'		=> 'company_name',
	'order' 		=> 'ASC',
	'meta_query' =>
	array(
		array(
			'relation' => 'AND', # OR						
			array(
				'key' => 'activate_membership',
				'value' => 'Yes',
				'compare' => "=",
			)
		)
	)						
);		

$all_users = get_users( $companies );

$data = array();

#create column headers
$data[] = array(
	"Company ID",
	"Company Name",
	"Main Address",
	"Industry",
	"Number of employees",
	"Graduates",
	"Apprentices",
	"Number of graduates hired/year",
	"Number of apprentices hired/year",
	"Primary Name",
	"Primary Position held",
	"Primary Email",
	"Primary Telephone number",
	"Secondary Name",
	"Secondary Position held",
	"Secondary Email",
	"Secondary Telephone number",
	"Add Another Contact",
	"Additional Name",
	"Additional Position held",
	"Additional Email",
	"Additional Telephone number",
	"Activate Membership",
	
	"Billing Firstname",
	"Billing Surname",
	"Billing Phone",
	"Billing Email",	
	"Billing Company",
	"Billing Address Line 1",
	"Billing Address Line 2",
	"Billing City",
	"Billing County",
	"Billing Postcode",
	"Billing Country",
	
	"Logo",
	"Biography",
	"1. What makes you a great company to work for?",
	"2. Number of Graduate Positions this year",
	"3. Total number of company employees",
	"4. Social Media Links",
	"5. Careers that you recruit for",
	"6. Locations",
	"7. Contact Details",
	"Gallery",
	"Promotional Video",
	"File Download"

); 

if ($all_users):
	
	foreach ( $all_users as $user ) :

$employer_registration_industry = get_field('employer_registration_industry', 'user_'.$user->ID );
$industry = implode(', ', $employer_registration_industry); 

$logo = get_field('logo', 'user_'.$user->ID );
	
if( have_rows('social_media_links', 'user_'.$user->ID) ):
	$social_media_links = array();
	while( have_rows('social_media_links', 'user_'.$user->ID) ): the_row(); 
		$facebook_url = get_sub_field('facebook_url');
		$twitter_url = get_sub_field('twitter_url');
		$linkedin_url = get_sub_field('linkedin_url');
		$youtube_url = get_sub_field('youtube_url');
		$other = get_sub_field('other');
		
		if( $facebook_url ):		
			$social_media_links[] = 'Facebook: '.$facebook_url;
		endif;
		if( $twitter_url ):		
			$social_media_links[] = 'Twitter: '.$twitter_url;
		endif;
		if( $linkedin_url ):		
			$social_media_links[] = 'LinkedIn: '.$linkedin_url;
		endif;
		if( $youtube_url ):		
			$social_media_links[] = 'YouTube: '.$youtube_url;
		endif;
		if( $other ):		
			$social_media_links[] = 'Other: '.$other;
		endif;

	endwhile;
endif; #endif $social_media_links
$social_media = implode(', ', $social_media_links); 

if( have_rows('contact_details', 'user_'.$user->ID) ):
	$contact_details = array();
	while( have_rows('contact_details', 'user_'.$user->ID) ): the_row(); 
		$phone = get_sub_field('phone');
		$email = get_sub_field('email');
		$web = get_sub_field('web');
		$address = get_sub_field('address');
		$contact_name = get_sub_field('contact_name');
		
		if( $phone ):		
			$contact_details[] = 'Phone: '.$phone;
		endif;
		if( $email ):		
			$contact_details[] = 'Email: '.$email;
		endif;
		if( $web ):		
			$contact_details[] = 'Web: '.$web;
		endif;
		if( $address ):		
			$contact_details[] = 'Address: '.$addressW['address'];
		endif;
		if( $contact_name ):		
			$contact_details[] = 'Contact Name: '.$contact_name;
		endif;

	endwhile;
endif; #endif $social_media_links
$contact_details = implode(', ', $contact_details); 

if( have_rows('gallery', 'user_'.$user->ID) ):
	$gallery = array();
	while( have_rows('gallery', 'user_'.$user->ID) ): the_row(); 
		$image = get_sub_field('image');
		
		if( $image ):		
			$gallery[] = $image['url'];
		endif;

	endwhile;
endif; #endif $social_media_links
$gallery = implode(', ', $gallery); 
	
$file_download = get_field('file_download', 'user_'.$user->ID);

$data[] = array(
	"Company ID" => $user->ID,
	"Company Name" => get_user_meta( $user->ID, 'company_name', true ),
	"Main Address" => get_user_meta( $user->ID, 'main_address', true ),
	"Industry" => $industry,
	"Number of employees" => get_user_meta( $user->ID, 'number_of_employees', true ),
	"Graduates" => get_user_meta( $user->ID, 'graduates', true ),
	"Apprentices" => get_user_meta( $user->ID, 'apprentices', true ),
	"Number of graduates hired/year" => get_user_meta( $user->ID, 'number_of_graduates_hiredyear', true ),
	"Number of apprentices hired/year" => get_user_meta( $user->ID, 'number_of_apprentices_hiredyear', true ),
	"Primary Name" => get_user_meta( $user->ID, 'primary_name', true ),
	"Primary Position held" => get_user_meta( $user->ID, 'primary_position_held', true ),
	"Primary Email" => get_user_meta( $user->ID, 'primary_email', true ),
	"Primary Telephone number" => get_user_meta( $user->ID, 'primary_telephone_number', true ),
	"Secondary Name" => get_user_meta( $user->ID, 'secondary_name', true ),
	"Secondary Position held" => get_user_meta( $user->ID, 'secondary_position_held', true ),
	"Secondary Email" => get_user_meta( $user->ID, 'secondary_email', true ),
	"Secondary Telephone number" => get_user_meta( $user->ID, 'secondary_telephone_number', true ),
	"Add Another Contact" => get_user_meta( $user->ID, 'add_another_contact', true ),
	"Additional Name" => get_user_meta( $user->ID, 'additional_name', true ),
	"Additional Position held" => get_user_meta( $user->ID, 'additional_position_held', true ),
	"Additional Email" => get_user_meta( $user->ID, 'additional_email', true ),
	"Additional Telephone number" => get_user_meta( $user->ID, 'additional_telephone_number', true ),
	"Activate Membership" => get_user_meta( $user->ID, 'activate_membership', true ),

	"Billing Firstname" => get_user_meta( $user->ID, 'billing_first_name', true ),
	"Billing Surname" => get_user_meta( $user->ID, 'billing_last_name', true ),
	"Billing Phone" => get_user_meta( $user->ID, 'billing_phone', true ),
	"Billing Email" => get_user_meta( $user->ID, 'billing_email', true ),
	"Billing Company" => get_user_meta( $user->ID, 'billing_company', true ),
	"Billing Address Line 1" => get_user_meta( $user->ID, 'billing_address_1', true ),
	"Billing Address Line 2" => get_user_meta( $user->ID, 'billing_address_2', true ),
	"Billing City" => get_user_meta( $user->ID, 'billing_city', true ),
	"Billing County" => get_user_meta( $user->ID, 'billing_state', true ),
	"Billing Postcode" => get_user_meta( $user->ID, 'billing_postcode', true ),
	"Billing Country" => get_user_meta( $user->ID, 'billing_country', true ),
	
	"Logo" => $logo['url'],
	"Biography" => get_user_meta( $user->ID, 'biography', true ),
	"1. What makes you a great company to work for?" => get_user_meta( $user->ID, 'what_makes_you_a_great_company_to_work_for', true ),
	"2. Number of Graduate Positions this year" => get_user_meta( $user->ID, 'number_of_graduate_positions_this_year', true ),
	"3. Total number of company employees" => get_user_meta( $user->ID, 'total_number_of_company_employees', true ),
	"4. Social Media Links" => $social_media, #repeater
	"5. Careers that you recruit for" => get_user_meta( $user->ID, 'careers_that_you_recruit_for', true ),
	"6. Locations" => get_user_meta( $user->ID, 'locations', true ),
	"7. Contact Details" => $contact_details, #repeater
	"Gallery" => $gallery, #repeater
	"Promotional Video" => get_user_meta( $user->ID, 'promotional_video', true ),
	"File Download" => $file_download['url']

); 






	endforeach;

endif;



	



	#create the download
	$output = fopen("php://output", "w");
	foreach ($data as $row) {
		fputcsv($output, $row); // here you can change delimiter/enclosure
	}
	fclose($output);


	
	#redirect 
	$redirect = $_SERVER['HTTP_REFERER'].'&selectReport=true';
	#echo $redirect; #debug
	#header('Location: '.$redirect); die; 
	
endif; #endif $nonce
?>