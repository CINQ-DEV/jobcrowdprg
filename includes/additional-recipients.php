<?php
# Our include
require_once('../../../../wp-load.php');

$nonce = $_REQUEST['_wpnonce'];

if ( ! wp_verify_nonce( $nonce, 'prg-additional-recipients-nonce' ) ) :
	
	#nonce doesn't match or exist so terminate script!
	die( "<h1>Security Check!</h1><p>You're trying to access this page directly. If you believe you're seeing this page in error, please contact the administrator!</p>" ); 

else:

	$post_id = $_GET['aid'];
	$file = get_field('csv', $post_id);
	$skip_first_row = get_field('skip_first_row', $post_id);
	#echo '<p>Post ID: '.$post_id.'</p>'; #debug
	#echo '<p>File: '.$file.'</p>'; #debug
	
	if( $file ):
		
		$csvFile = fopen($file, 'r');	
		
		#skip first line if it has column headings
		if ($skip_first_row == 1){
			fgetcsv($csvFile);
		}
					
		#parse data from csv file line by line
		#$last_row = count( get_field( 'recipients', $post_id ) ); #set counter
		#$x = ($last_row + 3);

		#https://support.advancedcustomfields.com/forums/topic/get-specific-row-from-repeater-field/
		#run the repeater	
		#$post_id was 1078 - may have been for testing
		while( have_rows('recipients', $post_id) ): the_row();
			$my_data[] = get_row();
		endwhile;

		#get the last row on the list
		$total_rows = count( get_field( 'recipients', $post_id ) );
		#echo '<p>Total rows: '. $total_rows.'</p>';

		#need to remove one due to indexing discrepancies
		$row_index = ($total_rows - 1);
		#echo '<p>New total: '. $row_index.'</p>';

		#$row_index = 20;
		$import_id = $my_data[$row_index][field_5996a05bc0d7d];
		$forename = $my_data[$row_index][field_5947dbd28c36e];
		$email = $my_data[$row_index][field_5947dbdc8c370];
		/*
		echo '<p>import_id: '. $import_id.'</p>';
		echo '<p>forename: '. $forename.'</p>';
		echo '<p>email: '. $email.'</p>';
		*/

		#need to get the new row number to prevent issues should one be removed
		$row 		= explode("-", $import_id);
		$list_id	= $row[0];	
		$row_id		= $row[1];	

		$survey_type = get_field('survey_type',$post_id);
		$survey_type = get_field('survey_type',$post_id);
		if ($survey_type == 'Graduate') :
			$survey = 'student';
		else:
			$survey = 'apprentice';
		endif;

		#echo '<p>list_id: '. $list_id.'</p>';
		#echo '<p>row_id: '. $row_id.'</p>';

		#$new_import_id = ($row_id + 1);
		#echo '<p>new_import_id: '. $new_import_id.'</p>';
		
		while(($line = fgetcsv($csvFile)) !== FALSE){	
		
			$row_id++; #increment counter	
			
			#we need to create the user accounts on import
				
			#lets construct the user login
			$initial = substr($line[0], 0, 1);
			$user_login = $initial.$line[1];
				
			#we can also auto generate a password
			$password = wp_generate_password( 12, false ); 
				
			#set up the user meta
			$userdata = array(
				'first_name'	=> $line[0],
				'last_name'		=> $line[1],	
				'user_email'	=> $line[2],
				'user_login'	=> $user_login,	
				'role'			=> $survey_type,
				'user_pass'		=> $password
			);
				
			$user_id[$i] = wp_insert_user( $userdata ) ;
				
			# On success
			if ( ! is_wp_error( $user_id[$i] ) ) {
				$user_status =  "User created";
				#wp_mail( $email_address, 'Welcome!', 'Your Password: ' . $password );
				#wp_mail( $line[2], 'Welcome!', 'Your Password: ' . $password );
			} else {
				$user_status =  "User exists";	
			}			
			
			$row = array(
				
				'field_5947dbd28c36e'	=> $line[0],			#forename
				'field_5947dbd78c36f'	=> $line[1],			#surname
				'field_5947dbdc8c370'	=> $line[2],			#email address
				'field_5996cd0d95b6c' 	=> '',					#partially completed
				'field_5947dbe48c371' 	=> '',					#completed				
				'field_5996a05bc0d7d'	=> $post_id.'-'.$row_id,#import_id	
				'field_59e0b97b6788b' 	=> $user_status			#user_status	
				
			);
			
			$i = add_row('field_5947dbbd8c36d', $row, $post_id);
			
					
		}
					
		#close opened csv file
		fclose($csvFile);
		
	
	endif; #endif $file
	
#redirect 

$redirect = $_SERVER['HTTP_REFERER'].'&importSuccess=true';

header('Location: '.$redirect); die; 
	
endif; #endif $nonce
?>