<?php
# Our include
require_once('../../../../wp-load.php');

$nonce = $_REQUEST['_wpnonce'];

if ( ! wp_verify_nonce( $nonce, 'prg-delete-survey-recipient-nonce' ) ) :
	
	#nonce doesn't match or exist so terminate script!
	die( "<h1>Security Check!</h1><p>You're trying to access this page directly. If you believe you're seeing this page in error, please contact the administrator!</p>" ); 

else:

	$selector = 'field_5947dbbd8c36d';
	$row = $_GET['row'];
	$pID = $_GET['pID'];
	
	#delete the specific row
	delete_row( $selector, $row, $pID );	

	#get the email address without passing via URLs
	if( have_rows('recipients',$pID) ): 
		$counter = 0;
		while( have_rows('recipients',$pID) ): the_row(); 

			$counter++;
			#loop through until we get to the row we need, then return the email address
			if ($counter == $row) :
			   $email_address = get_sub_field('email_address');
			endif;

	  endwhile;
	endif;

	#get the user ID we need to delete from the email
	$user = get_user_by( 'email', $email_address );	

	if ( ! empty( $user ) ) :
		wp_delete_user( $user->ID );
	endif;

	$redirect = $_SERVER['HTTP_REFERER'];
	#$redirect = $_SERVER['REQUEST_URI'];
	echo $redirect;
	header('Location: '.$redirect); die; 

endif;
?>