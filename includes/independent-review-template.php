<?php
#get the post type from the ID
$post_type = get_post_type( $post_id );
if ($post_type == 'graduate_reviews'){
	$type = 'graduate';
} else {
	$type = 'apprentice';
}

$responsibility = get_field($type.'_review_how_would_you_rate_the_level_of_responsibility_you_have', $post_id );	
if ($responsibility == 'Excellent') {
	$responsibility_value = '5';
} elseif ($responsibility == 'Very Good') {
	$responsibility_value = '4';
} elseif ($responsibility == 'Good') {
	$responsibility_value = '3';
} elseif ($responsibility == 'Fair') {
	$responsibility_value = '2';
} elseif ($responsibility == 'Poor') {
	$responsibility_value = '1';	
} else {
	$responsibility_value = '0';
}

$work_life_balance = get_field($type.'_review_how_would_you_rate_your_worklife_balance', $post_id );	
if ($work_life_balance == 'Excellent') {
	$work_life_balance_value = '5';
} elseif ($work_life_balance == 'Very Good') {
	$work_life_balance_value = '4';
} elseif ($work_life_balance == 'Good') {
	$work_life_balance_value = '3';
} elseif ($work_life_balance == 'Fair') {
	$work_life_balance_value = '2';
} elseif ($work_life_balance == 'Poor') {
	$work_life_balance_value = '1';	
} else {
	$work_life_balance_value = '0';
}

$environmental_awareness = get_field($type.'_review_how_would_you_rate_the_ethical_environmental_awareness_of_your_company', $post_id );	
if ($environmental_awareness == 'Excellent') {
	$environmental_awareness_value = '5';
} elseif ($environmental_awareness == 'Very Good') {
	$environmental_awareness_value = '4';
} elseif ($environmental_awareness == 'Good') {
	$environmental_awareness_value = '3';
} elseif ($environmental_awareness == 'Fair') {
	$environmental_awareness_value = '2';
} elseif ($environmental_awareness == 'Poor') {
	$environmental_awareness_value = '1';	
} else {
	$environmental_awareness_value = '0';
}

$compensation_benefits = get_field($type.'_review_how_would_you_rate_the_benefits_package', $post_id );	
if ($compensation_benefits == 'Excellent') {
	$compensation_benefits_value = '5';
} elseif ($compensation_benefits == 'Very Good') {
	$compensation_benefits_value = '4';
} elseif ($compensation_benefits == 'Good') {
	$compensation_benefits_value = '3';
} elseif ($compensation_benefits == 'Fair') {
	$compensation_benefits_value = '2';
} elseif ($compensation_benefits == 'Poor') {
	$compensation_benefits_value = '1';	
} else {
	$compensation_benefits_value = '0';
}

$company_culture = get_field($type.'_review_how_would_you_rate_the_company_office_culture', $post_id );	
if ($company_culture == 'Excellent') {
	$company_culture_value = '5';
} elseif ($company_culture == 'Very Good') {
	$company_culture_value = '4';
} elseif ($company_culture == 'Good') {
	$company_culture_value = '3';
} elseif ($company_culture == 'Fair') {
	$company_culture_value = '2';
} elseif ($company_culture == 'Poor') {
	$company_culture_value = '1';	
} else {
	$company_culture_value = '0';
}

$career_progression = get_field($type.'_review_how_would_you_rate_the_career_progression_at_the_company_you_work_for', $post_id );	
if ($career_progression == 'Excellent') {
	$career_progression_value = '5';
} elseif ($career_progression == 'Very Good') {
	$career_progression_value = '4';
} elseif ($career_progression == 'Good') {
	$career_progression_value = '3';
} elseif ($career_progression == 'Fair') {
	$career_progression_value = '2';
} elseif ($career_progression == 'Poor') {
	$career_progression_value = '1';	
} else {
	$career_progression_value = '0';
}

$colleagues = get_field($type.'_review_how_would_you_rate_the_colleagues_in_the_department_you_work_in', $post_id );	
if ($colleagues == 'Excellent') {
	$colleagues_value = '5';
} elseif ($colleagues == 'Very Good') {
	$colleagues_value = '4';
} elseif ($colleagues == 'Good') {
	$colleagues_value = '3';
} elseif ($colleagues == 'Fair') {
	$colleagues_value = '2';
} elseif ($colleagues == 'Poor') {
	$colleagues_value = '1';	
} else {
	$colleagues_value = '0';
}

$training = get_field($type.'_review_how_would_you_rate_the_usefulness_of_your_training', $post_id );	
if ($training == 'Excellent') {
	$training_value = '5';
} elseif ($training == 'Very Good') {
	$training_value = '4';
} elseif ($training == 'Good') {
	$training_value = '3';
} elseif ($training == 'Fair') {
	$training_value = '2';
} elseif ($training == 'Poor') {
	$training_value = '1';	
} else {
	$training_value = '0';
}

#$enjoyment = get_field($type.'_review_how_would_you_rate_the_enjoyment_of_the_training', $post_id );	

$post_type = get_post_type( $post_id );
if ($post_type == 'graduate_reviews'){
	$enjoyment = get_field('graduate_review_now_in_general_how_much_do_you_enjoy_your_day-to-day_work', $post_id );
} else {
	$enjoyment = get_field('apprentice_review_now_in_general_how_much_do_you_enjoy_your_day_to_day_work', $post_id );
}

if ($enjoyment == 'Excellent') {
	$enjoyment_value = '5';
} elseif ($enjoyment == 'Very Good') {
	$enjoyment_value = '4';
} elseif ($enjoyment == 'Good') {
	$enjoyment_value = '3';
} elseif ($enjoyment == 'Fair') {
	$enjoyment_value = '2';
} elseif ($enjoyment == 'Poor') {
	$enjoyment_value = '1';	
} else {
	$enjoyment_value = '0';
}


$star_rating = $responsibility_value + $work_life_balance_value + $environmental_awareness_value + $compensation_benefits_value + $company_culture_value + $career_progression_value + $colleagues_value + $training_value + $enjoyment_value;
$stars = round(($star_rating/9),1);


$employer = get_field($type.'_review_employer', $post_id );
if ($employer){
	$company_name = get_field($type.'_review_employer', $post_id );
} else { 
	$company_name = get_field($type.'_review_employer_other', $post_id );
}

#pass the employer name to get the user/company ID
$company = get_users(
array(
	'meta_query' => array(
		array(
			'key' => 'company_name',
			'value' => $company_name,
			'compare' => '=='
		)
	)
)
);	
foreach ( $company as $user ) {
	$user_id = $user->ID;
}

#get the location (only applicable to enhnaced user profiles)
$locations = get_field('locations', 'user_'.$user_id);				
				
#get the main_address
$main_address = get_field('main_address', 'user_'.$user_id);
				
#use geocoding to get the necessary details
$geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?sensor=false&address=' . urlencode($main_address) . '&key=AIzaSyAj_Xc1gEDKeDqLIMFHgok9hhWgyf16TK0');
$output = json_decode($geocode);	
				
#get the address components
$address_data = $output->results[0]->address_components;
$street = str_replace('Dr', 'Drive', $address_data[1]->long_name);
$town = $address_data[2]->long_name;
$county = $address_data[3]->long_name;	

$subject = 'Checking the values';
$message = 'responsibility: '.$responsibility_value.'<br>';
$message .= 'work_life_balance: '.$work_life_balance_value.'<br>';
$message .= 'environmental_awareness: '.$environmental_awareness_value.'<br>';
$message .= 'compensation_benefits: '.$compensation_benefits_value.'<br>';
$message .= 'company_culture: '.$company_culture_value.'<br>';
$message .= 'career_progression: '.$career_progression_value.'<br>';
$message .= 'colleagues: '.$colleagues_value.'<br>';
$message .= 'training: '.$training_value.'<br>';
$message .= 'enjoyment: '.$enjoyment_value.'<br>';
$message .= 'star_rating: '.$stars.'<br>';
$message .= 'user_id: '.$user_id.'<br>';
$message .= 'location: '.$town.'<br>';
$email_address = 'ian@prgltd.co.uk';
#wp_mail( $email_address, $subject, $message );	

update_post_meta( $post_id, 'responsibility', $responsibility_value ); 
update_post_meta( $post_id, 'work_life_balance', $work_life_balance_value );
update_post_meta( $post_id, 'environmental_awareness', $environmental_awareness_value );
update_post_meta( $post_id, 'compensation_benefits', $compensation_benefits_value );
update_post_meta( $post_id, 'company_culture', $company_culture_value );
update_post_meta( $post_id, 'career_progression', $career_progression_value );
update_post_meta( $post_id, 'colleagues', $colleagues_value );
update_post_meta( $post_id, 'training', $training_value );
update_post_meta( $post_id, 'enjoyment', $enjoyment_value );
update_post_meta( $post_id, 'star_rating', $stars );
update_post_meta( $post_id, 'location', $town );
