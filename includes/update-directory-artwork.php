<?php
require_once('../../../../wp-load.php');
$nonce = $_REQUEST['_wpnonce'];
if ( ! wp_verify_nonce( $nonce, 'prg-artwork-nonce' ) ) :
	
	#nonce doesn't match or exist so terminate script!
	die( "<h1>Security Check!</h1><p>You're trying to access this page directly. If you believe you're seeing this page in error, please contact the administrator!</p>" ); 

else:

	#echo '<h1>Switching or Adding Image...</h1>';

	$post_id = $_GET['oid'];
	#echo $post_id;
	
	#Lets get the replacement image URL
	global $wpdb; 
	$meta_value = $wpdb->get_var("
		SELECT  meta_value
		FROM  $wpdb->postmeta
		WHERE  post_id = $post_id
		AND  meta_key LIKE  'replacement_artwork'
	");
	
	#echo $wpdb->last_query;
	#echo 'Meta Value: '.$meta_value.'<br>'; #debug
	
	$url = wp_get_attachment_url( $meta_value );
	#echo $url.'<br>'; #debug

	global $woocommerce, $wpdb; 	
	$woocommerce_order_itemmeta = $wpdb->prefix.'woocommerce_order_itemmeta';
	$woocommerce_order_items = $wpdb->prefix.'woocommerce_order_items';
	#Get the existing image - if one exists
	$meta_value = $wpdb->get_var("
		SELECT $woocommerce_order_itemmeta.meta_value
		FROM $woocommerce_order_itemmeta
		INNER JOIN $woocommerce_order_items ON $woocommerce_order_itemmeta.order_item_id = $woocommerce_order_items.order_item_id
		WHERE $woocommerce_order_itemmeta.meta_key LIKE 'Artwork'
		AND $woocommerce_order_items.order_id = $post_id"
	);
	#Get the order item ID
	$order_item_id = $wpdb->get_var("
		SELECT order_item_id
		FROM $woocommerce_order_items
		WHERE order_id = $post_id"
	);	
	#echo $wpdb->last_query.'<br>';
	#echo 'Meta Value: '.$meta_value.'<br>'; #debug
	#echo 'Order Item ID: '.$order_item_id.'<br>'; #debug
	
	#Create the new image URL
	$new_meta_value = '<a href="'.$url.'">'.$url.'</a>';
	
	if ($meta_value):
			
		global $woocommerce, $wpdb; 
		$table_name = $wpdb->prefix.'woocommerce_order_itemmeta';
		
		$wpdb->query( $wpdb->prepare( 
			"
			UPDATE $table_name
			SET meta_value = %s
			WHERE order_item_id = %d
				AND meta_key = %s
			",
				$new_meta_value, $order_item_id, 'Artwork'
		) );
		
		#echo $wpdb->last_query.'<br>';
		#echo '<p>File Replaced</p>';
		
		$meta_value = $wpdb->get_var("
			SELECT $woocommerce_order_itemmeta.meta_value
			FROM $woocommerce_order_itemmeta
			INNER JOIN $woocommerce_order_items ON $woocommerce_order_itemmeta.order_item_id = $woocommerce_order_items.order_item_id
			WHERE $woocommerce_order_itemmeta.meta_key LIKE 'Artwork'
			AND $woocommerce_order_items.order_id = $post_id"
		);
		
		#echo $wpdb->last_query.'<br>';
	
	else:
	
		global $woocommerce, $wpdb; 
		$table_name = $wpdb->prefix.'woocommerce_order_itemmeta';
		$wpdb->insert($table_name, array(
			'order_item_id' => $order_item_id,
			'meta_key' => 'Artwork',
			'meta_value' => $new_meta_value
		));
		#echo $wpdb->last_query.'<br>';
		#echo '<p>New File</p>';
		
	endif;
	
	#redirect 
	$redirect = $_SERVER['HTTP_REFERER'];
	header('Location: '.$redirect);die;
	
endif;
?>