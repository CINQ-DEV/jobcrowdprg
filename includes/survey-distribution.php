<?php
# Our include
require_once('../../../../wp-load.php');

$nonce = $_REQUEST['_wpnonce'];

if ( ! wp_verify_nonce( $nonce, 'prg-survey-status-nonce' ) ) :
	
	#nonce doesn't match or exist so terminate script!
	die( "<h1>Security Check!</h1><p>You're trying to access this page directly. If you believe you're seeing this page in error, please contact the administrator!</p>" ); 

else:

	$lid = $_GET['lid'];
	$uid = $_GET['uid'];
	$survey = $_GET['survey'];

	
	#get the repeater, for each row in the repeater use wp_mail()
	#append survey url to email
	if( have_rows('recipients', $lid) ):
		while( have_rows('recipients', $lid) ): the_row(); 
		
			# vars
			$import_id 				= get_sub_field('import_id');			
			$forename 				= get_sub_field('forename');
			$surname 				= get_sub_field('surname');
			$email_address 			= get_sub_field('email_address');
			$partially_completed 	= get_sub_field('partially_completed');
			$completed 				= get_sub_field('completed');
			$survey_type			= get_sub_field('survey_type');

			$pw_reset_link = wp_lostpassword_url();
			$login_url = site_url().'/my-account/'; 
			
			#the survey hasn't been completed so send email
			if ( $completed != '1' && $email_address != '' ):
				
				$newsletter = get_field('newsletter', $lid);
				if( $newsletter ) :
					
					# create the mail function
					$to 		= $email_address;
					#$to = 'ian@prgltd.co.uk';
					$subject	= "The Job Crowd :: Your Survey";
					$message	= "Dear {$forename}, <br/>";
					$message	.= "{$newsletter} <br/>";
					$message	.= "If you've not logged in before, please reset your password here: {$pw_reset_link}.<br/>";
					$message	.= "Alternatively, you can login with this email address by clicking here: {$login_url}.";
					
					if ($survey == 'graduate'):
						$message	.= "Survey: <a href=\"".get_the_permalink( 318 )."?iid={$import_id}&uid={$uid}\">Click here</a>.";	
					else:
						$message	.= "Survey: <a href=\"".get_the_permalink( 460 )."?iid={$import_id}&uid={$uid}\">Click here</a>.";	
					endif;
					
					$headers = array(
						'Content-Type: text/html; charset=UTF-8',
						'From: '.get_option( 'blogname' ).' <info@thejobcrowd.com>'
					);		
					wp_mail ($to, $subject, $message, $headers);							
					
				endif; #endif $newsletter
				
			endif; #endif $email_address
		
		endwhile;
		
	endif; #endif #recipients
	
#redirect 
$redirect = $_SERVER['HTTP_REFERER'].'&?surveySent=true';
#echo $nonce.'<br>';
#echo $redirect;
header('Location: '.$redirect); die; 


endif; #endif $nonce
?>