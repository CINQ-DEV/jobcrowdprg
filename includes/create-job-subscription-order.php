<?php

		$disableOrderCompleteEmail = 'true';	

		#set the subscription date to start when the initial order is placed
		#$start_date = $order_data['date_created']->date('Y-m-d H:i:s');
	
		$date = date('Y-m-d', strtotime($order->order_date));
		$time = '00:00:00'; #need to set the time to zero, otherwise it errors stating the subscription can't start on the same day
		$start_date = $date.' '.$time;	

		#map all the existing order details in to the new order
		$address = array(
			'first_name' => $order_data['billing']['first_name'],
			'last_name'  => $order_data['billing']['last_name'],
			'company'    => $order_data['billing']['company'],
			'email'      => $order_data['billing']['email'],
			'phone'      => $order_data['billing']['phone'],
			'address_1'  => $order_data['billing']['address_1'],
			'address_2'  => $order_data['billing']['address_2'], 
			'city'       => $order_data['billing']['city'],
			'state'      => $order_data['billing']['state'],
			'postcode'   => $order_data['billing']['postcode'],
			'country'    => $order_data['billing']['country']
		);	

		#set the main subscription product ID (parent)
		$parent_product = wc_get_product(117);

		$args = array(
			'attribute_subscription-length' => '12 Months' #set which subscription to use
		);	

		$product_variation = $parent_product->get_matching_variation($args);

		$product = wc_get_product($product_variation);  

		$quantity = 1;

		#as far as I can see, you need to create the order first, then the sub
		$order = wc_create_order(array('customer_id' => $user_id));

		$order->add_product( $product, $quantity, $args);
		$order->set_address( $address, 'billing' );

		$order->calculate_totals();

		$order->update_status("completed", 'Imported order', TRUE);
		
		###
		# Order created, now create sub attached to it 
		###
	
		#each variation has a different subscription period
		$period = WC_Subscriptions_Product::get_period( $product );
		$interval = WC_Subscriptions_Product::get_interval( $product );	

		#for ($x = 0; $x <= $maximum; $x++) {
	
			$sub = wcs_create_subscription(array('order_id' => $order->id, 'billing_period' => $period, 'billing_interval' => $interval, 'start_date' => $start_date));

			$sub->add_product( $product, $quantity, $args);
			$sub->set_address( $address, 'billing' );

			$sub->calculate_totals();

			WC_Subscriptions_Manager::activate_subscriptions_for_order($order);	

				$subject = 'Checking our for loop v2';
				$message = 'Iteration: '.$x;
				$email_address = 'ian@prgltd.co.uk';
				#wp_mail( $email_address, $subject, $message );				
			
		#}#end for $x