<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Survey Status Reports</title>
<link rel='stylesheet' id='main-stylesheet-css'  href='https://www.thejobcrowd.com/wp-content/themes/jobcrowdprg/dist/assets/css/app.css' type='text/css' media='all' />

</head>
<body>
<div class="page-header row">

	<div class="medium-5 columns">
		<h1>Survey Status Reports</h1>

	</div>

</div><!-- page-header -->

<div class="main-content fullwidth" data-equalizer="main-content">



		<div class="main-content-main" data-equalizer-watch="main-content">

<?php
# Our include
require_once('../../../../wp-load.php');

$nonce = $_REQUEST['_wpnonce'];

if ( ! wp_verify_nonce( $nonce, 'prg-survey-status-nonce' ) ) :
	
	#nonce doesn't match or exist so terminate script!
	die( "<h1>Security Check!</h1><p>You're trying to access this page directly. If you believe you're seeing this page in error, please contact the administrator!</p>" ); 

else:

	/*
	function my_posts_where( $where ) {
		$where = str_replace("meta_key = 'recipients_%", "meta_key LIKE 'recipients_%", $where);
		return $where;
	}
	add_filter('posts_where', 'my_posts_where'); 
	*/	
	function my_posts_where( $where ) {
		global $wpdb;
		$where = str_replace(
			"meta_key = 'recipients_%", 
			"meta_key LIKE 'recipients_%",
			$wpdb->remove_placeholder_escape($where)
		);
		return $where;
	}

	add_filter('posts_where', 'my_posts_where');	


	# lets get all surveys with at least some survey data
	$args = array(
		'numberposts'	=> -1,
		'post_type'		=> 'surveys',
		'meta_key'		=> 'recipients_%_completed',
		'meta_value'	=> '1',	
		'meta_compare'	=> 'LIKE'	
	);


	# query
	$the_query = new WP_Query( $args );

	?>
	<?php if( $the_query->have_posts() ): ?>
		<table id="example" class="items display" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th>Company</th>
				<th>Non-Starters</th>
				<th>Partially Completed</th>
				<th>Completed</th>
				<th>Total Recipients</th>
				<th>Survey</th>
				<th>Link</th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<th>Company</th>
				<th>Non-Starters</th>
				<th>Partially Completed</th>
				<th>Completed</th>
				<th>Total Recipients</th>
				<th>Survey</th>
				<th>Link</th> 
			</tr>
		</tfoot>    
		<tbody>
		<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
			<tr id="<?php the_ID(); ?>" class="item <?php echo $selected; ?>">

	<?php
	$rows = get_field('recipients',$post->ID);
	$completed_counter = array();
	$partially_completed_counter = array();	
	$non_starters_counter = array();			
	if($rows):
		foreach($rows as $row):

			$numrows = count($rows);		
			$completed = $row['completed'];
			$partially_completed = $row['partially_completed'];	


			if ($completed):	
				$completed_counter[] = $post->ID;
			elseif ($partially_completed):	
				$partially_completed_counter[] = $post->ID;
			else: 
				$non_starters_counter[] = $post->ID;
			endif;		

		endforeach;
	endif;

	$numrows = 	count($rows);		
	$completed_surveys = count($completed_counter);
	$partially_surveys = count($partially_completed_counter);
	$none = ($completed_surveys+$partially_surveys)	;
	$non_starters = count(non_starters_counter);

	$post_author_id = get_post_field( 'post_author', $post->ID ); 	
	$company_name = get_field('company_name', 'user_'.$post_author_id);	
	$url = 'https://www.thejobcrowd.com/wp-admin/post.php?post=';

	?>		
				<td><?php echo $company_name; ?></td>
				<td><?php $none = ($numrows-$none); if ($none != 0): echo $none; endif; ?></td>
				<td><?php if ($partially_surveys != 0): echo $partially_surveys; endif; ?></td>
				<td><?php if ($completed_surveys != 0): echo $completed_surveys; endif; ?></td>
				<td><?php echo $numrows; ?></td>
				<td><?php echo get_the_title($post->ID); ?></td>
				<td><a href="<?php echo $url.$post->ID; ?>&action=edit">view the survey here</a></td>

			</tr>
		<?php endwhile; ?>
		</tbody>
		</table>   
		<link href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet" />
		
		<script src="//code.jquery.com/jquery-1.12.4.js"></script>
		<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

		<script src="//cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
		<script src="//cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
		<script src="//cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
		<script src="//cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>


		<script>
		$(document).ready(function() {
			$('#example').DataTable( {
				dom: 'Bfrtip',
				buttons: [
					'copy', 'csv', 'excel', 'pdf', 'print'
				]
			} );
		} );	
		</script>
	
	<?php endif; 

	wp_reset_query();	 // Restore global post data stomped by the_post(). 
	
endif; #endif $nonce
?>

		</div><!-- main-content-main -->



</div>
 
</body>
</html>