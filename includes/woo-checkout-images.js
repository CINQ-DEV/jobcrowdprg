/*********************************************************************************/
/* JavaScript Document                                                           */
/*********************************************************************************/


/*********************************************************************************/
/* Lets convert image URL to thumbnail WooCommerce > Orders page > Order details */
/* This saves column space and stops the table blowing out                       */
/*********************************************************************************/
function wrap( str ) {
	return '<img src="' + str + '" width="50px" />';
};

function replaceText() {
	jQuery(".display_meta a").each( function(){
		jQuery(this).html(jQuery(this).html().replace(/\bhttp[^ ]+/ig, wrap));
	})

}

jQuery(document).ready(replaceText);