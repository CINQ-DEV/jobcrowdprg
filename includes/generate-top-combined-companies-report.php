<?php
# Our include
require_once('../../../../wp-load.php');

$nonce = $_REQUEST['_wpnonce'];

if ( ! wp_verify_nonce( $nonce, 'prg-generate-top-1oo-report-nonce' ) ) :
	
	#nonce doesn't match or exist so terminate script!
	die( "<h1>Security Check!</h1><p>You're trying to access this page directly. If you believe you're seeing this page in error, please contact the administrator!</p>" ); 

else:

	$filename = "top_combined_companies_report_".date("d-m-Y_H-i",time());

	#setup download headers
	header("Content-Type: text/csv");
	#header("Content-Disposition: attachment; filename=User_Sample.csv");
	header('Content-Disposition: attachement; filename="'.$filename.'.csv"');
	# Disable caching
	header("Cache-Control: no-cache, no-store, must-revalidate"); # HTTP 1.1
	header("Pragma: no-cache"); # HTTP 1.0
	header("Expires: 0"); # Proxies

	date_default_timezone_set("Europe/London");

	$custom_report_from = get_field('custom_report_from',option , false, false);
	$custom_report_from = new DateTime($custom_report_from);
	$from_date_time = $custom_report_from->format('Y-m-d').' 00:00:00';
	#echo '<p>From: '.$from_date_time.'</p>';

	$custom_report_to = get_field('custom_report_to',option , false, false);
	$custom_report_to = new DateTime($custom_report_to);
	$to_date_time = $custom_report_to->format('Y-m-d').' 00:00:00';
	#echo '<p>To: '.$to_date_time.'</p>';

	$number_of_results = get_field('number_of_results', option);

	$args = array(
		#'role__in'		=> array('employer', 'subscriber'),
		'role__in'		=> array('customer', 'subscriber'),
		'role__not_in'	=> array('administrator'),
		'exclude'		=> array(1),			
		'meta_key'		=> 'activate_membership',
		'meta_value'	=> 'Yes',
		'meta_compare'	=> '=',
		'orderby'		=> 'order_clause',
		'order'			=> 'DESC',
		'number'		=> $number_of_results,
		'meta_query' 	=> array(
				'order_clause' => array(
					'key' => 'overall_rating',
					'value' => array(0,5),
					'compare' => 'BETWEEN'
					#'type' => 'NUMERIC' # unless the field is not a number											
				)
		),		
		'date_query'	 => array(
			array(
				'before'     => $to_date_time,
				'inclusive' => true,
			),		
			array(
				'after'     => $from_date_time,
				'inclusive' => true,
			)
		)			
	);
	$users = get_users($args);	

	$data = array();

	if ( $users ) :
	
		#create column headers
		$data[] = array(
				'id',
				'Company Name',
				'Overall Rating',
				'Responsibility',
				'Work Life Balance',
				'Environmental Awareness',
				'Compensation &amp; Benefits',
				'Company Culture',
				'Career Progression',
				'Colleagues',
				'Training',
				'Enjoyment'
		); 
	
		foreach ($users as $user) : 
			$id = $user->ID;

			$data[] = array(
				'id' => $user->ID,
				'Company Name' => get_field('company_name', 'user_'.$user->ID),  
				'Overall Rating' => get_field('overall_rating', 'user_'.$user->ID),
				'Responsibility' => get_field('average_responsibility', 'user_'.$user->ID),
				'Work Life Balance' => get_field('average_work_life_balance', 'user_'.$user->ID),
				'Environmental Awareness' => get_field('average_environmental_awareness', 'user_'.$user->ID),
				'Compensation &amp; Benefits' => get_field('average_compensation_benefits', 'user_'.$user->ID),
				'Company Culture' => get_field('average_company_culture', 'user_'.$user->ID),
				'Career Progression' => get_field('average_career_progression', 'user_'.$user->ID),
				'Colleagues' => get_field('average_colleagues', 'user_'.$user->ID),
				'Training' => get_field('average_training', 'user_'.$user->ID),
				'Enjoyment' => get_field('average_enjoyment', 'user_'.$user->ID)
			);

		endforeach;
		
	endif;
	#reset 
	wp_reset_postdata();

	#create the download
	$output = fopen("php://output", "w");
	foreach ($data as $row) {
		fputcsv($output, $row); // here you can change delimiter/enclosure
	}
	fclose($output);


	
	#redirect 
	$redirect = $_SERVER['HTTP_REFERER'].'&selectReport=true';
	#echo $redirect; #debug
	#header('Location: '.$redirect); die; 
	
endif; #endif $nonce
?>