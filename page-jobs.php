<?php
/*
 Template Name: Jobs  
 */

get_header(); ?>


<div class="page-header row">

	<div class="medium-5 columns">
		<h1>Find a Job</h1>
		<!--<span class="search-results-count">1,465 jobs found</span>-->
	</div>
	
	<?php get_template_part('template-parts/page-header-search'); ?>

</div><!-- page-header -->


<div class="main-content no-right-column" data-equalizer="main-content">


	<div class="main-content-main" >

				<div class="main-content-main--breadcrumbs" data-equalizer-watch="main-content-headers">
							<?php
							if ( function_exists('yoast_breadcrumb') ) {
							yoast_breadcrumb('
							<p id="breadcrumbs">','</p>
							');
							}
							?>
						
						<div class="save-share">

							<?php get_template_part('template-parts/share-button'); ?>

						</div><!--save-share-->

				</div>



<?php
# let's handle the form submission
$industries	= $_POST['industries'];
$town 		= $_POST['towns'];	
$types 		= $_POST['types'];		
$submit		= $_POST['submit'];


if ($submit):
		
	#create an array
	$industry_result = array();

	#loop through each industry and check for a match, add to array if so
	foreach($industries as $industry) :

				$cleanedindustry = str_replace("&","&amp;",$industry);		

				$job_ind = get_posts(array(
					'post_type' => 'jobs',
					'post_status' => 'publish',
					'meta_query' => array(
						array(
							'key' 	=> 'dynamic_filled_industry',
							'value' => $cleanedindustry,
							'compare' => 'LIKE'
						)
					)	

				));

				foreach($job_ind as $ind) : 
					#pass the results into the array
					$industry_result[] = $ind->ID;
				endforeach;	


	endforeach;

	#echo '<pre><p>Industry</p>';
	#print_r($industry_result);
	#echo '</pre>';
		
	####################
		
	#create an array
	$town_result = array();

	$towns = get_posts(array(
		'post_type' => 'jobs',
		'post_status' => 'publish',
		'meta_query' => array(
			array(
				'key' 	=> 'dynamic_filled_town',
				'value' => $town,
				'compare' => 'LIKE'
			)
		)	

	));

	foreach($towns as $town) : 
		#pass the results into the array
		$town_result[] = $town->ID;
	endforeach;	

	#echo '<pre><p>Town</p>';
	#print_r($town_result);
	#echo '</pre>';		
	
		
	####################	
		
	#create an array
	$type_result = array();

	#loop through each industry and check for a match, add to array if so
	foreach($types as $type) :

				$job_type = get_posts(array(
					'post_type' => 'jobs',
					'post_status' => 'publish',
					'meta_query' => array(
						array(
							'key' 	=> 'dynamic_filled_type',
							'value' => $type,
							'compare' => 'LIKE'
						)
					)	

				));

				foreach($job_type as $type) : 
					#pass the results into the array
					$type_result[] = $type->ID;
				endforeach;	


	endforeach;

	#echo '<pre><p>Type</p>';
	#print_r($type_result);
	#echo '</pre>';		
		
	####################
		
	#merge all results, remove duplicates and any empty arrays		
	$search_results = array();
	$search_results[] = $industry_result;
	$search_results[] = $town_result;
	$search_results[] = $type_result;	

	$unique_results = array();

	foreach($search_results as $search) {
		if(is_array($search)) {
			$unique_results = array_unique(array_merge($unique_results, $search));
		}
	}

	#echo '<pre><p>Unique</p>';
	#print_r($unique_results);
	#echo '</pre>';			

	#not an empty array					
	if($unique_results):

		$args = array (
			'post_type' => 'jobs',
			'post__in'	=> $unique_results
		);	

		$wp_query = new WP_Query( $args );

		echo '<div class="row column">';

			$industrieslist = implode(', ', $industries );
			echo '<span><br/>Searching for: <strong>'.$industrieslist.'</strong><br/>';

			#get a total number of results				
			$total = $wp_query->found_posts;
			echo 'Results Found: <strong>'.$total.'</strong></span>';

		echo '</div>';


		if ( $wp_query->have_posts() ) :

			while ( $wp_query->have_posts() ) : $wp_query->the_post(); 

				$post_id = get_the_ID();	
				#echo $post_id;
							// set selected class
				$selected = '';
				if(isset($_SESSION['shortlist'])) {
					if ( in_array($id, $_SESSION['shortlist']) ) {
						$selected = 'selected';
					}
				}

				$employer = get_field('dynamic_filled_company', $post_id);


				$blogusers = get_users(array('meta_key' => 'company_name', 'meta_value' => $employer));
				foreach ( $blogusers as $user ) {
				#access the user meta
				#echo '<span>' . esc_html( $user->ID ) . '</span>'; #debug

				}	
				$author_id=$post->post_author; 
				#get the main_address
				$logo = get_field('logo', 'user_'.$user->ID);
				$company_name = get_field('company_name', 'user_'.$user->ID);
				$industry = get_field('dynamic_filled_industry');

				$overall_rating = get_field('overall_rating', 'user_'.$user->ID);
				//$overall_rating_new = get_field('star_rating');
				$overall_rating_percent = $overall_rating * 20;
				#echo $user->ID;

				?>


							<div id="<?php echo $post_id; ?>" class="search-result-block item <?php echo $selected; ?>">

								<a href="<?php the_permalink(); ?>"><div class="search-results-block--logo">
									<div class="logo-container">
										<?php if($logo): ?>
											<img src="<?php echo $logo['sizes']['medium']; ?>" alt="logo"> 
										<?php else: ?>
											<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/no-logo-uploaded.png" alt="No Logo Uploaded"> 
										<?php endif; ?>
									</div>
								</div></a>

								<div class="search-results-block--details">

									<a href="<?php the_permalink(); ?>"><span><strong><?php the_title(); ?></strong><br/>
									<?php if($company_name): ?>Company: <strong><?php echo $company_name; ?></strong><br/><?php endif; ?>

									<?php if((implode(', ', $industry) != '- Select Industry -') && (implode(', ', $industry) != '')): ?>
										Industry: <strong><?php the_field('dynamic_filled_industry'); ?></strong><br/>
									<?php endif; ?>

									<?php $job_type = get_field('dynamic_filled_type'); 
									if ($job_type == '- Select Job Type -'):
										$job = '';
									else:
										$job = $job_type;
									endif;
									?>
									</span></a>

								</div>

								<div class="search-results-block--location">

									<span>
										<?php $jobtown = get_field('dynamic_filled_town');
												if($jobtown): ?>
													<?php if(implode(', ', $jobtown) != "- Select Town -"): ?>
														Location:   <strong><?php echo implode(', ', $jobtown); ?></strong><br/>
													<?php endif; 
												endif; ?>

										<?php if(get_field('salary')): ?>
											<!--Salary:       <strong><?php #$salary = get_field('salary'); echo '£'.number_format($salary,2); ?></strong>-->
											Salary:       <strong><?php $salary = get_field('salary'); echo $salary; ?></strong>
										<?php endif; ?>

										<?php if(get_field('additional_salary')): ?>
											Additional Salary:       <strong><?php $additional_salary = get_field('additional_salary'); the_field('additional_salary'); //number_format($salary,2); ?></strong>
										<?php endif; ?>

									</span>

								</div>


								<a href="<?php the_permalink(); ?>">
									<div class="search-results-block--moredetails">

										<div class="stars">
												<div class="stars-gold" style="width: <?php echo $overall_rating_percent; ?>%;"> &nbsp; </div>
												<div class="stars-white"> &nbsp; </div>
										</div>

										<span class="review-count"><?php echo $number_of_reviews; ?> Reviews</span>

										<span class="button blue">More details</span>

									</div>
								</a>


							</div><!--search-result-block-->



				<?php

			endwhile;

		endif;

	else:

		echo '<p>No jobs matched your criteria, please try again</p>';

	endif;

#=============================================================================the form hasn't been submitted, so show all jobs: 
else:

		$paged = get_query_var('paged') ? get_query_var('paged') : 1;
		$args = array (
			'post_type' 		=> 'jobs',
			'posts_per_page' 	=> '20',
			'post_status' 		=> 'publish',
			'orderby' 			=> 'date',			
			'order' 			=> 'desc',
			'paged' 			=> $paged
		);

		$wp_query = new WP_Query( $args );
					
		if ( $wp_query->have_posts() ) : ?>
			

			<?php while ( have_posts() ) : the_post();

			// set selected class
			$selected = '';
			if(isset($_SESSION['shortlist'])) {
				if ( in_array($id, $_SESSION['shortlist']) ) {
					$selected = 'selected';
				}
			}

			?>


			<?php # get the user ID by matching the reviewed company name with an actual company
			
			$post_id = get_the_ID();
			$employer = get_field('dynamic_filled_company', $post_id);


			$blogusers = get_users(array('meta_key' => 'company_name', 'meta_value' => $employer));
			foreach ( $blogusers as $user ) {
			#access the user meta
			#echo '<span>' . esc_html( $user->ID ) . '</span>'; #debug
			
			}	
			$author_id=$post->post_author; 
			#get the main_address
			$logo = get_field('logo', 'user_'.$user->ID);
			$company_name = get_field('company_name', 'user_'.$user->ID);
			$industry = get_field('dynamic_filled_industry');

			$overall_rating = get_field('overall_rating', 'user_'.$user->ID);
			//$overall_rating_new = get_field('star_rating');
			$overall_rating_percent = $overall_rating * 20;
			#echo $employer;
			?>


						<div id="<?php echo $post_id; ?>" class="search-result-block item <?php echo $selected; ?>">

							<a href="<?php the_permalink(); ?>"><div class="search-results-block--logo">
								<div class="logo-container">
									<?php if($logo): ?>
										<img src="<?php echo $logo['sizes']['medium']; ?>" alt="logo"> 
									<?php else: ?>
										<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/no-logo-uploaded.png" alt="No Logo Uploaded"> 
									<?php endif; ?>
								</div>
							</div></a>

							<div class="search-results-block--details">
								
								<a href="<?php the_permalink(); ?>"><span><strong><?php the_title(); ?></strong><br/>
								<?php if($company_name): ?>Company: <strong><?php echo $company_name; ?></strong><br/><?php endif; ?>
								
								<?php if((implode(', ', $industry) != '- Select Industry -') && (implode(', ', $industry) != '')): ?>
									Industry: <strong><?php the_field('dynamic_filled_industry'); ?></strong><br/>
								<?php endif; ?>
														
								<?php $job_type = get_field('dynamic_filled_type'); 
								if ($job_type == '- Select Job Type -'):
									$job = '';
								else:
									$job = $job_type;
								endif;
								?>
								</span></a>

							</div>

							<div class="search-results-block--location">
								
								<span>
									<?php $jobtown = get_field('dynamic_filled_town');
											if($jobtown): ?>
												<?php if(implode(', ', $jobtown) != "- Select Town -"): ?>
													Location:   <strong><?php echo implode(', ', $jobtown); ?></strong><br/>
												<?php endif; 
											endif; ?>

									<?php if(get_field('salary')): ?>
										<!--Salary:       <strong><?php #$salary = get_field('salary'); echo '£'.number_format($salary,2); ?></strong>-->
										Salary:       <strong><?php $salary = get_field('salary'); echo $salary; ?></strong><br/>
									<?php endif; ?>

									<?php if(get_field('additional_salary')): ?>
										Additional Salary:       <strong><?php $additional_salary = get_field('additional_salary'); the_field('additional_salary'); //number_format($salary,2); ?></strong>
									<?php endif; ?>

								</span>

							</div>

							<a href="<?php the_permalink(); ?>">
								<div class="search-results-block--moredetails">
									
									<div class="stars">
											<div class="stars-gold" style="width: <?php echo $overall_rating_percent; ?>%;"> &nbsp; </div>
											<div class="stars-white"> &nbsp; </div>
									</div>

									<span class="review-count"><?php echo $number_of_reviews; ?> Reviews</span>

									<span class="button blue">More details</span>

								</div>
							</a>


						</div><!--search-result-block-->


			<?php endwhile; ?>
		    
		    
		<?php else : ?>
			<p>No jobs found.</p>
		<?php endif; 


		/* Display navigation to next/previous pages when applicable */ 
		if ( function_exists( 'foundationpress_pagination' ) ) :
			foundationpress_pagination();
		elseif ( is_paged() ) :
		?>
			<nav id="post-nav">
				<div class="post-previous"><?php next_posts_link( __( '&larr; Older posts', 'foundationpress' ) ); ?></div>
				<div class="post-next"><?php previous_posts_link( __( 'Newer posts &rarr;', 'foundationpress' ) ); ?></div>
			</nav>
		<?php endif;?>











<?php endif; // submit form check ?>

	</div><!--main-content-main -->










	<div class="sidebar-left" >


			<div class="sidebar--header" data-equalizer-watch="main-content-headers">
				<h3>Refine your search</h3> <svg class="icon icon-refine"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-refine"></use></svg>
			</div>


<form id="job-search" action="<?php bloginfo('url');?>/find-a-job/" method="post">


			<ul class="results-categories">
			<li>	
				<div class="results-categories--container">
					<ul class="results-categories--subcategories">
						<?php
						# we need to get a list of all industries from the options page

						# reset choices
						$field['industries'] = array();
						        
						# get the textarea value from options page without any formatting
						$industries = get_field('industry', 'option', false);
						    
						# explode the value so that each line is a new array piece
						$industries = explode("\n", $industries);
						    
						# remove any unwanted white space
						$industries = array_map('trim', $industries);
						    
						# loop through array and add to field 'choices'
						if( is_array($industries) ) :
						        
							foreach( $industries as $industry ) :
								if($industry === '- Select Industry -') continue; #skip first item    	
								
								$field['industries'][ $industry ] = '<li><label class="container" for="'.$industry.'"><input type="checkbox" name="industries[]" id="'.$industry.'" value="'.$industry.'" > <span class="checkmark"></span>'.$industry.'</label></li>';
						            
							endforeach;
						        
						endif;

						# convert array into comma-separated string
						$industries = implode('', $field['industries'] );

						echo '<p>Industry: <ul>'.$industries.'</ul></p>';
						?>
					</ul>
				</div>
				<span class="industries-more">More <svg class="icon icon-downarrow-small"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-downarrow-small"></use></svg></span>
			</li><!-- /industries -->
				
			<li>	
				<div class="results-categories--container">
					<ul class="results-categories--subcategories">
						<!-- /include the jquery auto complete files -->
						<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
						<script src="//code.jquery.com/jquery-1.12.4.js"></script>
						<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
						<?php
						# we need to get a list of all towns from the options page

						# reset choices
						$field['towns'] = array();

						# get the textarea value from options page without any formatting
						$towns = get_field('town', 'option', false);

						# explode the value so that each line is a new array piece
						$towns = explode("\n", $towns);

						# remove any unwanted white space
						$towns = array_map('trim', $towns);

						# loop through array and add to field 'choices'
						if( is_array($towns) ) :

							foreach( $towns as $town ) :
								if($town === '- Select Town -') continue; #skip first item                
								$field['towns'][ $town ] = '"'.$town.'"';

							endforeach;

						endif;

						# convert array into comma-separated string
						$towns = implode(',', $field['towns'] );
						?>	
						<script>
						$( function() {
							var availableTowns = [
								<?php echo $towns; #output the towns as a comma-separated list ?>
							];
							$( "#towns" ).autocomplete({
								source: availableTowns
							});
						});
						</script>
						<label for="towns">Towns: </label>
						<input id="towns" name="towns" value="">																
					</ul>
				</div>
				</li><!-- /towns -->	
				
			<li>	
				<div class="results-categories--container">
					<ul class="results-categories--subcategories job-type--filter">
						<?php
						# we need to get a list of all types from the options page

						# reset choices
						$field['types'] = array();
						        
						# get the textarea value from options page without any formatting
						$types = get_field('type', 'option', false);
						    
						# explode the value so that each line is a new array piece
						$types = explode("\n", $types);
						    
						# remove any unwanted white space
						$types = array_map('trim', $types);
						    
						# loop through array and add to field 'choices'
						if( is_array($types) ) :
						        
							foreach( $types as $type ) :
								if($type === '- Select Job Type -') continue; #skip first item    	
								
								$field['types'][ $type ] = '<li><label class="container" for="'.$type.'"><input type="checkbox" name="types[]" id="'.$type.'" value="'.$type.'" > <span class="checkmark"></span>'.$type.'</label></li>';
						            
							endforeach;
						        
						endif;

						# convert array into comma-separated string
						$types = implode('', $field['types'] );

						echo '<p>Type: <ul>'.$types.'</ul></p>';
						?>
					</ul>
				</div>

			</li><!-- /type -->										

	<input value="Search Jobs" type="submit" name="submit" class="filter-reviews-button button blue block" /> 

	</form> 

	</div><!--sidebar-left -->






<?php wp_reset_query(); ?>



<?php
#get_sidebar();
get_footer();
