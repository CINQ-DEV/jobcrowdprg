<?php

acf_form_head(); #add this here to allow editing of the survey
get_header();

$nonce = $_REQUEST['_wpnonce'];

if ( ! wp_verify_nonce( $nonce, 'prg-survey-status-nonce' ) ) :
	
	#nonce doesn't match or exist so terminate script!
	die( "<h1>Security Check!</h1><p>You're trying to access this page directly. If you believe you're seeing this page in error, please contact the administrator!</p>" ); 

else:
?>

<div class="page-header row">

	<div class="medium-5 columns">
		<h1>Your Survey <?php the_title(); ?> Details</h1>
	</div>

	<?php get_template_part('template-parts/page-header-search'); ?>

</div><!-- page-header -->





<div class="main-content no-right-column" data-equalizer="main-content" data-equalize-on="medium">

		<div class="main-content-main" style="min-height: 100vh">

					<div class="main-content-main--breadcrumbs" data-equalizer-watch="main-content-headers">
							
							<?php
							if ( function_exists('yoast_breadcrumb') ) {
							yoast_breadcrumb('
							<p id="breadcrumbs">','</p>
							');
							}
							?>

					</div>

					<div class="entry-content padded">

		
		<?php
		global $current_user;
		get_currentuserinfo();
				
		
		#lets check the survey is being accessed by the correct author	
		if (is_user_logged_in() && $current_user->ID == $post->post_author) :

			#get the status, we can then action accordingly
			$status = $_GET[status];
			
			if ($status == 'submit'):
				
				#we only wish to summarise and send the email
				echo '<p>Status: '.$status.'</p>';
				
				#we can get the data
				if( have_rows('recipients') ):  $i = 1; ?>
				<h3>Email Recipients</h3>
				<table id="example" class="display" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>Forename</th>
							<th>Surname</th>
							<th>Email Address</th>
						</tr>							
					</thead>
					<tfoot>
						<tr>
							<th>Forename</th>
							<th>Surname</th>
							<th>Email Address</th>
						</tr>
					</tfoot>    
					<tbody>			
					<?php 				
					$total_rows= 0; 
					$match = 0;
					while( have_rows('recipients') ): the_row(); 
						
						# vars
						$forename 				= get_sub_field('forename');
						$surname				= get_sub_field('surname');
						$email_address 			= get_sub_field('email_address');
						$survey_type			= get_sub_field('survey_type');
						
						echo '<tr>';					
							echo '<td>';
							if( $forename ):
								echo $forename;
							endif;	
							echo '</td>';
							echo '<td>';
							if( $surname ):
								echo $surname;
							endif;	
							echo '</td>';
							echo '<td>';
							if( $email_address ):
								echo $email_address;
							endif;	
							echo '</td>';				
						echo '</tr>';					
					
					#lets count how many completed surveys we have
					if( $completed == '1' ):
						$match++;	
					endif;
					
					#lets count the total number of rows
					$total_rows++; 
						
					$i++; endwhile;
					?>
					</tbody>
				</table>				
				<link href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet" />
				<script src="//code.jquery.com/jquery-1.12.4.js"></script>
				<script src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>	
				<script>
				jQuery(document).ready(function() {
					$('#example').DataTable();
				} );				
				</script>				
				<?php	
				endif;	
		
				$newsletter = get_field('newsletter', $lid);
				if( $newsletter ) :			
					echo '<h3>Survey Email</h3>';
					echo $newsletter;
				endif;	
				
				$survey_type = get_field('survey_type', $lid);
				if( $survey_type ) :			
					echo '<h3>Survey Type</h3>';
					echo $survey_type;
				endif;				
				
				#check whether a new list has been created, if so, provide a button to send the email
				#$submitted = $_GET['surveySent'];

				$url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
				$parts = explode('?', $url);
				$submitted = $parts[count($parts) - 1];												
						
				if ($submitted == 'surveySent=true'):
					
					echo '<h3>Congratulations!</h3>';
					echo "<p>You've survey email is now being distributed.</p>";						
						
				else:
					
					$nonce = wp_create_nonce( 'prg-survey-status-nonce' );
				
					$lid = get_the_ID();	
					
					$uid = get_current_user_id();
					
					if ($survey_type == 'Graduate'):
						$survey = 'graduate';
					else:
						$survey = 'apprentice';
					endif;
					
					$style = "display: block; width: 150px; height: 30px; line-height: 30px; background: #0085ba; border:1px solid #006799; color: #fff; border-radius: 5px; text-align: center;";
					
					#if the total number of rows equals the total number of completed, stop any more sends
					if ($total_rows == $match) :
						
						echo '<h3>Send Survey?</h3>';
						echo '<p>All recipients have completed the survey, you can no longer resend this survey.</p>';
						
					else:
																
						$incomplete = ($total_rows - $match);
						$survey_name = get_the_title();
						/*
						echo '<p>Matched: '. $match .'</p>';
						echo '<p>incomplete: '. $incomplete .'</p>';
						echo '<p>Total Rows: '. $total_rows .'</p>';	
						*/
						echo '<h3>Send Survey?</h3>';
						echo '<p>A total of<strong>: '. $incomplete .' out of: '. $total_rows .'</strong> did not completed the survey: <u>'.$survey_name.'</u>. Would you like to send the survey again?</p>';	
						echo '<p><strong>Please note:</strong> This will only send to those who did not previously complete the survey.</p>';
						
						echo "<a href='" . get_stylesheet_directory_uri() . "/includes/survey-distribution.php?_wpnonce=$nonce&lid=$lid&uid=$uid&survey=$survey' style='$style'>Send Survey Email Now</a>";

					
					endif;
					
				endif;	#endif is submitted					
			
			elseif ($status == 'email'):
				#echo $post->ID;
				#edit the email subject/body
				echo '<p>Status: '.$status.'</p>';
				
				$args = array(
					'post_id' => $post->ID,
					'fields' => array(
						#Group 75 :: Survey Import Recipient List
						'field_5a05c296218c7',	#replacement post title
						'field_5947dbf58c372'	#email			
					),
					'submit_value' => 'Update Email Settings',
					'return' => 'https://www.thejobcrowd.com/my-account/your-surveys/',	#redirect incase permalink changes
					'html_after_fields' => '<input type="hidden" name="acf[survey_content_change]" value="true"/>',
					'kses'	=> true
				);
					
				acf_form( $args );				

			elseif ($status == 'recipients'):
				
				#add/remove recipients
				echo '<p>Status: '.$status.'</p>';
				
				#we can get the data
				if( have_rows('recipients') ):  $i = 1; ?>
				<h3>Email Recipients</h3>
				<table id="example" class="display" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>Forename</th>
							<th>Surname</th>
							<th>Email Address</th>
							<th>Delete Recipient</th>
						</tr>							
					</thead>
					<tfoot>
						<tr>
							<th>Forename</th>
							<th>Surname</th>
							<th>Email Address</th>
							<th>Delete Recipient</th>
						</tr>
					</tfoot>    
					<tbody>			
					<?php 				
					$total_rows= 0; 
					$match = 0;
					while( have_rows('recipients') ): the_row(); 
						
						# vars
						$forename 				= get_sub_field('forename');
						$surname				= get_sub_field('surname');
						$email_address 			= get_sub_field('email_address');
						$survey_type			= get_sub_field('survey_type');
						
						echo '<tr>';					
							echo '<td>';
							if( $forename ):
								echo $forename;
							endif;	
							echo '</td>';
							echo '<td>';
							if( $surname ):
								echo $surname;
							endif;	
							echo '</td>';
							echo '<td>';
							if( $email_address ):
								echo $email_address;
							endif;	
							echo '</td>';
							echo '<td class="text-center">';
							$nonce = wp_create_nonce( 'prg-delete-survey-recipient-nonce' ); 
							$post_id = get_the_id(); ?>
							<a href="<?php echo get_stylesheet_directory_uri(); ?>/includes/delete-survey-recipient.php?_wpnonce=<?php echo $nonce; ?>&row=<?php echo $i; ?>&pID=<?php echo $post_id; ?>" title="Delete Recipient: <?php echo $forename.' '.$surname; ?>" class="delete-button">x</a>	
							<?php echo '</td>';					
						echo '</tr>';					
					
					#lets count how many completed surveys we have
					if( $completed == '1' ):
						$match++;	
					endif;
					
					#lets count the total number of rows
					$total_rows++; 
						
					$i++; endwhile;
					?>
					</tbody>
				</table>				
				<link href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet" />
				<script src="//code.jquery.com/jquery-1.12.4.js"></script>
				<script src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>	
				<script>
				jQuery(document).ready(function() {
					$('#example').DataTable();
				} );				
				</script>				
				<?php	
				endif;	#endif; if( have_rows('recipients') ):
						
				#########################
				echo '<label for="acf-field_5a0ab7d5344c3"><strong>Add A Single Recipient</strong></label>';
				$args = array(
					'post_id' => $post->ID,
					'fields' => array(
						#Group 75 :: Survey Import Recipient List
						'field_5947dbd28c36e',	#forename
						'field_5947dbd78c36f',	#surname			
						'field_5947dbdc8c370',	#email address
					),
					'submit_value' => 'Add A Single Recipient',
					'html_after_fields' => '<input type="hidden" name="acf[survey_additional_single_recipient]" value="true"/>',
					'updated_message' => __("Recipient Added", 'acf'),
					'kses'	=> true
				);
					
				acf_form( $args );												
						
				#########################						
				
				#########################
				$args = array(
					'post_id' => $post->ID,
					'fields' => array(
						#Group 2388 :: Additional Recipients (Employer Frontend Form)
						'field_5a0ab7d5344c3',	#file
						'field_5a0ac9f9344c4'	#skip row			
					),
					'submit_value' => 'Import Additional Recipients',
					'html_after_fields' => '<input type="hidden" name="acf[survey_import_additional_recipients]" value="true"/>',
					'updated_message' => __("", 'acf'),
					'kses'	=> true
				);
					
				acf_form( $args );		
				#########################
				
			else:
				
				#fallback, if by any miracle we've hit here, don't do anything!
				echo '';
			
			endif;
							
		else:
			
			echo "<p>You're not the author of this survey list and therefore not able to make alterations, if you believe you're seeing this message in error, please contact the administrator.</p>";
			
		endif;				
		?>

</div>

		</div><!-- main-content-main -->


		<div class="sidebar-left" style="min-height: 100vh">


			<div class="sidebar--header" data-equalizer-watch="main-content-headers">
				<h3></h3>
			</div>

				
				<a class="button blue" href="https://www.thejobcrowd.com/my-account/your-surveys/"><br><strong>< Back to surveys<br> &nbsp;</strong></a>

		</div><!--sidebar-left -->


</div><!-- #primary -->
</div><!-- #primary -->

<?php
get_footer();

endif; #endif $nonce