<?php get_header(); ?>

<div class="page-header row">

	<div class="medium-5 columns">
			<h1 class="entry-title"><?php _e( 'File Not Found', 'foundationpress' ); ?></h1>
		<span class="search-results-count"><?php echo get_the_date('Y-m-d'); ?></span>
	</div>

	<?php get_template_part('template-parts/page-header-search'); ?>

</div><!-- page-header -->




<div class="main-content fullwidth" data-equalizer="main-content" data-equalize-on="medium">



		<div class="main-content-main" data-equalizer-watch="main-content">

					<div class="main-content-main--breadcrumbs" data-equalizer-watch="main-content-headers">
							
							<?php
							if ( function_exists('yoast_breadcrumb') ) {
							yoast_breadcrumb('
							<p id="breadcrumbs">','</p>
							');
							}
							?>

					</div>

				<article class="main-content">

					<div class="entry-content padded">

						<div class="entry-content">
							<div class="error">
								<p class="bottom"><?php _e( 'The page you are looking for might have been removed, had its name changed, or is temporarily unavailable.', 'foundationpress' ); ?></p>
							</div>
							<p><?php _e( 'Please try the following:', 'foundationpress' ); ?></p>
							<ul>
								<li><?php _e( 'Check your spelling', 'foundationpress' ); ?></li>
								<li>
									<?php
										/* translators: %s: home page url */
										printf( __(
											'Return to the <a href="%s">home page</a>', 'foundationpress' ),
											home_url()
										);
									?>
								</li>
								<li><?php _e( 'Click the <a href="javascript:history.back()">Back</a> button', 'foundationpress' ); ?></li>
							</ul>
						</div>
					</div>
				</article>

		</div><!-- main-content-main -->


</div>

<?php get_footer();
