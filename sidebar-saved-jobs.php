<div id="loadedcontent">
<?php 
// debug_shortlist();
 require($_SERVER['DOCUMENT_ROOT'].'/wp-load.php'); 

// get session array
$shortlist = array(0);

if( isset($_SESSION['shortlist']) && (!empty($_SESSION['shortlist'])) ) {
	$shortlist = $_SESSION['shortlist'];

	$args = array (
		'post_type' => 'jobs',
		'orderby' => 'title',			
		'order' => 'asc',
		'post__in' => $shortlist,
	);

} else {

	$args = array();

}

$wp_query = new WP_Query( $args );

if ( have_posts() ) : 
?>

		<div class="no-saved-items item padded">
			<p>Save some jobs to add them to your shortlist.</p>
		</div>

			<?php 
			while ( have_posts() ) : the_post(); 

			// set selected class
			$selected = '';
			if(isset($_SESSION['shortlist'])) {
				if ( in_array($id, $_SESSION['shortlist']) ) {
					$selected = 'selected';
				}
			}

			?>

					<div id="<?php the_ID(); ?>" class="item saved-jobs--job">
						<strong><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></strong><br>
						<?php $dynamic_filled_type = get_field('dynamic_filled_type'); if($dynamic_filled_type != '- Select Job Type -') { echo $dynamic_filled_type .'<br>'; }?>
						<?php $salary = get_field('salary'); if($salary): ?><strong>Salary:</strong> <?php echo number_format($salary,2); ?><br><?php endif; ?>
						<a href="#" class="btn action remove" data-action="remove"><i class="fa fa-minus-circle" aria-hidden="true"></i> Remove</a>
					</div>

			<?php endwhile; ?>

<?php else: ?>

	<div class="item padded">
		<p>Save some jobs to add them to your shortlist.</p>
	</div>

<?php endif; ?>

</div>

<script type="text/javascript">
		$(".saved-jobs .btn.remove").click(function() { 	// ========= the saved jobs sidebar functionality
		var thisJob = $(this).parent();
		var thisJobId = thisJob.attr("id");

		var savedJobs = $(this).parent().parent();
		var otherJobs = $(".saved-jobs .saved-jobs--job").not(thisJob).not(".removed-job");

		thisJob.fadeOut("fast").addClass("removed-job"); 
		$("#" + thisJobId + ".selected").removeClass("selected");

		console.log("removed " + thisJob);

		if (otherJobs.length) {
			console.log("there ARE other jobs");
		} else {
			console.log("there are no other jobs");
			$(".no-saved-items").fadeIn('fast');
		};

	});
</script>