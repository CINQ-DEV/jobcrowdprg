<?php
/*
 Template Name: View User
 */

get_header(); 

$company = $_GET["company"];
$cID = $_GET["user"];

$all_meta_for_user = get_user_meta( $cID );
?>

<?php if( current_user_can('shop_manager') || current_user_can('administrator') ): ?>


<div class="page-header row">

	<div class="medium-5 columns">

<?php
$activate_membership = get_user_meta( $cID, 'activate_membership', $single = true);

if ($cID !=''):

	if ($activate_membership == 'Yes') :
	?>
		<?php	
		$company_name = $all_meta_for_user['company_name'][0];
		$company_address = $all_meta_for_user['main_address'][0];
		$company_industry = $all_meta_for_user['employer_registration_industry'][0];
		$company_employees = $all_meta_for_user['number_of_employees'][0];
		$logo = $all_meta_for_user['logo'][0];
		$logo = get_field('logo', 'user_'.$cID);
		if ($company_name):
			echo '<h1>'.$company_name.'</h1>';
		endif;

		if($company_industry) {
		//	echo $company_industry[3];
		}

		//$employer_registration_industry = get_field('employer_registration_industry', 'user_'.$cID);

		?>

	</div>
	

	<?php get_template_part('template-parts/page-header-search'); ?>

</div><!-- page-header -->


<div class="main-content" data-equalizer="main-content">


	<div class="main-content-main" data-equalizer-watch="main-content">

				<div class="main-content-main--breadcrumbs" data-equalizer-watch="main-content-headers">
				
							<?php
							if ( function_exists('yoast_breadcrumb') ) {
							yoast_breadcrumb('
							<p id="breadcrumbs">','</p>
							');
							}
							?>
							
					<div class="save-share">

						<!--<a href="#" class="savethis">
							<svg class="icon icon-heart"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-heart"></use></svg> Save this
						</a>-->

						<?php get_template_part('template-parts/share-button'); ?>

					</div>

				</div><!-- main-content-main-breadcrumbs -->

				<div class="padded">



					<p class="page-intro">All of the below information comes directly from job reviews written by <?php echo $company_name; ?> employees.<!--, and is based on 38 reviews.--></p>

					<div class="reviews-graphs row">


<?php
$average_responsibility = $all_meta_for_user['average_responsibility'][0];
$average_responsibility_percent = $average_responsibility * 20;

if ($average_responsibility != 'NAN') :
	echo '<div class="medium-6 columns review-graph">';
	echo 'Responsibility:'; ?>
	<div class="review-graph-black">
									<span class="review-graph-number"><?php echo ''.$average_responsibility.'/5'; ?></span>
									<div class="review-graph-blue" style="max-width: <?php echo $average_responsibility_percent; ?>%">&nbsp;</div>
								</div>
	<?php
	echo '</div> <!-- end review graph -->';
endif;

$average_work_life_balance = $all_meta_for_user['average_work_life_balance'][0];
$average_work_life_balance_percent = $average_work_life_balance * 20;
if ($average_work_life_balance != 'NAN') :
	echo '<div class="medium-6 columns review-graph">';
	echo 'Work Life Balance:'; ?>
	<div class="review-graph-black">
									<span class="review-graph-number"><?php echo ''.$average_work_life_balance.'/5'; ?></span>
									<div class="review-graph-blue" style="max-width: <?php echo $average_work_life_balance_percent; ?>%">&nbsp;</div>
								</div>
	<?php
	echo '</div> <!-- end review graph -->';
endif;



$average_environmental_awareness = $all_meta_for_user['average_environmental_awareness'][0];
$average_environmental_awareness_percent = $average_environmental_awareness * 20;
if ($average_environmental_awareness != 'NAN') :
	echo '<div class="medium-6 columns review-graph">';
	echo 'Environmental Awareness:'; ?>
	<div class="review-graph-black">
									<span class="review-graph-number"><?php echo ''.$average_environmental_awareness.'/5'; ?></span>
									<div class="review-graph-blue" style="max-width: <?php echo $average_environmental_awareness_percent; ?>%">&nbsp;</div>
								</div>
	<?php
	echo '</div> <!-- end review graph -->';
endif;




$average_compensation_benefits = $all_meta_for_user['average_compensation_benefits'][0];
$average_compensation_benefits_percent = $average_compensation_benefits * 20;
if ($average_compensation_benefits != 'NAN') :
	echo '<div class="medium-6 columns review-graph">';
	echo 'Compensation &amp; Benefits:'; ?>
	<div class="review-graph-black">
									<span class="review-graph-number"><?php echo ''.$average_compensation_benefits.'/5'; ?></span>
									<div class="review-graph-blue" style="max-width: <?php echo $average_compensation_benefits_percent; ?>%">&nbsp;</div>
								</div>
	<?php
	echo '</div> <!-- end review graph -->';
endif;





$average_company_culture = $all_meta_for_user['average_company_culture'][0];
$average_company_culture_percent = $average_company_culture * 20;
if ($average_company_culture != 'NAN') :
	echo '<div class="medium-6 columns review-graph">';
	echo 'Company Culture:'; ?>
	<div class="review-graph-black">
									<span class="review-graph-number"><?php echo ''.$average_company_culture.'/5'; ?></span>
									<div class="review-graph-blue" style="max-width: <?php echo $average_company_culture_percent; ?>%">&nbsp;</div>
								</div>
	<?php
	echo '</div> <!-- end review graph -->';
endif;





$average_career_progression = $all_meta_for_user['average_career_progression'][0];
$average_career_progression_percent = $average_career_progression * 20;
if ($average_career_progression != 'NAN') :
	echo '<div class="medium-6 columns review-graph">';
	echo 'Career Progression:'; ?>
	<div class="review-graph-black">
									<span class="review-graph-number"><?php echo ''.$average_career_progression.'/5'; ?></span>
									<div class="review-graph-blue" style="max-width: <?php echo $average_career_progression_percent; ?>%">&nbsp;</div>
								</div>
	<?php
	echo '</div> <!-- end review graph -->';
endif;






$average_colleagues = $all_meta_for_user['average_colleagues'][0];
$average_colleagues_percent = $average_colleagues * 20;
if ($average_colleagues != 'NAN') :
	echo '<div class="medium-6 columns review-graph">';
	echo 'Colleagues:'; ?>
	<div class="review-graph-black">
									<span class="review-graph-number"><?php echo ''.$average_colleagues.'/5'; ?></span>
									<div class="review-graph-blue" style="max-width: <?php echo $average_colleagues_percent; ?>%">&nbsp;</div>
								</div>
	<?php
	echo '</div> <!-- end review graph -->';
endif;







$average_training = $all_meta_for_user['average_training'][0];
$average_training_percent = $average_training * 20;
if ($average_training != 'NAN') :
	echo '<div class="medium-6 columns review-graph">';
	echo 'Training:'; ?>
	<div class="review-graph-black">
									<span class="review-graph-number"><?php echo ''.$average_training.'/5'; ?></span>
									<div class="review-graph-blue" style="max-width: <?php echo $average_training_percent; ?>%">&nbsp;</div>
								</div>
	<?php
	echo '</div> <!-- end review graph -->';
endif;







$average_enjoyment = $all_meta_for_user['average_enjoyment'][0];
$average_enjoyment_percent = $average_enjoyment * 20;
if ($average_enjoyment != 'NAN') :
	echo '<div class="medium-6 columns review-graph">';
	echo 'Enjoyment:'; ?>
	<div class="review-graph-black">
									<span class="review-graph-number"><?php echo ''.$average_enjoyment.'/5'; ?></span>
									<div class="review-graph-blue" style="max-width: <?php echo $average_enjoyment_percent; ?>%">&nbsp;</div>
								</div>
	<?php
	echo '</div> <!-- end review graph -->';
endif;






$overall_rating = $all_meta_for_user['overall_rating'][0];
$overall_rating_percent = $overall_rating * 20;
if ($overall_rating != 'NAN') :
	echo '<div class="medium-6 columns review-graph last">';
	echo 'Overall Rating:'; ?>
	<div class="review-graph-black">
									<span class="review-graph-number"><?php echo ''.$overall_rating.'/5'; ?></span>
									<div class="review-graph-blue" style="max-width: <?php echo $overall_rating_percent; ?>%">&nbsp;</div>
								</div>
	<?php
	echo '</div> <!-- end review graph -->';
endif;



?> 
		
</div>

</div>



<div class="review-container padded row">

<!-- Nav tabs -->
<ul class="tabs" data-tabs id="reviews-tabs">
	<li class="tabs-title is-active"><a href="#graduates" aria-controls="graduates" role="tab">Graduate Reviews</a></li>
	<li class="tabs-title"><a href="#apprentices" aria-controls="apprentices" role="tab">Apprentices Reviews</a></li>
	<li class="tabs-title"><a href="#jobs" aria-controls="jobs" role="tab">Jobs</a></li>
	<li class="tabs-title"><a href="#enhanced" aria-controls="enhanced" role="tab">Enhanced Profile</a></li>
</ul>

<!-- Tab panes -->
<div class="tabs-content" data-tabs-content="reviews-tabs">
  <div class="tabs-panel is-active" id="graduates">
	
	<?php
    $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
	$args = array(
		'posts_per_page'	=> -1,
		'post_type'			=> 'graduate_reviews',
		'meta_key'			=> 'graduate_review_employer',	
		'meta_value'		=> $company_name	
	);
	
	// query
	$the_query = new WP_Query( $args );
	
	if( $the_query->have_posts() ): ?>

		<?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>
			
						<div class="review-container--header row">

								<div class="review-container--header--left">
										<span class="review-title">
											<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
										</span>
								</div><!-- review-container-header-left -->

						</div><!-- review-container-header row -->

						<div class="review-container--body">

								<div class="review-container--body--left">
								
									<?php $graduate_review_industry = get_field('graduate_review_industry'); if ($graduate_review_industry): ?>
										<span>Industry: <strong><?php echo $graduate_review_industry; ?></strong></span>
									<?php endif; ?>

									<?php $graduate_review_department = get_field('graduate_review_department'); if ($graduate_review_department): ?>
										<span>Department: <strong><?php echo $graduate_review_department; ?></strong></span>
									<?php endif; ?>

									<?php $graduate_review_job_title = get_field('graduate_review_job_title'); if ($graduate_review_job_title): ?>
										<span>Job Title: <strong><?php echo $graduate_review_job_title; ?></strong></span>
									<?php endif; ?>

								</div>

									<div class="review-container--body--right">
										<?php $graduate_review_what_are_the_best_and_worst_things_about_your_company_best = get_field('graduate_review_what_are_the_best_and_worst_things_about_your_company_best'); if ($graduate_review_what_are_the_best_and_worst_things_about_your_company_best): ?>
											<p><strong>What are the best and worst things about your company?</strong><br/>
											<?php the_field('graduate_review_what_are_the_best_and_worst_things_about_your_company_best'); ?>
										<?php endif; ?>
										<a href="<?php the_permalink(); ?>" class="button">Read more</a></p>
									</div>

						</div><!-- review-container-body-->
						<br/>

		<?php endwhile; ?>
	
	<?php else: ?>

		<p>There are not yet any graduate reviews entered for this company. Why not <a href="<?php bloginfo('home'); ?>/reviews/graduate-review">be the first to write one</a>?</p>


	<?php endif; 
	
	wp_reset_query();	 // Restore global post data stomped by the_post().
	?>	
	
	</div>


	  <div class="tabs-panel" id="apprentices">

	
	<?php
	$args = array(
		'posts_per_page'	=> -1,
		'post_type'			=> 'apprentice_reviews',
		'meta_key'			=> 'apprentice_review_employer',
		'meta_value'		=> $company_name	
	);
	
	// query
	$the_query = new WP_Query( $args );
	
	if( $the_query->have_posts() ): ?>
	

		<?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>
			

						<div class="review-container--header row">

								<div class="review-container--header--left">
										<span class="review-title">
											<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
										</span>
								</div><!-- review-container-header-left -->

						</div><!-- review-container-header row -->

						<div class="review-container--body">

								<div class="review-container--body--left">
								
									<?php $apprentice_review_industry = get_field('apprentice_review_industry'); if ($apprentice_review_industry): ?>
										<span>Industry: <strong><?php echo implode(', ', $apprentice_review_industry); ?></strong></span>
									<?php endif; ?>

									<?php $apprentice_review_department = get_field('apprentice_review_department'); if ($apprentice_review_department): ?>
										<span>Department: <strong><?php echo $apprentice_review_department; ?></strong></span>
									<?php endif; ?>

									<?php $apprentice_review_job_title = get_field('apprentice_review_job_title'); if ($apprentice_review_job_title): ?>
										<span>Job Title: <strong><?php echo $apprentice_review_job_title; ?></strong></span>
									<?php endif; ?>

								</div>

								<?php $apprentice_review_what_are_the_best_and_worst_things_about_your_company_best = get_field('apprentice_review_what_are_the_best_and_worst_things_about_your_company_best'); if ($apprentice_review_what_are_the_best_and_worst_things_about_your_company_best): ?>
									<div class="review-container--body--right">
											<p><strong>What are the best and worst things about your company?</strong><br/>
											<?php the_field('apprentice_review_what_are_the_best_and_worst_things_about_your_company_best'); ?>
											<a href="<?php the_permalink(); ?>" class="button">Read more</a></p>
									</div>
								<?php endif; ?>

						</div><!-- review-container-body-->
						<br/>
		<?php endwhile; ?>

	<?php else: ?>

		<p>There are not yet any apprentice reviews entered for this company. Why not <a href="<?php bloginfo('home'); ?>/reviews/apprentice-review">be the first to write one</a>?</p>

	<?php endif; 
	
	wp_reset_query();	 // Restore global post data stomped by the_post().
	?>	
	
	
	</div>
	<div class="tabs-panel" id="jobs">
	<?php
	$args = array(
		'posts_per_page'	=> -1,
		'post_type'			=> 'jobs',
		'post_status'		=> 'publish',
		'meta_key'			=> 'dynamic_filled_company',
		'meta_value'		=> $company_name	
	);
	
	// query
	$the_query = new WP_Query( $args );
	
	if( $the_query->have_posts() ): ?>
	

		<?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>
			

						<div class="review-container--header row">

								<div class="review-container--header--left">
										<span class="review-title">
											<?php 
											$salary = get_field('salary');
											$additional_salary = get_field('additional_salary');
											?>
											<a href="<?php the_permalink(); ?>"><?php the_title(); ?> (
											<?php
											if ($salary):
												echo $salary;
											else:
												echo $additional_salary;
											endif;
											?>
											)</a>
										</span>
								</div><!-- review-container-header-left -->

						</div><!-- review-container-header row -->

						<div class="review-container--body">
						<?php
						$job_description = get_field('job_description');
						if ($job_description):
							echo $job_description;
						endif;
						?>
						</div><!-- review-container-body-->
						<br/>
		<?php endwhile; ?>

	<?php else: ?>

		<p>There are no jobs listed for this company.</p>

	<?php endif; 
	
	wp_reset_query();	 // Restore global post data stomped by the_post().
	?>	
	</div><!-- jobs -->
	


	<div class="tabs-panel" id="enhanced"> 





		<?php   

	#echo $cID;
    # get an array of all the group ID's the user belongs to
    $groups_user = new Groups_User( $cID );
    # get group objects
    $user_groups = $groups_user->groups;
    # get group ids (user is direct member)
    $user_group_ids = $groups_user->group_ids;
    # get group ids (user is direct member or by group inheritance)
    $user_group_ids_deep = $groups_user->group_ids_deep;
    #print_r ( $user_group_ids_deep ); #debug
    
    # create an array of all basic groups
    $basic_groups = array(5,6,7);
    # check if the company belongs to any of those groups
    $basic_results = array_intersect($user_group_ids_deep,$basic_groups);
    if ($basic_results):
        foreach ($basic_results as $result):
            #echo $result.'<br/>';
            $basic_group = '0';
        endforeach;
    else:	
        $basic_group = '1';
    endif;
    
    # create an array of all enhanced groups
    $enhanced_groups = array(8,9,10);
    # check if the company belongs to any of those groups
    $enhanced_results = array_intersect($user_group_ids_deep,$enhanced_groups);
    if ($enhanced_results):
        foreach ($enhanced_results as $result):
            #echo $result.'<br/>';
            $enhanced_group = '0';
        endforeach;
    else:
        $enhanced_group = '1';
    endif;
	
    if ( ($basic_group == 0) && ($enhanced_group == 1) ) :
       # echo 'basic profile';
    endif;
	
    if ( ($basic_group == 1) && ($enhanced_group == 0) ) :
        #echo 'enhanced profile';
		#echo '<span class="enhanced-profile-title">View Enhanced Profile <svg class="icon icon-downarrow"><use xlink:href="' . get_stylesheet_directory_uri() . '/img/icons.svg#icon-downarrow"></use></svg></span>';
		#echo '<div class="row column enhanced-profile">';

		/*$image = get_field('logo', 'user_'.$cID);
		
		if( !empty($image) ): ?>
			<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		<?php endif;*/
		
		$biography = get_field('biography', 'user_'.$cID);
		if( $biography ) :
			#echo '<div class="company-biography medium-8 columns">';
			echo $biography;
			echo '<hr/>';
			#echo '</div>';
		endif;

		#echo '<div class="medium-4 columns">';
		
				$what_makes_you_a_great_company_to_work_for = get_field('what_makes_you_a_great_company_to_work_for', 'user_'.$cID);
				if( $what_makes_you_a_great_company_to_work_for ) :
					echo '<p> What makes you a great company to work for?<br/>';
					echo '' . $what_makes_you_a_great_company_to_work_for . '</p>';
				endif;
				
				$number_of_graduate_positions_this_year = get_field('number_of_graduate_positions_this_year', 'user_'.$cID);
				if( $number_of_graduate_positions_this_year ) :
					echo '<p>Number of Graduate Positions this year<br/>';
					echo '' . $number_of_graduate_positions_this_year . '</p>';
				endif;
				
				$total_number_of_company_employees = get_field('total_number_of_company_employees', 'user_'.$cID);
				if( $total_number_of_company_employees ) :
					echo '<p>Total number of company employees<br/>';
					echo '' . $total_number_of_company_employees . '</p>';
				endif;
				
				if( have_rows('social_media_links', 'user_'.$cID) ): ?>
				
					<p>Social Media Links:<br>
				
					<?php while( have_rows('social_media_links', 'user_'.$cID) ): the_row(); 
				
						# vars
						$facebook_url = get_sub_field('facebook_url');
						$twitter_url = get_sub_field('twitter_url');
						$linkedin_url = get_sub_field('linkedin_url');
						$youtube_url = get_sub_field('youtube_url');
						$other = get_sub_field('other');
						?>
				
							<?php if( $facebook_url ): ?>
								<a href="<?php echo $facebook_url; ?>" target="_blank"><svg class="icon icon-facebook"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-facebook"></use></svg> &nbsp; </a>
							<?php endif; ?>
							
							<?php if( $twitter_url ): ?>
								<a href="<?php echo $twitter_url; ?>" target="_blank"><svg class="icon icon-twitter"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-twitter"></use></svg> &nbsp; </a>
							<?php endif; ?>
							
							<?php if( $linkedin_url ): ?>
								<a href="<?php echo $linkedin_url; ?>" target="_blank"><svg class="icon icon-linkedin"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-linkedin"></use></svg> &nbsp; </a>
							<?php endif; ?>
							
							<?php if( $youtube_url ): ?>
								<a href="<?php echo $youtube_url; ?>" target="_blank"><svg class="icon icon-youtube"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-youtube"></use></svg> &nbsp; </a>
							<?php endif; ?>      
							
							<?php if( $other ): ?>
								<a href="<?php echo $other; ?>" target="_blank">Other</a><br/>
							<?php endif; ?>                                                
				
					<?php endwhile; ?>
				
					</p>
				
				<?php endif;
				
				$careers_that_you_recruit_for = get_field('careers_that_you_recruit_for', 'user_'.$cID);
				if( $careers_that_you_recruit_for ) :
					echo '<p>Careers that you recruit for<br/>';
					echo '' . $careers_that_you_recruit_for . '</p>';
				endif;
				
				$locations = get_field('locations', 'user_'.$cID);
				if( $locations ) :
					echo '<p>Locations<br/>';
					echo '' . $locations . '</p>';
				endif;
				
				if( have_rows('contact_details', 'user_'.$cID) ): ?>
				
					<p>Contact Details
				
					<?php while( have_rows('contact_details', 'user_'.$cID) ): the_row(); 
				
						# vars
						$phone = get_sub_field('phone');
						$email = get_sub_field('email');
						$web = get_sub_field('web');
						$address = get_sub_field('address');
						$contact_name = get_sub_field('contact_name');
						?>
				
							<?php if( $phone ): ?>
								<a href="<?php echo $phone; ?>"><?php echo $phone; ?></a><br/>
							<?php endif; ?>
							
							<?php if( $email ): ?>
								<a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a><br/>
							<?php endif; ?>
							
							<?php if( $web ): ?>
								<a href="<?php echo $web; ?>" target="_blank"><?php echo $web; ?></a><br/>
							<?php endif; ?>
							
							<?php if( $address ): ?>
							<div class="acf-map">
								<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
								<style type="text/css">
								
								.acf-map {
									width: 100%;
									height: 400px;
									border: #ccc solid 1px;
									margin: 20px 0;
								}
								
								/* fixes potential theme css conflict */
								.acf-map img {
								   max-width: inherit !important;
								}
								
								</style>
								<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAj_Xc1gEDKeDqLIMFHgok9hhWgyf16TK0"></script>
								<script type="text/javascript">
								(function($) {
								
								/*
								*  new_map
								*
								*  This function will render a Google Map onto the selected jQuery element
								*
								*  @type	function
								*  @date	8/11/2013
								*  @since	4.3.0
								*
								*  @param	$el (jQuery element)
								*  @return	n/a
								*/
								
								function new_map( $el ) {
									
									// var
									var $markers = $el.find('.marker');
									
									
									// vars
									var args = {
										zoom		: 16,
										center		: new google.maps.LatLng(0, 0),
										mapTypeId	: google.maps.MapTypeId.ROADMAP
									};
									
									
									// create map	        	
									var map = new google.maps.Map( $el[0], args);
									
									
									// add a markers reference
									map.markers = [];
									
									
									// add markers
									$markers.each(function(){
										
										add_marker( $(this), map );
										
									});
									
									
									// center map
									center_map( map );
									
									
									// return
									return map;
									
								}
								
								/*
								*  add_marker
								*
								*  This function will add a marker to the selected Google Map
								*
								*  @type	function
								*  @date	8/11/2013
								*  @since	4.3.0
								*
								*  @param	$marker (jQuery element)
								*  @param	map (Google Map object)
								*  @return	n/a
								*/
								
								function add_marker( $marker, map ) {
								
									// var
									var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );
								
									// create marker
									var marker = new google.maps.Marker({
										position	: latlng,
										map			: map
									});
								
									// add to array
									map.markers.push( marker );
								
									// if marker contains HTML, add it to an infoWindow
									if( $marker.html() )
									{
										// create info window
										var infowindow = new google.maps.InfoWindow({
											content		: $marker.html()
										});
								
										// show info window when marker is clicked
										google.maps.event.addListener(marker, 'click', function() {
								
											infowindow.open( map, marker );
								
										});
									}
								
								}
								
								/*
								*  center_map
								*
								*  This function will center the map, showing all markers attached to this map
								*
								*  @type	function
								*  @date	8/11/2013
								*  @since	4.3.0
								*
								*  @param	map (Google Map object)
								*  @return	n/a
								*/
								
								function center_map( map ) {
								
									// vars
									var bounds = new google.maps.LatLngBounds();
								
									// loop through all markers and create bounds
									$.each( map.markers, function( i, marker ){
								
										var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );
								
										bounds.extend( latlng );
								
									});
								
									// only 1 marker?
									if( map.markers.length == 1 )
									{
										// set center of map
										map.setCenter( bounds.getCenter() );
										map.setZoom( 16 );
									}
									else
									{
										// fit to bounds
										map.fitBounds( bounds );
									}
								
								}
								
								/*
								*  document ready
								*
								*  This function will render each map when the document is ready (page has loaded)
								*
								*  @type	function
								*  @date	8/11/2013
								*  @since	5.0.0
								*
								*  @param	n/a
								*  @return	n/a
								*/
								// global var
								var map = null;
								
								$(document).ready(function(){
								
									$('.acf-map').each(function(){
								
										// create map
										map = new_map( $(this) );
								
									});
								
								});
								
								})(jQuery);
								</script>                
							</div>
							<?php endif; ?>      
							
							<?php if( $contact_name ): ?>
								<p>Contact: <?php echo $contact_name; ?></p>
							<?php endif; ?>                                                
				
					<?php endwhile; ?>
				
					</ul>
				
				<?php endif; ?>
				</p>
				
				<?php if( have_rows('gallery', 'user_'.$cID) ): ?>
				
				<div class="row small-up-2 medium-up-4">

				
					<?php while( have_rows('gallery', 'user_'.$cID) ): the_row(); 
				
						# vars
						$image = get_sub_field('image');
				
						if( $image ): ?>
								
								<div class="column column-block">
										<img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt'] ?>" />			
								</div>

						<?php endif;
					endwhile; ?>
				
					</div>
				
				<?php endif; 
				
				$promotional_video = get_field('promotional_video', 'user_'.$cID);
				if( $promotional_video ) : ?>
				<div class="embed-container">
					<?php echo $promotional_video; ?>
				</div>
				<style>
					.embed-container { 
						position: relative; 
						padding-bottom: 56.25%;
						height: 0;
						overflow: hidden;
						max-width: 100%;
						height: auto;
					} 
				
					.embed-container iframe,
					.embed-container object,
					.embed-container embed { 
						position: absolute;
						top: 0;
						left: 0;
						width: 100%;
						height: 100%;
					}
				</style>
				<?php endif;
		     	


		    endif;	

		    if ( ($basic_group == 1) && ($enhanced_group == 1) ) :
		        echo 'Sorry, there is no profile for this company. Here are some profiles for similar companies:';
				
				#select all users where employer_registration_industry = 
				
				$args = array(
					#'role__in'		=> array('employer', 'subscriber'),
					'role__in'		=> array('customer', 'subscriber'),
					'role__not_in'	=> array('administrator'),
					'exclude'		=> array(1),
					'orderby'		=> 'user_nicename',
					'order'			=> 'ASC',
					'meta_query'	=>
					array(
						array(
							'relation' => 'AND',

							array(
								'key' => 'employer_registration_industry',
								'value' => $all_meta_for_user['employer_registration_industry'][0],
								'compare' => "="
							),
							array(
								'key' => 'activate_membership',
								'value' => 'Yes',
								'compare' => "="
							)	

						)
					)

				);
				$users = get_users($args);
				if ( $users ) :
					foreach ($users as $user) : 
						$company_name = get_field('company_name', 'user_'.$user->ID);
						$hyphenate_company_name = str_replace(" ", "-", $company_name); 
						?>
						<a href="<?php echo esc_url( home_url( '/' ) ).'companies/details/?company='.$hyphenate_company_name.'&cid='.$user->ID; ?>" title="View the full details on <?php echo $company_name; ?> &raquo;"><?php echo $company_name ?> </a>
						<?php
					endforeach;
				endif;
				
				
				
		    endif;

    #	echo '</div>'; // enhanced-profile row
    	#echo '</div>';

    ?>    



	</div><!-- enhanced tabs panel -->










	</div>
</div>		
</div>	

	<?php
	endif;
	
endif;
?>

	<div class="sidebar-left" data-equalizer-watch="main-content">


			<div class="sidebar--header" data-equalizer-watch="main-content-headers">
				<h3>Company Details</h3> <svg class="icon icon-details"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-details"></use></svg>
			</div>
<?php if ($logo): ?>

			<div class="sidebar-left--company-logo">
				<img src="<?php echo $logo['sizes']['medium']; ?>" alt="" />
			</div>

<?php endif; ?>


			<div class="sidebar-left--company-details row">

				<div class="small-6 columns">Company</div>
				<div class="small-6 columns"><?php echo $company_name; ?></div>
				

				<?php if($main_address): ?>
					<div class="small-6 columns">Location</div>
					<div class="small-6 columns"><?php echo $main_address; ?></div>
				<?php endif; ?>

				<!--<div class="small-6 columns">Positions Available</div>
				<div class="small-6 columns">10 - 15</div>
				-->
				
				<?php if($company_employees): ?>
					<div class="small-6 columns">Total Employees</div>
					<div class="small-6 columns"><?php echo $company_employees; ?></div>
				<?php endif; ?>



                <?php if ($employer_registration_industry): ?>
            	<div class="small-6 columns">Industry</div>
                <div class="small-6 columns bolder">
						<?php foreach($employer_registration_industry as $industry): ?>
									<a href="<?php bloginfo('url'); ?>/industries/?industry=<?php echo $industry; ?>"><?php echo $industry; ?></a>
						<?php endforeach; ?>
                </div>
            	<?php endif; ?>

			</div><!-- row -->

			<?php /*if( have_rows('gallery', 'user_'.$cID)): ?>

				<div class="sidebar-left--company-media row">

					<?php while( have_rows('gallery', 'user_'.$cID)): the_row(); 
					$image = get_sub_field('image');
					?>
						<?php if($image): ?><img src="<?php echo $image['sizes']['medium']; ?>" alt="image" /><br/><?php endif; ?>
					<?php endwhile; ?>

				</div><!-- sidebar-left-company-media -->

			<?php endif; */?>


			<!--<div class="button-centered">
				<a href="#" class="button blue">See full profile</a>
			</div>-->



<!--
			<div class="sidebar--header">
				<h3>Key Information</h3> <svg class="icon icon-info"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-info"></use></svg>
			</div>

			<div class="sidebar-left--key-information">

				<a href="#" class="key-info">
					<svg class="icon icon-money"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-money"></use></svg> Average salary at Anglian Water
				</a>

				<a href="#" class="key-info">
					<svg class="icon icon-tips"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-tips"></use></svg> Interview tips for Anglian Water
				</a>

				<a href="#" class="key-info">
					<svg class="icon icon-clock"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-clock"></use></svg> Working hours at Anglian Water
				</a>

				<a href="#" class="key-info">
					<svg class="icon icon-thumbsup"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-thumbsup"></use></svg> Best and worst aspects of working at Anglian Water
				</a>


			</div> --><!-- sidebar-left-key-information-->





			<div class="sidebar--header">
				<h3>Application Tips</h3> <svg class="icon icon-bulb"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-bulb"></use></svg>
			</div>

			<div class="sidebar-left--application-tips">

					<p>Employers constantly tell us that there is nothing that impresses them more than a graduate who has properly researched the company and really knows why they want to work there - so make sure that you tell them in your application that you have read-up all about them on TheJobCrowd and so you really know what makes that company great and why they are right for you. It will help your application, we promise!</p>

			</div> <!-- sidebar-left-key-information-->




	</div><!--sidebar-left -->








	<div class="sidebar-right" data-equalizer-watch="main-content">

		<div class="sidebar--header" data-equalizer-watch="main-content-headers">
			<h3>Your saved jobs</h3>
		</div>

		<div class="saved-jobs">

			<?php get_template_part("sidebar-saved-jobs"); ?>

		</div><!-- saved-jobs -->

	</div><!--sidebar-right -->



</div> <!-- main-content -->


<script type="text/javascript">
	

	$(".search-results-block--savethis .btn.action").click(function() {

		//
		$(".saved-jobs").load("<?php echo get_stylesheet_directory_uri(); ?>/sidebar-saved-jobs.php");


	});


</script>

</div> <!-- main-content -->


<?php else: ?>

	<div class="row column">

		<p>Sorry! You don't have permission to view this content.</p>

	</div>

<?php endif; ?>

<?php
#get_sidebar();
get_footer();
