

<?php if( have_rows('statistics','option') ):

$numrows = count( get_field( 'statistics','option' ) );

?>

<section class="stats-cta">

	<div class="row medium-up-2 large-up-<?php echo $numrows; ?>">

	<?php while( have_rows('statistics','option') ): the_row(); 
	$larger_text = get_sub_field('larger_text');
	$smaller_text = get_sub_field('smaller_text');
	?>

	    <div class="column column-block">
			<span class="large"><?php echo $larger_text; ?></span>
			<span class="small"><?php echo $smaller_text; ?></span>
		</div>

	<?php endwhile; ?>

	</div>


	<svg version="1.1" id="stats-cta-anim" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="1713.9px" height="256px" viewBox="0 0 1713.9 256" style="enable-background:new 0 0 1713.9 256;" xml:space="preserve">
		<g>
			<g>
				<path class="stats-cta-anim--lines" d="M15.6,122c22.9-37.2,67.6-69.7,98.4-84c47.1-21.8,193.9-47.5,269-6.7C426.5,55,467.6,108.9,503.5,148
					c66.8,72.9,150.4,123,236.4,97.3c97-29,113-140.7,204.6-190.3c98.7-53.5,266.7-89.3,378.4,21.3c18.5,18.3,52.6,70,73,86
					c99.1,77.8,172.8,115.8,302.6-21.7"/>
			</g>
		</g>
	</svg>


</section>

<?php endif; ?>

</section><!-- stats-cta -->






<?php if(get_field('display_interview_tips_in_footer','option')): ?>

<section class="interview-tips-cta row" data-equalizer="tips-cta">

	<div class="column text-center padded">
		
		<h3><?php the_field('interview_tips_title','option'); ?></h3>

		<p><?php the_field('interview_tips_intro','option'); ?></p>
		
	</div>


	<div class="interview-tips--person-container">
		
		<?php 

		$image = get_field('interview_tips_image','option');

		if( !empty($image) ): ?>

			<img src="<?php echo $image['url']; ?>" class="interview-tips--person img-responsive" alt="interview tips" />

		<?php endif; ?>

		
	</div>

	<div class="interview-tips--person-details">
		
			<span><?php the_field('interview_tips_bio','option'); ?></span>
	</div>


	<div class="interview-tips--tip-container first-tip" data-equalizer-watch="tips-cta">
		
			<div class="interview-tips--tip">

				<span class="interview-tips--tip-number">1</span>

				<div class="interview-tips--tip-content">
					<span class="interview-tips--title"><?php the_field('interview_tip_one_title','option'); ?></span>
					<span class="interview-tips--text"><?php the_field('interview_tip_one_text','option'); ?></span>	
				</div>

			</div>

	</div><!--interview-tips-tip-container-->



	<div class="interview-tips--tip-container" data-equalizer-watch="tips-cta">
		
			<div class="interview-tips--tip">

				<span class="interview-tips--tip-number">2</span>

				<div class="interview-tips--tip-content">
					<span class="interview-tips--title"><?php the_field('interview_tip_two_title','option'); ?></span>
					<span class="interview-tips--text"><?php the_field('interview_tip_two_text','option'); ?></span>	
				</div>

			</div>
			
	</div><!--interview-tips-tip-container-->

	<?php $interview_tips_url = get_field('interview_tips_url','option');
	if ($interview_tips_url): ?>
	<div class="column text-center interview-tips--button">
		
		<a href="<?php echo $interview_tips_url; ?>" class="button blue">Read more job interview tips</a>
			
	</div>
	<?php endif; ?>
</section><!--interview-tips-cta -->

<?php endif; // display interview tips ?>

</section>
			<footer class="site-footer row">

				<div class="footer-logo-container">

						<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo-footer.png" alt="the job crowd" />

						<div class="footer--column1">
							<?php the_field('footer_column_one','option'); ?>
						</div>

						<div class="footer--social-media right">
						
							<a href="http://twitter.com/thejobcrowd" target="_blank">
								<svg class="icon icon-twitter"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-twitter"></use></svg>
							</a>
							
							<a href="http://www.facebook.com/jobcrowd" target="_blank">
								<svg class="icon icon-facebook"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-facebook"></use></svg>
							</a>
							
							<a href="http://www.linkedin.com" target="_blank">
								<svg class="icon icon-linkedin"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-linkedin"></use></svg>
							</a>




						</div>


				</div>

				<div class="footer-available-jobs">

						<?php the_field('footer_column_two','option'); ?>

						<a href="https://www.thejobcrowd.com/find-a-job" class="button blue">View more available jobs</a>

				</div>

				<div class="footer-salary">

						<?php the_field('footer_column_three','option'); ?>

				</div>

				<div class="footer-graduates">

						<?php the_field('footer_column_four','option'); ?>

				</div>

				<div class="footer-employers">

						<?php the_field('footer_column_five','option'); ?>

				</div>

			</footer>

			<span class="footer-copyright">© Copyright TheJobCrowd <?php echo date("Y"); ?> - <a href="<?php bloginfo('url'); ?>/terms-conditions/">Terms &amp; Conditions</a> - <a href="<?php bloginfo('url'); ?>/privacy-policy/">Privacy</a></span>

			<span class="footer-credit">Website by <a href="https://www.prgltd.co.uk" target="_blank">PRG Marketing Ltd</a></span>



			<?php wp_reset_query(); if(get_field('display_pop_up','option') && get_field('pop_up_contents','option')): ?>

				<?php if(get_field('display_pop_up_on_every_page','option')): ?>

					<div class="<?php if(get_field('pop_up_size','option')): the_field('pop_up_size','option'); endif; ?> reveal" id="sitePopup" 
				<?php $entereddelay = get_field('pop_up_delay','option'); if($entereddelay): $revealdelay = $entereddelay * 1000; else: $revealdelay = 0; endif; ?>data-popupdelay="<?php echo $revealdelay; ?>"
					data-reveal>

					<?php if(get_field('pop_up_contents','option')): the_field('pop_up_contents','option'); endif; ?>

					  <button class="close-button" data-close aria-label="Close modal" type="button">
					    <span aria-hidden="true">&times;</span>
					  </button>

					</div><!--reveal -->

				<?php elseif(is_front_page()): ?>

					<div class="<?php if(get_field('pop_up_size','option')): the_field('pop_up_size','option'); endif; ?> reveal" id="sitePopup" 
				<?php $entereddelay = get_field('pop_up_delay','option'); if($entereddelay): $revealdelay = $entereddelay * 1000; else: $revealdelay = 0; endif; ?>data-popupdelay="<?php echo $revealdelay; ?>"
					data-reveal>

					<?php if(get_field('pop_up_contents','option')): the_field('pop_up_contents','option'); endif; ?>

					  <button class="close-button" data-close aria-label="Close modal" type="button">
					    <span aria-hidden="true">&times;</span>
					  </button>

					</div><!--reveal -->

				<?php endif; // display popup on every page?>

			<?php endif; // they want to display a popup, and there's contents entered?>



<?php wp_footer(); ?>





<?php 
if (is_checkout()): 
#allows us to toggle additional fields depending on the additional contact required radio button
?>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script type='text/javascript'>
//<![CDATA[
jQuery(function ($) {
	
	$('#additional_name_field').hide();
	$('#additional_position_held_field').hide();
	$('#additional_email_field').hide();
	$('#additional_telephone_number_field').hide();

    $("input[name=add_another_contact]:radio").click(function () {

        if ($('input[name=add_another_contact]:checked').val() == "Yes") {
            $('#additional_name_field_field').show();
            $('#additional_position_held_field').show();
			$('#additional_email_field').show();
			$('#additional_telephone_number_field').show();
			
			$('label[for=additional_name], input#additional_name').show();
			$('label[for=additional_position_held], input#additional_position_held').show();
			$('label[for=additional_email], input#additional_email').show();
			$('label[for=additional_telephone_number], input#additional_telephone_number').show();			

        } else if ($('input[name=add_another_contact]:checked').val() == "No") {
            $('#additional_name_field').hide();
            $('#additional_position_held_field').hide();
			$('#additional_email_field').hide();
			$('#additional_telephone_number_field').hide();
			
			$('label[for=additional_name], input#additional_name').hide();
			$('label[for=additional_position_held], input#additional_position_held').hide();
			$('label[for=additional_email], input#additional_email').hide();
			$('label[for=additional_telephone_number], input#additional_telephone_number').hide();

        }
    });
	
});//]]> 

</script>
<?php endif; #endif is_checkout() ?>


</body>
</html>
