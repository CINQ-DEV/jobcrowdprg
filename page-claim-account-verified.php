<?php
/*
Template Name: Claim Account Verified
*/

get_header(); ?>

<div class="page-header row">

	<div class="medium-5 columns">
		<h1>Verify Account</h1>
	</div>
	
	<?php get_template_part('template-parts/page-header-search'); ?>

</div><!-- page-header -->



<div class="main-content fullwidth" data-equalizer="main-content">

	<div class="main-content-main" data-equalizer-watch="main-content">

				<div class="main-content-main--breadcrumbs" data-equalizer-watch="main-content-headers">
							<?php
							if ( function_exists('yoast_breadcrumb') ) {
							yoast_breadcrumb('
							<p id="breadcrumbs">','</p>
							');
							}
							?>
					<div class="save-share">

						<!--<a href="#" class="savethis">
							<svg class="icon icon-heart"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-heart"></use></svg> Save this
						</a>-->

						<?php get_template_part('template-parts/share-button'); ?>

					</div><!--save-share-->

				</div><!-- main-content-main-breadcrumbs -->

				<div class="padded">
		
					<div class="row">

					<div class="medium-6 columns">
					&nbsp;
					</div> <!-- medium-6 -->

					<div class="medium-6 columns">
<?php
if(isset($_GET['email']) && !empty($_GET['email']) AND isset($_GET['hash']) && !empty($_GET['hash'])):
						
    # Verify data						
	$find = ["\'.", ".\'"];
	$replace   = ["", ""];

	$email = str_replace($find, $replace, $_GET['email']); 
	$email = filter_var($email, FILTER_SANITIZE_EMAIL); #add an additional check 
	#$hash = str_replace("\'.", "", $_GET['hash']); 		
	$hash = str_replace($find, $replace, $_GET['hash']); 						
						
	#echo 'email: '.$email.'<br>';
	#echo 'hash: '.$hash.'<br>';
	#$hash2='6faa8040da20ef399b63a72d0e4ab575';
						
						
	$args = array(
		array('customer', 'subscriber'),
		'meta_query' =>
		array(
		
			array(
				'relation' => 'AND', # OR						

				array(
					'key' => 'hash',
					'value' => $hash,
					'compare' => "=",
				),	
				array(
					'key' => 'activate_membership',
					'value' => 'Yes',
					'compare' => "=",
				),
				array(
					'key' => 'account_verified',
					'value' => '0',
					'compare' => "=",
				)	
				
			)
		)
			 
	);
	
	$users = get_users( $args );
	
	if ($users):
	
		foreach ( $users as $user ) :

			#lets auto login the user
			#prg_account_verified_auto_login( $user->ID, $email, $hash );		
						
			#We have a match, activate the account
			echo '<h3>Success!</h3>';
			echo '<p>You have successfully claimed this account. You will shortly receive an email with your login details</p>';	
			#echo '<a href="https://www.thejobcrowd.com/register/" class="button">Click here to Complete Verification</a>';			
			?>
            <a href="https://www.thejobcrowd.com/register/?verified=true&userID=<?php echo $user->ID; ?>" class="button">Click here to Complete Verification</a>
            <?php
						
			#$email = $user->user_email;
			update_user_meta( $user->ID, 'account_verified', '1' );
						
			#reset the hash to prevent the URL being used again
			update_user_meta( $user->ID, 'hash', '' );
			
			#update email address
			global $wpdb;
			$old_username = get_field( 'existing_email_address', 'user_'.$user->ID );

			# Update username!						
			$q = $wpdb->prepare( "UPDATE $wpdb->users SET user_login = %s WHERE user_login = %s", $email, $old_username );
			$wpdb->query( $q );						
			#var_dump( $wpdb->last_query );	

			# Update user_nicename
			$qnn = $wpdb->prepare( "UPDATE $wpdb->users SET user_nicename = %s WHERE user_login = %s AND user_nicename = %s", $email, $email, $old_username );
			$wpdb->query( $qnn );

			# Update user_email
			$qem = $wpdb->prepare( "UPDATE $wpdb->users SET user_email = %s WHERE user_login = %s AND user_email = %s", $email, $email, $old_username );
			$wpdb->query( $qem );						

			# Update display_name
			$qdn = $wpdb->prepare( "UPDATE $wpdb->users SET display_name = %s WHERE user_login = %s AND display_name = %s", $email, $email, $old_username );
			$wpdb->query( $qdn );

			# Update nickname
			#$nickname = get_user_meta( $user->ID, 'nickname', true );
			#if ( $nickname == $old_username ) {
				update_user_meta( $user->ID, 'nickname', $email );
			#}												
						
			#send email with password
			$pw = wp_generate_password( 16, true, true ); #Generate a 16 character password consisting of letters, numbers, special characters																
			$to			= $email;
			#$to		= get_option( 'admin_email' );			
			$subject	= "The Job Crowd :: Account Ownership Verification :: Success!";
			$message	= "Hi {$email},<br/><br/>";	
			#$message	.= "Email Address: {$email} <br/>";	
			#$message	.= "Old Email Address: {$old_username} <br/>";
			$message	.= "Your account has now been verified. Your login details are below: <br/>";
			$message	.= "https://www.thejobcrowd.com/my-account <br>";
			$message	.= "Username: {$email} <br/>";			
			$message	.= "Password: {$pw} <br/><br/>";		
			#$message	.= "Alternatively, you can proceed directly to the checkout and login there to complete your transaction: <br/>";					
			#$message	.= "https://www.thejobcrowd.com/register <br>";						
			$headers = array(
				'Content-Type: text/html; charset=UTF-8',
				#'From: '.get_option( 'blogname' ).' <'.get_option( 'admin_email' ).'>'
				'From: '.get_option( 'blogname' ).' <info@thejobcrowd.com>'
			);
			wp_mail ($to, $subject, $message, $headers);						
					
			

		endforeach;#end $user
	
	else:
						
		# No match -> invalid url or account has already been activated.
		echo '<h3>Error!</h3>';
        echo '<p>The url is either invalid or you already have activated your account.</p>';
		update_user_meta( $user->ID, 'account_verified', '0' );
						
	endif; #endif $users;							
						
else:
						
    # Invalid approach
	echo '<h3>Error!</h3>';
	echo '<p>Invalid approach, please use the link that has been send to your email.</p>';
	update_user_meta( $user->ID, 'account_verified', '0' );
						
endif;
?>


					</div> <!-- medium-6 -->

					</div> <!-- row -->

				</div> <!-- padded -->
			
	</div><!--main-content-main -->



</div> <!-- main-content -->

<?php get_footer();