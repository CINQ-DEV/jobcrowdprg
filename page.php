<?php get_header(); ?>


<div class="page-header row">

	<div class="medium-5 columns">
		<h1><?php the_title(); ?></h1>
		<!--<span class="search-results-count"><?php echo get_the_date('Y-m-d'); ?></span>-->
	</div>

	<?php get_template_part('template-parts/page-header-search'); ?>

</div><!-- page-header -->



<?php if ($post->post_parent == 308) : ?>
<div class="main-content" data-equalizer="main-content" data-equalize-on="medium">
<?php elseif (is_page('checkout')) : ?>
<div class="main-content fullwidth" data-equalizer="main-content" data-equalize-on="medium">
<?php else: ?>
<div class="main-content no-right-column" data-equalizer="main-content" data-equalize-on="medium">
<?php endif; ?>


		<div class="main-content-main" data-equalizer-watch="main-content">

					<div class="main-content-main--breadcrumbs" data-equalizer-watch="main-content-headers">
							
							<?php
							if ( function_exists('yoast_breadcrumb') ) {
							yoast_breadcrumb('
							<p id="breadcrumbs">','</p>
							');
							}
							?>

						<!--<div class="sort-results">
							<span>Relevance</span> &nbsp; | &nbsp; <a href="#">Newest First</a>
						</div>-->

					</div>
				<div class="row top-companies padded job-reviews-by-company">

			<?php while ( have_posts() ) : the_post(); ?>
				<article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">

					<div class="entry-content padded">
						<?php the_content(); ?>

						<?php get_template_part('layout'); ?>

						<?php 					
						###
						# Graduate Review Page 
						###
						if (is_page(318)):
							echo do_shortcode('[gravityform id="5" title="false" description="false"]');
						##Apprenrice review page:
						elseif (is_page(460)):
							echo do_shortcode('[gravityform id="6" title="false" description="false"]');
						endif;
						?>
						
						<?php 					
						###
						# Contact Page 
						###
						if (is_page(58)):
							echo do_shortcode('[gravityform id="11" title="false" description="false"]');
						endif;
						?>						
						
						<?php
						global $post;

						if ($post->post_parent == 308) :
	
						$graduate_list_small_intake = get_post_meta(get_the_ID(), 'graduate_list_small_intake', true);
						$graduate_list_large_intake = get_post_meta(get_the_ID(), 'graduate_list_large_intake', true);
						$apprentice_list 			= get_post_meta(get_the_ID(), 'apprentice_list', true);	
						
						$rename = get_field('rename_graduate_tab');
						if ($rename == '1'):
							$tab_wording = 'Graduates';
							$tab_id = 'graduates';
						else:
							$tab_wording = 'Graduate: Smaller Intake';
							$tab_id = 'smaller';
						endif;
						
						if ($graduate_list_small_intake || $graduate_list_large_intak || $apprentice_list ):
						?>
						<!-- Nav tabs -->
						<ul class="tabs" data-tabs data-deep-link="true" data-deep-link-smudge="true" data-deep-link-smudge="500" id="reviews-tabs" data-equalizer-watch="main-content-headers">
							<?php if ($graduate_list_small_intake): ?>
							<li class="tabs-title is-active"><a href="#<?php echo $tab_id; ?>" aria-controls="<?php echo $tab_id; ?>" role="tab"><?php echo $tab_wording; ?></a></li>
							<?php endif; 
							if ($graduate_list_large_intake): ?>
							<li class="tabs-title"><a href="#larger" aria-controls="larger" role="tab">Graduate: Larger Intake</a></li>
							<?php endif; 
							if ($apprentice_list): ?>
							<li class="tabs-title"><a href="#apprentices" aria-controls="apprentices" role="tab">Apprentice</a></li>
							<?php endif; ?>
						</ul>
						<?php endif; ?>
						<!-- Tab panes -->
						<div class="tabs-content" data-tabs-content="reviews-tabs">

							<div class="tabs-panel is-active" id="<?php echo $tab_id; ?>">	
							<?php if( have_rows('graduate_list_small_intake') ): 
						 		$i = 1;
								 while( have_rows('graduate_list_small_intake') ): the_row(); 

									// vars
									$company_name = get_sub_field('company_name');
									$logo = get_sub_field('logo');
									$quote = get_sub_field('quote');
									$company_name = str_replace("&amp;","&",$company_name);

									$users = get_users(array('meta_key' => 'company_name', 'meta_value' => $company_name ));
									foreach ( $users as $user ) :
										$cID =  $user->ID;
									endforeach;		

									$hyphenate_company_name = str_replace(" ", "-", $company_name); 			
									?>

									<div class="medium-12 columns job-reviews-by-company--job-review">

										<div class="job-review--header">
											<span class="job-review-title"><?php echo $company_name; ?></span>
										</div><!-- /job-review-header -->

										<div class="job-review--footer" data-equalizer-watch="job-review-footer">

											<div class="logo-container">
											<img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt'] ?>" />
											</div><!-- /logo -->

									<span class="top-companies--number">
										<?php echo $i; ?>
									</span>

									<?php 
									
									if ($quote):
									echo '<p class="company--quote">' . $quote . '</p>'; 
									endif; ?>

											<p><a href="<?php echo esc_url( home_url( '/' ) ).'companies/details/?company='.$hyphenate_company_name.'&cid='.$cID ?>" class="button pink" title="View the full details on <?php echo $company_name; ?>">Overview</a>

											<a href="<?php echo esc_url( home_url( '/' ) ).'companies/details/?company='.$hyphenate_company_name.'&cid='.$cID ?>#graduates" class="button" title="View the full details on <?php echo $company_name; ?>">Reviews</a></p>					

										</div><!-- /job-review-footer -->

									</div><!-- /medium-6 columns -->

								<?php $i++; endwhile; 

							endif; ?>	
							</div><!-- /smaller_intake -->
							
							<div class="tabs-panel" id="larger">	
							<?php if( have_rows('graduate_list_large_intake') ): 
						 		$i = 1;
								 while( have_rows('graduate_list_large_intake') ): the_row(); 

									// vars
									$company_name = get_sub_field('company_name');
									$logo = get_sub_field('logo');
									$quote = get_sub_field('quote');
									$company_name = str_replace("&amp;","&",$company_name);

									$users = get_users(array('meta_key' => 'company_name', 'meta_value' => $company_name ));
									foreach ( $users as $user ) :
										$cID =  $user->ID;
									endforeach;		

									$hyphenate_company_name = str_replace(" ", "-", $company_name); 			
									?>

									<div class="medium-12 columns job-reviews-by-company--job-review">

										<div class="job-review--header">
											<span class="job-review-title"><?php echo $company_name; ?></span>
										</div><!-- /job-review-header -->

										<div class="job-review--footer" data-equalizer-watch="job-review-footer">

											<div class="logo-container">
											<img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt'] ?>" />
											</div><!-- /logo -->

									<span class="top-companies--number">
										<?php echo $i; ?>
									</span>

									<?php 
									
									if ($quote):
									echo '<p class="company--quote">' . $quote . '</p>'; 
									endif; ?>

											<p><a href="<?php echo esc_url( home_url( '/' ) ).'companies/details/?company='.$hyphenate_company_name.'&cid='.$cID ?>" class="button pink" title="View the full details on <?php echo $company_name; ?>">Overview</a>

											<a href="<?php echo esc_url( home_url( '/' ) ).'companies/details/?company='.$hyphenate_company_name.'&cid='.$cID ?>#graduates" class="button" title="View the full details on <?php echo $company_name; ?>">Reviews</a></p>					

										</div><!-- /job-review-footer -->

									</div><!-- /medium-6 columns -->

								<?php $i++; endwhile; 

							endif; ?>	
							</div><!-- /larger_intake -->							

							<div class="tabs-panel" id="apprentices">	
							<?php if( have_rows('apprentice_list') ): 
								 $i = 1;
								 while( have_rows('apprentice_list') ): the_row(); 

									// vars
									$company_name = get_sub_field('company_name');
									$logo = get_sub_field('logo');
									$quote = get_sub_field('quote');
									$company_name = str_replace("&amp;","&",$company_name);

									$users = get_users(array('meta_key' => 'company_name', 'meta_value' => $company_name ));
									foreach ( $users as $user ) :
										$cID =  $user->ID;
									endforeach;		

									$hyphenate_company_name = str_replace(" ", "-", $company_name); 			
									?>

									<div class="medium-12 columns job-reviews-by-company--job-review">

										<div class="job-review--header">
											<span class="job-review-title"><?php echo $company_name; ?></span>
										</div><!-- /job-review-header -->

										<div class="job-review--footer" data-equalizer-watch="job-review-footer">

											<div class="logo-container">
											<img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt'] ?>" />
											</div><!-- /logo -->

											<span class="top-companies--number">
												<?php echo $i; ?>
											</span>

											<?php 

											if ($quote):
											echo '<p class="company--quote">' . $quote . '</p>'; 
											endif; ?>

											<p><a href="<?php echo esc_url( home_url( '/' ) ).'companies/details/?company='.$hyphenate_company_name.'&cid='.$cID ?>" class="button pink" title="View the full details on <?php echo $company_name; ?>">Overview</a>

											<a href="<?php echo esc_url( home_url( '/' ) ).'companies/details/?company='.$hyphenate_company_name.'&cid='.$cID ?>#graduates" class="button" title="View the full details on <?php echo $company_name; ?>">Reviews</a></p>					

										</div><!-- /job-review-footer -->

									</div><!-- /medium-6 columns -->

								<?php $i++; endwhile; 

							endif; ?>	
							</div><!-- /apprentice -->

						</div><!-- /review-tabs -->												

						<?php
						endif; #endif post_parent
						?>
						
						
					<footer>
						<?php
							wp_link_pages(
								array(
									'before' => '<nav id="page-nav"><p>' . __( 'Pages:', 'foundationpress' ),
									'after'  => '</p></nav>',
								)
							);
						?>
						<p><?php the_tags(); ?></p>
					</footer>
					</div>

				</article>
			<?php endwhile;?>
			</div>
		</div><!-- main-content-main -->

<?php if (!is_page('checkout')) : ?>
		<div class="sidebar-left" data-equalizer-watch="main-content222">

		<?php if(is_woocommerce() || is_account_page() || is_wc_endpoint_url()): ?>

				<?php if (is_user_logged_in()): ?>
					<a href="<?php bloginfo('url'); ?>/my-account" class="button blue padded strong">View your account</a>
				<?php endif; ?>

		<?php else: ?>

            <div class="sidebar--header" data-equalizer-watch="main-content-headers">
                <h3>Industries</h3> <svg class="icon icon-details"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-details"></use></svg>
            </div>

            <div class="padded">
                
			<?php
			global $post;

			#if ($post->post_parent == 308) :

                
                # we need to get a list of all industries from the options page

                # reset choices
                $field['industries'] = array();
                        
                # get the textarea value from options page without any formatting
                $industries = get_field('industry', 'option', false);
                    
                # explode the value so that each line is a new array piece
                $industries = explode("\n", $industries);
                    
                # remove any unwanted white space
                $industries = array_map('trim', $industries);
                $url = get_site_url();

                # loop through array and add to field 'choices'
                if( is_array($industries) ) :
                    foreach( $industries as $industry ) :
                        if($industry === '- Select Industry -') continue; #skip first item      
                        #$industry2 = str_replace("&","&amp;",$industry);     
				
						$args = array(
							'post_type'  => 'page', 
							posts_per_page => '1',
							'meta_query' => array( 
								array(
									'key'   => 'industry_sector', 
									#'value' => 'Accountancy &amp; Insurance',
									'value' => $industry,	
									'compare' => '='
								)
							)
						);			

						$the_query = new WP_Query( $args );
						if( $the_query->have_posts() ):
							while( $the_query->have_posts() ) : $the_query->the_post(); ?>
								<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>		
							<?php endwhile;			
						endif;	wp_reset_query();			
                            
                    endforeach;
                endif; 				
				
			#endif; #endif post_parent
			?>


            </div>

		<?php endif; ?>

	</div><!--sidebar-left -->
<?php endif; //is_page(checkout) ?>


	<?php global $post; if ($post->post_parent == 308) : ?>
	<div class="sidebar-right" data-equalizer-watch="main-content">
	<?php $industry = get_field('industry_sector');
	if ($industry):
		
		#lets search for the relevant row with our advert
		global $wpdb;

		$adverts = $wpdb->get_results(
			$wpdb->prepare( 
			"
			SELECT *
			FROM $wpdb->options
			WHERE option_name LIKE %s
			AND option_value = %s
			",
			'options_advertisement_%_sector',
			$industry             
			)
		);
		#echo $wpdb->last_query.'<br>';             #debug 

		#we can now obtain the specific repeater row details
		foreach ( $adverts as $advert ):            
			$option_id		= $advert->option_id;
			$option_name	= $advert->option_name;
			$option_value	= $advert->option_value;
		endforeach;

		#identify the row number
		$row = preg_replace("/[^0-9]/","",$option_name);

		#we can now get the specific advert from the row
		$image_id = get_option( 'options_advertisement_'.$row.'_advert' );

		#finally, we can get the image by attachment ID
		$image = wp_get_attachment_image_src( $image_id, 'full' );		
		?>
		<div class="padded">
			<img src="<?php echo $image['0']; ?>" />
		</div>
	<?php
	endif; #endif get field
	?>	
	</div> <!-- sidebar-right -->
	<?php endif; #endif post_parent ?>

</div>
</div>

 <?php get_footer();
