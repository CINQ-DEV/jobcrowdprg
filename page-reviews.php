<?php
/*
Template Name: Review
 */

 get_header(); ?>


<div class="page-header row">

	<div class="medium-5 columns">
		<h1>Search Reviews</h1>
	</div>
	
	<?php get_template_part('template-parts/page-header-search'); ?>

</div><!-- page-header -->

<!-- /include the jquery auto complete files -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.12.4.js"></script>
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	
<?php
# we need to get a list of all towns from the options page

# reset choices
$field['towns'] = array();
        
# get the textarea value from options page without any formatting
$towns = get_field('town', 'option', false);
    
# explode the value so that each line is a new array piece
$towns = explode("\n", $towns);
    
# remove any unwanted white space
$towns = array_map('trim', $towns);
    
# loop through array and add to field 'choices'
if( is_array($towns) ) :
        
	foreach( $towns as $town ) :
		if($town === '- Select Town -') continue; #skip first item                
		$field['towns'][ $town ] = '"'.$town.'"';
            
	endforeach;
        
endif;

# convert array into comma-separated string
$towns = implode(',', $field['towns'] );
?>	
<script>
$( function() {
	var availableTowns = [
		<?php echo $towns; #output the towns as a comma-separated list ?>
	];
	$( "#towns" ).autocomplete({
		source: availableTowns
	});
});
</script>

<div class="main-content no-right-column" data-equalizer="main-content">

	<div class="main-content-main" data-equalizer-watch="main-content">


<?php
# let's handle the form submission
$town 		= $_POST['towns'];
$industries	= $_POST['industries'];
$ratings	= $_POST['ratings'];
$submit		= $_POST['submit'];

if ($submit):
	echo '<div class="main-content-main--breadcrumbs" data-equalizer-watch="main-content-headers">';
	if ($town):
		echo 'The town you entered is: <strong>' . $town .'</strong><br/>';
	endif;
	if ($industries):	
		echo 'The industries you selected: <strong>' . implode(',', $_POST['industries']) . '</strong><br/>';
	endif;
	if ($ratings):	
		echo 'The rating you selected: <strong>' . $ratings . '</strong><br/>';     
	endif;
							
	$locations = get_posts(array(
		'post_type' => array('graduate_reviews','apprentice_reviews'),		
		'meta_query' => array(
			array(
				'key' 	=> 'location',
				'value' => $town,
				'compare' => '='
			)
		)	
	));
	#create an array
	$location_result = array();
	foreach($locations as $location) : 
		#pass the results into the array
		$location_result[] = $location->ID;
	endforeach;		


	#create our arrays
	$app_ind_result = array();	
	$grad_ind_result = array();

	#loop through each industry and check for a match, add to array if so					
	foreach($industries as $industry) : 

			$app_ind = get_posts(array(
				'post_type' => 'apprentice_reviews',
				'posts_per_page'=> '-1',
				'meta_query' => array(
					array(
						'key' 	=> 'apprentice_review_industry',
						'value' => $industry,
						'compare' => 'LIKE'
					)
				)	

			));

			foreach($app_ind as $app) : 
				#pass the results into the array
				$app_ind_result[] = $app->ID;
			endforeach;	

			$grad_ind = get_posts(array(
				'post_type' => 'graduate_reviews',
				'posts_per_page'=> '-1',
				'meta_query' => array(
					array(
						'key' 	=> 'graduate_review_industry',
						'value' => $industry,
						'compare' => 'LIKE'
					)
				)	

			));
			foreach($grad_ind as $grad) : 
				#pass the results into the array
				$grad_ind_result[] = $grad->ID;
			endforeach;							

	endforeach;													


	if(isset($ratings ) && $ratings ) :

		$stars = get_posts(array(
			'post_type' => array('graduate_reviews','apprentice_reviews'),	
			'posts_per_page'=> '-1',
			'meta_query' => array(
				array(
					'key' 	=> 'star_rating',
					'value' => array( $ratings, $ratings.'.9' ),
					'type'    	=> 'numeric',
					'compare' => 'BETWEEN'
				)
			)	
		));
		#create an array
		$stars_result = array();
		foreach($stars as $star) : 
			#pass the results into the array
			$stars_result[] = $star->ID;
		endforeach;											

	endif;									
	/*	
	echo '<pre>';
	print_r($grad_ind_result);					
	echo '</pre>';

	echo '<pre>';
	print_r($app_ind_result);					
	echo '</pre>';					
	*/
	#if a rating has been set, we can filter the results more specifically
	if(isset($ratings ) && $ratings ) :

		#combine all results from the OR criteria and remove duplicates				
		$combine = array_unique( array_merge( $location_result, $app_ind_result, $grad_ind_result ) );	

		#now compare the combined results of the OR search with the AND value (stars)
		$unique = array_intersect( $combine,  $stars_result);					

	else:

		$unique = array_unique( array_merge( $location_result, $app_ind_result, $grad_ind_result ) );

	endif;				

	#not an empty array					
	if($unique):

	$args = array (
		'post_type' => array('graduate_reviews','apprentice_reviews'),
		'posts_per_page'=> '20',
		'post__in'	=> $unique		
	);	
	
	$wp_query = new WP_Query( $args );
	
	#get a total number of results				
	$total = $wp_query->found_posts;
	echo 'Results Found: <strong>'.$total.'</strong><br/>';					
	
	echo '</div>'; // close page breadcrumbs/header


	if ( $wp_query->have_posts() ) :	
		echo '<div class="row medium-up-2 reviews-items">';

		while ( $wp_query->have_posts() ) : $wp_query->the_post(); 
		
		#need the ascertain the post type, so we can now get the company that's been reviewed
		
		#need to exclude any results whereby the industry hasn't been specified
		#if ($industry != '- Select Industry -'):

			$post_id = get_the_ID();	
			#echo $post_id;
				$post_type = get_post_type( get_the_ID() );	

				if ($post_type == 'graduate_reviews'):
					$employer = get_field('graduate_review_employer', $post_id);
					$ind = get_field('graduate_review_industry', $post_id);
				else:
					$employer = get_field('apprentice_review_employer', $post_id);
					$ind = get_field('apprentice_review_industry', $post_id);
				endif;

				$blogusers = get_users(array('meta_key' => 'company_name', 'meta_value' => $employer));
				foreach ( $blogusers as $user ) {
					#access the user meta
					#echo '<span>' . esc_html( $user->ID ) . '</span>'; #debug
				}	
				
				#we can now get the company rating as it's stored against the user
				$overall_rating = get_field('overall_rating', 'user_'.$user->ID);
				$overall_rating_new = get_field('star_rating');
				
				$overall_rating_percent = $overall_rating_new * 20;


				#get the location (only applicable to enhnaced user profiles)
				$locations = get_field('locations', 'user_'.$user->ID);				
				
				#get the main_address
				$main_address = get_field('main_address', 'user_'.$user->ID);
				
				#use geocoding to get the necessary details
				$geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?sensor=false&address=' . urlencode($main_address) . '&key=AIzaSyAj_Xc1gEDKeDqLIMFHgok9hhWgyf16TK0');
				$output = json_decode($geocode);	
				
				#get the address components
				$address_data = $output->results[0]->address_components;
				$street = str_replace('Dr', 'Drive', $address_data[1]->long_name);
				$town = $address_data[2]->long_name;


		?>

									
			<div id="<?php the_ID(); ?>" class="column column-block review <?php echo $selected; ?>">
										<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">

				<span class="employer-title"><?php echo $employer; ?></span>

				<div class="stars">
						<div class="stars-gold" style="width: <?php echo $overall_rating_percent; ?>%;"> &nbsp; </div>
						<div class="stars-white"> &nbsp; </div>
				</div>

				<div class="review-bottom">

					<?php if($ind !== "- Select Industry -"): ?><span class="industry">Industry: <strong><?php echo $ind; ?></strong></span><?php endif; ?>
					<?php if($town): ?><span class="industry">Town: <strong><?php echo $town; ?></strong></span><?php endif; ?>	
		        	<span class="entry-date">Review date: <?php echo get_the_date(); ?></span>
			        
				</div><!-- review-bottom-->		</a>

			</div><!--review -->
				


		<?php		
		#endif; #endif $industry
		endwhile; #wp_reset_query(); 	
		echo '</div>';	
			
	else:	
		
		echo '<h3>No reviews matched your criteria.</h3>';
		
	endif; #endif $users
					
	/* Display navigation to next/previous pages when applicable */ 
	if ( function_exists( 'foundationpress_pagination' ) ) :
		foundationpress_pagination();
	elseif ( is_paged() ) :
	?>
		<nav id="post-nav">
			<div class="post-previous"><?php next_posts_link( __( '&larr; Older posts', 'foundationpress' ) ); ?></div>
			<div class="post-next"><?php previous_posts_link( __( 'Newer posts &rarr;', 'foundationpress' ) ); ?></div>
		</nav>
	<?php endif;										
					
	else:

		echo '<p>No results matched your criteria, please try again.';

	endif; #endif $unique not empty
					


else: #=============================================================================the form hasn't been submitted, so show all reviews
			
?>
<div class="review-container row">
	
	<!-- Nav tabs -->
	<ul class="tabs" data-tabs id="reviews-tabs" data-tabs data-deep-link="true" data-deep-link-smudge="true" data-deep-link-smudge="500" data-equalizer-watch="main-content-headers">
		<li class="tabs-title is-active"><a href="#graduates" aria-controls="graduates" role="tab">Recent Graduate Reviews</a></li>
		<li class="tabs-title"><a href="#apprentices" aria-controls="apprentices" role="tab">Recent Apprentices Reviews</a></li>
		<li class="tabs-title"><a href="#companies" aria-controls="companies" role="tab">All Reviews By Company</a></li>
	</ul>
	
	<div class="entry-content padded">
	<?php get_template_part('layout'); ?>
	</div><!-- /entry-content -->

	<!-- Tab panes -->
	<div class="tabs-content" data-tabs-content="reviews-tabs">

		<div class="tabs-panel is-active" id="graduates">		
		<?php										
		$paged = get_query_var('paged') ? get_query_var('paged') : 1;
		$args = array(
			#'post_type'		=> array('graduate_reviews','apprentice_reviews'), #combine post types as we store them separately
			'post_type'		=> 'graduate_reviews',
			'post_status'	=> 'publish',
			'orderby'		=> 'post_date',
			'order'			=> 'DESC',		
			'posts_per_page'=> '20',
			'fields'		=> 'ids',
			'paged' 		=> $paged
		);				
		$wp_query = new WP_Query($args);

		if ( $wp_query->have_posts() ) :	

			?>

			<div class="row medium-up-2 reviews-items">

			<?php while ( have_posts() ) : the_post(); 

				$post_id = get_the_ID();	
				//echo $post_id;
					$post_type = get_post_type( get_the_ID() );	

					if ($post_type == 'graduate_reviews'):
						$employer = get_field('graduate_review_employer', $post_id);
						$ind = get_field('graduate_review_industry', $post_id);
					else:
						$employer = get_field('apprentice_review_employer', $post_id);
						$ind = get_field('apprentice_review_industry', $post_id);
					endif;

					$blogusers = get_users(array('meta_key' => 'company_name', 'meta_value' => $employer));
					foreach ( $blogusers as $user ) {
						#access the user meta
						#echo '<span>' . esc_html( $user->ID ) . '</span>'; #debug
					}	

					#we can now get the company rating as it's stored against the user
					$overall_rating = get_field('overall_rating', 'user_'.$user->ID);
					$overall_rating_new = get_field('star_rating');

					$overall_rating_percent = $overall_rating_new * 20;

					#get the location (only applicable to enhnaced user profiles)
					$locations = get_field('locations', 'user_'.$user->ID);				

					#get the main_address
					$main_address = get_field('main_address', 'user_'.$user->ID);

					#use geocoding to get the necessary details
					$geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?sensor=false&address=' . urlencode($main_address) . '&key=AIzaSyAj_Xc1gEDKeDqLIMFHgok9hhWgyf16TK0');
					$output = json_decode($geocode);	

					#get the address components
					$address_data = $output->results[0]->address_components;
					$street = str_replace('Dr', 'Drive', $address_data[1]->long_name);
					$town = $address_data[2]->long_name;

					?>


				<div id="<?php the_ID(); ?>" class="column column-block review <?php echo $selected; ?>">
					<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">

					<span class="employer-title"><?php echo $employer; ?></span>

					<div class="stars">
							<div class="stars-gold" style="width: <?php echo $overall_rating_percent; ?>%;"> &nbsp; </div>
							<div class="stars-white"> &nbsp; </div>
					</div>

					<div class="review-bottom">
						
						<?php if($ind !== "- Select Industry -"): ?><span class="industry">Industry: <strong><?php echo $ind; ?></strong></span><?php endif; ?>
						<?php if($town): ?><span class="industry">Town: <strong><?php echo $town; ?></strong></span><?php endif; ?>
						<span class="entry-date">Review date: <?php echo get_the_date(); ?></span>

						<?php
						# Check the user role and they're logged in
						#if (is_user_logged_in() && $current_user->ID !='' && in_array("student", $user_roles)) :
						if (is_user_logged_in() && $current_user->ID !='' ) :

							$review_id = get_the_ID();
							global $wpdb;

							$stored_reviews = $wpdb->get_results(
								$wpdb->prepare( 
									"
										SELECT meta_value
										FROM $wpdb->usermeta
										WHERE meta_key LIKE %s
										AND meta_value = %s
									",
									'stored_reviews_%_review_id',
									$review_id
								)
							);
							#echo $wpdb->last_query;

							foreach ( $stored_reviews as $review ):
							   $stored_review_id = $review->meta_value;
							endforeach;
							?>


							<?php if ($stored_review_id != $review_id): ?>
								<a href="#" rid="<?php the_ID(); ?>" uid="<?php echo $current_user->ID; ?>" title="<?php the_title(); ?>" class="add-review">Add</a>		
							<?php else:
							$nonce = wp_create_nonce( 'prg-delete-review-nonce' );
							?>
								<i class='fa fa-check' aria-hidden='true'></i>	

								<a href="<?php echo get_stylesheet_directory_uri(); ?>/includes/delete-review-frontend.php?_wpnonce=<?php echo $nonce; ?>&review_id=<?php echo $review_id; ?>" title="Delete Stored Review <?php echo $review_title; ?>"><i class="fa fa-minus-circle" aria-hidden="true"></i></a>		
							<?php endif; ?>				

						<?php endif; ?>		

						</div>
					</a>

				</div>

			<?php endwhile; #wp_reset_query(); ?>

			</div>

		<?php endif;

		/* Display navigation to next/previous pages when applicable */ 
		if ( function_exists( 'foundationpress_pagination' ) ) :
			foundationpress_pagination();
		elseif ( is_paged() ) :
		?>
			<nav id="post-nav">
				<div class="post-previous"><?php next_posts_link( __( '&larr; Older posts', 'foundationpress' ) ); ?></div>
				<div class="post-next"><?php previous_posts_link( __( 'Newer posts &rarr;', 'foundationpress' ) ); ?></div>
			</nav>
		<?php endif;?>
		</div><!-- /graduates -->


		<div class="tabs-panel" id="apprentices">	
		<?php										
		$paged = get_query_var('paged') ? get_query_var('paged') : 1;
		$args = array(
			#'post_type'		=> array('graduate_reviews','apprentice_reviews'), #combine post types as we store them separately
			'post_type'		=> 'apprentice_reviews',
			'post_status'	=> 'publish',
			'orderby'		=> 'post_date',
			'order'			=> 'DESC',		
			'posts_per_page'=> '20',
			'fields'		=> 'ids',
			'paged' 		=> $paged
		);				
		$wp_query = new WP_Query($args);

		if ( $wp_query->have_posts() ) :	

			?>

			<div class="row medium-up-2 reviews-items">

			<?php while ( have_posts() ) : the_post(); 

				$post_id = get_the_ID();	
				//echo $post_id;
					$post_type = get_post_type( get_the_ID() );	

					if ($post_type == 'graduate_reviews'):
						$employer = get_field('graduate_review_employer', $post_id);
						$ind = get_field('graduate_review_industry', $post_id);
					else:
						$employer = get_field('apprentice_review_employer', $post_id);
						$ind = get_field('apprentice_review_industry', $post_id);
					endif;

					$blogusers = get_users(array('meta_key' => 'company_name', 'meta_value' => $employer));
					foreach ( $blogusers as $user ) {
						#access the user meta
						#echo '<span>' . esc_html( $user->ID ) . '</span>'; #debug
					}	

					#we can now get the company rating as it's stored against the user
					$overall_rating = get_field('overall_rating', 'user_'.$user->ID);
					$overall_rating_new = get_field('star_rating');

					$overall_rating_percent = $overall_rating_new * 20;

					#get the location (only applicable to enhnaced user profiles)
					$locations = get_field('locations', 'user_'.$user->ID);				

					#get the main_address
					$main_address = get_field('main_address', 'user_'.$user->ID);

					#use geocoding to get the necessary details
					$geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?sensor=false&address=' . urlencode($main_address) . '&key=AIzaSyAj_Xc1gEDKeDqLIMFHgok9hhWgyf16TK0');
					$output = json_decode($geocode);	

					#get the address components
					$address_data = $output->results[0]->address_components;
					$street = str_replace('Dr', 'Drive', $address_data[1]->long_name);
					$town = $address_data[2]->long_name;

					?>

				
				<div id="<?php the_ID(); ?>" class="column column-block review <?php echo $selected; ?>">
					<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
					
					<span class="employer-title"><?php echo $employer; ?></span>

					<div class="stars">
							<div class="stars-gold" style="width: <?php echo $overall_rating_percent; ?>%;"> &nbsp; </div>
							<div class="stars-white"> &nbsp; </div>
					</div>

					<div class="review-bottom">

						<?php if($ind !== "- Select Industry -"): ?><span class="industry">Industry: <strong><?php echo $ind; ?></strong></span><?php endif; ?>
						<?php if($town): ?><span class="industry">Town: <strong><?php echo $town; ?></strong></span><?php endif; ?>
						<span class="entry-date">Review date: <?php echo get_the_date(); ?></span>

						<?php
						# Check the user role and they're logged in
						#if (is_user_logged_in() && $current_user->ID !='' && in_array("student", $user_roles)) :
						if (is_user_logged_in() && $current_user->ID !='' ) :

							$review_id = get_the_ID();
							global $wpdb;

							$stored_reviews = $wpdb->get_results(
								$wpdb->prepare( 
									"
										SELECT meta_value
										FROM $wpdb->usermeta
										WHERE meta_key LIKE %s
										AND meta_value = %s
									",
									'stored_reviews_%_review_id',
									$review_id
								)
							);
							#echo $wpdb->last_query;

							foreach ( $stored_reviews as $review ):
							   $stored_review_id = $review->meta_value;
							endforeach;
							?>


							<?php if ($stored_review_id != $review_id): ?>
								<a href="#" rid="<?php the_ID(); ?>" uid="<?php echo $current_user->ID; ?>" title="<?php the_title(); ?>" class="add-review">Add</a>		
							<?php else:
							$nonce = wp_create_nonce( 'prg-delete-review-nonce' );
							?>
								<i class='fa fa-check' aria-hidden='true'></i>	

								<a href="<?php echo get_stylesheet_directory_uri(); ?>/includes/delete-review-frontend.php?_wpnonce=<?php echo $nonce; ?>&review_id=<?php echo $review_id; ?>" title="Delete Stored Review <?php echo $review_title; ?>"><i class="fa fa-minus-circle" aria-hidden="true"></i></a>		
							<?php endif; ?>				

						<?php endif; ?>		

						</div><!-- review-bottom-->
					</a>
				</div>
				

			<?php endwhile; #wp_reset_query(); ?>

			</div>

		<?php endif;

		/* Display navigation to next/previous pages when applicable */ 
		if ( function_exists( 'foundationpress_pagination' ) ) :
			foundationpress_pagination();
		elseif ( is_paged() ) :
		?>
			<nav id="post-nav">
				<div class="post-previous"><?php next_posts_link( __( '&larr; Older posts', 'foundationpress' ) ); ?></div>
				<div class="post-next"><?php previous_posts_link( __( 'Newer posts &rarr;', 'foundationpress' ) ); ?></div>
			</nav>
		<?php endif;?>
		</div>
		
		
		<div class="tabs-panel" id="companies">	
		<?php
		#https://wordpress.stackexchange.com/questions/57427/how-to-display-pagination-links-for-wp-user-query	
		$args = array(
			'role__in'     	=> array('customer', 'subscriber'),
			'role__not_in' 	=> array('administrator'),
			'exclude'      	=> array(1),
			'orderby' 		=> 'meta_value',
			'meta_key'		=> 'company_name',
			'order' 		=> 'ASC',
			'meta_query' =>
			array(

				array(
					'relation' => 'AND', # OR						

					array(
						'key' => 'activate_membership',
						'value' => 'Yes',
						'compare' => "=",
					)

				)
			)			
			
			#'meta_key'     => 'activate_membership',
			#'meta_value'   => 'Yes',
			#'meta_compare' => '='	
		);
		$users = get_users($args);	

		if ($users):
			
			echo '<div class="row medium-up-2 reviews-items">';

			foreach ( $users as $user ) :

				$company_name = get_field('company_name','user_'.$user->ID);
				$hyphenate_company_name = str_replace(" ", "-", $company_name);
				if ($company_name != ''): ?>
				
				<div id="<?php echo $user->ID; ?>" class="column column-block review <?php echo $selected; ?>">
					<a href="<?php echo esc_url( home_url( '/' ) ).'companies/details/?company='.$hyphenate_company_name.'&cid='.$user->ID; ?>" title="<?php echo $company_name; ?>">
					<span class="employer-title"><?php echo $company_name; ?></span>
					<?php $overall_rating = get_field('overall_rating','user_'.$user->ID);
					if ($overall_rating):  
					$overall_rating_percent = $overall_rating * 20;
					?>
					<div class="stars">
							<div class="stars-gold" style="width: <?php echo $overall_rating_percent; ?>%;"> &nbsp; </div>
							<div class="stars-white"> &nbsp; </div>
					</div>
					<?php endif; ?>			
			
					<?php
					#get the main_address
					$industry = get_field('employer_registration_industry', 'user_'.$user->ID);	
					?>
				
					<div class="review-bottom">

						<?php if($industry !== "- Select Industry -"): ?><span class="industry">Industry: <strong><?php echo implode(', ', $industry); ?></strong></span><?php endif; ?>
						<span class="entry-date">Review date: <?php echo get_the_date(); ?></span>
						</div>

					</a>					
				</div>
					
				<?php endif;

			endforeach;

			echo '</div>';

		endif;
		?>		
		</div><!-- /companies -->	
		

	</div><!-- /panes -->
				
</div><!-- /review-container -->

<?php endif; #endif $submit ?>
			
	</div><!--main-content-main -->


	<div class="sidebar-left" data-equalizer-watch="main-content222">


			<div class="sidebar--header" data-equalizer-watch="main-content-headers">
				<h3>Find reviews</h3> <svg class="icon icon-details"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-details"></use></svg>
			</div>


			<form id="job-reviews-by-company" class="padded" action="<?php bloginfo('url');?>/reviews/" method="post">

				<?php
				# we need to get a list of all industries from the options page

				# reset choices
				$field['industries'] = array();

				# get the textarea value from options page without any formatting
				$industries = get_field('industry', 'option', false);

				# explode the value so that each line is a new array piece
				$industries = explode("\n", $industries);

				# remove any unwanted white space
				$industries = array_map('trim', $industries);

				# loop through array and add to field 'choices'
				if( is_array($industries) ) :
					foreach( $industries as $industry ) :
						if($industry === '- Select Industry -') continue; #skip first item    	
						#$industry2 = str_replace("&","&amp;",$industry);		
						$field['industries'][ $industry ] = '<li><label class="container"><input type="checkbox" name="industries[]" value="'.$industry.'"><span class="checkmark"></span> '.$industry.'</label></li>';

					endforeach;
				endif;

				# convert array into comma-separated string
				$industries = implode('', $field['industries'] );
					echo '<h2>Reviews by Industry </h2>';
					echo '<div class="industries-wrapper show-all-industries">';
					echo '<ul>'.$industries.'</ul>';
					echo '</div>';
					//echo '<span class="industries-more">More <svg class="icon icon-downarrow-small"><use xlink:href="'. get_stylesheet_directory_uri() .'/img/icons.svg#icon-downarrow-small"></use></svg></span>';
					echo '<span class="industries-more">More <img src="'. get_stylesheet_directory_uri() .'/img/icon-down-arrow.png" class="more-icon-down-arrow icon-rotated"/></span>';
				?>

				 <hr>

			<label for="towns"><h2>Reviews by Town</h2></label>
			<input id="towns" name="towns" value="" placeholder="E.g. Brighton">

				 <hr>

			<h2>Reviews by Rating</h2>
			<ul class="reviews-by-rating">
			<li>
				<label class="radio-container container">
				<input type="radio" name="ratings" value="0"> 
						<span class="checkmark"></span>
						<div class="stars">
							<div class="stars-gold" style="width: 0%;"> &nbsp; </div>
							<div class="stars-white"> &nbsp; </div>
						</div>
				</label>
			</li>

			<li>
				<label class="radio-container container">
				<input type="radio" name="ratings" value="1"> 
						<span class="checkmark"></span>
						<div class="stars">
							<div class="stars-gold" style="width: 20%;"> &nbsp; </div>
							<div class="stars-white"> &nbsp; </div>
						</div>
				</label>

			</li>

			<li>
				<label class="radio-container container">
				<input type="radio" name="ratings" value="2"> 
						<span class="checkmark"></span>
						<div class="stars">
							<div class="stars-gold" style="width: 40%;"> &nbsp; </div>
							<div class="stars-white"> &nbsp; </div>
						</div>
				</label>

			</li>

			<li>
				<label class="radio-container container">
				<input type="radio" name="ratings" value="3"> 
						<span class="checkmark"></span>
						<div class="stars">
							<div class="stars-gold" style="width: 60%;"> &nbsp; </div>
							<div class="stars-white"> &nbsp; </div>
						</div>
				</label>

			</li>

			<li>
				<label class="radio-container container">
				<input type="radio" name="ratings" value="4"> 
						<span class="checkmark"></span>
						<div class="stars">
							<div class="stars-gold" style="width: 80%;"> &nbsp; </div>
							<div class="stars-white"> &nbsp; </div>
						</div>
				</label>

			</li>

			<li>
				<label class="radio-container container">
				<input type="radio" name="ratings" value="5"> 
						<span class="checkmark"></span>
						<div class="stars">
							<div class="stars-gold" style="width: 100%;"> &nbsp; </div>
							<div class="stars-white"> &nbsp; </div>
						</div>
				</label>

			</li>

			</ul> 

			<br/>
			<input value="Search Reviews" type="submit" name="submit" class="filter-reviews-button button blue block" /> 

			</form> 

	</div><!--sidebar-left -->


</div> <!-- main-content -->

 <?php get_footer();
