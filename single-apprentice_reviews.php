<?php get_header(); ?>



<div class="page-header row">

	<div class="medium-5 columns">
		<h1><?php the_title(); ?></h1>
	</div>
	

	<?php get_template_part('template-parts/page-header-search'); ?>

</div><!-- page-header -->


<div class="main-content no-right-column" data-equalizer="main-content">






	<div class="main-content-main" data-equalizer-watch="main-content">

				<div class="main-content-main--breadcrumbs" data-equalizer-watch="main-content-headers">
							<?php
							if ( function_exists('yoast_breadcrumb') ) {
							yoast_breadcrumb('
							<p id="breadcrumbs">','</p>
							');
							}
							?>
							
					<div class="save-share">

						<!--<a href="#" class="savethis">
							<svg class="icon icon-heart"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-heart"></use></svg> Save this
						</a>-->

						<?php get_template_part('template-parts/share-button'); ?>

					</div>

				</div><!-- main-content-main-breadcrumbs -->


<div class="row padded">
		
		<div class="medium-8 columns">


		
<?php
$apprentice_review_what_are_the_best_and_worst_things_about_your_and_job_best = get_field( "apprentice_review_what_are_the_best_and_worst_things_about_your_and_job_best" ); 

$apprentice_review_what_are_the_best_and_worst_things_about_your_and_job_worst = get_field( "apprentice_review_what_are_the_best_and_worst_things_about_your_and_job_worst" ); 

if( $apprentice_review_what_are_the_best_and_worst_things_about_your_and_job_best || $apprentice_review_what_are_the_best_and_worst_things_about_your_and_job_worst  ) :
	echo '<p>What are the Best and Worst things about your job?</p>';
endif; 

if( $apprentice_review_what_are_the_best_and_worst_things_about_your_and_job_best ) :
	echo '<p>Best</p>';
	echo $apprentice_review_what_are_the_best_and_worst_things_about_your_and_job_best;
endif;

if( $apprentice_review_what_are_the_best_and_worst_things_about_your_and_job_worst ) :
	echo '<p>Worst</p>';
	echo $apprentice_review_what_are_the_best_and_worst_things_about_your_and_job_worst;
endif;

$apprentice_review_starting_salary = get_field( "apprentice_review_starting_salary" ); 

$apprentice_review_current_salary = get_field( "apprentice_review_current_salary" ); 

if( $apprentice_review_starting_salary || $apprentice_review_current_salary ) :
	echo '<p>What is the annual salary for this role?</p>';
endif; 

if( $apprentice_review_starting_salary ) :
	echo '<p>Starting salary: '.$apprentice_review_starting_salary.'</p>';
endif;

if( $apprentice_review_current_salary ) :
	echo '<p>Current  salary: '.$apprentice_review_current_salary.'</p>';
endif;



$apprentice_review_what_time_do_you_normally_start_and_finish_work_start_time = get_field( "apprentice_review_what_time_do_you_normally_start_and_finish_work_start_time" ); 

$apprentice_review_what_time_do_you_normally_start_and_finish_work_finish_time = get_field( "apprentice_review_what_time_do_you_normally_start_and_finish_work_finish_time" ); 

if( $apprentice_review_what_time_do_you_normally_start_and_finish_work_start_time && $apprentice_review_what_time_do_you_normally_start_and_finish_work_finish_time ) :
	echo '<p>What hours do you actually work, on average?</p>';
endif;

if( $apprentice_review_what_time_do_you_normally_start_and_finish_work_start_time ) :
	echo '<p>Start: '.$apprentice_review_what_time_do_you_normally_start_and_finish_work_start_time.'</p>';
endif;


if( $apprentice_review_what_time_do_you_normally_start_and_finish_work_finish_time ) :
	echo '<p>Finish: '.$apprentice_review_what_time_do_you_normally_start_and_finish_work_finish_time.'</p>';
endif;



$apprentice_review_application_advice = get_field( "apprentice_review_application_advice" ); 
if( $apprentice_review_application_advice ) :
	echo '<p>What advice would you give to someone applying to this role?</p>';
	echo '<p>'.$apprentice_review_application_advice.'</p>';
endif;



$apprentice_review_interview_advice = get_field('apprentice_review_interview_advice');
if( $apprentice_review_interview_advice ) :
echo '<p><strong>Do you have any interview tips?</strong></p>';
echo $apprentice_review_interview_advice;
endif; 


?>

	</div>

			<div class="medium-4 columns star-ratings-container">

						<?php 
						$responsibility = get_field('responsibility');
						$responsibility_percent = $responsibility * 20;

						if ($responsibility_percent != 0): ?>
							<div class="rating">
							<span>Corporate Responsibility:</span>

							<div class="stars" title="<?php echo $responsibility; ?> out of 5">
								<div class="stars-gold" style="width: <?php echo $responsibility_percent; ?>%;"> &nbsp; </div>
								<div class="stars-white"> &nbsp; </div>
							</div>
							</div>
							
						<?php endif; ?>


						<?php 
						$work_life_balance = get_field('work_life_balance');
						$work_life_balance_percent = $work_life_balance * 20;

						if ($work_life_balance_percent != 0): ?>
							<div class="rating">
							<span>Work Life Balance:</span>

							<div class="stars" title="<?php echo $work_life_balance; ?> out of 5">
								<div class="stars-gold" style="width: <?php echo $work_life_balance_percent; ?>%;"> &nbsp; </div>
								<div class="stars-white"> &nbsp; </div>
							</div>
							</div>
							
						<?php endif; ?>





						<?php 
						$environmental_awareness = get_field('environmental_awareness');
						$environmental_awareness_percent = $environmental_awareness * 20;

						if ($environmental_awareness_percent != 0): ?>
							<div class="rating">
							<span>Environmental Awareness:</span>

							<div class="stars" title="<?php echo $environmental_awareness; ?> out of 5">
								<div class="stars-gold" style="width: <?php echo $environmental_awareness_percent; ?>%;"> &nbsp; </div>
								<div class="stars-white"> &nbsp; </div>
							</div>
							</div>
							
						<?php endif; ?>







						<?php 
						$compensation_benefits = get_field('compensation_benefits');
						$compensation_benefits_percent = $compensation_benefits * 20;

						if ($compensation_benefits_percent != 0): ?>
							<div class="rating">
							<span>Compensation &amp; Benefits:</span>

							<div class="stars" title="<?php echo $compensation_benefits; ?> out of 5">
								<div class="stars-gold" style="width: <?php echo $compensation_benefits_percent; ?>%;"> &nbsp; </div>
								<div class="stars-white"> &nbsp; </div>
							</div>
							</div>
							
						<?php endif; ?>









						<?php 
						$company_culture = get_field('company_culture');
						$company_culture_percent = $company_culture * 20;

						if ($company_culture_percent != 0): ?>
							<div class="rating">
							<span>Company Culture:</span>

							<div class="stars" title="<?php echo $company_culture; ?> out of 5">
								<div class="stars-gold" style="width: <?php echo $company_culture_percent; ?>%;"> &nbsp; </div>
								<div class="stars-white"> &nbsp; </div>
							</div>
							</div>
							
						<?php endif; ?>









						<?php 
						$career_progression = get_field('career_progression');
						$career_progression_percent = $career_progression * 20;

						if ($career_progression_percent != 0): ?>
							<div class="rating">
							<span>Career Progression:</span>

							<div class="stars" title="<?php echo $career_progression; ?> out of 5">
								<div class="stars-gold" style="width: <?php echo $career_progression_percent; ?>%;"> &nbsp; </div>
								<div class="stars-white"> &nbsp; </div>
							</div>
							</div>
							
						<?php endif; ?>









						<?php 
						$colleagues = get_field('colleagues');
						$colleagues_percent = $colleagues * 20;

						if ($colleagues_percent != 0): ?>
							<div class="rating">
							<span>Colleagues:</span>

							<div class="stars" title="<?php echo $colleagues; ?> out of 5">
								<div class="stars-gold" style="width: <?php echo $colleagues_percent; ?>%;"> &nbsp; </div>
								<div class="stars-white"> &nbsp; </div>
							</div>
							</div>
							
						<?php endif; ?>








						<?php 
						$training = get_field('training');
						$training_percent = $training * 20;

						if ($training_percent != 0): ?>
							<div class="rating">
							<span>Training:</span>

							<div class="stars" title="<?php echo $training; ?> out of 5">
								<div class="stars-gold" style="width: <?php echo $training_percent; ?>%;"> &nbsp; </div>
								<div class="stars-white"> &nbsp; </div>
							</div>
							</div>
							
						<?php endif; ?>







						<?php 
						$enjoyment = get_field('enjoyment');
						$enjoyment_percent = $enjoyment * 20;

						if ($enjoyment_percent != 0): ?>
							<div class="rating">
							<span>Enjoyment:</span>

							<div class="stars" title="<?php echo $enjoyment; ?> out of 5">
								<div class="stars-gold" style="width: <?php echo $enjoyment_percent; ?>%;"> &nbsp; </div>
								<div class="stars-white"> &nbsp; </div>
							</div>
							</div>
							
						<?php endif; ?>






						<?php 
						$overall_rating_new = get_field('star_rating');
						$overall_rating_percent = $overall_rating_new * 20;

						if ($overall_rating_new != 0): ?>
							<div class="rating">
							<span><strong>Overall Rating:</strong></span>

							<div class="stars" title="<?php echo $overall_rating_new; ?> out of 5">
								<div class="stars-gold" style="width: <?php echo $overall_rating_percent; ?>%;"> &nbsp; </div>
								<div class="stars-white"> &nbsp; </div>
							</div>
							</div>
							
						<?php endif; ?>
			<div id="msg"></div>			
			<?php
			# Check the user role and they're logged in
			#if (is_user_logged_in() && $current_user->ID !='' && in_array("student", $user_roles)) :
			if (is_user_logged_in() && $current_user->ID !='' ) :

				$review_id = get_the_ID();
				global $wpdb;

				$stored_reviews = $wpdb->get_results(
					$wpdb->prepare( 
						"
							SELECT meta_value
							FROM $wpdb->usermeta
							WHERE meta_key LIKE %s
							AND meta_value = %s
						",
						'stored_reviews_%_review_id',
						$review_id
					)
				);
				#echo $wpdb->last_query;

				foreach ( $stored_reviews as $review ):
				   $stored_review_id = $review->meta_value;
				endforeach;
			
				
				if ($stored_review_id != $review_id): ?>
					<a href="#" rid="<?php the_ID(); ?>" uid="<?php echo $current_user->ID; ?>" title="<?php the_title(); ?>" class="add-review">Store Review</a>		
				<?php else:
				$nonce = wp_create_nonce( 'prg-delete-review-nonce' );
				?>					
					<a href="<?php echo get_stylesheet_directory_uri(); ?>/includes/delete-review-frontend.php?_wpnonce=<?php echo $nonce; ?>&review_id=<?php echo $review_id; ?>" title="Delete Stored Review <?php echo $review_title; ?>">Remove Review</a>		
				<?php endif; ?>				
				

			<?php endif; ?>								
			</div>
		</div> <!-- row padded -->
<?php
// If comments are open or we have at least one comment, load up the comment template.
if ( comments_open() || get_comments_number() ) :
	comments_template();
endif;
?>	
	</div><!--main-content-main -->










	<div class="sidebar-left" data-equalizer-watch="main-content">


			<div class="sidebar--header" data-equalizer-watch="main-content-headers">
				<h3>Review Details</h3> <svg class="icon icon-details"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-details"></use></svg>
			</div>


				<?php $post_id = get_the_ID();	
			//echo $post_id;
				$post_type = get_post_type( get_the_ID() );	

				if ($post_type == 'graduate_reviews'):
					$employer = get_field('graduate_review_employer', $post_id);
					$ind = get_field('graduate_review_industry', $post_id);
				else:
					$employer = get_field('apprentice_review_employer', $post_id);
					$ind = get_field('apprentice_review_industry', $post_id);
				endif;

				$blogusers = get_users(array('meta_key' => 'company_name', 'meta_value' => $employer));
				foreach ( $blogusers as $user ) {
					#access the user meta
					#echo '<span>' . esc_html( $user->ID ) . '</span>'; #debug
					$company_name = get_field('company_name', 'user_'.$user->ID);
					$hyphenate_company_name = str_replace(" ", "-", $company_name); 					
				}	
				
				#we can now get the company rating as it's stored against the user
				$overall_rating = get_field('overall_rating', 'user_'.$user->ID);
				$overall_rating_new = get_field('star_rating');
				
				$overall_rating_percent = $overall_rating_new * 20;
				$logo = get_field('logo', 'user_'.$user->ID);
				$location = get_field('locations', 'user_'.$user->ID);
				?>
				
				<?php if($logo): ?>
							<div class="sidebar-left--company-logo">
									<?php if($hyphenate_company_name && $user->ID): ?>
										<a href="<?php echo esc_url( home_url( '/' ) ).'companies/details/?company='.$hyphenate_company_name.'&cid='.$user->ID; ?>">
									<?php endif; ?>
													<img src="<?php echo $logo['sizes']['medium']; ?>" alt="logo">
									<?php if($hyphenate_company_name && $user->ID): ?>
										</a>
									<?php endif; ?>
							</div>
				<?php endif; ?> 


			<div class="sidebar-left--company-details row">


				<?php $graduate_review_job_title = get_field('graduate_review_job_title'); if ($graduate_review_job_title): ?>
					<div class="small-6 columns">Job Title</div>
					<div class="small-6 columns"><?php echo $graduate_review_job_title; ?></div>
				<?php endif; ?>

				<?php if ($company_name): ?>
					<div class="small-6 columns">Company</div>
					<div class="small-6 columns">
					<?php if($hyphenate_company_name && $user->ID): ?>
							<a href="<?php echo esc_url( home_url( '/' ) ).'companies/details/?company='.$hyphenate_company_name.'&cid='.$user->ID; ?>">
						<?php endif; ?>
							<?php echo $company_name; ?>
						<?php if($hyphenate_company_name && $user->ID): ?>
							</a>
						<?php endif; ?>

					</div>
				<?php endif; ?>

				<?php if($location): ?>
				<div class="small-6 columns">Location</div>
				<div class="small-6 columns"><?php echo $location; ?></div>
				<?php endif; ?>
				
				<?php if( $graduate_review_current_salary ): ?>
					<div class="small-6 columns">Salary</div>
					<div class="small-6 columns"><?php echo $graduate_review_current_salary; ?></div>
				<?php endif; ?>


				<div class="small-6 columns">Date Posted</div>
				<div class="small-6 columns"><?php the_date('Y-m-d'); ?></div>
								

				<?php $graduate_review_industry = get_field('graduate_review_industry'); if ($graduate_review_industry): ?>
					<div class="small-6 columns">Industry</div>
					<div class="small-6 columns"><?php echo $graduate_review_industry; ?></div>
				<?php endif; ?>

				<!--<div class="small-6 columns">Job Type</div>
				<div class="small-6 columns">Graduate, Full Time</div>-->
				
			</div><!-- row -->

			<?php if( have_rows('gallery', 'user_'.$user->ID)): ?>

				<div class="sidebar-left--company-media row">

					<?php while( have_rows('gallery', 'user_'.$user->ID)): the_row(); 
					$image = get_sub_field('image');
					?>
						<?php if($image): ?><img src="<?php echo $image['sizes']['medium']; ?>" alt="image" /><br/><?php endif; ?>
					<?php endwhile; ?>

				</div><!-- sidebar-left-company-media -->

			<?php endif; ?>






			<div class="sidebar--header">
				<h3>Application Tips</h3> <svg class="icon icon-bulb"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-bulb"></use></svg>
			</div>

			<div class="sidebar-left--application-tips">

				<?php if( $apprentice_review_interview_advice ) : 
						echo $apprentice_review_interview_advice;
						?>

						<?php if($hyphenate_company_name && $user->ID): ?>
								<a href="<?php echo esc_url( home_url( '/' ) ).'companies/details/?company='.$hyphenate_company_name.'&cid='.$user->ID; ?>#advice" class="button">
									Read more interview advice for <?php echo $company_name; ?>
								</a>
						<?php endif; ?>


				<?php else: ?>

						<?php if($hyphenate_company_name && $user->ID): ?>
								<a href="<?php echo esc_url( home_url( '/' ) ).'companies/details/?company='.$hyphenate_company_name.'&cid='.$user->ID; ?>#advice" class="button">
									Read interview advice for <?php echo $company_name; ?>
								</a>
						<?php endif; ?>

				<?php endif; ?>
							

			</div> <!-- sidebar-left-key-information-->




	</div><!--sidebar-left -->










</div> <!-- main-content -->


<?php get_footer();
