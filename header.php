<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,900|Roboto+Slab:100,300,400,700" rel="stylesheet">

		<?php wp_head(); ?>
		
<script type="text/javascript">
<!--//--><![CDATA[//><!--
(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","http://www.thejobcrowd.com/sites/default/files/googleanalytics/analytics.js?p3su46","ga");ga("create", "UA-4178513-2", {"cookieDomain":"auto"});ga("send", "pageview");
//--><!]]>
</script>		
		
	</head>
	<body <?php body_class(); ?> data-count="<?php echo list_count(); ?>">

		<div class="off-canvas position-right" id="offCanvas" data-off-canvas>
		     	
		     	<ul class="off-canvas--nav">
					<li><a href="<?php bloginfo('home'); ?>/digital-editions">Digital Editions</a></li>
					<li><a href="<?php bloginfo('home'); ?>/about-us">About Us</a></li>
					<li><a href="<?php bloginfo('home'); ?>/blog">Blog</a></li>
					<!--<li><a href="<?php bloginfo('home'); ?>/employers-area-register/"><svg class="icon icon-crown"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-crown"></use></svg> Employers Area</a></li>-->
					
					<?php
					global $woocommerce;
					if ( sizeof( $woocommerce->cart->cart_contents) > 0 ) :
					?>
					<li><a href="<?php echo $woocommerce->cart->get_cart_url(); ?>">Basket</a></li>
					<?php endif; ?>
					
					<?php if ( is_user_logged_in() ): ?>
					<li class="off-canvas--nav--signup"><a href="<?php bloginfo('home'); ?>/my-account/customer-logout"><svg class="icon icon-signup"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-signup"></use></svg> Sign Out</a></li>
					<?php else: ?>
					<li class="off-canvas--nav--signup"><a href="<?php bloginfo('home'); ?>/my-account"><svg class="icon icon-signup"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-signup"></use></svg> Sign In</a></li>
					<?php endif; ?>					
					
					<li><a href="<?php bloginfo('home'); ?>/reviews">Job Reviews</a></li>
					<li><a href="<?php bloginfo('home'); ?>/companies/top-100-graduate-employers">Top Companies Ranking</a></li>
					<li class="off-canvas--nav--find-a-job"><a href="<?php bloginfo('home'); ?>/find-a-job">Find a Job</a></li>
					<li class="employers-link"><a href="<?php bloginfo('home'); ?>/employers/"><svg class="icon icon-crown"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-crown"></use></svg> Employer?</a></li>
					<li class="off-canvas--nav--review-your-job"><a data-open="reviewsModal"><svg class="icon icon-edit"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-edit"></use></svg> Review your Job</a></li>
				</ul>

		</div>

	<div class="off-canvas-content" data-off-canvas-content>

		<header class="site-header">

			<a href="<?php bloginfo('home'); ?>">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.png" alt="The Job Crowd" class="logo" />
			</a>

			<a href="#" class="site-header--menu-link" data-toggle="offCanvas">
				<svg class="icon icon-menu"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-menu"></use></svg> Menu
			</a>

			<ul class="site-header--top-nav">
				<li><a href="<?php bloginfo('home'); ?>/digital-editions">Digital Editions</a></li>
				<li><a href="<?php bloginfo('home'); ?>/about-us">About Us</a></li>
				<li><a href="<?php bloginfo('home'); ?>/blog">Blog</a></li>
				<!--<li class="employers-link"><a href="<?php bloginfo('home'); ?>/employers-area-register/"><svg class="icon icon-crown"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-crown"></use></svg> Employers Area</a></li>-->
				<?php
				global $woocommerce;
				if ( sizeof( $woocommerce->cart->cart_contents) > 0 ) :
					?>
					<li><a href="<?php echo $woocommerce->cart->get_cart_url(); ?>">Basket</a></li>
				<?php endif; ?>
					
				<?php if ( is_user_logged_in() ): ?>
					<li class="off-canvas--nav--signup"><a href="<?php bloginfo('home'); ?>/my-account/customer-logout"><svg class="icon icon-signup"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-signup"></use></svg> Sign Out</a></li>
				<?php else: ?>
					<li class="off-canvas--nav--signup"><a href="<?php bloginfo('home'); ?>/my-account"><svg class="icon icon-signup"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-signup"></use></svg> Sign In</a></li>
				<?php endif; ?>		
			</ul>

		</header>

		<?php if(get_field('image','option')): ?>
			<div class="header-advert">
				<?php if(get_field('link','option')): ?><a href="<?php the_field('link','option'); ?>" target="_blank"><?php endif; ?>
						<img src="<?php the_field('image','option'); ?>" alt="Advert" />
				<?php if(get_field('link','option')): ?></a><?php endif; ?>
			</div>
		<?php endif; ?>
		
		<div class="header-advert">		
			<ins data-revive-zoneid="75" data-revive-ct0="%c" data-revive-id="61e70eb4e6540d709b58af9c4c0f8891"></ins>
			<script async src="//adverts.lewisbusinessmedia.co.uk/www/delivery/asyncjs.php"></script>		
		</div>		
		
		<nav class="main-nav-container">

			<ul class="main-nav-container--main-nav">
				<li><a href="<?php bloginfo('home'); ?>/reviews/">Reviews</a></li>
				<li class="menu-item-has-child-pages"><a>Top Companies Ranking</a>
					<ul class="sub-menu">
						<li><a href="<?php bloginfo('home'); ?>/companies/top-100-graduate-employers">Top companies for Graduates to work for</a>
						<li><a href="<?php bloginfo('home'); ?>/companies/top-50-apprentice-employers">Top companies for Apprentices to work for</a>
					</ul>
				</li>
				<li class="main-nav--find-a-job"><a href="<?php bloginfo('home'); ?>/find-a-job">Find a Job</a></li>
				
				<li class="menu-item-has-child-pages"><a>Employer?</a>
					<ul class="sub-menu">
						<li><a href="<?php bloginfo('home'); ?>/employers/what-we-do/">What we do</a>
						<li><a href="<?php bloginfo('home'); ?>/employers/why-register">Why register</a>
						<li><a href="<?php bloginfo('home'); ?>/employers/">Register</a>
						<li><a href="<?php bloginfo('home'); ?>/employers/job-subscriptions/">Jobs</a>						
						<li><a href="<?php bloginfo('home'); ?>/employers/recruiter-solutions/">Recruiter Solutions</a>						
						<?php if ( is_user_logged_in() ): ?>
							<li><a href="<?php bloginfo('home'); ?>/my-account/customer-logout">Log Out</a>
						<?php else: ?>
							<li><a href="<?php bloginfo('home'); ?>/my-account">Log In</a>
						<?php endif; ?>													
					</ul>
				</li>				

				<li class="main-nav--review-your-job"><a data-open="reviewsModal"><svg class="icon icon-edit"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-edit"></use></svg> Review your Job</a></li>
			</ul>

			<div class="reveal text-center" id="reviewsModal" data-reveal>
			  <p class="lead">Would you like to do a Graduate review, or an Apprentice review?</p>

			  <a href="<?php bloginfo('home'); ?>/reviews/graduate-review" class="button">A Graduate review!</a>
			  <a href="<?php bloginfo('home'); ?>/reviews/apprentice-review" class="button">An Apprentice review!</a>
			  <br>
			  <br>

			  <a href="#" class="not-sure-which-toggle">Not sure which?</a>
			  
			  <p class="not-sure-which-review">If you're a recent graduate who has joined a company and would like to review that company, <a href="<?php bloginfo('home'); ?>/reviews/graduate-review">click here!</a>

			  Otherwise if you're an apprentice with a company and would like to write a review, <a href="<?php bloginfo('home'); ?>/reviews/apprentice-review">here's where you need to go</a>.</p>

			  <button class="close-button" data-close aria-label="Close modal" type="button">
			    <span aria-hidden="true">&times;</span>
			  </button>
			</div>

		</nav>

	<section class="container" data-equalizer="main-content-headers" data-equalize-on="medium">