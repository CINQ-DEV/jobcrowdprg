<?php get_header(); ?>                       

<div class="page-header row">

	<div class="medium-5 columns">
		 <h1 class="entry-title"><?php _e( 'Search Results for', 'foundationpress' ); ?> "<?php echo get_search_query(); ?>"</h1>		
		 <!--<span class="search-results-count"><?php //echo get_the_date('Y-m-d'); ?></span>-->
	</div>

	<?php get_template_part('template-parts/page-header-search'); ?>

</div><!-- page-header -->




<div class="main-content no-right-column" data-equalizer="main-content" data-equalize-on="medium">



		<div class="main-content-main" data-equalizer-watch="main-content">

					<div class="main-content-main--breadcrumbs" data-equalizer-watch="main-content-headers">
							
							<?php
							if ( function_exists('yoast_breadcrumb') ) {
							yoast_breadcrumb('
							<p id="breadcrumbs">','</p>
							');
							}
							?>

						<!--<div class="sort-results">
							<span>Relevance</span> &nbsp; | &nbsp; <a href="#">Newest First</a>
						</div>-->

					</div>

		<div class="entry-content padded">


			<article <?php post_class('main-content') ?> id="search-results">

			<?php
            if(isset($_GET['post_type'])) :
                $type = $_GET['post_type'];

                if($type == 'jobs') :
                    #echo "Post type is Jobs:";
                    include(TEMPLATEPATH."/template-parts/search-jobs.php");
                endif; 
            
                if($type == 'reviews') :
                    #echo "Post type is Reviews:";
                    include(TEMPLATEPATH."/template-parts/search-reviews.php");
                endif; 
           
           		if($type == 'post') :

	                #no post type set so show default results
					if ( have_posts() ) :	

						while ( have_posts() ) : the_post(); ?>
							<a href="<?php the_permalink(); ?>" class="normal-search">
								<?php the_title(); ?><br>
								<?php the_excerpt(); ?>
								<?php //get_template_part( 'layout' ); ?>
							</a>
						<?php endwhile;

						else:
							get_template_part( 'template-parts/content', 'none' );

					endif;

					if ( function_exists( 'foundationpress_pagination' ) ) :
						foundationpress_pagination();
					elseif ( is_paged() ) :
					?>

					<nav id="post-nav">
						<div class="post-previous"><?php next_posts_link( __( '&larr; Older posts', 'foundationpress' ) ); ?></div>
						<div class="post-next"><?php previous_posts_link( __( 'Newer posts &rarr;', 'foundationpress' ) ); ?></div>
					</nav>

					<?php endif; ?>

            <?php endif; endif; #end of isset
            ?>    

			</article>


		</div>


		</div><!-- main-content-main -->

<?php if(isset($_GET['post_type'])) : $type = $_GET['post_type']; endif; 
if($type == 'post'): 
?>

		<div class="sidebar-left" data-equalizer-watch="main-content">


			<div class="sidebar--header" data-equalizer-watch="main-content-headers">
				<h3>Blog Articles</h3>
			</div>

			<div class="padded blog-sidebar">
				
				<?php dynamic_sidebar( 'sidebar-widgets' ); ?>

				<h3>Archive</h3>
		        <?php $month= date('m', strtotime('-1 month')); #echo $month; ?>
		        
		        <ul>
			        
			        <?php #wp_get_archives('type=monthly&limit=6'); ?>
			        <?php wp_get_archives('type=monthly&limit='.$month); ?>
			        <?php wp_get_archives('type=yearly'); ?>            
		            
		        </ul>

			</div>


	</div><!--sidebar-left -->



<?php else: ?>

		<div class="sidebar-left" data-equalizer-watch="main-content">


			<div class="sidebar--header" data-equalizer-watch="main-content-headers">
				<h3>Refine your search</h3> <svg class="icon icon-refine"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-refine"></use></svg>
			</div>

		<form id="job-search" action="<?php bloginfo('url');?>/find-a-job/" method="post">

			<ul class="results-categories">
			<li>
					<ul class="results-categories--subcategories">

						<?php
						# we need to get a list of all industries from the options page

						# reset choices
						$field['industries'] = array();
						        
						# get the textarea value from options page without any formatting
						$industries = get_field('industry', 'option', false);
						    
						# explode the value so that each line is a new array piece
						$industries = explode("\n", $industries);
						    
						# remove any unwanted white space
						$industries = array_map('trim', $industries);
						    
						# loop through array and add to field 'choices'
						if( is_array($industries) ) :
						        
							foreach( $industries as $industry ) :
								if($industry === '- Select Industry -') continue; #skip first item    	
								#$industry = str_replace("&","&amp;",$industry);		
								$field['industries'][ $industry ] = '<li><input type="checkbox" name="industries[]" value="'.$industry.'"> '.$industry.'</li>';
						            
							endforeach;
						        
						endif;

						# convert array into comma-separated string
						$industries = implode('', $field['industries'] );

						echo '<p>Industry: <ul>'.$industries.'</ul></p>';
						?>
						</ul>
				</li>

		<input value="Search Jobs" type="submit" name="submit" class="filter-reviews-button button blue block" /> 

		</form> 

	</div><!--sidebar-left -->

<?php endif; ?>

</div> <!-- main-content -->



<?php get_footer();