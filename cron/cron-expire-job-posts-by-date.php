<?php
# Our include
require_once('../../../../wp-load.php');

$today = date('Ymd'); 				# matches the date stored in the DB
$args = array(
	'post_type' => array('jobs'), 	# array of the post types we want to check
	'posts_per_page' => 500,		# get all the posts but don't set it t o -1 since its a risk of overloading the server!!!
	'post_status' => 'publish',
	'orderby' => 'post_date',
	'order' => 'DESC',				# DESC / ASC
	'meta_query' => array(
		array(
			'key' => 'expiry_date',
			'value' => $today,
			'compare' => '=' 		# may have to switch this out for >
		)
	)
);
$the_query = new WP_Query( $args );
if ( $the_query->have_posts() ) {
	while ( $the_query->have_posts() ) {
			
		$the_query->the_post();
			
		# change the post status
		# wp_transition_post_status('draft', 'publish', $post); #couldn't get this working so opted for the below solution
		$postdata = array(
			'ID' => $post->ID,
			'post_status' => 'draft'
		);	
		wp_update_post($postdata);
			
		# notify admins
		$title = wp_strip_all_tags(get_the_title($post->ID ));
		#$url = get_permalink($post->ID );
		
		$url = home_url( '/' );	
		$edit_post = $url.'wp-admin/post.php?post='.$post->ID.'&action=edit';
		$view_post = $url.'?p='.$post->ID;			
			
		$to			= get_option( 'admin_email' );
		$subject	= "Job Expired By Custom Date: {$title}";
		$message	= "Link to expired job: <br/>";
		$message	.= "<a href=\"{$edit_post}\">{$title}</a>";
		$headers = array(
			'Content-Type: text/html; charset=UTF-8',
			'From: '.get_option( 'blogname' ).' <info@thejobcrowd.com>'
		);
			
		wp_mail ($to, $subject, $message, $headers);							
		
	}
wp_reset_postdata();
}
?>