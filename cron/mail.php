<?php
# Our include
require_once('../../../../wp-load.php');

#######################################
# PURELY USED TO TEST THE CRON
#######################################

date_default_timezone_set("Europe/London"); #need to specify this to ensure correct time
$date = date("d-m-Y");
$time = date("h:i:s");
$timestamp = $date.' '.$time;	

$to = 'ian@prgltd.co.uk';
$subject = 'Testing the function';
$body = 'The email body content. Sent: '.$timestamp;

wp_mail( $to, $subject, $body );
?>