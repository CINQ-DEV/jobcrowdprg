<?php
# Our include			
require_once('../../../../wp-load.php');

#need to specify this to ensure correct time
date_default_timezone_set("Europe/London"); 
			
#get todays date
$today = date("Y-m-d");
#echo 'Today: '.$today."<br />";			
			
#get our stored date
$previous_date = get_field('survey_reminder_date', 'option' );
#echo 'Previous: '.$previous_date."<br />";	

#calculate the date difference
$datetime1 = new DateTime($previous_date);
$datetime2 = new DateTime($today);
$interval = $datetime1->diff($datetime2);
#echo 'date difference: '.$interval->format('%R%a days').'<br>';

#if we're 7 days on from last time, we can send the reminders
$days = $interval->format('%a');			
if ($days == '7'):	

	#proceed and send our reminder emails now...
	echo '<p>Reminder emails now sending...</p>';

	function my_posts_where( $where ) {
		global $wpdb;
		$where = str_replace(
			"meta_key = 'recipients_%", 
			"meta_key LIKE 'recipients_%",
			$wpdb->remove_placeholder_escape($where)
		);
		return $where;
	}

	add_filter('posts_where', 'my_posts_where');	

	# lets get all surveys regardless of data quantity
	$args = array(
		'numberposts'	=> -1,
		'post_type'		=> 'surveys',
		'meta_key'		=> 'recipients_%_completed',
		#'meta_value'	=> '1',	#remove the limit in order to see ALL results
		#'meta_compare'	=> 'LIKE'	
	);

	# query
	$the_query = new WP_Query( $args );

	?>
	<?php if( $the_query->have_posts() ): ?>

		<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

			<?php
			$rows = get_field('recipients',$post->ID);
			$completed_counter = array();
			$partially_completed_counter = array();	
			$non_starters_counter = array();			
			if($rows):
				foreach($rows as $row):

					$numrows = count($rows);		
					$completed = $row['completed'];
					$partially_completed = $row['partially_completed'];	

					if ($completed):	
						$completed_counter[] = $post->ID;
					elseif ($partially_completed):	
						$partially_completed_counter[] = $post->ID;
					else: 
						$non_starters_counter[] = $post->ID;
					endif;		

				endforeach;
			endif;

			$numrows = 	count($rows);		
			$completed_surveys = count($completed_counter);
			$partially_surveys = count($partially_completed_counter);
			$none = ($completed_surveys+$partially_surveys)	;
			$non_starters = count(non_starters_counter);

			$post_author_id = get_post_field( 'post_author', $post->ID ); 	
			$company_name = get_field('company_name', 'user_'.$post_author_id);	
				
			$survey_name = get_the_title($post->ID);
			
			$user_info = get_userdata($post_author_id );
			$email_address = $user_info->user_email;

			#calculate the completed percentage of the list
			$reminder = ($completed_surveys / $numrows) * 100;	
			
			#check if the email reminder has been disabled by admin
			$disable_email_reminder = get_field('disable_email_reminder',$post->ID);
				
			if ($disable_email_reminder !='1'):
				$disable_email_reminder = 'No';
			else:
				$disable_email_reminder = 'Yes';
			endif;

			if ($reminder < 50): 
				$reminder_status = 'Yes';
				
				#add a check if the list has a stop value i.e. if checked, prevents email reminder being sent
				if ($disable_email_reminder != 'Yes' ):
				
					$login_url = get_site_url(null, '/my-account/', 'https');
					$format = 'd-m-Y';
					$created = get_the_date($format,$post->ID);
				
					$to			= get_option( 'admin_email' );
					#$to			= $email_address;
					$subject	= "The Job Crowd :: Survey Reminder :: {$survey_name}";
					$message	= "Dear {$company_name}, <br/><br/>";
					$message	.= "Your survey: <strong>{$survey_name}</strong>, created <strong>{$created}</strong>, is currently <strong>{$reminder}%</strong> complete.<br/><br/>";
					$message	.= "Please login and re-send your survey as a reminder to the remaining recipients on your distribution list.<br/><br/>";
					$message	.= "<a href='{$login_url}'>Login Now</a>.<br/><br/>";
					$message	.= "Kind Regards <br/>";
					$message	.= "&nbsp;<br/>";
					$message	.= "TheJobCrowd <br/>";
					$headers = array(
						'Content-Type: text/html; charset=UTF-8',
						'From: '.get_option( 'blogname' ).' <info@thejobcrowd.com>'
					);
					wp_mail ($to, $subject, $message, $headers);				
				
				endif;
				
			else:
				$reminder_status = 'No';
				update_post_meta( $post->ID, 'disable_email_reminder', '1');
			endif;				
			?>		

		<?php endwhile; ?>
	
	<?php endif; 

	wp_reset_query();	 // Restore global post data stomped by the_post(). 

	#update the options page with todays date, this prevents them being resent if someone lands on this page
	update_field( 'survey_reminder_date', $today, 'option' );

else:

	echo '<p>No reminder emails due.</p>';

endif; #endif $days == 7
?>