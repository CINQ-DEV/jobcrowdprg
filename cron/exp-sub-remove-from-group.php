<?php 
# Our include
require_once('../../../../wp-load.php');

#https://www.thejobcrowd.com/wp-content/themes/jobcrowdprg/cron/exp-sub-remove-from-group.php

	# get the employers from woocommerce, so target specific roles
	$args = array(
		'role__in'     	=> array('customer'),
		'role__not_in' 	=> array('administrator'),
		'exclude'      	=> array(1),
		'number' 		=> -1,
		'orderby' 		=> 'meta_value',
		'meta_key'		=> 'company_name',
		'order' 		=> 'ASC',
		'meta_query' =>
		array(
			array(
				'relation' => 'AND', # OR						
				array(
					'key' => 'activate_membership',
					'value' => 'Yes',
					'compare' => "=",
				)
			)
		)						
	);	

	$users = get_users($args);	
	


	if ($users){
		echo '<p>Total: '.count($users).'</p>';
		foreach ( $users as $user ) {
			

			$choices = get_user_meta( $user->ID, 'company_name', true );
			echo '<p>ID:'.$user->ID.' Company: '.$choices.'</p>';
			
			prg_remove_from_group( $user->ID );
			#prg_remove_from_group( 416 );
			
		}
	}
?>