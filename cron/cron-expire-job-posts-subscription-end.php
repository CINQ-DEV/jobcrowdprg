<?php
# Our include
require_once('../../../../wp-load.php');

# Get current date (this is an array)
$today = getdate();

# Get all customers subscriptions
$customer_subscriptions = get_posts( array(
	'numberposts'	=> -1,
	'meta_key'		=> '_customer_user', 
	#'meta_value'	=> get_current_user_id(),	# Or $user_id
	'post_type'		=> 'shop_subscription',		# WC orders post type
	'post_status'	=> 'wc-expired',			# Only orders with status "expired"
	'orderby'		=> 'post_date',
	'order' 		=> 'ASC',					# ACS or DESC
	'date_query' => array(
		array(
			'year'  => $today['year'],
			'month' => $today['mon'],
			'day'   => $today['mday'],
		),
	),			
) );
	
if ($customer_subscriptions){
	
	# Iterating through each post subscription object
	foreach( $customer_subscriptions as $customer_subscription ){
		
		# Getting an instance of the WC_Subscription object
		$subscription = new WC_Subscription($customer_subscription->ID);
		
		# Getting the related ORDER ID
		$order_id = $subscription->order->id;
		#echo '<p>Order #: '.$order_id.'</p>'; #debug
			
		# Get the job id from the ACF field
		$job_id = get_field( "job_subscription_order_id", $order_id );			
		#echo '<p>Job ID: '.$job_id.'</p><hr/>';	#debug
		
		# Check the order has a job ID otherwise don't continue
		if ($job_id) :	
			
			# Update the job status
			$postdata = array(
				'ID' => $job_id,
				'post_status' => 'draft'
			);	
			wp_update_post($postdata);		
				
			# notify admins
			$title = wp_strip_all_tags(get_the_title(job_id));
			#$url = get_permalink($job_id );
			
			$url = home_url( '/' );	
			$edit_post = $url.'wp-admin/post.php?post='.$job_id.'&action=edit';
			$view_post = $url.'?p='.$job_id;	
				
			$to			= get_option( 'admin_email' );
			$subject	= "Job Expired By Subscription End: {$title}";
			$message	= "Link to expired job: <br/>";
			$message	.= "<a href=\"{$edit_post}\">{$title}</a>";
			$headers = array(
				'Content-Type: text/html; charset=UTF-8',
				'From: '.get_option( 'blogname' ).' <info@thejobcrowd.com>'
			);
				
			wp_mail ($to, $subject, $message, $headers);	
		
		endif; #endif job id is not empty
		
	}
		
} #endif $customer_subscriptions				
?>