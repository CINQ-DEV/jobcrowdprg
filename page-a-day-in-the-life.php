<?php
/*
Template Name: A Day in the Life
 */

 get_header(); ?>


<div class="page-header row">

	<div class="medium-5 columns">
		<h1>A Day in the Life</h1>
	</div>
	
	<?php get_template_part('template-parts/page-header-search'); ?>


</div><!-- page-header -->


<div class="main-content" data-equalizer="main-content">






	<div class="main-content-main fullwidth">

				<div class="main-content-main--breadcrumbs">
							
							<?php
							if ( function_exists('yoast_breadcrumb') ) {
							yoast_breadcrumb('
							<p id="breadcrumbs">','</p>
							');
							}
							?>
							
					<div class="save-share">

						<!--<a href="#" class="savethis">
							<svg class="icon icon-heart"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-heart"></use></svg> Save this
						</a>-->

						<?php get_template_part('template-parts/share-button'); ?>

					</div>

				</div><!-- main-content-main-breadcrumbs -->


				<div class="blog-posts-container row small-up-1 medium-up-2 large-up-3" data-equalizer="blog-post">

		<?php // Display blog posts on any page @ https://m0n.co/l
		$temp = $wp_query; $wp_query= null;
		$wp_query = new WP_Query(); $wp_query->query('posts_per_page=12&post_type="day_in_life"' . '&paged='.$paged);
		if ( $wp_query->have_posts() ) :
		while ($wp_query->have_posts()) : $wp_query->the_post(); ?>

								<a href="<?php the_permalink(); ?>" class="column">

									<div class="blog-posts-container--post" style="background-image: url('<?php if(has_post_thumbnail()): the_post_thumbnail_url('large'); else: echo get_stylesheet_directory_uri().'/img/employees-bg.jpg'; endif; ?>');" data-equalizer-watch="blog-post">

										<div class="blog-posts-container--details">

											<h2><?php the_title(); ?></h2>

											<span><?php echo get_the_date( 'd-m-Y' ); ?></span>

										</div>


									</div>

								</a>

		<?php endwhile; ?>

		<?php if ($paged > 1) { ?>

		<nav id="nav-posts">
			<div class="prev"><?php next_posts_link('&laquo; Previous Posts'); ?></div>
			<div class="next"><?php previous_posts_link('Newer Posts &raquo;'); ?></div>
		</nav>

		<?php } else { ?>

		<nav id="nav-posts">
			<div class="prev"><?php next_posts_link('&laquo; Previous Posts'); ?></div>
		</nav>

		<?php } ?>

		<?php wp_reset_postdata(); ?>



					<?php
					if ( function_exists( 'foundationpress_pagination' ) ) :
						foundationpress_pagination();
					elseif ( is_paged() ) :
					?>
						<nav id="post-nav">
							<div class="post-previous"><?php next_posts_link( __( '&larr; Older posts', 'foundationpress' ) ); ?></div>
							<div class="post-next"><?php previous_posts_link( __( 'Newer posts &rarr;', 'foundationpress' ) ); ?></div>
						</nav>
					<?php endif; ?>
		
		<?php else: ?>
		<p>Unfortunately, there are currently no case studies available.</p>
		<?php endif; ?>
				</div><!-- blog-posts-container -->


			
	</div><!--main-content-main -->










</div> <!-- main-content -->

 <?php get_footer();
