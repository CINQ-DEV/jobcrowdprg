<?php
/*
Template Name: Salary
*/

get_header(); ?>

<div class="page-header row">

	<div class="medium-5 columns">
		<h1>Company Salaries</h1>
	</div>
	
	<?php get_template_part('template-parts/page-header-search'); ?>

</div><!-- page-header -->



<div class="main-content fullwidth" data-equalizer="main-content">

	<div class="main-content-main" data-equalizer-watch="main-content">

				<div class="main-content-main--breadcrumbs" data-equalizer-watch="main-content-headers">
							<?php
							if ( function_exists('yoast_breadcrumb') ) {
							yoast_breadcrumb('
							<p id="breadcrumbs">','</p>
							');
							}
							?>
					<div class="save-share">

						<!--<a href="#" class="savethis">
							<svg class="icon icon-heart"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-heart"></use></svg> Save this
						</a>-->

						<?php get_template_part('template-parts/share-button'); ?>

					</div><!--save-share-->

				</div><!-- main-content-main-breadcrumbs -->

				<div class="padded">
		
		<div class="row">

		<div class="medium-6 columns">

<?php
$args = array(
	'role__in'     	=> array('customer', 'subscriber'),
	'role__not_in' 	=> array('administrator'),
	'exclude'      	=> array(1),
	'orderby' 		=> 'user_nicename',
	'order' 		=> 'ASC',
		
	#'meta_key'     => 'activate_membership',
	#'meta_value'   => 'Yes',
	#'meta_compare' => '='	
);
$users = get_users($args);	
	
if ($users):
	echo '<p>Please use the below to filter the jobs by company:</p>'; 
	echo '<form method="post" action="'.site_url().'/company-salary-results/">';
	echo '<select name="company">';
	
	foreach ( $users as $user ) :

		$company_name = get_field('company_name','user_'.$user->ID);
		if ($company_name != ''):
			echo '<option value="'.$user->ID.'">'.$company_name.'</option>';
		endif;
	
	endforeach;
	
	echo '</select>';
	echo '<input type="submit" />';
	echo '</form>';
	
endif;
?>
</div>

<div class="medium-6 columns">

<?php
# we need to get a list of all careers from the options page
# reset choices
$field['careers'] = array();

# get the textarea value from options page without any formatting
$careers = get_field('careers', 'option', false);

# explode the value so that each line is a new array piece
$careers = explode("\n", $careers);

# remove any unwanted white space
$careers = array_map('trim', $careers);

# loop through array and add to field 'choices'
if( is_array($careers) ) :

	foreach( $careers as $career ) :

		if($career === '- Select Career -') continue; #skip first item   				
		$field['careers'][ $career ] = '<option value="'.$career.'">'.$career.'</option>';

	endforeach;

endif;

# convert array into comma-separated string
$careers = implode('', $field['careers'] );

echo '<p>Please use the below to filter the jobs by career sector:</p>';
echo '<form method="post" action="'.site_url().'/career-salary-results/">';

	echo '<select name="career" id="career">';
	echo $careers;
	echo '</select>';
	echo '<input type="submit" />';				
echo '</form>';
?>	
</div>
</div>

				</div> <!-- padded -->
			
	</div><!--main-content-main -->


	<?php /*<div class="sidebar-left" data-equalizer-watch="main-content222">


			<div class="sidebar--header" data-equalizer-watch="main-content-headers">
				<h3>Find reviews</h3> <svg class="icon icon-details"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-details"></use></svg>
			</div>


	</div><!--sidebar-left -->


	<div class="sidebar-right" data-equalizer-watch="main-content">

		<div class="sidebar--header" data-equalizer-watch="main-content-headers">
			<!--<h3>Your saved jobs</h3>-->
		</div>

		<div class="saved-jobs">

		<?php #get_template_part("sidebar-saved-jobs"); ?>

		</div>


	</div><!--sidebar-right -->

*/?>
</div> <!-- main-content -->

<?php get_footer();