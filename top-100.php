<?php
# Our include
require_once('../../../wp-load.php');
?>

<?php
#get all industries from the CMS

#reset choices
$field['choices'] = array();

#get the textarea value from options page without any formatting
$choices = get_field('industry', 'option', false);
    
#explode the value so that each line is a new array piece
$choices = explode("\n", $choices);
    
#remove any unwanted white space
$choices = array_map('trim', $choices);
    
#loop through array and add to field 'choices'
if( is_array($choices) ) :

	echo '<form action="'. htmlspecialchars($_SERVER["PHP_SELF"]) .'" method="post">';
	echo '<select name="industry">';
        
	foreach( $choices as $choice ) :

		#split each line on ' : '
		$parts = explode(' : ', $choice);

		#make sure we have 2 parts		
		if (count($parts) > 1) :
			$value = $parts[0];
			$label = $parts[1];
		else:
			#no separator found
			$value = $choice;
			$label = $choice;
		endif;
		
		#create our drop down options
		echo '<option value="'.$value.'">'.$label.'</option>';
            
	endforeach;

	echo '</select>';
	echo '<input type="submit" name="submit" value="Select"/>';
	echo '</form>';

endif;

if ( !empty($_POST["industry"]) && ($_POST["industry"] !='- Select Industry -') ):

	$industry = $_POST['industry'];
	echo '<p>You selected: '.$industry.'</p>';

	#create an initial loop to get all users (Employers) that match the industry
	$search_industries 	= array(
		'role__in' 		=> array('customer', 'subscriber'),
		'number' 		=> 100,
		'meta_key' 		=> 'employer_registration_industry',
		'meta_value' 	=> $industry,
		'meta_compare' 	=> 'LIKE'
	);

	$all_users = get_users( $search_industries );

	#create an array of users
	$matched_users = array();

	if ($all_users):

		foreach ( $all_users as $user ) :		

			#add all users from the search into an array
			$matched_users[] = $user->ID;

		endforeach;

	endif;

	#check if the array has values
	if (!empty($matched_users)) :

		$returned_users = array(
			'include' 	=> $matched_users,
			'number'	=> 100,
			'orderby' 	=> 'meta_value_num',
			'meta_key' 	=> 'overall_rating',
			'order' 	=> $order,
		);

		$users = get_users( $returned_users );

		if ($users):

			foreach ( $users as $user ) :

				$overall_rating = get_field( 'overall_rating', 'user_'.$user->ID );
				$industry = get_field( 'employer_registration_industry', 'user_'.$user->ID );

				echo 'Email: '.$user->user_email.'<br>';
				echo 'ID: '.$user->ID.'<br>';
				echo 'Rating: '.$overall_rating.'<br>';
				echo implode(', ', $industry); 
				echo '<hr>';

			endforeach;

		endif; #endif $users;

	else:

		echo '<p>No companies matched your criteria</p>';

	endif; #endif check array is empty

else:
	
	echo '<p>Please select an industry</p>';

endif; #endif $_POST
?>