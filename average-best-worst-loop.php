 <?php
# Our include
require_once('../../../wp-load.php');

$company_id = $_POST['uid'];
$career = $_POST['car'];
#echo '<h3>'.$company_id.'</h3>';
#echo '<h3>'.$clean_career.'</h3>';

$all_meta_for_user = get_user_meta( $company_id );
$company_name = $all_meta_for_user['company_name'][0];
if ($company_name):
	#echo '<h1>'.$company_name.'</h1>';
endif;	

$graduate_reviews = get_posts(array(
	'post_type'		=> 'graduate_reviews',
	'post_status'	=> 'publish',
	'posts_per_page'=> '-1',
			
	'meta_query'	=> array(
		'relation'		=> 'AND',
		array(
			'key' 	=> 'graduate_review_employer',
			'value' => $company_name,
			'compare' => '='
		),
		array(
			'key' 	=> 'graduate_review_industry',
			'value' => $career,
			'compare' => 'LIKE'
		)
	)			
			
));		

#create our array
#$grad_ids = array();
if ($graduate_reviews):

	foreach($graduate_reviews as $graduate) : 

		#pass the values to the array
		#$grad_ids[] = $graduate->ID;
		$graduate_review_what_are_the_best_and_worst_things_about_your_company_best = get_field('graduate_review_what_are_the_best_and_worst_things_about_your_company_best',$graduate->ID);
		$graduate_review_what_are_the_best_and_worst_things_about_your_company_worst = get_field('graduate_review_what_are_the_best_and_worst_things_about_your_company_worst',$graduate->ID);
		if ($graduate_review_what_are_the_best_and_worst_things_about_your_company_best):
			echo '<p>Best: '.$graduate_review_what_are_the_best_and_worst_things_about_your_company_best.'</p>';
		endif;
		if ($graduate_review_what_are_the_best_and_worst_things_about_your_company_worst):
			echo '<p>Worst: '.$graduate_review_what_are_the_best_and_worst_things_about_your_company_worst.'</p>';
		endif;		
		?>
        <a href="<?php echo get_the_permalink( $graduate->ID ); ?>" title="<?php echo get_the_title( $graduate->ID ); ?>"><?php echo get_the_title( $graduate->ID ); ?></a>
        <a href="<?php echo get_the_permalink( $graduate->ID ); ?>" title="<?php echo get_the_title( $graduate->ID ); ?>" class="button">View &raquo;</a>
		<?php

	endforeach;

else:

	$graduate_reviews = '0';

endif;
wp_reset_query();

$apprentice_reviews = get_posts(array(
	'post_type'		=> 'apprentice_reviews',
	'post_status'	=> 'publish',
	'posts_per_page'=> '-1',
			
	'meta_query'	=> array(
		'relation'		=> 'AND',
		array(
			'key' 	=> 'apprentice_review_employer',
			'value' => $company_name,
			'compare' => '='
		),
		array(
			'key' 	=> 'apprentice_review_industry',
			'value' => $career,
			'compare' => 'LIKE'
		)
	)			
			
));		

#create our array
#$app_ids = array();
if ($apprentice_reviews):

	foreach($apprentice_reviews as $apprentice) : 

		#pass the values to the array
		#$app_ids[] = $apprentice->ID;
		$apprentice_review_what_are_the_best_and_worst_things_about_your_company_best = get_field('apprentice_review_what_are_the_best_and_worst_things_about_your_company_best',$graduate->ID);
		$apprentice_review_what_are_the_best_and_worst_things_about_your_company_worst = get_field('graduate_review_what_are_the_best_and_worst_things_about_your_company_worst',$graduate->ID);
		if ($apprentice_review_what_are_the_best_and_worst_things_about_your_company_best):
			echo '<p>Best: '.$apprentice_review_what_are_the_best_and_worst_things_about_your_company_best.'</p>';
		endif;
		if ($apprentice_review_what_are_the_best_and_worst_things_about_your_company_worst):
			echo '<p>Worst: '.$apprentice_review_what_are_the_best_and_worst_things_about_your_company_worst.'</p>';
		endif;		
		?>		
		<a href="<?php echo get_the_permalink( $apprentice->ID ); ?>" title="<?php echo get_the_title( $apprentice->ID ); ?>"><?php echo get_the_title( $apprentice->ID ); ?></a>
        <a href="<?php echo get_the_permalink( $apprentice->ID ); ?>" title="<?php echo get_the_title( $apprentice->ID ); ?>" class="button">View &raquo;</a>
		<?php

	endforeach;

else:

	$apprentice_reviews = '0';

endif;
wp_reset_query();

if ( ($graduate_reviews == '0') && ($apprentice_reviews == '0') ):

	echo '<p>Unfortunately, there are no reviews for '.$company_name.' within '.$career.'</p>';

endif;
?>