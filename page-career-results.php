<?php
/*
Template Name: Career Results
*/

get_header(); ?>

<div class="page-header row">

	<div class="medium-5 columns">
		<h1>Career Salaries</h1>
	</div>
	
	<?php get_template_part('template-parts/page-header-search'); ?>

</div><!-- page-header -->



<div class="main-content fullwidth" data-equalizer="main-content">

	<div class="main-content-main" data-equalizer-watch="main-content">

				<div class="main-content-main--breadcrumbs" data-equalizer-watch="main-content-headers">
							<?php
							if ( function_exists('yoast_breadcrumb') ) {
							yoast_breadcrumb('
							<p id="breadcrumbs">','</p>
							');
							}
							?>
					<div class="save-share">

						<!--<a href="#" class="savethis">
							<svg class="icon icon-heart"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-heart"></use></svg> Save this
						</a>-->

						<?php get_template_part('template-parts/share-button'); ?>

					</div><!--save-share-->

				</div><!-- main-content-main-breadcrumbs -->

				<div class="padded">
		
<!-- Nav tabs -->
<ul class="tabs" data-tabs id="reviews-tabs">
	<li class="tabs-title is-active"><a href="#filters" aria-controls="filters" role="tab">Average Salary</a></li>
	<li class="tabs-title"><a href="#graduates" aria-controls="graduates" role="tab">Graduate Reviews</a></li>
	<li class="tabs-title"><a href="#apprentices" aria-controls="apprentices" role="tab">Apprentices Reviews</a></li>
</ul>

<!-- Tab panes -->
<div class="tabs-content" data-tabs-content="reviews-tabs">

	<div class="tabs-panel is-active" id="filters">
	
	<canvas id="bar-chart" width="800" height="300"></canvas>


	<?php			
	#check for form data	
	if ( !empty($_POST["career"]) ):	

		#get the form data
		$career = $_POST['career'];	
		
		echo '<p>Career: '.$career.'</p>';					

		#run the loop a second time otherwise pagination breaks the results
		#wp_reset_query();
		$reviews = get_posts(array(
			'post_type'		=> array('graduate_reviews','apprentice_reviews'),
			'post_status'	=> 'publish',
			'posts_per_page'=> '-1',
			
			'meta_query'	=> array(
				'relation'		=> 'OR',
				array(
					'key' 	=> 'graduate_review_industry',
					'value' => $career,
					'compare' => 'LIKE'
				),
				array(
					'key' 	=> 'apprentice_review_industry',
					'value' => $career,
					'compare' => 'LIKE'
				)
			)			
			
		));		

		#create our array
		$salary = array();
		foreach($reviews as $review) : 

			$starting_salary = get_field('graduate_review_starting_salary', $review->ID);	

			#pass the values to the array
			$salary[] = $starting_salary;		

		endforeach;
		wp_reset_query();
		
		/*
		echo '<pre>';
		print_r(array_unique($salary));
		echo '</pre>';

		echo '<pre>';
		print_r(array_count_values($salary));
		echo '</pre>';
		echo '<hr>';
		*/
		
			#debug to check the right no of jobs are shown
			$paged = get_query_var('paged') ? get_query_var('paged') : 1;
			$job_args = array(
				'post_type'		=> 'jobs',
				'post_status'	=> 'publish',
				'orderby'		=> 'post_date',
				'order'			=> 'DESC',		
				'posts_per_page'=> '-1',
				'paged' 		=> $paged,
				'meta_query'	=> array(
	
					array(
						'key'	  	=> 'dynamic_filled_careers',
						'value'	  	=> $career,
						'compare' 	=> 'LIKE',
					),

				),			
			);				
			$wp_query = new WP_Query($job_args);
			if ( $wp_query->have_posts() ) :
				while ( have_posts() ) : the_post(); 
				$post_id = get_the_ID()	;
				#echo $post_id.'<br>';
				endwhile; #wp_reset_query(); #removed as stops pagination
				#echo '<hr>';
			else:
				#echo '<p>No jobs available</p>';			
			endif;		

		#create an associative array
		$arr = array_count_values($salary);
		#create an array to base the total on
		$total = array();
		foreach ($arr as $key => $value) :
			#echo 'Key: '.$key.' Value: '.$value.'<br>';
			#echo 'Salary: '.$key.' No. of reviews with this salary: '.$value.'<br>';
			
			#split the salary so we can query a min and max	
			$salary = explode(" - ", $key);
			#echo $salary[0];
			#echo $salary[1];								
			
			#now list each job within the salary range
			$paged = get_query_var('paged') ? get_query_var('paged') : 1;
			$job_args = array(
				'post_type'		=> 'jobs',
				'post_status'	=> 'publish',
				'orderby'		=> 'post_date',
				'order'			=> 'DESC',		
				'posts_per_page'=> '-1',
				'paged' 		=> $paged,
				'meta_query'	=> array(
					'relation'		=> 'AND',
					array(
						'key'	  	=> 'dynamic_filled_careers',
						'value'	  	=> $career,
						'compare' 	=> '=',
					),
					array(
						'key'	  	=> 'salary',
						'value'	  	=> array($salary[0], $salary[1]),
						'compare' 	=> 'BETWEEN',
					),
				),			
			);				
			$wp_query = new WP_Query($job_args);
			if ( $wp_query->have_posts() ) :
				while ( have_posts() ) : the_post(); 
				$post_id = get_the_ID()	;
				$career = get_field('dynamic_filled_careers', $post_id);
				?>
					<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?> <?php if( $career ):  echo '/ '. implode(', ', $career); endif; ?></a><br/>
				<?php
				endwhile; wp_reset_query(); #removed as stops pagination
				echo '<hr/>';
			else:
				#echo '<p>No jobs available</p>';			
			endif;	
			
			#add to the array
			$total[] = $value;
			
		endforeach; #endforeach $arr

		echo '<p>Based on ' . array_sum($total) . ' reviews</p>';

		# we need to get a list of all employers
		$args = array(
			'role__in'     	=> array('customer', 'subscriber'),
			'role__not_in' 	=> array('administrator'),
			'exclude'      	=> array(1),
			'orderby' 		=> 'user_nicename',
			'order' 		=> 'ASC',

			#'meta_key'     => 'activate_membership',
			#'meta_value'   => 'Yes',
			#'meta_compare' => '='	
		);
		$users = get_users($args);	

		if ($users):
			
			echo '<p>Please use the below to filter the reviews by employer:</p>';
			echo '<form name="employers">';
			echo '<select name="employer" id="employer">';

			foreach ( $users as $user ) :

				$company_name = get_field('company_name','user_'.$user->ID);
				if ($company_name != ''):
					echo '<option value="'.$user->ID.'">'.$company_name.'</option>';
				endif;

			endforeach;

			echo '</select>';
			echo '</form>';

		endif;

	endif;
	?>	

	<!-- Ajax Review Results -->
	<div id="reviews_content">
	</div>
	<!-- /Ajax Review Results -->	

	<script>
	jQuery('select').on('change', function() {

		var uid = this.value;
		var car = '<?php echo $career; ?>';		
		console.log("Employer ID = "+uid);
		console.log("Career = " +car);

		jQuery.ajax({
			type        : "POST",
			data        : {uid: uid, car: car},
			dataType    : "html",
			url         : "<?php echo get_bloginfo('template_directory'); ?>/average-salary-reviews-loop.php",

			success     : function(data) {
				console.log(this.data);
				jQuery("#reviews_content").html(data);
				console.log("success!");

			},
			error       : function(xhr, status, error) {
				var err = eval("(" + xhr.responseText + ")");
				alert(err.Message);
			}
		});
	});
	</script>	
	







<?php $i = 0; foreach ($arr as $key => $value): if($key) { 
	if($value) {
		$i++;
	}
}
endforeach; 

if ($i > 0): ?>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.min.js"></script>

	<script>

	$("#bar-chart").show(); 

		// Bar chart
		Chart.defaults.global.defaultFontFamily='"Lato",sans-serif';
		Chart.defaults.global.defaultFontColor="#231F20";

		new Chart(document.getElementById("bar-chart"), {
		    type: 'bar',
		    data: {
		     // labels: ["1", "2", "3"],
		      labels: [<?php foreach ($arr as $key => $value): if($key) { echo '"'.$key.'",'; } endforeach; ?>],
		      datasets: [
		        {
		          label: "",
		          backgroundColor: [<?php foreach ($arr as $key => $value): if($key) { echo '"#00BDD6",'; } endforeach; ?>],
		          //data: [25,30, 45]
   		          data: [<?php foreach ($arr as $key => $value): if($key) { echo '"'.$value.'",'; } endforeach; ?>]
		        }
		      ]
		    },

		    options: {
		      legend: { display: false },
		      title: {
		        display: true,
		        fontSize: 16,
		        text: 'Salary ranges for <?php echo $career; ?> careers'
		      }
		    }
		});


	</script>

<?php else: ?>
		<script>
		$("#bar-chart").hide(); 
		</script>

<?php endif; ?>







	</div>
	
	<div class="tabs-panel" id="graduates">
	
	<?php	
	$graduate_args = array(
		'post_type'		=> 'graduate_reviews',
		'post_status'	=> 'publish',
		'orderby'		=> 'post_date',
		'order'			=> 'DESC',		
		'posts_per_page'=> '-1',
		'meta_query' => array(
			array(
				'key' 	=> 'graduate_review_industry',
				'value' => $career,
				'compare' => 'LIKE'
			)
		)			
	);				
	$wp_query = new WP_Query($graduate_args);

	if ( $wp_query->have_posts() ) :
			while ( have_posts() ) : the_post(); 
				$post_id = get_the_ID()	;
				?>
					<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a><br/>
				<?php
			endwhile; wp_reset_query(); #removed as stops pagination
	else:
		echo '<p>No reviews available</p>';			
	endif;
	?>		
	
	</div>
	
	<div class="tabs-panel" id="apprentices">
	
	<?php			
	$apprentice_args = array(
		'post_type'		=> 'apprentice_reviews',
		'post_status'	=> 'publish',
		'orderby'		=> 'post_date',
		'order'			=> 'DESC',		
		'posts_per_page'=> '-1',
		'meta_query' => array(
			array(
				'key' 	=> 'apprentice_review_industry',
				'value' => $career,
				'compare' => 'LIKE'
			)
		)			
	);				
	$wp_query = new WP_Query($apprentice_args);

	if ( $wp_query->have_posts() ) :
			while ( have_posts() ) : the_post(); 
				$post_id = get_the_ID()	;
				?>
					<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a><br/>
				<?php
			endwhile; wp_reset_query(); #removed as stops pagination
	else:
		echo '<p>No reviews available</p>';			
	endif;
	?>		
	
	</div>	
	
</div> <!-- /tab-panes -->


				


				</div> <!-- padded -->
			
	</div><!--main-content-main -->

<?php /*
	<div class="sidebar-left" data-equalizer-watch="main-content222">


			<div class="sidebar--header" data-equalizer-watch="main-content-headers">
				<h3>Find reviews</h3> <svg class="icon icon-details"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-details"></use></svg>
			</div>


	</div><!--sidebar-left -->


	<div class="sidebar-right" data-equalizer-watch="main-content">

		<div class="sidebar--header" data-equalizer-watch="main-content-headers">
			<!--<h3>Your saved jobs</h3>-->
		</div>

		<div class="saved-jobs">

		<?php #get_template_part("sidebar-saved-jobs"); ?>

		</div>


	</div><!--sidebar-right -->
*/?>

</div> <!-- main-content -->

<?php get_footer();