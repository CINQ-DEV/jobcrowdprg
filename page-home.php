<?php
/*
Template Name: Home
*/

get_header(); ?>


<div class="home-hero" role="main">

		<span class="home-hero--large-text">
			<?php if(get_field('homepage_slider_main_title')): ?>
				<?php the_field('homepage_slider_main_title'); ?>
			<?php else: ?>
				Research. Reviews. Recruitment.
			<?php endif; ?>
		</span>
		
		<span class="home-hero--medium-text">
			<?php if(get_field('homepage_slider_small_title')): ?>
				<?php the_field('homepage_slider_small_title'); ?>
			<?php else: ?>
				The essential hub for planning YOUR early career
			<?php endif; ?>
		</span>


		<form class="hero--search" role="search" method="get" id="search-jobs" action="<?php echo home_url( '/' ); ?>">


			<span class="hero--search--subject">
				<input type="text" class="input--search-subject" type="text" value="" name="s" id="s" />
				<label class="input--search-subject---label">
					Search by <strong>Company Name</strong>
				</label>
			</span>
			
			<input type="hidden" value="reviews" name="post_type" />

			<span class="hero--search--button">
				<input type="submit" class="input--search-button button" id="searchsubmit" value="Search"/>
			</span>

		</form>

<?php if( have_rows('homepage_slider_images') ): ?>

	<div class="home-hero--images">

		<?php while( have_rows('homepage_slider_images') ): the_row(); 
		$image = get_sub_field('image');
		?>

			<div class="home-hero--images--image">
				<img src="<?php echo $image['sizes']['large']; ?>" alt="The Job Crowd image" />
			</div>

		<?php endwhile; ?>

	</div>

<?php else: ?>

	<div class="home-hero--images">
			<div class="home-hero--images--image">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/carousel1.jpg" alt="image" />
			</div>
	</div>

<?php endif; ?>


</div><!--home-hero -->




<div class="row home-intro">

	<div class="medium-9 columns">

			<div class="home-intro-text">
			<?php if(get_field('intro_text')): the_field('intro_text'); endif; ?>
			</div>

	</div>

	<div class="medium-3 columns">

			<!--/*
			  *
			  * Revive Adserver Asynchronous JS Tag
			  * - Generated with Revive Adserver v4.0.0
			  * - Doubleclick/DFP
			  *
			*/-->
			<div class="padded">
			<ins data-revive-zoneid="17" data-revive-target="_blank" data-revive-ct0="%c" data-revive-id="61e70eb4e6540d709b58af9c4c0f8891"></ins>
			<script async src="//adverts.lewisbusinessmedia.co.uk/www/delivery/asyncjs.php"></script>
			</div>			

			<?php if( get_field('call_to_action_freeform_or_advert') == 'cta' ): ?>

				<?php if(get_field('call_to_action_link')): ?>
					<a href="<?php the_field('call_to_action_link'); ?>" class="home-top100-cta--link">
				<?php endif; ?>

					<div class="home-top100-cta">

						<span class="home-top100-cta--large"><?php the_field('call_to_action_large_text'); ?></span>
						<span class="home-top100-cta--medium"><?php the_field('call_to_action_medium_text'); ?></span>

						<span class="home-top100-cta--bottom">
							<?php the_field('call_to_action_bottom'); ?>
						 	<?php if(get_field('call_to_action_button_text')): ?>
						 		<span class="button pink"><?php the_field('call_to_action_button_text'); ?></span>
						 	<?php endif; ?>
						</span>

					</div><!-- home-top100-cta -->

				<?php if(get_field('call_to_action_link')): ?>
					</a>
				<?php endif; ?>

			<?php elseif( get_field('call_to_action_freeform_or_advert') == 'advert' ): ?>

				<?php if(get_field('advert_link')): ?>
					<a href="<?php the_field('advert_link'); ?>" target="_blank">
				<?php endif; ?>

					<?php 

					$image = get_field('advert_image');

					if( !empty($image) ): ?>

						<img src="<?php echo $image['sizes']['large']; ?>" class="home-advert" alt="<?php echo $image['alt']; ?>" />

					<?php endif; ?>


				<?php if(get_field('advert_link')): ?>
					</a>
				<?php endif; ?>

			<?php elseif( get_field('call_to_action_freeform_or_advert') == 'freeform' ): ?>

					<div class="padded">

						<?php the_field('freeform'); ?>	

					</div>

			<?php endif; ?>
		
	</div>

</div><!-- row -->






<section class="row fullwidth flush-columns home-jobs-container">

	<section class="medium-6 columns background-grey job-reviews">

		<div class="section-header">

			<div class="padded">
				
				<h2>Latest Job Reviews</h2>

				<a href="<?php bloginfo('home'); ?>/reviews/graduate-reviews" class="button white"><svg class="icon icon-edit"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-edit"></use></svg> Review your Job</a>
			</div>

		</div>

<?php 
	$args = array(
		'post_type'		=> array('graduate_reviews','apprentice_reviews'), #combine post types as we store them separately
		'post_status'	=> 'publish',
		'orderby'		=> 'post_date',
		'order'			=> 'DESC',
		'posts_per_page' => '3',
	);				
	$wp_query = new WP_Query($args);
	
	if ( $wp_query->have_posts() ) :	?>

		<div class="job-reviews-container">

	<?php while ( have_posts() ) : the_post(); 

			$post_id = get_the_ID();	
			//echo $post_id;
				$post_type = get_post_type( get_the_ID() );	

				if ($post_type == 'graduate_reviews'):
					$employer = get_field('graduate_review_employer', $post_id);
					$ind = get_field('graduate_review_industry', $post_id);
				else:
					$employer = get_field('apprentice_review_employer', $post_id);
					$ind = get_field('apprentice_review_industry', $post_id);
				endif;

				$blogusers = get_users(array('meta_key' => 'company_name', 'meta_value' => $employer));
				foreach ( $blogusers as $user ) {
					#access the user meta
					#echo '<span>' . esc_html( $user->ID ) . '</span>'; #debug
				}	
				
				#we can now get the company rating as it's stored against the user
				$overall_rating = get_field('overall_rating', 'user_'.$user->ID);
				$overall_rating_new = get_field('star_rating');
				
				$overall_rating_percent = $overall_rating_new * 20;
				$logo = get_field('logo', 'user_'.$user->ID);


				#get the location (only applicable to enhnaced user profiles)
				$locations = get_field('locations', 'user_'.$user->ID);				
				
				#get the main_address
				$main_address = get_field('main_address', 'user_'.$user->ID);
				
				#use geocoding to get the necessary details
				$geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?sensor=false&address=' . urlencode($main_address) . '&key=AIzaSyAj_Xc1gEDKeDqLIMFHgok9hhWgyf16TK0');
				$output = json_decode($geocode);	
				
				#get the address components
				$address_data = $output->results[0]->address_components;
				$street = str_replace('Dr', 'Drive', $address_data[1]->long_name);
				$town = $address_data[2]->long_name;

				?>

		<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">


			<div class="job-review">
					
					<div class="job-review--logo">
								<?php if($logo): ?>
									<img src="<?php echo $logo['sizes']['medium']; ?>" alt="logo">
								<?php else: echo $employer; endif; ?> 
					</div>
					
					<div class="job-review--middle">

						<span class="job-review--title"><?php echo $employer; ?></span>

						<?php if($ind !== "- Select Industry -"): ?><span class="industry">Industry: <strong><?php echo $ind; ?></strong></span><br><?php endif; ?>
						<?php if($town): ?><span class="industry">Town: <strong><?php echo $town; ?></strong></span><br><?php endif; ?>

						<div class="stars">
								<div class="stars-gold" style="width: <?php echo $overall_rating_percent; ?>%;"> &nbsp; </div>
								<div class="stars-white"> &nbsp; </div>
						</div>


					</div>

					<span class="job-review--button button pink">Read Review</span>

			</div><!--review -->

			</a>

		<?php endwhile; ?>

		</div><!-- job-reviews-container -->

	<?php endif; wp_reset_query(); ?>
		<a href="<?php bloginfo('home'); ?>/reviews" class="button pink button-footer">See all job reviews</a>


	</section> <!-- job reviews -->







	<section class="medium-6 columns background-lightest-grey job-vacancies">

		<div class="section-header">

			<div class="padded">

				<h2>Latest Job Vacancies</h2>

			</div>

		</div>
<?php  
$args = array (
	'post_type' 	=> 'jobs',
	'post_status' 	=> 'publish',
	'orderby' 		=> 'ID',			
	'order' 		=> 'desc',
	'posts_per_page' => '3',
);

$wp_query = new WP_Query( $args );
			
if ( have_posts() ) : ?>
	
	<div class="job-reviews-container">

	<?php while ( have_posts() ) : the_post(); ?>

	<?php # get the user ID by matching the reviewed company name with an actual company
	
	$post_id = get_the_ID();
	$employer = get_field('dynamic_filled_company', $post_id);


	$blogusers = get_users(array('meta_key' => 'company_name', 'meta_value' => $employer));
	foreach ( $blogusers as $user ) {
	#access the user meta
	#echo '<span>' . esc_html( $user->ID ) . '</span>'; #debug
	}	
	$author_id=$post->post_author; 
	#get the main_address
			$logo = get_field('logo', 'user_'.$user->ID);
			$company_name = get_field('company_name', 'user_'.$user->ID);
	?>

		<div class="job-reviews-container">

			<a href="<?php the_permalink(); ?>">
				<div class="job-review">

					<div class="job-review--logo">
								<?php if($logo): ?>
									<img src="<?php echo $logo['sizes']['medium']; ?>" alt="logo">
								<?php else: echo $company_name; endif; ?> 
					</div>

					<div class="job-review--middle">

						<span class="job-review--title"><?php the_title(); ?><br>
							<?php $jobtown = get_field('dynamic_filled_town');
									if($jobtown): ?>
										<?php if(implode(', ', $jobtown) != "- Select Town -"): ?>
											Location:   <strong><?php echo implode(', ', $jobtown); ?></strong><br/>
										<?php endif; 
									endif; ?>

									<?php if(get_field('salary')): ?>
										<strong><?php $salary = get_field('salary'); echo '£'.number_format($salary,2); ?></strong>
							<?php endif; ?>
						</span>
						
					</div>

					<span class="job-review--button button blue">Find out more</span>

				</div><!-- job-review -->
			</a>

		<?php endwhile; ?>			

		</div>

	<?php endif; ?>

		<a href="<?php bloginfo('home'); ?>/find-a-job" class="button dark-grey button-footer">See all job vacancies</a>

	</section><!-- job vacancies -->


</section><!-- row -->





 <?php get_footer();
