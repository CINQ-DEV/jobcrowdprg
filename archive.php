<?php get_header(); ?>


<div class="page-header row">

	<div class="medium-5 columns">
		<h1><?php the_title(); ?></h1>
		<span class="search-results-count"><?php echo get_the_date('Y-m-d'); ?></span>
	</div>

	<?php get_template_part('template-parts/page-header-search'); ?>

</div><!-- page-header -->


<div class="main-content no-right-column" data-equalizer="main-content">



		<div class="main-content-main" data-equalizer-watch="main-content">

					<div class="main-content-main--breadcrumbs" data-equalizer-watch="main-content-headers">
							
							<?php
							if ( function_exists('yoast_breadcrumb') ) {
							yoast_breadcrumb('
							<p id="breadcrumbs">','</p>
							');
							}
							?>

						<!--<div class="sort-results">
							<span>Relevance</span> &nbsp; | &nbsp; <a href="#">Newest First</a>
						</div>-->

					</div>

				<div class="blog-posts-container row small-up-1 medium-up-2 large-up-3" data-equalizer="blog-post">

	<?php if ( have_posts() ) : ?>

		<?php while ( have_posts() ) : the_post(); ?>

								<a href="<?php the_permalink(); ?>" class="column">

									<div class="blog-posts-container--post" style="background-image: url('<?php if(has_post_thumbnail()): the_post_thumbnail_url('large'); else: echo get_stylesheet_directory_uri().'/img/employees-bg.jpg'; endif; ?>');" data-equalizer-watch="blog-post">

										<div class="blog-posts-container--details">

											<h2><?php the_title(); ?></h2>

											<span><?php echo get_the_date( 'Y-m-d' ); ?></span>

										</div>


									</div>

								</a>

		<?php endwhile; ?>

		</div>
		
		<?php else : ?>
			<?php get_template_part( 'template-parts/content', 'none' ); ?>

		<?php endif; // End have_posts() check. ?>

		<?php
		if ( function_exists( 'foundationpress_pagination' ) ) :
			foundationpress_pagination();
		elseif ( is_paged() ) :
		?>
			<nav id="post-nav">
				<div class="post-previous"><?php next_posts_link( __( '&larr; Older posts', 'foundationpress' ) ); ?></div>
				<div class="post-next"><?php previous_posts_link( __( 'Newer posts &rarr;', 'foundationpress' ) ); ?></div>
			</nav>
		<?php endif; ?>

		</div><!-- main-content-main -->


		<div class="sidebar-left" data-equalizer-watch="main-content">


			<div class="sidebar--header" data-equalizer-watch="main-content-headers">
				<h3>Blog Articles</h3>
			</div>

			<div class="padded blog-sidebar">
				
				<?php dynamic_sidebar( 'sidebar-widgets' ); ?>

				<h3>Archive</h3>
		        <?php $month= date('m', strtotime('-1 month')); #echo $month; ?>
		        
		        <ul>
			        
			        <?php #wp_get_archives('type=monthly&limit=6'); ?>
			        <?php wp_get_archives('type=monthly&limit='.$month); ?>
			        <?php wp_get_archives('type=yearly'); ?>            
		            
		        </ul>

			</div>


	</div><!--sidebar-left -->




</div>

<?php get_footer();
