<?php
/*
Template Name: Search Results
 */

 get_header(); ?>


<div class="page-header row">

	<div class="medium-5 columns">
		<h1>Software Engineer Jobs in Brighton</h1>
		<span class="search-results-count">1,465 jobs found</span>
	</div>
	
	<?php get_template_part('template-parts/page-header-search'); ?>

</div><!-- page-header -->


<div class="main-content" data-equalizer="main-content">






	<div class="main-content-main" data-equalizer-watch="main-content">

				<div class="main-content-main--breadcrumbs" data-equalizer-watch="main-content-headers">
							<?php
							if ( function_exists('yoast_breadcrumb') ) {
							yoast_breadcrumb('
							<p id="breadcrumbs">','</p>
							');
							}
							?>
							
					<div class="sort-results">
						<span>Relevance</span> &nbsp; | &nbsp; <a href="#">Newest First</a>
					</div>

				</div>

			

				<div class="search-result-block">

					<div class="search-results-block--logo">
						<div class="logo-container">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/example-logo4.png" alt="logo">
						</div>
					</div>

					<div class="search-results-block--details">
						
						<span><strong>SQL Server Database Administrator</strong><br/>
						Company: <strong>TPP</strong><br/>
						Full Time (Graduate Job)</span>

					</div>

					<div class="search-results-block--location">
						
						<span>Location:   <strong>Brighton</strong><br/>    
						Salary:       <strong>£32,000</strong></span>

					</div>

					<div class="search-results-block--savethis">
						
						<a href="#" class="savethis"><svg class="icon icon-heart"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-heart"></use></svg> Save this</a>

					</div>

					<a href="<?php bloginfo('home'); ?>/hr-graduate-scheme-at-bakkavor-group/">
						<div class="search-results-block--moredetails">
							
							<div class="stars">
								<div class="stars-gold" style="width: 88%;"> &nbsp; </div>
								<div class="stars-white"> &nbsp; </div>
							</div>

							<span class="review-count">12 Reviews</span>

							<span class="button blue">More details</span>

						</div>
					</a>


				</div><!--search-result-block-->

			

				<div class="search-result-block">

					<div class="search-results-block--logo">
						<div class="logo-container">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/example-logo5.png" alt="logo">
						</div>
					</div>

					<div class="search-results-block--details">
						
						<span><strong>Software Developer</strong><br/>
						Company: <strong>nucleargraduates</strong><br/>
						Full Time (Graduate Job)</span>

					</div>

					<div class="search-results-block--location">
						
						<span>Location:   <strong>Brighton</strong><br/>    
						Salary:       <strong>£45,000</strong></span>

					</div>

					<div class="search-results-block--savethis">
						
						<a href="#" class="savethis"><svg class="icon icon-heart"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-heart"></use></svg> Save this</a>

					</div>

					<a href="<?php bloginfo('home'); ?>/hr-graduate-scheme-at-bakkavor-group/">
						<div class="search-results-block--moredetails">
							
							<div class="stars">
								<div class="stars-gold" style="width: 75%;"> &nbsp; </div>
								<div class="stars-white"> &nbsp; </div>
							</div>

							<span class="review-count">12 Reviews</span>


							<span class="button blue">More details</span>

						</div>
					</a>


				</div><!--search-result-block-->

			

				<div class="search-result-block">

					<div class="search-results-block--logo">
						<div class="logo-container">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/example-logo6.png" alt="logo">
						</div>
					</div>

					<div class="search-results-block--details">
						
						<span><strong>Software Architect</strong><br/>
						Company: <strong>Smith & Williamson</strong><br/>
						Full Time (Graduate Job)</span>

					</div>

					<div class="search-results-block--location">
						
						<span>Location:   <strong>Brighton</strong><br/>    
						Salary:       <strong>£36,000</strong></span>

					</div>

					<div class="search-results-block--savethis">
						
						<a href="#" class="savethis"><svg class="icon icon-heart"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-heart"></use></svg> Save this</a>

					</div>

					<a href="<?php bloginfo('home'); ?>/hr-graduate-scheme-at-bakkavor-group/">
						<div class="search-results-block--moredetails">
						
							<div class="stars">
								<div class="stars-gold" style="width: 96%;"> &nbsp; </div>
								<div class="stars-white"> &nbsp; </div>
							</div>

							<span class="review-count">12 Reviews</span>


							<span class="button blue">More details</span>

						</div>
					</a>


				</div><!--search-result-block-->

			

				<div class="search-result-block">

					<div class="search-results-block--logo">
						<div class="logo-container">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/example-logo4.png" alt="logo">
						</div>
					</div>

					<div class="search-results-block--details">
						
						<span><strong>SQL Server Database Administrator</strong><br/>
						Company: <strong>TPP</strong><br/>
						Full Time (Graduate Job)</span>

					</div>

					<div class="search-results-block--location">
						
						<span>Location:   <strong>Brighton</strong><br/>    
						Salary:       <strong>£28,000</strong></span>

					</div>

					<div class="search-results-block--savethis">
						
						<a href="#" class="savethis"><svg class="icon icon-heart"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-heart"></use></svg> Save this</a>

					</div>

					<a href="<?php bloginfo('home'); ?>/hr-graduate-scheme-at-bakkavor-group/">
						<div class="search-results-block--moredetails">
							
							<div class="stars">
								<div class="stars-gold" style="width: 65%;"> &nbsp; </div>
								<div class="stars-white"> &nbsp; </div>
							</div>

							<span class="review-count">12 Reviews</span>


							<span class="button blue">More details</span>

						</div>
					</a>


				</div><!--search-result-block-->

			

				<div class="search-result-block">

					<div class="search-results-block--logo">
						<div class="logo-container">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/example-logo5.png" alt="logo">
						</div>
					</div>

					<div class="search-results-block--details">
						
						<span><strong>Senior Software Developer</strong><br/>
						Company: <strong>TPP</strong><br/>
						Full Time (Graduate Job)</span>

					</div>

					<div class="search-results-block--location">
						
						<span>Location:   <strong>Brighton</strong><br/>    
						Salary:       <strong>£30,000</strong></span>

					</div>

					<div class="search-results-block--savethis">
						
						<a href="#" class="savethis"><svg class="icon icon-heart"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-heart"></use></svg> Save this</a>

					</div>

					<a href="<?php bloginfo('home'); ?>/hr-graduate-scheme-at-bakkavor-group/">
						<div class="search-results-block--moredetails">
							
							<div class="stars">
								<div class="stars-gold" style="width: 100%;"> &nbsp; </div>
								<div class="stars-white"> &nbsp; </div>
							</div>

							<span class="review-count">12 Reviews</span>


							<span class="button blue">More details</span>

						</div>
					</a>


				</div><!--search-result-block-->

			

				<div class="search-result-block">

					<div class="search-results-block--logo">
						<div class="logo-container">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/example-logo6.png" alt="logo">
						</div>
					</div>

					<div class="search-results-block--details">
						
						<span><strong>SQL Server Database Administrator</strong><br/>
						Company: <strong>TPP</strong><br/>
						Full Time (Graduate Job)</span>

					</div>

					<div class="search-results-block--location">
						
						<span>Location:   <strong>Brighton</strong><br/>    
						Salary:       <strong>£32,000</strong></span>

					</div>

					<div class="search-results-block--savethis">
						
						<a href="#" class="savethis"><svg class="icon icon-heart"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-heart"></use></svg> Save this</a>

					</div>

					<a href="<?php bloginfo('home'); ?>/hr-graduate-scheme-at-bakkavor-group/">
						<div class="search-results-block--moredetails">
						
							<div class="stars">
								<div class="stars-gold" style="width: 50%;"> &nbsp; </div>
								<div class="stars-white"> &nbsp; </div>
							</div>

							<span class="review-count">12 Reviews</span>


							<span class="button blue">More details</span>

						</div>
					</a>


				</div><!--search-result-block-->

	</div><!--main-content-main -->










	<div class="sidebar-left" data-equalizer-watch="main-content">


			<div class="sidebar--header" data-equalizer-watch="main-content-headers">
				<h3>Refine your search</h3> <svg class="icon icon-refine"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-refine"></use></svg>
			</div>

			<ul class="results-categories">
				<li><a href="#">Industry</a>
					<ul class="results-categories--subcategories">
						<li><a href="#">Arts &amp; heritage  <em>21</em></a></li>
						<li><a href="#">Charities  <em>32</em></a></li>
						<li><a href="#">Construction  <em>14</em></a></li>
						<li><a href="#">Design  <em>9</em></a></li>
						<li><a href="#">Finance &amp; accounting  <em>25</em></a></li>
						<li><a href="#">General  <em>31</em></a></li>
						<li><a href="#">Higher education  <em>11</em></a></li>
						<li><a href="#">Housing  <em>46</em></a></li>
						<li><a href="#">Marketing &amp; PR  <em>123</em></a></li>
						<li><a href="#">Media  <em>12</em></a></li>
						<li><a href="#">Recruitment  <em>19</em></a></li>
						<li><a href="#">Schools  <em>18</em></a></li>
						<li><a href="#">Technology  <em>11</em></a></li>
					</ul>
				</li>

				<li><a href="#">Job Type</a>
					<ul class="results-categories--subcategories">
						<li><a href="#">Full time  <em>21</em></a></li>
						<li><a href="#">Part time  <em>32</em></a></li>
						<li><a href="#">Graduate  <em>14</em></a></li>
					</ul>
				</li>
			</ul><!-- results-categories -->


	</div><!--sidebar-left -->








	<div class="sidebar-right" data-equalizer-watch="main-content">

		<div class="sidebar--header" data-equalizer-watch="main-content-headers">
			<h3>Your saved jobs</h3>
		</div>

		<div class="saved-jobs">
			<div class="saved-jobs--job">
				<strong>Software Architect</strong><br>
				Smith &amp; Williamson<br>
				Full Time (Graduate Job)<br>
				Location:   <strong>Brighton</strong> <br>  
				Salary:       <strong>£36,000</strong>
			</div>
			<div class="saved-jobs--job">
				<strong>Senior Software Developer</strong><br>
				nucleargraduates<br>
				Full Time (Graduate Job)<br>
				Location:   <strong>Brighton</strong> <br>  
				Salary:       <strong>£29,000</strong>
			</div>
			<div class="saved-jobs--job">
				<strong>Software Architect</strong><br>
				Smith &amp; Williamson<br>
				Full Time (Graduate Job)<br>
				Location:   <strong>Brighton</strong> <br>  
				Salary:       <strong>£34,000</strong>
			</div>
		</div>

	</div><!--sidebar-right -->



</div> <!-- main-content -->

 <?php get_footer();
