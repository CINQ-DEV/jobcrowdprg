<?php
/*
 Template Name: Companies  Details
 */

get_header(); 

$company = $_GET["company"];
$cID = $_GET["cid"];

$all_meta_for_user = get_user_meta( $cID );

/*
if( wc_memberships_is_user_active_member( $cID, $employer_registration ) || wc_memberships_is_user_active_member( $cID, $basic_legacy ) || wc_memberships_is_user_active_member( $cID, $enhanced_legacy ) ) {
	echo 'active';
}

$groups_user = new Groups_User( $cID );
# get group objects
$user_groups = $groups_user->groups;
# get group ids (user is direct member)
$user_group_ids = $groups_user->group_ids;
# get group ids (user is direct member or by group inheritance)
$user_group_ids_deep = $groups_user->group_ids_deep;
print_r( $user_group_ids_deep);
*/
?>


<div class="page-header row">

	<div class="medium-5 columns">

<?php
$activate_membership = get_user_meta( $cID, 'activate_membership', $single = true);

if ($cID !=''):

	if ($activate_membership == 'Yes') :
	?>
		<?php	
		$company_name = $all_meta_for_user['company_name'][0];
		$company_address = $all_meta_for_user['main_address'][0];
		$company_industry = $all_meta_for_user['employer_registration_industry'][0];
		$company_employees = $all_meta_for_user['number_of_employees'][0];
		$logo = $all_meta_for_user['logo'][0];
		$logo = get_field('logo', 'user_'.$cID);
		if ($company_name):
			echo '<h1>'.$company_name.'</h1>';
		endif;

		echo $company_industry[3];

		$employer_registration_industry = get_field('employer_registration_industry', 'user_'.$cID);

		?>

	</div>
	

	<?php get_template_part('template-parts/page-header-search'); ?>

</div><!-- page-header -->


<div class="main-content no-right-column" data-equalizer="main-content">


	<div class="main-content-main" data-equalizer-watch="main-content22">

<!-- Nav tabs -->
<div class="tabs-container">
<ul class="tabs" data-tabs data-deep-link="true" data-deep-link-smudge="true" data-deep-link-smudge="500" id="reviews-tabs" data-equalizer-watch="main-content-headers">
	<li class="tabs-title is-active"><a href="#overview" aria-controls="overview" role="tab">Overview</a></li>
	<li class="tabs-title"><a href="#enhanced" aria-controls="enhanced" role="tab">Company Profile</a></li>
	<li class="tabs-title"><a href="#graduates" aria-controls="graduates" role="tab">Graduate Reviews</a></li>
	<li class="tabs-title"><a href="#apprentices" aria-controls="apprentices" role="tab">Apprentices Reviews</a></li>
	<li class="tabs-title"><a href="#jobs" aria-controls="jobs" role="tab">Jobs</a></li>
	<li class="tabs-title"><a href="#salary" aria-controls="salary" role="tab">Salary</a></li>
	<li class="tabs-title"><a href="#hours" aria-controls="hours" role="tab">Working Hours</a></li>
    <li class="tabs-title"><a href="#advice" aria-controls="advice" role="tab">Interview Advice</a></li>
    <li class="tabs-title"><a href="#best_worst" aria-controls="best_worst" role="tab">Best and Worst Things</a></li>
    <a href="#" class="more-tabs">
    	<span>More</span><svg class="icon icon-menu"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-menu"></use></svg>
    </a>
</ul>
</div>

<!-- Tab panes -->
<div class="tabs-content" data-tabs-content="reviews-tabs">

	<div class="tabs-panel is-active" id="overview">

	<div class="padded">

		<?php $overall_rating = $all_meta_for_user['overall_rating'][0];
		$overall_rating_percent = $overall_rating * 20;
		$average_total_reviews = $all_meta_for_user['average_total_reviews'][0];
		?>

		<div class="page-intro">All of the below information comes directly from job reviews written by <?php echo $company_name; ?> employees based on <?php echo $average_total_reviews; ?> reviews. 

			<?php if($overall_rating_percent): ?>

				<div class="stars right">
						<div class="stars-gold" style="width: <?php echo $overall_rating_percent; ?>%;"> &nbsp; </div>
						<div class="stars-white"> &nbsp; </div>
				</div>

			<?php endif; ?>

		</div>

		<div class="reviews-graphs row">

		<?php
		$average_responsibility = $all_meta_for_user['average_responsibility'][0];
		$average_responsibility_percent = $average_responsibility * 20;

		if ($average_responsibility != 'NAN') :
			echo '<div class="medium-6 columns review-graph">';
			echo 'Responsibility:'; ?>
			<div class="review-graph-black">
						<span class="review-graph-number"><?php echo ''.$average_responsibility.'/5'; ?></span>
						<div class="review-graph-blue" style="max-width: <?php echo $average_responsibility_percent; ?>%">&nbsp;</div>
					</div>
			<?php
			echo '</div> <!-- end review graph -->';
		endif;

		$average_work_life_balance = $all_meta_for_user['average_work_life_balance'][0];
		$average_work_life_balance_percent = $average_work_life_balance * 20;
		if ($average_work_life_balance != 'NAN') :
			echo '<div class="medium-6 columns review-graph">';
			echo 'Work Life Balance:'; ?>
			<div class="review-graph-black">
						<span class="review-graph-number"><?php echo ''.$average_work_life_balance.'/5'; ?></span>
						<div class="review-graph-blue" style="max-width: <?php echo $average_work_life_balance_percent; ?>%">&nbsp;</div>
					</div>
			<?php
			echo '</div> <!-- end review graph -->';
		endif;

		$average_environmental_awareness = $all_meta_for_user['average_environmental_awareness'][0];
		$average_environmental_awareness_percent = $average_environmental_awareness * 20;
		if ($average_environmental_awareness != 'NAN') :
			echo '<div class="medium-6 columns review-graph">';
			echo 'Environmental Awareness:'; ?>
			<div class="review-graph-black">
						<span class="review-graph-number"><?php echo ''.$average_environmental_awareness.'/5'; ?></span>
						<div class="review-graph-blue" style="max-width: <?php echo $average_environmental_awareness_percent; ?>%">&nbsp;</div>
					</div>
			<?php
			echo '</div> <!-- end review graph -->';
		endif;

		$average_compensation_benefits = $all_meta_for_user['average_compensation_benefits'][0];
		$average_compensation_benefits_percent = $average_compensation_benefits * 20;
		if ($average_compensation_benefits != 'NAN') :
			echo '<div class="medium-6 columns review-graph">';
			echo 'Compensation &amp; Benefits:'; ?>
			<div class="review-graph-black">
						<span class="review-graph-number"><?php echo ''.$average_compensation_benefits.'/5'; ?></span>
						<div class="review-graph-blue" style="max-width: <?php echo $average_compensation_benefits_percent; ?>%">&nbsp;</div>
					</div>
			<?php
			echo '</div> <!-- end review graph -->';
		endif;

		$average_company_culture = $all_meta_for_user['average_company_culture'][0];
		$average_company_culture_percent = $average_company_culture * 20;
		if ($average_company_culture != 'NAN') :
			echo '<div class="medium-6 columns review-graph">';
			echo 'Company Culture:'; ?>
			<div class="review-graph-black">
						<span class="review-graph-number"><?php echo ''.$average_company_culture.'/5'; ?></span>
						<div class="review-graph-blue" style="max-width: <?php echo $average_company_culture_percent; ?>%">&nbsp;</div>
					</div>
			<?php
			echo '</div> <!-- end review graph -->';
		endif;

		$average_career_progression = $all_meta_for_user['average_career_progression'][0];
		$average_career_progression_percent = $average_career_progression * 20;
		if ($average_career_progression != 'NAN') :
			echo '<div class="medium-6 columns review-graph">';
			echo 'Career Progression:'; ?>
			<div class="review-graph-black">
						<span class="review-graph-number"><?php echo ''.$average_career_progression.'/5'; ?></span>
						<div class="review-graph-blue" style="max-width: <?php echo $average_career_progression_percent; ?>%">&nbsp;</div>
					</div>
			<?php
			echo '</div> <!-- end review graph -->';
		endif;

		$average_colleagues = $all_meta_for_user['average_colleagues'][0];
		$average_colleagues_percent = $average_colleagues * 20;
		if ($average_colleagues != 'NAN') :
			echo '<div class="medium-6 columns review-graph">';
			echo 'Colleagues:'; ?>
			<div class="review-graph-black">
						<span class="review-graph-number"><?php echo ''.$average_colleagues.'/5'; ?></span>
						<div class="review-graph-blue" style="max-width: <?php echo $average_colleagues_percent; ?>%">&nbsp;</div>
					</div>
			<?php
			echo '</div> <!-- end review graph -->';
		endif;

		$average_training = $all_meta_for_user['average_training'][0];
		$average_training_percent = $average_training * 20;
		if ($average_training != 'NAN') :
			echo '<div class="medium-6 columns review-graph">';
			echo 'Training:'; ?>
			<div class="review-graph-black">
						<span class="review-graph-number"><?php echo ''.$average_training.'/5'; ?></span>
						<div class="review-graph-blue" style="max-width: <?php echo $average_training_percent; ?>%">&nbsp;</div>
					</div>
			<?php
			echo '</div> <!-- end review graph -->';
		endif;

		$average_enjoyment = $all_meta_for_user['average_enjoyment'][0];
		$average_enjoyment_percent = $average_enjoyment * 20;
		if ($average_enjoyment != 'NAN') :
			echo '<div class="medium-6 columns review-graph">';
			echo 'Enjoyment:'; ?>
			<div class="review-graph-black">
						<span class="review-graph-number"><?php echo ''.$average_enjoyment.'/5'; ?></span>
						<div class="review-graph-blue" style="max-width: <?php echo $average_enjoyment_percent; ?>%">&nbsp;</div>
					</div>
			<?php
			echo '</div> <!-- end review graph -->';
		endif;

		if ($overall_rating != 'NAN') :
			echo '<div class="medium-6 columns review-graph last">';
			echo 'Overall Rating:'; ?>
			<div class="review-graph-black">
						<span class="review-graph-number"><?php echo ''.$overall_rating.'/5'; ?></span>
						<div class="review-graph-blue" style="max-width: <?php echo $overall_rating_percent; ?>%">&nbsp;</div>
					</div>
			<?php
			echo '</div> <!-- end review graph -->';
		endif;
		?> 	
		</div><!-- /reviews-graphs -->

	</div><!-- padded -->

	</div><!--#overview panel -->


	<div class="tabs-panel" id="graduates">

	<?php
    $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
	$args = array(
		'posts_per_page'	=> -1,
		'post_type'			=> 'graduate_reviews',
		'meta_key'			=> 'graduate_review_employer',	
		'meta_value'		=> $company_name	
	);
	
	// query
	$the_query = new WP_Query( $args );
	
	if( $the_query->have_posts() ): ?>

		<?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>
			
		<div class="review-container--header row">

			<div class="review-container--header--left">
				<span class="review-title">
					<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
				</span>
			</div><!-- review-container-header-left -->

		</div><!-- review-container-header row -->

		<div class="review-container--body">

			<div class="review-container--body--left">

				<?php $graduate_review_industry = get_field('graduate_review_industry'); if ($graduate_review_industry): ?>
				<span>Industry: <strong><?php echo $graduate_review_industry; ?></strong></span>
				<?php endif; ?>

				<?php $graduate_review_department = get_field('graduate_review_department'); if ($graduate_review_department): ?>
				<span>Department: <strong><?php echo $graduate_review_department; ?></strong></span>
				<?php endif; ?>

				<?php $graduate_review_job_title = get_field('graduate_review_job_title'); if ($graduate_review_job_title): ?>
				<span>Job Title: <strong><?php echo $graduate_review_job_title; ?></strong></span>
				<?php endif; ?>

			</div><!-- /review-container--body--left -->

			<div class="review-container--body--right">

				<?php $graduate_review_what_are_the_best_and_worst_things_about_your_company_best = get_field('graduate_review_what_are_the_best_and_worst_things_about_your_company_best'); if ($graduate_review_what_are_the_best_and_worst_things_about_your_company_best): ?>
				<p><strong>What are the best and worst things about your company?</strong><br/>
				<?php the_field('graduate_review_what_are_the_best_and_worst_things_about_your_company_best'); ?>
				<?php endif; ?>
				<a href="<?php the_permalink(); ?>" class="button">Read more</a></p>

			</div><!-- /review-container--body--right -->

		</div><!-- review-container-body-->
		<br/>

		<?php endwhile; ?>
	
	<?php else: ?>

		<p>There are not yet any graduate reviews entered for this company. Why not <a href="<?php bloginfo('home'); ?>/reviews/graduate-review">be the first to write one</a>?</p>


	<?php endif; 
	
	wp_reset_query();	 // Restore global post data stomped by the_post().
	?>	

	</div><!-- /graduates -->


	<div class="tabs-panel" id="apprentices">

	
	<?php
	$args = array(
		'posts_per_page'	=> -1,
		'post_type'			=> 'apprentice_reviews',
		'meta_key'			=> 'apprentice_review_employer',
		'meta_value'		=> $company_name	
	);
	
	// query
	$the_query = new WP_Query( $args );
	
	if( $the_query->have_posts() ): ?>
	

		<?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>
			

			<div class="review-container--header row">

				<div class="review-container--header--left">
				<span class="review-title">
				<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
				</span>
				</div><!-- review-container-header-left -->

			</div><!-- review-container-header row -->

			<div class="review-container--body">

				<div class="review-container--body--left">

					<?php $apprentice_review_industry = get_field('apprentice_review_industry'); if ($apprentice_review_industry): ?>
						<span>Industry: <strong><?php echo implode(', ', $apprentice_review_industry); ?></strong></span>
					<?php endif; ?>

					<?php $apprentice_review_department = get_field('apprentice_review_department'); if ($apprentice_review_department): ?>
						<span>Department: <strong><?php echo $apprentice_review_department; ?></strong></span>
					<?php endif; ?>

					<?php $apprentice_review_job_title = get_field('apprentice_review_job_title'); if ($apprentice_review_job_title): ?>
						<span>Job Title: <strong><?php echo $apprentice_review_job_title; ?></strong></span>
					<?php endif; ?>

				</div><!-- /review-container--body--left -->

				<?php $apprentice_review_what_are_the_best_and_worst_things_about_your_company_best = get_field('apprentice_review_what_are_the_best_and_worst_things_about_your_company_best'); if ($apprentice_review_what_are_the_best_and_worst_things_about_your_company_best): ?>
				<div class="review-container--body--right">
					<p><strong>What are the best and worst things about your company?</strong><br/>
					<?php the_field('apprentice_review_what_are_the_best_and_worst_things_about_your_company_best'); ?>
					<a href="<?php the_permalink(); ?>" class="button">Read more</a></p>
				</div><!-- /review-container--body--right -->
				<?php endif; ?>

			</div><!-- review-container-body-->
			<br/>
		<?php endwhile; ?>

	<?php else: ?>

		<p>There are not yet any apprentice reviews entered for this company. Why not <a href="<?php bloginfo('home'); ?>/reviews/apprentice-review">be the first to write one</a>?</p>

	<?php endif; 
	
	wp_reset_query();	 // Restore global post data stomped by the_post().
	?>	
	
	
	</div><!-- /apprentices -->
	
	<div class="tabs-panel" id="jobs">
	<?php
	$args = array(
		'posts_per_page'	=> -1,
		'post_type'			=> 'jobs',
		'post_status'		=> 'publish',
		'meta_key'			=> 'dynamic_filled_company',
		'meta_value'		=> $company_name	
	);
	
	// query
	$the_query = new WP_Query( $args );
	
	if( $the_query->have_posts() ): ?>
	
		<p>Jobs listed for this company.</p>
		<?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>
			
			<div class="review-container--header row">

				<div class="review-container--header--left">
					<span class="review-title">
					<?php 
					$salary = get_field('salary');
					$additional_salary = get_field('additional_salary');
					?>
					<a href="<?php the_permalink(); ?>"><?php the_title(); ?>
					<?php
					if ($salary):
						echo $salary;
					else:
						echo $additional_salary;
					endif;
					?>
					</a>
					</span>
				</div><!-- review-container-header-left -->

			</div><!-- review-container-header row -->

			<div class="review-container--body">
			<?php
			#echo $post->ID;
			$job_description = get_field('job_description');
			if ($job_description):
				#echo $job_description;
			endif;
						
			$salary = get_field('salary');
			if ($salary):
				#echo $salary;
			endif;							
			?>
			</div><!-- review-container-body-->
			<br/>
		<?php endwhile; ?>

	<?php else: ?>

		<h3>There are no jobs listed for this company.</h3>
		<?php 
		$employer_registration_industry = get_field('employer_registration_industry', 'user_'.$cID);
		echo '<pre>';
		#print_r($employer_registration_industry);
		echo '</pre>';
		
		#create an array
		$industry_result = array();		

		#loop through each industry and check for a match, add to array if so
		foreach($employer_registration_industry as $industry) :

			$cleanedindustry = str_replace("&","&amp;",$industry);		
			#echo $industry.'<hr/>';
			$job_ind = get_posts(array(
				'post_type' => 'jobs',
				'post_status' => 'publish',
				'meta_query' => array(
					array(
						'key' 	=> 'dynamic_filled_industry',
						'value' => $cleanedindustry,
						'compare' => 'LIKE'
					)
				)	
			));

			foreach($job_ind as $ind) : 
				#pass the results into the array
				$industry_result[] = $ind->ID;
			endforeach;	

		endforeach;	
		
		echo '<pre>';
		#print_r($industry_result);
		echo '</pre>';		
		
		#pass the IDs into our main loop
		
		#if we have results, continue
		if (!$industry_result) :
			echo "<p>Sorry, no jobs within this industry</p>";
		else:

			$args = array(		
				'posts_per_page'	=> -1,
				'post_type'			=> 'jobs',
				'post__in'			=> array_unique($industry_result)
			);

			# query
			$the_query = new WP_Query( $args );
			$total = $the_query->found_posts;
			#echo 'Results Found: <strong>'.$total.'</strong></span>';	

			echo '<p>Here are other jobs within the same industry</p>';		

			while( $the_query->have_posts() ) : $the_query->the_post(); ?>

				<div class="review-container--header row">

					<div class="review-container--header--left">
						<span class="review-title">
						<?php 
						$salary = get_field('salary');
						$additional_salary = get_field('additional_salary');						
						?>
						<a href="<?php the_permalink(); ?>"><?php the_title(); ?> 
						<?php
						if ($salary):
							echo $salary;
						else:
							echo $additional_salary;
						endif;
						?>
						</a>
						</span>
					</div><!-- review-container-header-left -->

				</div><!-- review-container-header row -->

				<div class="review-container--body">
				<?php
				#echo $post->ID;
				$job_description = get_field('job_description');
				if ($job_description):
					#echo $job_description;
				endif;

				$salary = get_field('salary');
				if ($salary):
					echo 'Salary: '.$salary.'<br/>';
				endif;	

				$additional_salary = get_field('additional_salary');
				if ($additional_salary):
					echo 'Additional Salary: '.$additional_salary.'<br/>';
				endif;						

				$employer = get_field('dynamic_filled_company');	
				if ($employer):
					echo 'Employer: '.$employer.'<br/>';
				endif;		

				$deadline_date = get_field('deadline_date');
				if ($deadline_date):
					echo 'Deadline Date: '.$deadline_date.'<br/>';
				endif;	

				$deadline_other_info = get_field('deadline_other_info');
				if ($deadline_other_info):
					echo 'Deadline Date: '.$deadline_other_info.'<br/>';
				endif;						
				?>
				</div><!-- review-container-body-->
				<br/>

			<?php endwhile;

			wp_reset_query();	 // Restore global post data stomped by the_post().			
		
		endif; #endif not empty $industry_result
		

		
		
	endif; #endif $the_query main job loop					
	?>	
	</div><!-- jobs -->
	

	<div class="tabs-panel" id="enhanced"> 

	<?php   
	#echo $cID;
	# get an array of all the group ID's the user belongs to
	$groups_user = new Groups_User( $cID );
	# get group objects
	$user_groups = $groups_user->groups;
	# get group ids (user is direct member)
	$user_group_ids = $groups_user->group_ids;
	# get group ids (user is direct member or by group inheritance)
	$user_group_ids_deep = $groups_user->group_ids_deep;
	#print_r ( $user_group_ids_deep ); #debug

	# create an array of all basic groups
	$basic_groups = array(5,6,7);
	# check if the company belongs to any of those groups
	$basic_results = array_intersect($user_group_ids_deep,$basic_groups);
	if ($basic_results):
		foreach ($basic_results as $result):
			#echo $result.'<br/>';
			$basic_group = '0';
		endforeach;
	else:	
		$basic_group = '1';
	endif;

	# create an array of all enhanced groups
	$enhanced_groups = array(8,9,10);
	# check if the company belongs to any of those groups
	$enhanced_results = array_intersect($user_group_ids_deep,$enhanced_groups);
	if ($enhanced_results):
		foreach ($enhanced_results as $result):
			#echo $result.'<br/>';
			$enhanced_group = '0';
		endforeach;
	else:
		$enhanced_group = '1';
	endif;

	if ( ($basic_group == 0) && ($enhanced_group == 1) ) :
	   # echo 'basic profile';
	endif;

	if ( ($basic_group == 1) && ($enhanced_group == 0) ) :

		$biography = get_field('biography', 'user_'.$cID);
		if($biography):

			echo '<div class="row"><div class="company-biography medium-8 columns">';
			echo $biography;
			echo '<hr/>'; ?>

				<?php if( have_rows('gallery', 'user_'.$cID) ): ?>

				<div class="row small-up-2 medium-up-4">

					<?php while( have_rows('gallery', 'user_'.$cID) ): the_row(); 

						# vars
						$image = get_sub_field('image');

						if( $image ): ?>

								<div class="column column-block">
						<img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt'] ?>" />			
								</div>

						<?php endif;
					endwhile; ?>

					</div>

				<?php endif; ?>

			</div>
		<div class="company-biography--facts medium-4 columns">

		<?php endif; //$biography


			$promotional_video = get_field('promotional_video', 'user_'.$cID);
			if( $promotional_video ) : ?>
				<div class="embed-container">
					<?php echo $promotional_video; ?>
				</div>
			<?php endif;

			$what_makes_you_a_great_company_to_work_for = get_field('what_makes_you_a_great_company_to_work_for', 'user_'.$cID);
			if( $what_makes_you_a_great_company_to_work_for ) :
				echo '<p> What makes you a great company to work for?<br/>';
				echo '' . $what_makes_you_a_great_company_to_work_for . '</p>';
			endif;

			$number_of_graduate_positions_this_year = get_field('number_of_graduate_positions_this_year', 'user_'.$cID);
			if( $number_of_graduate_positions_this_year ) :
				echo '<p>Number of Graduate Positions this year<br/>';
				echo '' . $number_of_graduate_positions_this_year . '</p>';
			endif;

			$total_number_of_company_employees = get_field('total_number_of_company_employees', 'user_'.$cID);
			if( $total_number_of_company_employees ) :
				echo '<p>Total number of company employees<br/>';
				echo '' . $total_number_of_company_employees . '</p>';
			endif;

			if( have_rows('social_media_links', 'user_'.$cID) ): ?>

				<p>Social Media Links:<br>

				<?php while( have_rows('social_media_links', 'user_'.$cID) ): the_row(); 

					# vars
					$facebook_url = get_sub_field('facebook_url');
					$twitter_url = get_sub_field('twitter_url');
					$linkedin_url = get_sub_field('linkedin_url');
					$youtube_url = get_sub_field('youtube_url');
					$other = get_sub_field('other');
					?>

					<?php if( $facebook_url ): ?>
						<a href="<?php echo $facebook_url; ?>" target="_blank"><svg class="icon icon-facebook"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-facebook"></use></svg> &nbsp; </a>
					<?php endif; ?>

					<?php if( $twitter_url ): ?>
						<a href="<?php echo $twitter_url; ?>" target="_blank"><svg class="icon icon-twitter"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-twitter"></use></svg> &nbsp; </a>
					<?php endif; ?>

					<?php if( $linkedin_url ): ?>
						<a href="<?php echo $linkedin_url; ?>" target="_blank"><svg class="icon icon-linkedin"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-linkedin"></use></svg> &nbsp; </a>
					<?php endif; ?>

					<?php if( $youtube_url ): ?>
						<a href="<?php echo $youtube_url; ?>" target="_blank"><svg class="icon icon-youtube"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-youtube"></use></svg> &nbsp; </a>
					<?php endif; ?>      

					<?php if( $other ): ?>
						<a href="<?php echo $other; ?>" target="_blank">Other</a><br/>
					<?php endif; ?>                                                

				<?php endwhile; ?>

				</p>

			<?php endif; #endif social_media_links

			$file = get_field('file_download');
			if( $file ): ?>
				<p>Downloads:<br/>
				<a href="<?php echo $file['url']; ?>"><?php echo $file['filename']; ?></a></p>
			<?php endif; 	

			$careers_that_you_recruit_for = get_field('careers_that_you_recruit_for', 'user_'.$cID);
			if( $careers_that_you_recruit_for ) :
				echo '<p>Careers that you recruit for<br/>';
				echo '' . $careers_that_you_recruit_for . '</p>';
			endif;

			$locations = get_field('locations', 'user_'.$cID);
			if( $locations ) :
				echo '<p>Locations<br/>';
				echo '' . $locations . '</p>';
			endif;

			if( have_rows('contact_details', 'user_'.$cID) ): ?>

			<p>Contact Details

				<?php while( have_rows('contact_details', 'user_'.$cID) ): the_row(); 

					# vars
					$phone = get_sub_field('phone');
					$email = get_sub_field('email');
					$web = get_sub_field('web');
					$address = get_sub_field('address');
					$contact_name = get_sub_field('contact_name');
					?>

					<?php if( $phone ): ?>
						<a href="<?php echo $phone; ?>"><?php echo $phone; ?></a><br/>
					<?php endif; ?>

					<?php if( $email ): ?>
						<a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a><br/>
					<?php endif; ?>

					<?php if( $web ): ?>
						<a href="<?php echo $web; ?>" target="_blank"><?php echo $web; ?></a><br/>
					<?php endif; ?>

					<?php if( $address ): ?>
					<div class="acf-map">
						<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
					<style type="text/css">

					.acf-map {
						width: 100%;
						height: 400px;
						border: #ccc solid 1px;
						margin: 20px 0;
					}

					/* fixes potential theme css conflict */
					.acf-map img {
					max-width: inherit !important;
					}

					</style>
					<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAj_Xc1gEDKeDqLIMFHgok9hhWgyf16TK0"></script>
					<script type="text/javascript">
					(function($) {

						/*
						*  new_map
						*
						*  This function will render a Google Map onto the selected jQuery element
						*
						*  @type	function
						*  @date	8/11/2013
						*  @since	4.3.0
						*
						*  @param	$el (jQuery element)
						*  @return	n/a
						*/

						function new_map( $el ) {

							// var
							var $markers = $el.find('.marker');


							// vars
							var args = {
								zoom		: 16,
								center		: new google.maps.LatLng(0, 0),
								mapTypeId	: google.maps.MapTypeId.ROADMAP
							};


							// create map	        	
							var map = new google.maps.Map( $el[0], args);


							// add a markers reference
							map.markers = [];


							// add markers
							$markers.each(function(){

								add_marker( $(this), map );

							});


							// center map
							center_map( map );


							// return
							return map;

						}

						/*
						*  add_marker
						*
						*  This function will add a marker to the selected Google Map
						*
						*  @type	function
						*  @date	8/11/2013
						*  @since	4.3.0
						*
						*  @param	$marker (jQuery element)
						*  @param	map (Google Map object)
						*  @return	n/a
						*/

						function add_marker( $marker, map ) {

							// var
							var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );

							// create marker
							var marker = new google.maps.Marker({
								position	: latlng,
								map			: map
							});

							// add to array
							map.markers.push( marker );

							// if marker contains HTML, add it to an infoWindow
							if( $marker.html() )
							{
								// create info window
								var infowindow = new google.maps.InfoWindow({
									content		: $marker.html()
								});

								// show info window when marker is clicked
								google.maps.event.addListener(marker, 'click', function() {

									infowindow.open( map, marker );

								});
							}

						}

						/*
						*  center_map
						*
						*  This function will center the map, showing all markers attached to this map
						*
						*  @type	function
						*  @date	8/11/2013
						*  @since	4.3.0
						*
						*  @param	map (Google Map object)
						*  @return	n/a
						*/

						function center_map( map ) {

							// vars
							var bounds = new google.maps.LatLngBounds();

							// loop through all markers and create bounds
							$.each( map.markers, function( i, marker ){

								var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );

								bounds.extend( latlng );

							});

							// only 1 marker?
							if( map.markers.length == 1 )
							{
								// set center of map
								map.setCenter( bounds.getCenter() );
								map.setZoom( 16 );
							}
							else
							{
								// fit to bounds
								map.fitBounds( bounds );
							}

						}

						/*
						*  document ready
						*
						*  This function will render each map when the document is ready (page has loaded)
						*
						*  @type	function
						*  @date	8/11/2013
						*  @since	5.0.0
						*
						*  @param	n/a
						*  @return	n/a
						*/
						// global var
						var map = null;

						$(document).ready(function(){

							$('.acf-map').each(function(){

								// create map
								map = new_map( $(this) );

							});

						});

					})(jQuery);
					</script>                
					</div>
					<?php endif; ?>      

					<?php if( $contact_name ): ?>
						<p>Contact: <?php echo $contact_name; ?></p>
					<?php endif; ?> 

				<?php endwhile; #contact_details ?>

			<!--</ul>-->

			<?php if( $biography ) : 
				echo "</div></div>"; 
			endif; ?>

			</p>			
			<?php endif; #contact_details  ?>



	<?php endif; #$basic_group == 1) && ($enhanced_group == 0	

	if ( ($basic_group == 1) && ($enhanced_group == 1) ) :
		echo 'Sorry, there is no profile for this company. Here are some profiles for similar companies:';
		#echo $all_meta_for_user['employer_registration_industry'][0];
		#select all users where employer_registration_industry = 
		$similar_industries = get_field('employer_registration_industry', 'user_'.$cID);

		$args = array(
			#'role__in'		=> array('employer', 'subscriber'),
			'role__in'		=> array('customer', 'subscriber'),
			'role__not_in'	=> array('administrator'),
			'exclude'		=> array(1),
			'orderby'		=> 'user_nicename',
			'order'			=> 'ASC',
			'exclude'		=> array($cID), #remove current company from the list
			'meta_query'	=>
			array(
				array(
					'relation' => 'AND',

					array(
						'key' => 'employer_registration_industry',
						#'value' => $all_meta_for_user['employer_registration_industry'][0],
						'value' => $similar_industries,
						'compare' => "="
					),
					array(
						'key' => 'activate_membership',
						'value' => 'Yes',
						'compare' => "="
					)	

				)
			)

		);
		$users = get_users($args);
		if ( $users ) :
			foreach ($users as $user) : 
				$company_name = get_field('company_name', 'user_'.$user->ID);
				$hyphenate_company_name = str_replace(" ", "-", $company_name); 
				?>
				<a class="enhanced-alternate-company" href="<?php echo esc_url( home_url( '/' ) ).'companies/details/?company='.$hyphenate_company_name.'&cid='.$user->ID; ?>" title="View the full details on <?php echo $company_name; ?> &raquo;"><?php echo $company_name ?> </a>
				<?php
			endforeach;
		endif; wp_reset_query(); #endif $users		
		?>

		<br>

		<p>Or find out the following information about <?php echo get_field('company_name', 'user_'.$cID);?>: </p>


		<a class="button" href="#salary">Salary</a>

		<a class="button" href="#advice">Interview Tips</a>

		<a class="button" href="#hours">Working Hours</a>

		<a class="button" href="#best_worst">Best &amp; Worst</a>


	<?php endif; #endif $basic_group == 1) && ($enhanced_group == 1 ?>    

	</div><!-- enhanced tabs panel -->

	<div class="tabs-panel" id="salary"> 

		<!-- Nav tabs -->
		<ul class="tabs" data-tabs data-deep-link="true" data-deep-link-smudge="true" data-deep-link-smudge="500" id="salary-tabs" >
			<li class="tabs-title is-active"><a href="#graduate_salary" aria-controls="graduate_salary" role="tab">Graduate Salaries</a></li>
			<li class="tabs-title"><a href="#apprentice_salary" aria-controls="apprentice_salary" role="tab">Apprentice Salaries</a></li>
		</ul>

		<!-- Tab panes -->
		<div class="tabs-content" data-tabs-content="salary-tabs">

			<div class="tabs-panel is-active" id="graduate_salary">

				<div class="padded">
				<canvas id="graduate-bar-chart" width="800" height="300"></canvas>

				<?php		
				#check for form data	
				if ( !empty($_GET["company"]) ):


					#get the form data
					$company_id = $_GET["cid"];	

					#we can now covert this user ID to a company name
					$all_meta_for_user = get_user_meta( $company_id );
					$company_name = $all_meta_for_user['company_name'][0];
					if ($company_name):
						#echo '<h4>'.$company_name.'</h4>';
					endif;						

					#run the loop a second time otherwise pagination breaks the results
					#wp_reset_query();
					$graduate_reviews = get_posts(array(
						'post_type'		=> 'graduate_reviews',
						'post_status'	=> 'publish',
						'posts_per_page'=> '-1',			
						'meta_key' 	=> 'graduate_review_employer',
						'meta_value' => $company_name,
						'meta_compare' => '='
					));		

					#create our array
					$grad_salary = array();

					foreach($graduate_reviews as $review) : 

						$grad_starting_salary = get_field('graduate_review_starting_salary', $review->ID);	

						#pass the values to the array
						$grad_salary[] = $grad_starting_salary;	

					endforeach;
					wp_reset_query();

					#debug to check the right no of jobs are shown
					$paged = get_query_var('paged') ? get_query_var('paged') : 1;
					$job_args = array(
						'post_type'		=> 'jobs',
						'post_status'	=> 'publish',
						'orderby'		=> 'post_date',
						'order'			=> 'DESC',		
						'posts_per_page'=> '-1',
						'paged' 		=> $paged,
						'meta_query'	=> array(

							array(
								'key'	  	=> 'dynamic_filled_company',
								'value'	  	=> $company_name,
								'compare' 	=> '=',
							),

						),			
					);				
					$wp_query = new WP_Query($job_args);
					if ( $wp_query->have_posts() ) :
						while ( have_posts() ) : the_post(); 
						$post_id = get_the_ID()	;
						#echo $post_id.'<br>';
						endwhile; #wp_reset_query(); #removed as stops pagination
						#echo '<hr>';
					else:
						#echo '<p>No jobs available</p>';			
					endif;		
					#$salary = (array_merge($grad_salary,$app_salary));

					#create an associative array
					$arr = array_count_values($grad_salary);
					#create an array to base the total on
					$total = array();
					foreach ($arr as $key => $value) :
						#echo 'Key: '.$key.' Value: '.$value.'<br>';
						?>

						<?php
						#split the salary so we can query a min and max	
						$salary = explode(" - ", $key);
						#echo $salary[0];
						#echo $salary[1];								

						#now list each job within the salary range
						$paged = get_query_var('paged') ? get_query_var('paged') : 1;
						$job_args = array(
							'post_type'		=> 'jobs',
							'post_status'	=> 'publish',
							'orderby'		=> 'post_date',
							'order'			=> 'DESC',		
							'posts_per_page'=> '-1',
							'paged' 		=> $paged,
							'meta_query'	=> array(
								'relation'		=> 'AND',
								array(
									'key'	  	=> 'dynamic_filled_company',
									'value'	  	=> $company_name,
									'compare' 	=> '=',
								),
								array(
									'key'	  	=> 'salary',
									'value'	  	=> array($salary[0], $salary[1]),
									'compare' 	=> 'BETWEEN',
								),
							),			
						);				
						$wp_query = new WP_Query($job_args);
						if ( $wp_query->have_posts() ) :
							while ( have_posts() ) : the_post(); 
							$post_id = get_the_ID()	;
							$career = get_field('dynamic_filled_careers', $post_id);
							?>
							<!--	<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?> <?php if( $career ):  echo '/ '. implode(', ', $career); endif; ?></a><br/>-->
							<?php
							endwhile; #wp_reset_query(); #removed as stops pagination
							echo '<hr/>';
						else:
							//echo '<p>No jobs available</p>';			
						endif;	

						#add to the array
						$total[] = $value;

					endforeach; #endforeach $arr
					?>

					<?php 
					$i = 0; 
					foreach ($arr as $key => $value): if($key) { 
						if($value) {
							$i++;
						}
					}
					endforeach; 

					if ($i > 0): ?>

						<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.min.js"></script>
						<script>

						$("#graduate-bar-chart").show(); 

							// Bar chart
							Chart.defaults.global.defaultFontFamily='"Lato",sans-serif';
							Chart.defaults.global.defaultFontColor="#231F20";

							new Chart(document.getElementById("graduate-bar-chart"), {
								type: 'bar',
								data: {
								 // labels: ["1", "2", "3"],
								  labels: [<?php foreach ($arr as $key => $value): if($key) { echo '"'.$key.'",'; } endforeach; ?>],
								  datasets: [
									{
									  label: "",
									  backgroundColor: [<?php foreach ($arr as $key => $value): if($key) { echo '"#00BDD6",'; } endforeach; ?>],
									  //data: [25,30, 45]
									  data: [<?php foreach ($arr as $key => $value): if($key) { echo '"'.$value.'",'; } endforeach; ?>]
									}
								  ]
								},

								options: {
								  legend: { display: false },
								  title: {
									display: true,
									fontSize: 16,
									text: 'Salary ranges for <?php echo $company_name; ?>'
								  }
								}
							});


						</script>

					<?php else: ?>
						<script>
						$("#graduate-bar-chart").hide(); 
						</script>
					<?php endif; #endif $i ?>

					<?php		
					echo '<p>Based on ' . array_sum($total) . ' reviews</p>';

					# we need to get a list of all careers from the options page
					# reset choices
					$field['careers'] = array();

					# get the textarea value from options page without any formatting
					$careers = get_field('careers', 'option', false);

					# explode the value so that each line is a new array piece
					$careers = explode("\n", $careers);

					# remove any unwanted white space
					$careers = array_map('trim', $careers);

					# loop through array and add to field 'choices'
					if( is_array($careers) ) :

						foreach( $careers as $career ) :

							if($career === '- Select Career -') continue; #skip first item   				
							$field['careers'][ $career ] = '<option value="'.$career.'">'.$career.'</option>';

						endforeach;

					endif;

					# convert array into comma-separated string
					$careers = implode('', $field['careers'] );

					echo '<p>Please use the below to filter the jobs by career sector:</p>';
					echo '<form name="careers">';
						#echo '<select name="career" onChange="handleOnChange(this);">';
						echo '<select name="career" id="career">';
						echo $careers;
						echo '</select>';
					echo '</form>';

				endif;
				?>	

				<!-- Ajax Jobs Results -->
				<div id="graduate_jobs_content">
				</div>
				<!-- /Ajax Jobs Results -->	

				<script>
				jQuery('select').on('change', function() {

					var cid = <?php echo $company_id; ?>;
					var car = this.value;
					//alert("Company ID = "+cid);
					//alert("Career = " +car);

					jQuery.ajax({
						type        : "POST",
						data        : {cid: cid, car: car},
						dataType    : "html",
						url         : "<?php echo get_bloginfo('template_directory'); ?>/average-salary-jobs-loop.php",

						success     : function(data) {
							//alert(this.data);
							jQuery("#graduate_jobs_content").html(data);
							console.log("success!");

						},
						error       : function(xhr, status, error) {
							var err = eval("(" + xhr.responseText + ")");
							alert(err.Message);
						}
					});
				});
				</script>	
				</div><!-- padded -->

			</div><!--#graduate_salary panel -->

			<div class="tabs-panel" id="apprentice_salary">

				<div class="padded">

				<canvas id="apprentice-bar-chart" width="800" height="300"></canvas>

				<?php		
				#check for form data	
				if ( !empty($_GET["company"]) ):


					#get the form data
					$company_id = $_GET["cid"];	

					#we can now covert this user ID to a company name
					$all_meta_for_user = get_user_meta( $company_id );
					$company_name = $all_meta_for_user['company_name'][0];
					if ($company_name):
						#echo '<h4>'.$company_name.'</h4>';
					endif;						

					#run the loop a second time otherwise pagination breaks the results
					#wp_reset_query();
					$apprentice_reviews = get_posts(array(
						'post_type'		=> 'apprentice_reviews',
						'post_status'	=> 'publish',
						'posts_per_page'=> '-1',			
						'meta_key' 	=> 'apprentice_review_employer',
						'meta_value' => $company_name,
						'meta_compare' => '='
					));		

					#create our array
					$app_salary = array();

					foreach($apprentice_reviews as $review) : 

						$app_starting_salary = get_field('apprentice_review_starting_salary', $review->ID);	

						#pass the values to the array
						$app_salary[] = $app_starting_salary;	

					endforeach;
					wp_reset_query();

					#debug to check the right no of jobs are shown
					$paged = get_query_var('paged') ? get_query_var('paged') : 1;
					$job_args = array(
						'post_type'		=> 'jobs',
						'post_status'	=> 'publish',
						'orderby'		=> 'post_date',
						'order'			=> 'DESC',		
						'posts_per_page'=> '-1',
						'paged' 		=> $paged,
						'meta_query'	=> array(

							array(
								'key'	  	=> 'dynamic_filled_company',
								'value'	  	=> $company_name,
								'compare' 	=> '=',
							),

						),			
					);				
					$wp_query = new WP_Query($job_args);
					if ( $wp_query->have_posts() ) :
						while ( have_posts() ) : the_post(); 
						$post_id = get_the_ID()	;
						#echo $post_id.'<br>';
						endwhile; #wp_reset_query(); #removed as stops pagination
						#echo '<hr>';
					else:
						#echo '<p>No jobs available</p>';			
					endif;		
					#$salary = (array_merge($grad_salary,$app_salary));

					#create an associative array
					$arr = array_count_values($app_salary);
					#create an array to base the total on
					$total = array();
					foreach ($arr as $key => $value) :
						#echo 'Key: '.$key.' Value: '.$value.'<br>';
						?>

						<?php
						#split the salary so we can query a min and max	
						$salary = explode(" - ", $key);
						#echo $salary[0];
						#echo $salary[1];								

						#now list each job within the salary range
						$paged = get_query_var('paged') ? get_query_var('paged') : 1;
						$job_args = array(
							'post_type'		=> 'jobs',
							'post_status'	=> 'publish',
							'orderby'		=> 'post_date',
							'order'			=> 'DESC',		
							'posts_per_page'=> '-1',
							'paged' 		=> $paged,
							'meta_query'	=> array(
								'relation'		=> 'AND',
								array(
									'key'	  	=> 'dynamic_filled_company',
									'value'	  	=> $company_name,
									'compare' 	=> '=',
								),
								array(
									'key'	  	=> 'salary',
									'value'	  	=> array($salary[0], $salary[1]),
									'compare' 	=> 'BETWEEN',
								),
							),			
						);				
						$wp_query = new WP_Query($job_args);
						if ( $wp_query->have_posts() ) :
							while ( have_posts() ) : the_post(); 
							$post_id = get_the_ID()	;
							$career = get_field('dynamic_filled_careers', $post_id);
							?>
							<!--	<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?> <?php if( $career ):  echo '/ '. implode(', ', $career); endif; ?></a><br/>-->
							<?php
							endwhile; #wp_reset_query(); #removed as stops pagination
							echo '<hr/>';
						else:
							//echo '<p>No jobs available</p>';			
						endif;	

						#add to the array
						$total[] = $value;

					endforeach; #endforeach $arr
					?>

					<?php 
					$i = 0; 
					foreach ($arr as $key => $value): if($key) { 
						if($value) {
							$i++;
						}
					}
					endforeach; 

					if ($i > 0): ?>

						<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.min.js"></script>
						<script>

						$("#apprentice-bar-chart").show(); 

							// Bar chart
							Chart.defaults.global.defaultFontFamily='"Lato",sans-serif';
							Chart.defaults.global.defaultFontColor="#231F20";

							new Chart(document.getElementById("apprentice-bar-chart"), {
								type: 'bar',
								data: {
								 // labels: ["1", "2", "3"],
								  labels: [<?php foreach ($arr as $key => $value): if($key) { echo '"'.$key.'",'; } endforeach; ?>],
								  datasets: [
									{
									  label: "",
									  backgroundColor: [<?php foreach ($arr as $key => $value): if($key) { echo '"#00BDD6",'; } endforeach; ?>],
									  //data: [25,30, 45]
									  data: [<?php foreach ($arr as $key => $value): if($key) { echo '"'.$value.'",'; } endforeach; ?>]
									}
								  ]
								},

								options: {
								  legend: { display: false },
								  title: {
									display: true,
									fontSize: 16,
									text: 'Salary ranges for <?php echo $company_name; ?>'
								  }
								}
							});


						</script>

					<?php else: ?>
						<script>
						$("#apprentice-bar-chart").hide(); 
						</script>
					<?php endif; #endif $i ?>

					<?php		
					echo '<p>Based on ' . array_sum($total) . ' reviews</p>';

					# we need to get a list of all careers from the options page
					# reset choices
					$field['careers'] = array();

					# get the textarea value from options page without any formatting
					$careers = get_field('careers', 'option', false);

					# explode the value so that each line is a new array piece
					$careers = explode("\n", $careers);

					# remove any unwanted white space
					$careers = array_map('trim', $careers);

					# loop through array and add to field 'choices'
					if( is_array($careers) ) :

						foreach( $careers as $career ) :

							if($career === '- Select Career -') continue; #skip first item   				
							$field['careers'][ $career ] = '<option value="'.$career.'">'.$career.'</option>';

						endforeach;

					endif;

					# convert array into comma-separated string
					$careers = implode('', $field['careers'] );

					echo '<p>Please use the below to filter the jobs by career sector:</p>';
					echo '<form name="careers">';
						#echo '<select name="career" onChange="handleOnChange(this);">';
						echo '<select name="career" id="career">';
						echo $careers;
						echo '</select>';
					echo '</form>';

				endif;
				?>	

				<!-- Ajax Jobs Results -->
				<div id="apprentice_jobs_content">
				</div>
				<!-- /Ajax Jobs Results -->	

				<script>
				jQuery('select').on('change', function() {

					var cid = <?php echo $company_id; ?>;
					var car = this.value;
					//alert("Company ID = "+cid);
					//alert("Career = " +car);

					jQuery.ajax({
						type        : "POST",
						data        : {cid: cid, car: car},
						dataType    : "html",
						url         : "<?php echo get_bloginfo('template_directory'); ?>/average-salary-jobs-loop.php",

						success     : function(data) {
							//alert(this.data);
							jQuery("#apprentice_jobs_content").html(data);
							console.log("success!");

						},
						error       : function(xhr, status, error) {
							var err = eval("(" + xhr.responseText + ")");
							alert(err.Message);
						}
					});
				});
				</script>				
			
				</div><!-- padded -->

			</div><!--#apprentice_salary panel -->	

		</div><!--#tabs-content -->  

	</div><!-- salary tabs panel -->



	<div class="tabs-panel" id="hours">
<?php
#check for form data	
if ( !empty($_GET["company"]) ):

	#get the form data
	$company_id = $_GET["cid"];	
	#echo $company_id;

	#we can now covert this user ID to a company name
	$all_meta_for_user = get_user_meta( $company_id );
	$company_name = $all_meta_for_user['company_name'][0];
	if ($company_name):
		echo '<h4>'.$company_name.'</h4>';
	endif;		

	#run the loop a second time otherwise pagination breaks the results
	wp_reset_query();
	$reviews = get_posts(array(
		'post_type'		=> array('graduate_reviews','apprentice_reviews'),
		'post_status'	=> 'publish',
		'posts_per_page'=> '-1',
		
		'meta_query'	=> array(
			'relation'		=> 'OR',
			array(
				'key' 	=> 'graduate_review_employer',
				'value' => $company_name,
				'compare' => '='
			),
			array(
				'key' 	=> 'apprentice_review_employer',
				'value' => $company_name,
				'compare' => '='
			)
		)			
		
	));		
	$count = count($reviews);
	#echo $count;
	#create our array
	$grad_start_time = array();
	$grad_end_time = array();

	$app_start_time = array();
	$app_end_time = array();
	foreach($reviews as $review) : 

		$grad_start = get_field('graduate_review_what_time_do_you_normally_start_and_finish_work_start_time', $review->ID);	
		$grad_end = get_field('graduate_review_what_time_do_you_normally_start_and_finish_work_finish_time', $review->ID);

		$app_start = get_field('apprentice_review_what_time_do_you_normally_start_and_finish_work_start', $review->ID);
		$app_end = get_field('apprentice_review_what_time_do_you_normally_start_and_finish_work_finish', $review->ID);

		#pass the values to the array
		$grad_start_time[] = $grad_start;	
		$grad_end_time[] = $grad_end;

		$app_start_time[] = $app_start;	
		$app_end_time[] = $app_end;	

	endforeach;
		
	echo '<pre>';
	#print_r($grad_start_time);
	echo '</pre>';		

	$start_time = (array_merge($grad_start_time,$app_start_time));
	$end_time = (array_merge($grad_end_time,$app_end_time));
	
	echo '<pre>';
	#print_r(array_count_values($start_time));
	#print_r(array_count_values($end_time));
	echo '</pre>';			
	
	#create an associative array
	$start = array_count_values($start_time);
	#create an array to base the total on
	$start_total = array();

	foreach ($start as $key => $value) :
		#echo 'Key: '.$key.' Value: '.$value.'<br>';

		if($key) {
			#echo 'There are '.$value.' reviews, who started at: '.$key.' am.<br>';
		} else {
			# no salary number available so just leave it
		}	
		
		#add to the array
		$start_total[] = $value;
		
	endforeach; #endforeach $start
		
	#need to get the most common start time
	$get_start_time = array_count_values($start_time);
	arsort($get_start_time);
	$ave_start_time = array_slice(array_keys($get_start_time), 0, 1, true); #1 limits the no. of results returned
	if ($ave_start_time[0] !=''):
		echo '<p>Average start time: '.$ave_start_time[0].'</p>';
		$calculated_start_time = $ave_start_time[0];
	else: 
		#use the next most popular option is the first one is blank
		$ave_start_time = array_slice(array_keys($get_start_time), 1, 1, true);	
		echo '<p>Average start time: '.$ave_start_time[1].'</p>';
		$calculated_start_time = $ave_start_time[1];
	endif;
		

	
	#create an associative array
	$end = array_count_values($end_time);
	#create an array to base the total on
	$end_total = array();

	foreach ($end as $key => $value) :
		#echo 'Key: '.$key.' Value: '.$value.'<br>';

		if($key) {
			#echo 'There are '.$value.' reviews, who finish at: '.$key.' am.<br>';
		} else {
			# no salary number available so just leave it
		}	
		
		#add to the array
		$end_total[] = $value;
		
	endforeach; #endforeach $end		


	#need to get the most common end time
	$get_end_time = array_count_values($end_time);
	arsort($get_end_time);
	$ave_end_time = array_slice(array_keys($get_end_time), 0, 1, true); #1 limits the no. of results returned	
	if ($ave_end_time[0] !=''):
		echo '<p>Average end time: '.$ave_end_time[0].'</p>';
		$calculated_end_time = $ave_end_time[0];
	else: 
		#use the next most popular option is the first one is blank
		$ave_end_time = array_slice(array_keys($get_end_time), 1, 1, true);
		echo '<p>Average end time: '.$ave_end_time[1].'</p>';
		$calculated_end_time = $ave_end_time[1];
	endif;	
		
		
	$start = strtotime($calculated_start_time);
	$finish = strtotime($calculated_end_time);
	$time_difference = round(abs($finish - $start) / 3600,2);

	echo '<p>Average number of working hours: '.$time_difference.'</p>';		
		
	echo '<p>Based on ' . array_sum($start_total) . ' reviews</p>';

	# we need to get a list of all careers from the options page
	# reset choices
	$field['careers'] = array();

	# get the textarea value from options page without any formatting
	$careers = get_field('careers', 'option', false);

	# explode the value so that each line is a new array piece
	$careers = explode("\n", $careers);

	# remove any unwanted white space
	$careers = array_map('trim', $careers);

	# loop through array and add to field 'choices'
	if( is_array($careers) ) :

		foreach( $careers as $career ) :

			if($career === '- Select Career -') continue; #skip first item   
			$field['careers'][ $career ] = '<option value="'.$career.'">'.$career.'</option>';

		endforeach;

	endif;


	#$newStartTime1 = str_replace(":", ".", $ave_start_time[0]);
		
	if ($ave_start_time[0] !=''):	
		$newStartTime1 = str_replace(":", ".", $ave_start_time[0]); 
	else:
		#use the next most popular option is the first one is blank
		$newStartTime1 = str_replace(":", ".", $ave_start_time[1]); 
	endif;			
		
	$newStartTime2 = str_replace("30", "5", $newStartTime1); 
		
	if ($ave_end_time[0] !=''):	
		$newEndTime1 = str_replace(":", ".", $ave_end_time[0]); 
	else:
		#use the next most popular option is the first one is blank
		$newEndTime1 = str_replace(":", ".", $ave_end_time[1]); 
	endif;		
		
	$newEndTime2 = str_replace("30", "5", $newEndTime1); 

	if($newEndTime2 && $newStartTime2): ?>

	<div class="time-graph">

		<div class="time-dot earlier-start-time" data-time="7"><br>Earliest&nbsp;start&nbsp;time</div>
		<div class="time-dot latest-end-time" data-time="19.5"><br>Latest&nbsp;finish</div>


		<div class="average-start-and-end" data-starttime="<?php echo $newStartTime2; ?>" data-endtime="<?php echo $newEndTime2; ?>">Average start and end</div>

		<div class="line-hour-bottom-div">
			<span class="range-hour-1">1am</span>
			<span class="range-hour-2">2am</span>
			<span class="range-hour-3">3am</span>
			<span class="range-hour-4">4am</span>
			<span class="range-hour-5">5am</span>
			<span class="range-hour-6">6am</span>
			<span class="range-hour-7">7am</span>
			<span class="range-hour-8">8am</span>
			<span class="range-hour-9">9am</span>
			<span class="range-hour-10">10am</span>
			<span class="range-hour-11">11am</span>
			<span class="range-hour-12">12pm</span>
			<span class="range-hour-13">13pm</span>
			<span class="range-hour-14">14pm</span>
			<span class="range-hour-15">15pm</span>
			<span class="range-hour-16">16pm</span>
			<span class="range-hour-17">17pm</span>
			<span class="range-hour-18">18pm</span>
			<span class="range-hour-19">19pm</span>
			<span class="range-hour-20">20pm</span>
			<span class="range-hour-21">21pm</span>
			<span class="range-hour-22">22pm</span>
			<span class="range-hour-23">23pm</span>
			<span class="range-hour-24">24pm</span>
		</div>

	</div><!--time-graph -->
	<?php endif; #endif $newEndTime2 ?>
<br>
<br>
	<?php
	# convert array into comma-separated string
	$careers = implode('', $field['careers'] );

	echo '<p>Please use the below to filter the reviews by career sector:</p>';
	echo '<form name="careers">';
		#echo '<select name="career" onChange="handleOnChange(this);">';
		echo '<select name="career" id="career">';
		echo $careers;
		echo '</select>';
	echo '</form>';

endif;
?>	

<!-- Ajax Reviews Results -->
<div id="reviews_content">
</div>
<!-- /Ajax Reviews Results -->	

<script>
jQuery('select').on('change', function() {

	var cid = <?php echo $company_id; ?>;
	var car = this.value;
	//alert("Company ID = "+cid);
	//alert("Career = " +car);

	jQuery.ajax({
		type        : "POST",
		data        : {uid: cid, car: car},
		dataType    : "html",
		url         : "<?php echo get_bloginfo('template_directory'); ?>/average-salary-reviews-loop.php",

		success     : function(data) {
			//alert(this.data);
			jQuery("#reviews_content").html(data);
			console.log("success!");

		},
		error       : function(xhr, status, error) {
			var err = eval("(" + xhr.responseText + ")");
			alert(err.Message);
		}
	});
});
</script>	

<script>
$(document).ready(function() {

	var timeDot = $(".time-dot");

	$(".time-dot").each(function() {
	var thisTime = $(this).data("time");

	var timePos = thisTime * 4.16666;

	$(this).css("left", timePos + "%");

	});

	var averageStartandEndTime = $(".average-start-and-end");

	var averageStartTime = $(".average-start-and-end").data("starttime");
	var averageEndTime = $(".average-start-and-end").data("endtime");

	var averageStartTimeCalc = averageStartTime * 4.16666;
	var averageEndTimeCalc = averageEndTime * 4.16666;

	averageStartandEndTime.css("left", averageStartTimeCalc + "%").css("width", (averageEndTimeCalc - averageStartTimeCalc) + "%");

});
</script>
	</div><!-- hours tabs panel -->
	
    <div class="tabs-panel" id="advice">

<?php

$company_id = $_GET["cid"];
#echo '<h3>'.$company_id.'</h3>';
#echo '<h3>'.$clean_career.'</h3>';

$all_meta_for_user = get_user_meta( $company_id );
$company_name = $all_meta_for_user['company_name'][0];
if ($company_name):
	#echo '<h1>'.$company_name.'</h1>';
endif;	

$graduate_reviews = get_posts(array(
	'post_type'		=> 'graduate_reviews',
	'post_status'	=> 'publish',
	'posts_per_page'=> '-1',
			
	'meta_query'	=> array(
		'relation'		=> 'AND',
		array(
			'key' 	=> 'graduate_review_employer',
			'value' => $company_name,
			'compare' => '='
		)
	)			
			
));		

#create our array
#$grad_ids = array();
if ($graduate_reviews):

	foreach($graduate_reviews as $graduate) : 

		#pass the values to the array
		#$grad_ids[] = $graduate->ID;
		$graduate_review_application_advice = get_field('graduate_review_application_advice',$graduate->ID);
		$graduate_review_interview_advice = get_field('graduate_review_interview_advice',$graduate->ID);
		if ($graduate_review_application_advice):
			echo '<p><strong>Application Advice:</strong> '.$graduate_review_application_advice.'</p>';
		endif;
		if ($graduate_review_interview_advice):
			echo '<p><strong>Interview Advice:</strong> '.$graduate_review_interview_advice.'</p>';
		endif;
		if ($graduate_review_application_advice || $graduate_review_interview_advice):
	
		?>
        <a href="<?php echo get_the_permalink( $graduate->ID ); ?>" title="<?php echo get_the_title( $graduate->ID ); ?>"><?php echo get_the_title( $graduate->ID ); ?></a><br>
        <a href="<?php echo get_the_permalink( $graduate->ID ); ?>" title="<?php echo get_the_title( $graduate->ID ); ?>" class="">Read the full review &raquo;</a><br>
        <hr>
        <br>
    	<?php endif; ?>
		<?php

	endforeach;

else:

	$graduate_reviews = '0';

endif;
wp_reset_query();

$apprentice_reviews = get_posts(array(
	'post_type'		=> 'apprentice_reviews',
	'post_status'	=> 'publish',
	'posts_per_page'=> '-1',
			
	'meta_query'	=> array(
		'relation'		=> 'AND',
		array(
			'key' 	=> 'apprentice_review_employer',
			'value' => $company_name,
			'compare' => '='
		),
		array(
			'key' 	=> 'apprentice_review_industry',
			'value' => $career,
			'compare' => 'LIKE'
		)
	)			
			
));		

#create our array
#$app_ids = array();
if ($apprentice_reviews):

	foreach($apprentice_reviews as $apprentice) : 

		#pass the values to the array
		#$app_ids[] = $apprentice->ID;
		$apprentice_review_application_advice = get_field('graduate_review_application_advice',$apprentice->ID);
		$apprentice_review_interview_advice = get_field('graduate_review_interview_advice',$apprentice->ID);
		if ($graduate_review_application_advice):
			echo '<p>Application Advice: '.$apprentice_review_application_advice.'</p>';
		endif;
		if ($graduate_review_interview_advice):
			echo '<p>Interview Advice: '.$apprentice_review_interview_advice.'</p>';
		endif;		
		?>		
		<a href="<?php echo get_the_permalink( $apprentice->ID ); ?>" title="<?php echo get_the_title( $apprentice->ID ); ?>"><?php echo get_the_title( $apprentice->ID ); ?></a>
        <a href="<?php echo get_the_permalink( $apprentice->ID ); ?>" title="<?php echo get_the_title( $apprentice->ID ); ?>" class="button">View &raquo;</a>
		<?php

	endforeach;

else:

	$apprentice_reviews = '0';

endif;
wp_reset_query();

if ( ($graduate_reviews == '0') && ($apprentice_reviews == '0') ):

	echo '<p>Unfortunately, there are no reviews for '.$company_name.' within '.$career.'</p>';

endif;
?>


	</div><!-- advice tabs panel -->	
    
    
    <div class="tabs-panel" id="best_worst">
<?php
	
#get the form data
$company_id = $_GET["cid"];	

#echo '<h3>'.$company_id.'</h3>';
#echo '<h3>'.$clean_career.'</h3>';

$all_meta_for_user = get_user_meta( $company_id );
$company_name = $all_meta_for_user['company_name'][0];
if ($company_name):
	#echo '<h1>'.$company_name.'</h1>';
endif;	

$graduate_reviews = get_posts(array(
	'post_type'		=> 'graduate_reviews',
	'post_status'	=> 'publish',
	'posts_per_page'=> '-1',
			
	'meta_query'	=> array(
		'relation'		=> 'AND',
		array(
			'key' 	=> 'graduate_review_employer',
			'value' => $company_name,
			'compare' => '='
		)
	)			
			
));		

#create our array
#$grad_ids = array();
if ($graduate_reviews):

	foreach($graduate_reviews as $graduate) : 

		#pass the values to the array
		#$grad_ids[] = $graduate->ID;
		$graduate_review_what_are_the_best_and_worst_things_about_your_company_best = get_field('graduate_review_what_are_the_best_and_worst_things_about_your_company_best',$graduate->ID);
		$graduate_review_what_are_the_best_and_worst_things_about_your_company_worst = get_field('graduate_review_what_are_the_best_and_worst_things_about_your_company_worst',$graduate->ID);
		if ($graduate_review_what_are_the_best_and_worst_things_about_your_company_best):
			echo '<p><strong>Best:</strong> '.$graduate_review_what_are_the_best_and_worst_things_about_your_company_best.'</p>';
		endif;
		if ($graduate_review_what_are_the_best_and_worst_things_about_your_company_worst):
			echo '<p><strong>Worst:</strong> '.$graduate_review_what_are_the_best_and_worst_things_about_your_company_worst.'</p>';
		endif;

		if ($graduate_review_what_are_the_best_and_worst_things_about_your_company_best || $graduate_review_what_are_the_best_and_worst_things_about_your_company_worst):

		?>
        <a href="<?php echo get_the_permalink( $graduate->ID ); ?>" title="<?php echo get_the_title( $graduate->ID ); ?>"><?php echo get_the_title( $graduate->ID ); ?></a><br>
        <a href="<?php echo get_the_permalink( $graduate->ID ); ?>" title="<?php echo get_the_title( $graduate->ID ); ?>" class="">Read the full review &raquo;</a><br>
        <hr>
        <br>

		<?php endif; ?>

		<?php

	endforeach;

else:

	$graduate_reviews = '0';

endif;
wp_reset_query();

$apprentice_reviews = get_posts(array(
	'post_type'		=> 'apprentice_reviews',
	'post_status'	=> 'publish',
	'posts_per_page'=> '-1',
			
	'meta_query'	=> array(
		'relation'		=> 'AND',
		array(
			'key' 	=> 'apprentice_review_employer',
			'value' => $company_name,
			'compare' => '='
		),
		array(
			'key' 	=> 'apprentice_review_industry',
			'value' => $career,
			'compare' => 'LIKE'
		)
	)			
			
));		

#create our array
#$app_ids = array();
if ($apprentice_reviews):

	foreach($apprentice_reviews as $apprentice) : 

		#pass the values to the array
		#$app_ids[] = $apprentice->ID;
		$apprentice_review_what_are_the_best_and_worst_things_about_your_company_best = get_field('apprentice_review_what_are_the_best_and_worst_things_about_your_company_best',$graduate->ID);
		$apprentice_review_what_are_the_best_and_worst_things_about_your_company_worst = get_field('graduate_review_what_are_the_best_and_worst_things_about_your_company_worst',$graduate->ID);
		if ($apprentice_review_what_are_the_best_and_worst_things_about_your_company_best):
			echo '<p>Best: '.$apprentice_review_what_are_the_best_and_worst_things_about_your_company_best.'</p>';
		endif;
		if ($apprentice_review_what_are_the_best_and_worst_things_about_your_company_worst):
			echo '<p>Worst: '.$apprentice_review_what_are_the_best_and_worst_things_about_your_company_worst.'</p>';
		endif;		
		?>		
		<a href="<?php echo get_the_permalink( $apprentice->ID ); ?>" title="<?php echo get_the_title( $apprentice->ID ); ?>"><?php echo get_the_title( $apprentice->ID ); ?></a>
        <a href="<?php echo get_the_permalink( $apprentice->ID ); ?>" title="<?php echo get_the_title( $apprentice->ID ); ?>" class="button">View &raquo;</a>
		<?php

	endforeach;

else:

	$apprentice_reviews = '0';

endif;
wp_reset_query();

if ( ($graduate_reviews == '0') && ($apprentice_reviews == '0') ):

	echo '<p>Unfortunately, there are no reviews for '.$company_name.' within '.$career.'</p>';

endif;
?>
    </div><!-- best_worst tabs panel --> 

	</div>
</div>		


	<?php
	endif;
	
endif;
?>

	<div class="sidebar-left" data-equalizer-watch="main-content">


			<div class="sidebar--header" data-equalizer-watch="main-content-headers">
				<h3>Company Details</h3> <svg class="icon icon-details"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-details"></use></svg>
			</div>

			<?php if ($logo): ?>

			<div class="sidebar-left--company-logo">
				<img src="<?php echo $logo['sizes']['medium']; ?>" alt="" />
			</div>

			<?php endif; ?>


			<div class="sidebar-left--company-details row">

				<div class="small-6 columns">Company</div>
				<div class="small-6 columns"><?php echo $company_name; ?></div>
				

				<?php if($main_address): ?>
					<div class="small-6 columns">Location</div>
					<div class="small-6 columns"><?php echo $main_address; ?></div>
				<?php endif; ?>

				<!--<div class="small-6 columns">Positions Available</div>
				<div class="small-6 columns">10 - 15</div>
				-->
				
				<?php if($company_employees): ?>
					<div class="small-6 columns">Total Employees</div>
					<div class="small-6 columns"><?php echo $company_employees; ?></div>
				<?php endif; ?>



                <?php if ($employer_registration_industry): ?>
            	<div class="small-6 columns">Industry</div>
                <div class="small-6 columns bolder">
						<?php foreach($employer_registration_industry as $industry): ?>
									<a href="<?php bloginfo('url'); ?>/industries/?industry=<?php echo $industry; ?>"><?php echo $industry; ?></a>
						<?php endforeach; ?>
                </div>
            	<?php endif; ?>

			</div><!-- row -->





			<div class="sidebar--header">
				<h3>Application Tips</h3> <svg class="icon icon-bulb"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-bulb"></use></svg>
			</div>

			<div class="sidebar-left--application-tips">

					<p>Employers constantly tell us that there is nothing that impresses them more than a graduate who has properly researched the company and really knows why they want to work there - so make sure that you tell them in your application that you have read-up all about them on TheJobCrowd and so you really know what makes that company great and why they are right for you. It will help your application, we promise!</p>

			</div> <!-- sidebar-left-key-information-->




	</div><!--sidebar-left -->









</div> <!-- main-content -->



</div> <!-- main-content -->

<?php
#get_sidebar();
get_footer();
