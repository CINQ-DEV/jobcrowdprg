<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 Template Name: Reviews Shortlist  
 */

get_header(); ?>



<div class="page-header row">

	<div class="medium-5 columns">
		<h1>Jobs Shortlist</h1>
	</div>
	
	<?php get_template_part('template-parts/page-header-search'); ?>

</div><!-- page-header -->


<div class="main-content" data-equalizer="main-content">


	<div class="main-content-main" data-equalizer-watch="main-content">

				<div class="main-content-main--breadcrumbs" data-equalizer-watch="main-content-headers">
							<?php
							if ( function_exists('yoast_breadcrumb') ) {
							yoast_breadcrumb('
							<p id="breadcrumbs">','</p>
							');
							}
							?>
					<div class="save-share">

						<!--<a href="#" class="savethis">
							<svg class="icon icon-heart"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-heart"></use></svg> Save this
						</a>-->

						<?php get_template_part('template-parts/share-button'); ?>

					</div>

				</div><!-- main-content-main-breadcrumbs -->

				<div class="padded">



<?php 
// debug_shortlist();

// get session array
$shortlist = array(0);

if( isset($_SESSION['shortlist']) && (!empty($_SESSION['shortlist'])) ) {
	$shortlist = $_SESSION['shortlist'];

	$args = array (
		'post_type' => 'jobs',
		'orderby' => 'title',			
		'order' => 'asc',
		'post__in' => $shortlist,
	);

} else {

	$args = array();

}

$wp_query = new WP_Query( $args );

if ( have_posts() ) : 
?>

    <table id="example" class="items display" cellspacing="0" width="100%">
	<thead>
    	<tr>
        	<th>Job</th>
            <th>Salary</th>
            <th>Type</th>
            <th>Deadline</th>
            <th>Remove</th>
        </tr>
    </thead>
	<tfoot>
    	<tr>
        	<th>Job</th>
            <th>Salary</th>
            <th>Type</th>  
            <th>Deadline</th>          
            <th>Remove</th>
        </tr>
    </tfoot>    
    <tbody>

	<?php 
	while ( have_posts() ) : the_post(); 

	// set selected class
	$selected = '';
	if(isset($_SESSION['shortlist'])) {
		if ( in_array($id, $_SESSION['shortlist']) ) {
			$selected = 'selected';
		}
	}

	?>
    
	<tr id="<?php the_ID(); ?>" class="item">
        <td><a href="<?php the_permalink(); ?>" title="View the full details on <?php the_title(); ?> &raquo;"><?php the_title(); ?></a></td>
        <td><?php $salary = get_field('salary'); echo number_format($salary,2); ?></td>	
        <td><?php the_field('dynamic_filled_type'); ?></td>	
        <td><?php the_field('deadline_date'); ?></td>					
        <td><a href="#" class="btn action remove" data-action="remove"><i class="fa fa-minus-circle" aria-hidden="true"></i> Remove</a></td>
	</tr>	    

	<?php endwhile; ?>

	</tbody>
	</table>
    <link href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet" />
    <script src="//code.jquery.com/jquery-1.12.4.js"></script>
    <script src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script>
	/*
    $(document).ready(function() {
        $('#example').DataTable();
    } );
	*/
	$(document).ready(function() {
		// Setup - add a text input to each footer cell
		$('#example tfoot th').not(":eq(4),:eq(5),:eq(6)").each( function () {
			var title = $('#example thead th').eq( $(this).index() ).text();
			$(this).html( '<input type="text" placeholder="Search '+title+'" />' );
		} );
	 
		// DataTable
		var table = $('#example').DataTable();
	 
		// Apply the search
		table.columns().eq( 0 ).each( function ( colIdx ) {
			if ( colIdx == 4 || colIdx == 5 || colIdx == 6 ) return;
			
			$( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () {
				table
					.column( colIdx )
					.search( this.value )
					.draw();
			} );
		} );
	} );
    </script> 


	<p class="shortlist-clear"><a class="btn btn-danger" href="#">Clear All</a></p>

<?php else : ?>

<p>No jobs selected.</p>

<?php endif; // have_posts ?>

				</div> <!-- padded -->
			
	</div><!--main-content-main -->










	<div class="sidebar-left" data-equalizer-watch="main-content222">


			<div class="sidebar--header" data-equalizer-watch="main-content-headers">
				<h3>Find reviews</h3> <svg class="icon icon-details"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-details"></use></svg>
			</div>




<form id="job-reviews-by-company" class="padded" action="<?php bloginfo('url');?>/reviews/" method="post">

	<?php
	# we need to get a list of all industries from the options page

	# reset choices
	$field['industries'] = array();
	        
	# get the textarea value from options page without any formatting
	$industries = get_field('industry', 'option', false);
	    
	# explode the value so that each line is a new array piece
	$industries = explode("\n", $industries);
	    
	# remove any unwanted white space
	$industries = array_map('trim', $industries);
	    
	# loop through array and add to field 'choices'
	if( is_array($industries) ) :
		foreach( $industries as $industry ) :
			if($industry === '- Select Industry -') continue; #skip first item    	
			#$industry2 = str_replace("&","&amp;",$industry);		
			$field['industries'][ $industry ] = '<li><label class="container"><input type="checkbox" name="industries[]" value="'.$industry.'"><span class="checkmark"></span> '.$industry.'</label></li>';
	            
		endforeach;
	endif;

	# convert array into comma-separated string
	$industries = implode('', $field['industries'] );
		echo '<h2>Reviews by Industry </h2>';
		echo '<div class="industries-wrapper">';
		echo '<ul>'.$industries.'</ul>';
		echo '</div>';
		//echo '<span class="industries-more">More <svg class="icon icon-downarrow-small"><use xlink:href="'. get_stylesheet_directory_uri() .'/img/icons.svg#icon-downarrow-small"></use></svg></span>';
		echo '<span class="industries-more">More <img src="'. get_stylesheet_directory_uri() .'/img/icon-down-arrow.png" class="more-icon-down-arrow"/></span>';
	?>
	 
	 <hr>
 
<label for="towns"><h2>Reviews by Town</h2></label>
<input id="towns" name="towns" value="" placeholder="E.g. Brighton">

	 <hr>

<h2>Reviews by Rating</h2>
<ul class="reviews-by-rating">
<li>
	<label class="radio-container container">
	<input type="radio" name="ratings" value="0"> 
			<span class="checkmark"></span>
			<div class="stars">
				<div class="stars-gold" style="width: 0%;"> &nbsp; </div>
				<div class="stars-white"> &nbsp; </div>
			</div>
	</label>
</li>

<li>
	<label class="radio-container container">
	<input type="radio" name="ratings" value="1"> 
			<span class="checkmark"></span>
			<div class="stars">
				<div class="stars-gold" style="width: 20%;"> &nbsp; </div>
				<div class="stars-white"> &nbsp; </div>
			</div>
	</label>

</li>

<li>
	<label class="radio-container container">
	<input type="radio" name="ratings" value="2"> 
			<span class="checkmark"></span>
			<div class="stars">
				<div class="stars-gold" style="width: 40%;"> &nbsp; </div>
				<div class="stars-white"> &nbsp; </div>
			</div>
	</label>

</li>

<li>
	<label class="radio-container container">
	<input type="radio" name="ratings" value="3"> 
			<span class="checkmark"></span>
			<div class="stars">
				<div class="stars-gold" style="width: 60%;"> &nbsp; </div>
				<div class="stars-white"> &nbsp; </div>
			</div>
	</label>

</li>

<li>
	<label class="radio-container container">
	<input type="radio" name="ratings" value="4"> 
			<span class="checkmark"></span>
			<div class="stars">
				<div class="stars-gold" style="width: 80%;"> &nbsp; </div>
				<div class="stars-white"> &nbsp; </div>
			</div>
	</label>

</li>

<li>
	<label class="radio-container container">
	<input type="radio" name="ratings" value="5"> 
			<span class="checkmark"></span>
			<div class="stars">
				<div class="stars-gold" style="width: 100%;"> &nbsp; </div>
				<div class="stars-white"> &nbsp; </div>
			</div>
	</label>

</li>

</ul> 

<br/>
<input value="Search Reviews" type="submit" name="submit" class="filter-reviews-button button blue block" /> 

</form> 







	</div><!--sidebar-left -->








	<div class="sidebar-right" data-equalizer-watch="main-content">

		
		<div class="sidebar--header">
			<h3>More Jobs you might be interested in</h3>
		</div>

		<div class="saved-jobs">
			<div class="saved-jobs--job">
				<strong>Senior Software Developer</strong><br>
				nucleargraduates<br>
				Full Time (Graduate Job)<br>
				Location:   <strong>Brighton</strong> <br>  
				Salary:       <strong>£29,000</strong>
			</div>
			<div class="saved-jobs--job">
				<strong>Software Architect</strong><br>
				Smith &amp; Williamson<br>
				Full Time (Graduate Job)<br>
				Location:   <strong>Brighton</strong> <br>  
				Salary:       <strong>£34,000</strong>
			</div>
		</div>



		
		<div class="sidebar--header">
			<h3>Energy and Utilities Employers</h3>
		</div>

		<div class="row padded">
				<strong>National Grid</strong>
										<div class="stars">
											<div class="stars-gold" style="width: 80%;"> &nbsp; </div>
											<div class="stars-white"> &nbsp; </div>
										</div>

		</div>

		<div class="row padded">
				<strong>Sellafield Ltd</strong>
										<div class="stars">
											<div class="stars-gold" style="width: 93%;"> &nbsp; </div>
											<div class="stars-white"> &nbsp; </div>
										</div>

		</div>





	</div><!--sidebar-right -->



</div> <!-- main-content -->

<?php
#get_sidebar();
get_footer();
