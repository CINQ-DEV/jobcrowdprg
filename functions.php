<?php

/** Various clean up functions */
require_once( 'library/cleanup.php' );

/** Required for Foundation to work properly */
require_once( 'library/foundation.php' );

/** Format comments */
require_once( 'library/class-foundationpress-comments.php' );

/** Register all navigation menus */
require_once( 'library/navigation.php' );

/** Add menu walkers for top-bar and off-canvas */
require_once( 'library/class-foundationpress-top-bar-walker.php' );
require_once( 'library/class-foundationpress-mobile-walker.php' );

/** Create widget areas in sidebar and footer */
require_once( 'library/widget-areas.php' );

/** Return entry meta information for posts */
require_once( 'library/entry-meta.php' );

/** Enqueue scripts */
require_once( 'library/enqueue-scripts.php' );

/** Add theme support */
require_once( 'library/theme-support.php' );

/** Add Nav Options to Customer */
require_once( 'library/custom-nav.php' );

/** Change WP's sticky post class */
require_once( 'library/sticky-posts.php' );

/** Configure responsive image sizes */
require_once( 'library/responsive-images.php' );

/** If your site requires protocol relative url's for theme assets, uncomment the line below */
// require_once( 'library/class-foundationpress-protocol-relative-theme-assets.php' );



#################################################################################################################################################
#### EVERYTHING BELOW IS NEW PRG CUSTOM CODE
#################################################################################################################################################


#################################################################################################################################################
#### EVERYTHING BELOW IS NEW PRG CUSTOM CODE
#################################################################################################################################################

##############################################################################
# CONTENTS
# 1) WORDPRESS
# 2) ADVANCED CUSTOM FIELDS
# 3) WOOCOMMERCE
# 4) GRAVITY FORMS
# 5) AJAX
# 6) ALERTS / CRON
# 7) MEMBERSHIP
# 8) MISC
##############################################################################

##############################################################################
# 1) WORDPRESS
##############################################################################

##########################
# CREATE NEW CUSTOM ROLES
##########################

$result = add_role( 'apprentice', __('Apprentice' ),
	array(
	
		'read' => true, 				# TRUE allows this capability
		'edit_posts' => true, 			# Allows user to edit their own posts
		'edit_pages' => false, 			# Allows user to edit pages
		'edit_others_posts' => false, 	# Allows user to edit others posts not just their own
		'create_posts' => false, 		# Allows user to create new posts
		'manage_categories' => false, 	# Allows user to manage post categories
		'publish_posts' => false, 		# Allows the user to publish, otherwise posts stays in draft mode
		'edit_themes' => false, 		# FALSE denies this capability. User can�t edit your theme
		'install_plugins' => false, 	# User cant add new plugins
		'update_plugin' => false, 		# User can�t update any plugins
		'update_core' => false 			# User cant perform core updates
	
	)
);

$result = add_role( 'student', __('Student' ),
	array(
	
		'read' => true, 				# TRUE allows this capability
		'edit_posts' => false, 			# Allows user to edit their own posts
		'edit_pages' => false, 			# Allows user to edit pages
		'edit_others_posts' => false, 	# Allows user to edit others posts not just their own
		'create_posts' => false, 		# Allows user to create new posts
		'manage_categories' => false, 	# Allows user to manage post categories
		'publish_posts' => false, 		# Allows the user to publish, otherwise posts stays in draft mode
		'edit_themes' => false, 		# FALSE denies this capability. User can�t edit your theme
		'install_plugins' => false, 	# User cant add new plugins
		'update_plugin' => false, 		# User can�t update any plugins
		'update_core' => false 			# User cant perform core updates
	
	)
);

##########################
# WOOCOMMERCE ALLOW SUBSCRIBER TO UPLOAD IMAGES
# used for enhanced profile gallery - switched to repeater to prevent subscriber needing upload function (security vulnerability)
##########################
/*
if ( current_user_can('subscriber') && !current_user_can('upload_files') )
add_action('admin_init', 'allow_subscriber_uploads');
function allow_customer_uploads() {
	$subscriber = get_role('subscriber');
	$subscriber->add_cap('upload_files');
}
*/

##########################
# CREATE JOBS CUSTOM POST 
# https://developer.wordpress.org/resource/dashicons/#images-alt2
##########################

add_action('init', 'jobs');
function jobs() {
	register_post_type(
		'jobs', array(
			'label' => 'Jobs',
			'menu_icon' => 'dashicons-businessman',
			'description' => '',
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'capability_type' => 'post',
			'map_meta_cap' => true,
			'hierarchical' => false,
			'rewrite' => array(
				'slug' => 'jobs', 
				'with_front' => true
			),
			'query_var' => true,
			'supports' => array(
				'title',
				'editor',
				'revisions',
				'author'
			),
			'labels' => array (
				'name' => 'Jobs',
				'singular_name' => 'Job',
				'menu_name' => 'Jobs',
				'add_new' => 'Add New',
				'add_new_item' => 'Add New Job',
				'edit' => 'Edit',
				'edit_item' => 'Edit Job',
				'new_item' => 'New Job',
				'view' => 'View Job',
				'view_item' => 'View Job',
				'search_items' => 'Search Jobs',
				'not_found' => 'No Jobs Found',
				'not_found_in_trash' => 'No Jobs found in Bin',
				'parent' => 'Parent',
			)
		) 
	);
}
	
###################################################
# Custom JOB Post Type Messages
###################################################
function jobs_updated_messages( $messages ) {
	global $post, $post_ID;

	$messages['jobs'] = array(
    0 => '', // Unused. Messages start at index 1.
    1 => sprintf( __('Job updated. <a href="%s">View Job</a>', 'www.thejobcrowds.com'), esc_url( get_permalink($post_ID) ) ),
    2 => __('Custom field updated.', 'www.thejobcrowds.com'),
    3 => __('Custom field deleted.', 'www.thejobcrowds.com'),
    4 => __('Job updated.', 'www.thejobcrowds.com'),
    /* translators: %s: date and time of the revision */
    5 => isset($_GET['revision']) ? sprintf( __('Job restored to revision from %s', 'www.thejobcrowds.com'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 => sprintf( __('Job published. <a href="%s">View Job</a>', 'www.thejobcrowds.com'), esc_url( get_permalink($post_ID) ) ),
    7 => __('Job saved.', 'www.thejobcrowds.com'),
    8 => sprintf( __('Job submitted. <a target="_blank" href="%s">Preview Job</a>', 'www.thejobcrowds.com'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    9 => sprintf( __('Job scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Job</a>', 'www.thejobcrowds.com'),
      // translators: Publish box date format, see http://php.net/date
      date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
    10 => sprintf( __('Job draft updated. <a target="_blank" href="%s">Preview Job</a>', 'www.thejobcrowds.com'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'jobs_updated_messages' );		

##########################
# CREATE CASE STUDIES CUSTOM POST 
##########################

add_action('init', 'case_studies');
function case_studies() {
	register_post_type(
		'case_studies', array(
			'label' => 'Case Studies',
			'menu_icon' => 'dashicons-clipboard',
			'description' => '',
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'capability_type' => 'post',
			'map_meta_cap' => true,
			'hierarchical' => false,
			'rewrite' => array(
				'slug' => 'case_studies', 
				'with_front' => true
			),
			'query_var' => true,
			'supports' => array(
				'title',
				'editor',
				'thumbnail',
				'revisions',
				'author'
			),
			'labels' => array (
				'name' => 'Case Studies',
				'singular_name' => 'Case Study',
				'menu_name' => 'Case Studies',
				'add_new' => 'Add New',
				'add_new_item' => 'Add New Case Study',
				'edit' => 'Edit',
				'edit_item' => 'Edit Case Study',
				'new_item' => 'New Case Study',
				'view' => 'View Case Study',
				'view_item' => 'View Case Study',
				'search_items' => 'Search Case Studies',
				'not_found' => 'No Case Studies Found',
				'not_found_in_trash' => 'No Case Studies found in Bin',
				'parent' => 'Parent',
			)
		) 
	);
}
	
###################################################
# Custom CASE STUDIES Post Type Messages
###################################################
function case_studies_updated_messages( $messages ) {
	global $post, $post_ID;

	$messages['case_studies'] = array(
    0 => '', // Unused. Messages start at index 1.
    1 => sprintf( __('Case Study updated. <a href="%s">View Case Study</a>', 'www.thejobcrowds.com'), esc_url( get_permalink($post_ID) ) ),
    2 => __('Custom field updated.', 'www.thejobcrowds.com'),
    3 => __('Custom field deleted.', 'www.thejobcrowds.com'),
    4 => __('Case Study updated.', 'www.thejobcrowds.com'),
    /* translators: %s: date and time of the revision */
    5 => isset($_GET['revision']) ? sprintf( __('Case Study restored to revision from %s', 'www.thejobcrowds.com'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 => sprintf( __('Case Study published. <a href="%s">View Case Study</a>', 'www.thejobcrowds.com'), esc_url( get_permalink($post_ID) ) ),
    7 => __('Case Study saved.', 'www.thejobcrowds.com'),
    8 => sprintf( __('Case Study submitted. <a target="_blank" href="%s">Preview Case Study</a>', 'www.thejobcrowds.com'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    9 => sprintf( __('Case Study scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Case Study</a>', 'www.thejobcrowds.com'),
      // translators: Publish box date format, see http://php.net/date
      date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
    10 => sprintf( __('Case Study draft updated. <a target="_blank" href="%s">Preview Case Study</a>', 'www.thejobcrowds.com'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'case_studies_updated_messages' );	

##########################
# CREATE SURVEYS CUSTOM POST 
# This is used for the survey mail import
# Uses import-recipients.php for processing
##########################

add_action('init', 'surveys');
function surveys() {
	register_post_type(
		'surveys', array(
			'label' => 'Surveys',
			'menu_icon' => 'dashicons-list-view',
			'description' => '',
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'capability_type' => 'post',
			'map_meta_cap' => true,
			'hierarchical' => false,
			'rewrite' => array(
				'slug' => 'surveys', 
				'with_front' => true
			),
			'query_var' => true,
			'supports' => array(
				'title',
				'editor',
				'revisions'
			),
			'labels' => array (
				'name' => 'Surveys',
				'singular_name' => 'Survey',
				'menu_name' => 'Surveys',
				'add_new' => 'Add New',
				'add_new_item' => 'Add New Survey',
				'edit' => 'Edit',
				'edit_item' => 'Edit Survey',
				'new_item' => 'New Survey',
				'view' => 'View Survey',
				'view_item' => 'View Survey',
				'search_items' => 'Search Surveys',
				'not_found' => 'No Surveys Found',
				'not_found_in_trash' => 'No Surveys found in Bin',
				'parent' => 'Parent',
			)
		) 
	);
}
	
###################################################
# Custom Surveys Post Type Messages
###################################################
function surveys_updated_messages( $messages ) {
	global $post, $post_ID;

	$messages['surveys'] = array(
    0 => '', // Unused. Messages start at index 1.
    1 => sprintf( __('Survey updated. <a href="%s">View Survey</a>', 'www.thejobcrowds.com'), esc_url( get_permalink($post_ID) ) ),
    2 => __('Custom field updated.', 'www.thejobcrowds.com'),
    3 => __('Custom field deleted.', 'www.thejobcrowds.com'),
    4 => __('Survey updated.', 'www.thejobcrowds.com'),
    /* translators: %s: date and time of the revision */
    5 => isset($_GET['revision']) ? sprintf( __('Survey restored to revision from %s', 'www.thejobcrowds.com'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 => sprintf( __('Survey published. <a href="%s">View Survey</a>', 'www.thejobcrowds.com'), esc_url( get_permalink($post_ID) ) ),
    7 => __('Survey saved.', 'www.thejobcrowds.com'),
    8 => sprintf( __('Survey submitted. <a target="_blank" href="%s">Preview Survey</a>', 'www.thejobcrowds.com'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    9 => sprintf( __('Survey scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Survey</a>', 'www.thejobcrowds.com'),
      // translators: Publish box date format, see http://php.net/date
      date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
    10 => sprintf( __('Survey draft updated. <a target="_blank" href="%s">Preview Survey</a>', 'www.thejobcrowds.com'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'surveys_updated_messages' );		

##########################
# CREATE GRADUATE REVIEW CUSTOM POST 
##########################
add_action('init', 'graduate_reviews');
function graduate_reviews() {
	register_post_type(
		'graduate_reviews', array(
			'label' => 'Graduate Reviews',
			'menu_icon' => 'dashicons-star-filled',
			'description' => '',
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'capability_type' => 'post',
			'map_meta_cap' => true,
			'hierarchical' => false,
			'rewrite' => array(
				'slug' => 'graduate-reviews', 
				'with_front' => true
			),
			'query_var' => true,
			'supports' => array(
				'title',
				'editor',
				'revisions',
				'comments',
				'author'
			),
			'labels' => array (
				'name' => 'Graduate Reviews',
				'singular_name' => 'Graduate Review',
				'menu_name' => 'Graduate Reviews',
				'add_new' => 'Add New',
				'add_new_item' => 'Add New Graduate Review',
				'edit' => 'Edit',
				'edit_item' => 'Edit Graduate Review',
				'new_item' => 'New Graduate Review',
				'view' => 'View Graduate Review',
				'view_item' => 'View Graduate Review',
				'search_items' => 'Search Graduate Reviews',
				'not_found' => 'No Graduate Reviews Found',
				'not_found_in_trash' => 'No Graduate Reviews found in Bin',
				'parent' => 'Parent',
			)
		) 
	);
}
	
###################################################
# Custom GRADUATE REVIEW Post Type Messages
###################################################
function graduate_reviews_updated_messages( $messages ) {
	global $post, $post_ID;

	$messages['graduate_reviews'] = array(
    0 => '', // Unused. Messages start at index 1.
    1 => sprintf( __('Graduate Review updated. <a href="%s">View Graduate Review</a>', 'www.thejobcrowds.com'), esc_url( get_permalink($post_ID) ) ),
    2 => __('Custom field updated.', 'www.thejobcrowds.com'),
    3 => __('Custom field deleted.', 'www.thejobcrowds.com'),
    4 => __('Graduate Review updated.', 'www.thejobcrowds.com'),
    /* translators: %s: date and time of the revision */
    5 => isset($_GET['revision']) ? sprintf( __('Graduate Review restored to revision from %s', 'www.thejobcrowds.com'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 => sprintf( __('Graduate Review published. <a href="%s">View Graduate Review</a>', 'www.thejobcrowds.com'), esc_url( get_permalink($post_ID) ) ),
    7 => __('Graduate Review saved.', 'www.thejobcrowds.com'),
    8 => sprintf( __('Graduate Review submitted. <a target="_blank" href="%s">Preview Graduate Review</a>', 'www.thejobcrowds.com'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    9 => sprintf( __('Graduate Review scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Graduate Review</a>', 'www.thejobcrowds.com'),
      // translators: Publish box date format, see http://php.net/date
      date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
    10 => sprintf( __('Graduate Review draft updated. <a target="_blank" href="%s">Preview Graduate Review</a>', 'www.thejobcrowds.com'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'graduate_reviews_updated_messages' );	

##########################
# CREATE APPRENTICE REVIEW CUSTOM POST 
##########################
add_action('init', 'apprentice_reviews');
function apprentice_reviews() {
	register_post_type(
		'apprentice_reviews', array(
			'label' => 'Apprentice Reviews',
			'menu_icon' => 'dashicons-star-empty',
			'description' => '',
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'capability_type' => 'post',
			'map_meta_cap' => true,
			'hierarchical' => false,
			'rewrite' => array(
				'slug' => 'apprentice-reviews', 
				'with_front' => true
			),
			'query_var' => true,
			'supports' => array(
				'title',
				'editor',
				'revisions',
				'comments',
				'author'
			),
			'labels' => array (
				'name' => 'Apprentice Reviews',
				'singular_name' => 'Apprentice Review',
				'menu_name' => 'Apprentice Reviews',
				'add_new' => 'Add New',
				'add_new_item' => 'Add New Apprentice Review',
				'edit' => 'Edit',
				'edit_item' => 'Edit Apprentice Review',
				'new_item' => 'New Apprentice Review',
				'view' => 'View Apprentice Review',
				'view_item' => 'View Apprentice Review',
				'search_items' => 'Search Apprentice Reviews',
				'not_found' => 'No Apprentice Reviews Found',
				'not_found_in_trash' => 'No Apprentice Reviews found in Bin',
				'parent' => 'Parent',
			)
		) 
	);
}
	
###################################################
# Custom APPRENTICE REVIEW Post Type Messages
###################################################
function apprentice_reviews_updated_messages( $messages ) {
	global $post, $post_ID;

	$messages['apprentice_reviews'] = array(
    0 => '', // Unused. Messages start at index 1.
    1 => sprintf( __('Apprentice Review updated. <a href="%s">View Apprentice Review</a>', 'www.thejobcrowds.com'), esc_url( get_permalink($post_ID) ) ),
    2 => __('Custom field updated.', 'www.thejobcrowds.com'),
    3 => __('Custom field deleted.', 'www.thejobcrowds.com'),
    4 => __('Apprentice Review updated.', 'www.thejobcrowds.com'),
    /* translators: %s: date and time of the revision */
    5 => isset($_GET['revision']) ? sprintf( __('Apprentice Review restored to revision from %s', 'www.thejobcrowds.com'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 => sprintf( __('Apprentice Review published. <a href="%s">View Apprentice Review</a>', 'www.thejobcrowds.com'), esc_url( get_permalink($post_ID) ) ),
    7 => __('Apprentice Review saved.', 'www.thejobcrowds.com'),
    8 => sprintf( __('Apprentice Review submitted. <a target="_blank" href="%s">Preview Apprentice Review</a>', 'www.thejobcrowds.com'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    9 => sprintf( __('Apprentice Review scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Apprentice Review</a>', 'www.thejobcrowds.com'),
      // translators: Publish box date format, see http://php.net/date
      date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
    10 => sprintf( __('Apprentice Review draft updated. <a target="_blank" href="%s">Preview Apprentice Review</a>', 'www.thejobcrowds.com'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'apprentice_reviews_updated_messages' );	

##########################
# CREATE DAY IN THE LIFE CUSTOM POST 
##########################
add_action('init', 'day_in_life');
function day_in_life() {
	register_post_type(
		'day_in_life', array(
			'label' => 'A Day In The Life Of',
			'menu_icon' => 'dashicons-smiley',
			'description' => '',
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'capability_type' => 'post',
			'map_meta_cap' => true,
			'hierarchical' => false,
			'rewrite' => array(
				'slug' => 'day-in-the-life', 
				'with_front' => true
			),
			'query_var' => true,
			'supports' => array(
				'title',
				'editor',
				'thumbnail',
				'revisions'
			),
			'labels' => array (
				'name' => 'A Day In The Life Of',
				'singular_name' => 'A Day In The Life Of',
				'menu_name' => 'A Day In The Life Of',
				'add_new' => 'Add New',
				'add_new_item' => 'Add New Day In The Life Of',
				'edit' => 'Edit',
				'edit_item' => 'Edit A Day In The Life Of',
				'new_item' => 'New Day In The Life Of',
				'view' => 'View A Day In The Life Of',
				'view_item' => 'View A Day In The Life Of',
				'search_items' => 'Search A Day In The Life Of',
				'not_found' => 'No Day In The Life Of Found',
				'not_found_in_trash' => 'No Day In The Life Of found in Bin',
				'parent' => 'Parent',
			)
		) 
	);
}
	
###################################################
# Custom DAY IN THE LIFE Post Type Messages
###################################################
function day_in_life_updated_messages( $messages ) {
	global $post, $post_ID;

	$messages['day_in_life'] = array(
    0 => '', // Unused. Messages start at index 1.
    1 => sprintf( __('A Day In The Life Of updated. <a href="%s">View A Day In The Life Of</a>', 'www.thejobcrowds.com'), esc_url( get_permalink($post_ID) ) ),
    2 => __('Custom field updated.', 'www.thejobcrowds.com'),
    3 => __('Custom field deleted.', 'www.thejobcrowds.com'),
    4 => __('A Day In The Life Of updated.', 'www.thejobcrowds.com'),
    /* translators: %s: date and time of the revision */
    5 => isset($_GET['revision']) ? sprintf( __('A Day In The Life Of restored to revision from %s', 'www.thejobcrowds.com'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 => sprintf( __('A Day In The Life Of published. <a href="%s">View A Day In The Life Of</a>', 'www.thejobcrowds.com'), esc_url( get_permalink($post_ID) ) ),
    7 => __('A Day In The Life Of saved.', 'www.thejobcrowds.com'),
    8 => sprintf( __('A Day In The Life Of submitted. <a target="_blank" href="%s">Preview A Day In The Life Of</a>', 'www.thejobcrowds.com'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    9 => sprintf( __('A Day In The Life Of scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview A Day In The Life Of</a>', 'www.thejobcrowds.com'),
      // translators: Publish box date format, see http://php.net/date
      date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
    10 => sprintf( __('A Day In The Life Of draft updated. <a target="_blank" href="%s">Preview A Day In The Life Of</a>', 'www.thejobcrowds.com'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'day_in_life_updated_messages' );	


##########################
# RESTRICT SEARCH TO CPT ONLY
##########################
/*
function prg_search_filter($query) {
  if ($query->is_search) {
    # Insert the specific post type you want to search
    $query->set('post_type', 'jobs');
  }
  return $query;
}
 
# This filter will jump into the loop and arrange our results before they're returned
add_filter('pre_get_posts','prg_search_filter');
*/

##########################
# ADD GRAVITY FORMS TO SHOP MANAGERS
##########################
function add_grav_forms(){
    $role = get_role('shop_manager');
    $role->add_cap('gform_full_access');
}

add_action('admin_init','add_grav_forms');

##########################
# ADD MENU CAPABILITIES TO SHOP MANAGERS
##########################
# get the the role object
$role_object = get_role( 'shop_manager' );
$role_object->add_cap( 'edit_theme_options' );

function hide_menu() {

    remove_submenu_page( 'themes.php', 'themes.php' );	# hide the theme selection submenu
    remove_submenu_page( 'themes.php', 'widgets.php' );	# hide the widgets submenu

}

add_action('admin_head', 'hide_menu');

##########################
# UPDATE COMPANY RATING WHEN GRADUATE REVIEW PUBLISHED
##########################
add_action( 'transition_post_status', 'graduate_reviews_ratings', 10, 3 );

function graduate_reviews_ratings( $new_status, $old_status, $post ) {

	if ( 'publish' !== $new_status or 'publish' === $old_status || 'graduate_reviews' !== get_post_type( $post ) )
	return;
	
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
	return;	
	
	#get the post ID, we can now use this on both publish and update functions
	$post_id = $post->ID;	
	
	#get the review template
	require get_template_directory() . '/includes/graduate-review-template.php';
	require get_template_directory() . '/includes/averages-review-template.php';	
	require get_template_directory() . '/includes/independent-review-template.php';

}

##########################
# UPDATE COMPANY RATING WHEN APPRENTICE REVIEW PUBLISHED
##########################
add_action( 'transition_post_status', 'apprentice_reviews_ratings', 10, 3 );

function apprentice_reviews_ratings( $new_status, $old_status, $post ) {

	if ( 'publish' !== $new_status or 'publish' === $old_status || 'apprentice_reviews' !== get_post_type( $post ) )
	return;
	
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
	return;	
	
	#get the post ID, we can now use this on both publish and update functions
	$post_id = $post->ID;
		
	#get the review template
	require get_template_directory() . '/includes/apprentice-review-template.php';	
	require get_template_directory() . '/includes/averages-review-template.php';
	require get_template_directory() . '/includes/independent-review-template.php';
	
}

##########################
# UPDATE GRADUATE AVERAGES & TOTALS
# the save_post action runs after a post has been saved/created
##########################
function update_graduate_average_reviews($post_id, $post) {
	
	#check the post type
	if ($post->post_type != 'graduate_reviews') 
	return;
		
	#get the review template
	require get_template_directory() . '/includes/graduate-review-template.php';	
	require get_template_directory() . '/includes/averages-review-template.php';
	require get_template_directory() . '/includes/independent-review-template.php';
	
}

# add the function late in the actions, at priority 10000
add_action('save_post', 'update_graduate_average_reviews', 10000, 2); 

##########################
# UPDATE APPRENTICE AVERAGES & TOTALS
# the save_post action runs after a post has been saved/created
##########################
function update_apprentice_average_reviews($post_id, $post) {
	
	#check the post type
	if ($post->post_type != 'apprentice_reviews') 
	return;
		
	#get the review template
	require get_template_directory() . '/includes/apprentice-review-template.php';	
	require get_template_directory() . '/includes/averages-review-template.php';
	require get_template_directory() . '/includes/independent-review-template.php';
	
}

# add the function late in the actions, at priority 10000
add_action('save_post', 'update_apprentice_average_reviews', 10000, 2); 

##########################
# ADDITIONAL ADMIN COLUMNs TO USERS
##########################

function prg_company_name_table( $column ) {
    $column['company_name'] = 'Company';
	$column['employer_registration_industry'] = 'Industry';
    return $column;
}
add_filter( 'manage_users_columns', 'prg_company_name_table' );

function prg_company_name_row( $val, $column_name, $user_id ) {
    switch ($column_name) {
        case 'company_name' :
            return get_the_author_meta( 'company_name', $user_id );
        break;
        case 'employer_registration_industry':
			 $employer_registration_industry = get_the_author_meta( 'employer_registration_industry', $user_id );
			 if($employer_registration_industry != ''){
				 $industries = implode(', ', $employer_registration_industry);
				 return $industries;
			 }			
        break;		
    }
    return $val;
}
add_filter( 'manage_users_custom_column', 'prg_company_name_row', 10, 3 );

##########################
# AMENDS POST COUNT IN WP-ADMIN>USERS 
# includes all post types not just posts
# http://www.wpcustoms.net/snippets/post-count-users-custom-post-type/
# https://wordpress.stackexchange.com/questions/136833/how-to-show-custom-post-type-count-in-the-users-admin-page
# https://gist.github.com/mikeschinkel/643240
##########################

add_action('manage_users_columns','yoursite_manage_users_columns');
function yoursite_manage_users_columns($column_headers) {
	unset($column_headers['posts']);
	$column_headers['custom_posts'] = 'Posts';
	return $column_headers;
}

add_action('manage_users_custom_column','yoursite_manage_users_custom_column',10,3);
function yoursite_manage_users_custom_column($custom_column,$column_name,$user_id) {
	if ($column_name=='custom_posts') {
		$counts = _yoursite_get_author_post_type_counts();
		$custom_column = array();
		if (isset($counts[$user_id]) && is_array($counts[$user_id]))
			foreach($counts[$user_id] as $count)
				$custom_column[] = "\t<tr><th>{$count['label']}</th>" .
														 "<td>{$count['count']}</td></tr>";
		$custom_column = implode("\n",$custom_column);
		if (empty($custom_column))
			$custom_column = "<th>No Posts!</th>";
		$custom_column = "<table>\n{$custom_column}\n</table>";
	}
	return $custom_column;
}

function _yoursite_get_author_post_type_counts() {
	static $counts;
	if (!isset($counts)) {
		global $wpdb;
		global $wp_post_types;
		#	AND post_type NOT IN ('revision','nav_menu_item', 'acf-field','acf-field-group')
		$sql = <<<SQL
SELECT
	post_type,
	post_author,
	COUNT(*) AS post_count
FROM
	{$wpdb->posts}
WHERE 1=1
	AND post_type IN ('post','page', 'case_studies','day_in_life','jobs','graduate_reviews','apprentice_reviews','surveys')
	AND post_status IN ('publish','pending')
GROUP BY
	post_type,
	post_author
SQL;
		$posts = $wpdb->get_results($sql);
		foreach($posts as $post) {
			$post_type_object = $wp_post_types[$post_type = $post->post_type];
			if (!empty($post_type_object->label))
				$label = $post_type_object->label;
			else if (!empty($post_type_object->labels->name))
				$label = $post_type_object->labels->name;
			else
				$label = ucfirst(str_replace(array('-','_'),' ',$post_type));
			if (!isset($counts[$post_author = $post->post_author]))
				$counts[$post_author] = array();
			$counts[$post_author][] = array(
				'label' => $label,
				'count' => $post->post_count,
				);
		}
	}
	return $counts;
}

##########################
# CREATE NEW VIEW LINK FOR USERS
########################## 
function edit_user_link($actions, $user_object) {
	$actions['edit_user_link'] = "<a class='edit-user-link' href='/migrationTest/view-users/?user=" . $user_object->ID ."' target='blank'>View User Details</a>";
	return $actions;
}
add_filter('user_row_actions', 'edit_user_link', 10, 2);

##########################
# ACCOUNT VERIFIED AUTO LOGIN
##########################

function prg_account_verified_auto_login($user_id) {
	
	#get the user meta from the ID
	$user_info = get_userdata($user_id);
	$loginusername = $user_info->user_login;	

	$loginpageid = '8';

	#only attempt to auto-login if at www.site.com/auto-login/ (i.e. www.site.com/?p=1234 )
    if (!is_user_logged_in() && is_page($loginpageid)) { 

        #login
        wp_set_current_user($user_id, $loginusername);
        wp_set_auth_cookie($user_id);
        do_action('wp_login', $loginusername);

		$url = get_permalink( '8' );
		wp_redirect($url);
        #wp_redirect( home_url() );
        exit;		
		
    }	

}
#add_action('wp', 'prg_account_verified_auto_login', 1);

##############################################################################
# 2) ADVANCED CUSTOM FIELDS
##############################################################################

##########################
# CREATE THEME OPTIONS PAGE
##########################

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Header Settings',
		'menu_title'	=> 'Header',
		'parent_slug'	=> 'theme-general-settings',
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Footer Settings',
		'menu_title'	=> 'Footer',
		'parent_slug'	=> 'theme-general-settings',
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Job Alert Settings',
		'menu_title'	=> 'Job Alerts',
		'parent_slug'	=> 'theme-general-settings',
	));	
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Reports',
		'menu_title'	=> 'Reports',
		'parent_slug'	=> 'theme-general-settings',
	));		
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Advertising Space',
		'menu_title'	=> 'Advertising Space',
		'parent_slug'	=> 'theme-general-settings',
	));			
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Survey Reminder Email',
		'menu_title'	=> 'Survey Reminder Email',
		'parent_slug'	=> 'theme-general-settings',
	));		
	
}

##########################
# ACF GOOGLE MAP API
##########################
function my_acf_init() {
	acf_update_setting('google_api_key', 'AIzaSyAj_Xc1gEDKeDqLIMFHgok9hhWgyf16TK0');
}

add_action('acf/init', 'my_acf_init');

##########################
# ACF SECURITY
##########################
function my_kses_post( $value ) {
		
	# is array
	if( is_array($value) ) {	
		return array_map('my_kses_post', $value);
	}			
			
	# return
	return wp_kses_post( $value );
	
}
add_filter('acf/update_value', 'my_kses_post', 10, 1);

##########################
# UPDATE ACF REPEATER FROM SINGLE FIELD
# THIS IS USED FOR STUDENT NOTES
##########################

function auto_timestamp_user_notes( $post_id ) {   
	
	# add conditional to check form was triggered
	if(($_POST['acf']['add_note'] == 'true')) {
	
		# bail early if editing in admin
		if( is_admin() ) {		
			return;		
		}
				
		#create our date/time stamp
		date_default_timezone_set("Europe/London"); #need to specify this to ensure correct time stamp
		$date = date("Y-m-d");
		$time = date("h:i:s");
		$timestamp = $date.' '.$time;	
	
		# get the value from our single form
		$notes = $_POST['acf']['field_595a54cf3d32a'];
		
		#add the row to the repeater
		$row = array(
			'field_595a545af9633'	=> $timestamp,
			'field_595a5471f9634'	=> $notes
		);
		$i = add_row('field_595a5449f9632', $row, $post_id);	
	
	}	
	    
}
add_action('acf/save_post', 'auto_timestamp_user_notes', 1);

###
# NOW CLEAR THE SINGLE FORM TO PREVENT PREVIOUS NOTES SHOWING
### 
function clear_note_acf_save_post( $post_id ) {
    
	# add conditional to check form was triggered
	if(($_POST['acf']['add_note'] == 'true')) {
	
		# bail early if editing in admin
		if( is_admin() ) {		
			return;		
		}	
				
		$user_id = get_current_user_id();		
		$meta_key = 'note';		
		$meta_value = '';
				
		delete_user_meta ( $user_id, $meta_key, $meta_value );
	
	}
    
}

add_action('acf/save_post', 'clear_note_acf_save_post', 20);


##########################
# IMPORT EMPLOYER CSV FILE
##########################

function import_csv_list( $post_id ) {   
	
	# add conditional to check form was triggered
	if(($_POST['acf']['create_survey'] == 'true')) {
	
		# bail early if editing in admin
		if( is_admin() ) {		
			return;		
		}
		# get the values we need		
		$file = get_field('csv', $post_id);
		$url = $file['url'];		
		$skip_first_row = get_field('skip_first_row', $post_id);
		
		if( $file ):
			
			$csvFile = fopen($file, 'r');	
			
			#skip first line if it has column headings
			if ($skip_first_row == 1){
				fgetcsv($csvFile);
			}		
			
			#wp_mail( 'ian@prgltd.co.uk', 'Welcome!', 'Role: ' . $_POST['acf']['survey_type'] );
						
			#parse data from csv file line by line
			$x = 0; #set counter
			while(($line = fgetcsv($csvFile)) !== FALSE){
				
				$x++; #increment counter
				
				#we need to create the user accounts on import
				
				#lets construct the user login
				$initial = substr($line[0], 0, 1);
				$user_login = $initial.$line[1];
				
				#we can also auto generate a password
				$password = wp_generate_password( 12, false ); 
				
				#set up the user meta
				$userdata = array(
					'first_name'	=> $line[0],
					'last_name'		=> $line[1],	
					'user_email'	=> $line[2],
					'user_login'	=> $user_login,	
					'role'			=> $_POST['acf']['survey_type'],
					'user_pass'		=> $password
				);
				
				$user_id[$i] = wp_insert_user( $userdata ) ;
				
				# On success
				if ( ! is_wp_error( $user_id[$i] ) ) {
					$user_status =  "User created";
					#wp_mail( $email_address, 'Welcome!', 'Your Password: ' . $password );
					#wp_mail( $line[2], 'Welcome!', 'Your Password: ' . $password );
				} else {
					$user_status =  "User exists";	
				}
				
				$row = array(
					'field_5947dbd28c36e'	=> $line[0],		#forename
					'field_5947dbd78c36f'	=> $line[1],		#surname
					'field_5947dbdc8c370'	=> $line[2],		#email address
					'field_5996cd0d95b6c' 	=> '',				#partially completed
					'field_5947dbe48c371' 	=> '',				#completed
					'field_5996a05bc0d7d'	=> $post_id.'-'.$x,	#import_id
					'field_59e0b97b6788b' 	=> $user_status		#user_status
				);
				
				$i = add_row('field_5947dbbd8c36d', $row, $post_id);					
						
			}
						
			#close opened csv file
			fclose($csvFile);
					
		endif; #endif $file
	
	}	
	    
}
#acf/save_post action after ACF has saved the $_POST data. This is possible by using a priority greater  than 1.
add_action('acf/save_post', 'import_csv_list', 20); 

##########################
# HIDE ACF FIELD IN WOOCOMMERCE ORDERS
# NOT NEEDED BY USERS ONLY TOP LEVEL ADMIN
##########################

add_action('admin_head', 'prg_hide_acf_field_woo_edit_orders');

function prg_hide_acf_field_woo_edit_orders() {
	echo '<style>.acf-field-595f933c6699e {display: none!important;}</style>';
}

##########################
# DYNAMICALLY POPULATE SELECT FIELDS
# THESE ALLOW ADMINS TO AMEND FORM DATA
##########################

###
# COMPANY
# we may need to run this hourly to ensure any new/old employers get added to the job alert drop down
###
function acf_load_dynamic_filled_company_field_choices( $field ) {

	# reset choices
	$field['choices'] = array();
	
	# get the employers from woocommerce, so target specific roles
	$args = array(
		'role__in'     	=> array('customer', 'subscriber'),
		'role__not_in' 	=> array('administrator'),
		'exclude'      	=> array(1),
		'orderby' 		=> 'meta_value',
		'meta_key'		=> 'company_name',
		'order' 		=> 'ASC',
		'meta_query' =>
		array(
			array(
				'relation' => 'AND', # OR						
				array(
					'key' => 'activate_membership',
					'value' => 'Yes',
					'compare' => "=",
				)
			)
		)						
	);	

	$users = get_users($args);	
	
	if ($users){
	
		# populate a blank option / add custom first option
		$choices[] = 'Search Companies';		
		
		foreach ( $users as $user ) {
			
			#$choices[] = get_user_meta( $user->ID, 'billing_company', true );
			$choices[] = get_user_meta( $user->ID, 'company_name', true );
			
		}
	}
	
	# loop through array and add to field 'choices'
	if( is_array($choices) ){
		
		foreach( $choices as $choice ){
			
			$field['choices'][ $choice ] = $choice;
			
		}
		
	}
    
	# return the field
	return $field;
    
}

add_filter('acf/load_field/name=dynamic_filled_company', 'acf_load_dynamic_filled_company_field_choices');

###
# INDUSTRY
###

function acf_load_dynamic_filled_industry_field_choices( $field ) {
    
	# reset choices
	$field['choices'] = array();
        
	# get the textarea value from options page without any formatting
	$choices = get_field('industry', 'option', false);
    
	# explode the value so that each line is a new array piece
	$choices = explode("\n", $choices);
    
	# remove any unwanted white space
	$choices = array_map('trim', $choices);
    
	# loop through array and add to field 'choices'
	if( is_array($choices) ) {
        
		foreach( $choices as $choice ) {
            
			$field['choices'][ $choice ] = $choice;
            
		}
        
	}
    
	# return the field
	return $field;
    
}

add_filter('acf/load_field/name=dynamic_filled_industry', 'acf_load_dynamic_filled_industry_field_choices');

###
# INDUSTRY
# used on the employer profile
###

function acf_load_employer_registration_industry_field_choices( $field ) {
    
	# reset choices
	$field['choices'] = array();
        
	# get the textarea value from options page without any formatting
	$choices = get_field('industry', 'option', false);
    
	# explode the value so that each line is a new array piece
	$choices = explode("\n", $choices);
    
	# remove any unwanted white space
	$choices = array_map('trim', $choices);
    
	# loop through array and add to field 'choices'
	if( is_array($choices) ) {
        
		foreach( $choices as $choice ) {
            
			$field['choices'][ $choice ] = $choice;
            
		}
        
	}
    
	# return the field
	return $field;
    
}

add_filter('acf/load_field/name=employer_registration_industry', 'acf_load_employer_registration_industry_field_choices');

###
# SALARY MIN
###

function acf_load_dynamic_filled_salary_min_field_choices( $field ) {
    
	# reset choices
	$field['choices'] = array();
        
	# get the textarea value from options page without any formatting
	$choices = get_field('salary_min', 'option', false);
    
	# explode the value so that each line is a new array piece
	$choices = explode("\n", $choices);
    
	# remove any unwanted white space
	$choices = array_map('trim', $choices);
    
	# loop through array and add to field 'choices'
	if( is_array($choices) ) {
        
		foreach( $choices as $choice ) {
            
			$field['choices'][ $choice ] = $choice;
            
		}
        
	}
    
	# return the field
	return $field;
    
}

add_filter('acf/load_field/name=dynamic_filled_salary_min', 'acf_load_dynamic_filled_salary_min_field_choices');

###
# SALARY MAX
###

function acf_load_dynamic_filled_salary_max_field_choices( $field ) {
    
	# reset choices
	$field['choices'] = array();
        
	# get the textarea value from options page without any formatting
	$choices = get_field('salary_max', 'option', false);
    
	# explode the value so that each line is a new array piece
	$choices = explode("\n", $choices);
    
	# remove any unwanted white space
	$choices = array_map('trim', $choices);
    
	# loop through array and add to field 'choices'
	if( is_array($choices) ) {
        
		foreach( $choices as $choice ) {
            
			$field['choices'][ $choice ] = $choice;
            
		}
        
	}
    
	# return the field
	return $field;
    
}

add_filter('acf/load_field/name=dynamic_filled_salary_max', 'acf_load_dynamic_filled_salary_max_field_choices');

###
# JOB TYPE
###

function acf_load_dynamic_filled_type_field_choices( $field ) {
    
	# reset choices
	$field['choices'] = array();
        
	# get the textarea value from options page without any formatting
	$choices = get_field('type', 'option', false);
    
	# explode the value so that each line is a new array piece
	$choices = explode("\n", $choices);
    
	# remove any unwanted white space
	$choices = array_map('trim', $choices);
    
	# loop through array and add to field 'choices'
	if( is_array($choices) ) {
        
		foreach( $choices as $choice ) {
            
			$field['choices'][ $choice ] = $choice;
            
		}
        
	}
    
	# return the field
	return $field;
    
}

add_filter('acf/load_field/name=dynamic_filled_type', 'acf_load_dynamic_filled_type_field_choices');

###
# TOWN
###

function acf_load_dynamic_filled_town_field_choices( $field ) {
    
	# reset choices
	$field['choices'] = array();
        
	# get the textarea value from options page without any formatting
	$choices = get_field('town', 'option', false);
    
	# explode the value so that each line is a new array piece
	$choices = explode("\n", $choices);
    
	# remove any unwanted white space
	$choices = array_map('trim', $choices);
    
	# loop through array and add to field 'choices'
	if( is_array($choices) ) {
        
		foreach( $choices as $choice ) {
            
			$field['choices'][ $choice ] = $choice;
            
		}
        
	}
    
	# return the field
	return $field;
    
}

add_filter('acf/load_field/name=dynamic_filled_town', 'acf_load_dynamic_filled_town_field_choices');

###
# COUNTY
###

function acf_load_dynamic_filled_county_field_choices( $field ) {
    
	# reset choices
	$field['choices'] = array();
        
	# get the textarea value from options page without any formatting
	$choices = get_field('county', 'option', false);
    
	# explode the value so that each line is a new array piece
	$choices = explode("\n", $choices);
    
	# remove any unwanted white space
	$choices = array_map('trim', $choices);
    
	# loop through array and add to field 'choices'
	if( is_array($choices) ) {
        
		foreach( $choices as $choice ) {
            
			$field['choices'][ $choice ] = $choice;
            
		}
        
	}
    
	# return the field
	return $field;
    
}

add_filter('acf/load_field/name=dynamic_filled_county', 'acf_load_dynamic_filled_county_field_choices');

###
# REGION
###

function acf_load_dynamic_filled_region_field_choices( $field ) {
    
	# reset choices
	$field['choices'] = array();
        
	# get the textarea value from options page without any formatting
	$choices = get_field('region', 'option', false);
    
	# explode the value so that each line is a new array piece
	$choices = explode("\n", $choices);
    
	# remove any unwanted white space
	$choices = array_map('trim', $choices);
    
	# loop through array and add to field 'choices'
	if( is_array($choices) ) {
        
		foreach( $choices as $choice ) {
            
			$field['choices'][ $choice ] = $choice;
            
		}
        
	}
    
	# return the field
	return $field;
    
}

add_filter('acf/load_field/name=dynamic_filled_region', 'acf_load_dynamic_filled_region_field_choices');

###
# CAREERS
###

function acf_load_dynamic_filled_careers_field_choices( $field ) {
    
	# reset choices
	$field['choices'] = array();
        
	# get the textarea value from options page without any formatting
	$choices = get_field('careers', 'option', false);
    
	# explode the value so that each line is a new array piece
	$choices = explode("\n", $choices);
    
	# remove any unwanted white space
	$choices = array_map('trim', $choices);
    
	# loop through array and add to field 'choices'
	if( is_array($choices) ) {
        
		foreach( $choices as $choice ) {
            
			$field['choices'][ $choice ] = $choice;
            
		}
        
	}
    
	# return the field
	return $field;
    
}

add_filter('acf/load_field/name=dynamic_filled_careers', 'acf_load_dynamic_filled_careers_field_choices');

###
# LOCATION (ENHANCED PROFILE)
# we may need to run this hourly to ensure any new/old employers get added to the job alert drop down
###
function acf_load_locations_field_choices( $field ) {
    
	# reset choices
	$field['choices'] = array();
        
	# get the textarea value from options page without any formatting
	$choices = get_field('town', 'option', false);
    
	# explode the value so that each line is a new array piece
	$choices = explode("\n", $choices);
    
	# remove any unwanted white space
	$choices = array_map('trim', $choices);
    
	# loop through array and add to field 'choices'
	if( is_array($choices) ) {
        
		foreach( $choices as $choice ) {
            
			$field['choices'][ $choice ] = $choice;
            
		}
        
	}
    
	# return the field
	return $field;
    
}

add_filter('acf/load_field/name=locations', 'acf_load_locations_field_choices');

###
# TOP 100 COMPANY
# we may need to run this hourly to ensure any new/old employers get added to the job alert drop down
###
function acf_load_top_100_company_name_field_choices( $field ) {

	# reset choices
	$field['choices'] = array();	
	
	# get the employers from woocommerce, so target specific roles
	$args = array(
		'role__in'     	=> array('customer', 'subscriber'),
		'role__not_in' 	=> array('administrator'),
		'exclude'      	=> array(1),
		'orderby' 		=> 'meta_value',
		'meta_key'		=> 'company_name',
		'order' 		=> 'ASC',
		'meta_query' =>
		array(
			array(
				'relation' => 'AND', # OR						
				array(
					'key' => 'activate_membership',
					'value' => 'Yes',
					'compare' => "=",
				)
			)
		)						
	);	
	$users = get_users($args);	
	
	if ($users){
	
		# populate a blank option / add custom first option
		$choices[] = 'Search Companies';		
		
		foreach ( $users as $user ) {
			
			#$choices[] = get_user_meta( $user->ID, 'billing_company', true );
			$choices[] = get_user_meta( $user->ID, 'company_name', true );
			
		}
	}
	
	# loop through array and add to field 'choices'
	if( is_array($choices) ){
		
		foreach( $choices as $choice ){
			
			$field['choices'][ $choice ] = $choice;
			
		}
		
	}
    
	# return the field
	return $field;
    
}
#https://support.advancedcustomfields.com/forums/topic/dynamically-populate-a-select-subfield-in-a-repeater-field/
add_filter('acf/load_field/key=field_5a5cdbf14004b', 'acf_load_top_100_company_name_field_choices');

###
# TOP 100 COMPANY - SMALL INTAKE
# we may need to run this hourly to ensure any new/old employers get added to the job alert drop down
###
function acf_load_top_100_small_intake_company_name_field_choices( $field ) {

	# reset choices
	$field['choices'] = array();	
	
	# get the employers from woocommerce, so target specific roles
	$args = array(
		'role__in'     	=> array('customer', 'subscriber'),
		'role__not_in' 	=> array('administrator'),
		'exclude'      	=> array(1),
		'orderby' 		=> 'meta_value',
		'meta_key'		=> 'company_name',
		'order' 		=> 'ASC',
		'meta_query' =>
		array(
			array(
				'relation' => 'AND', # OR						
				array(
					'key' => 'activate_membership',
					'value' => 'Yes',
					'compare' => "=",
				)
			)
		)						
	);	
	$users = get_users($args);	
	
	if ($users){

		# populate a blank option / add custom first option
		$choices[] = 'Search Companies';			
		
		foreach ( $users as $user ) {
			
			#$choices[] = get_user_meta( $user->ID, 'billing_company', true );
			$choices[] = get_user_meta( $user->ID, 'company_name', true );
			
		}
	}
	
	# loop through array and add to field 'choices'
	if( is_array($choices) ){
		
		foreach( $choices as $choice ){
			
			$field['choices'][ $choice ] = $choice;
			
		}
		
	}
    
	# return the field
	return $field;
    
}
#https://support.advancedcustomfields.com/forums/topic/dynamically-populate-a-select-subfield-in-a-repeater-field/
add_filter('acf/load_field/key=field_5a6b33ca9c304', 'acf_load_top_100_small_intake_company_name_field_choices');

###
# TOP 100 COMPANY - LARGE INTAKE
# we may need to run this hourly to ensure any new/old employers get added to the job alert drop down
###
function acf_load_top_100_large_intake_company_name_field_choices( $field ) {

	# reset choices
	$field['choices'] = array();
	
	# get the employers from woocommerce, so target specific roles
	$args = array(
		'role__in'     	=> array('customer', 'subscriber'),
		'role__not_in' 	=> array('administrator'),
		'exclude'      	=> array(1),
		'orderby' 		=> 'meta_value',
		'meta_key'		=> 'company_name',
		'order' 		=> 'ASC',
		'meta_query' =>
		array(
			array(
				'relation' => 'AND', # OR						
				array(
					'key' => 'activate_membership',
					'value' => 'Yes',
					'compare' => "=",
				)
			)
		)						
	);	
	$users = get_users($args);	
	
	if ($users){

		# populate a blank option / add custom first option
		$choices[] = 'Search Companies';			
		
		foreach ( $users as $user ) {
			
			#$choices[] = get_user_meta( $user->ID, 'billing_company', true );
			$choices[] = get_user_meta( $user->ID, 'company_name', true );
			
		}
	}
	
	# loop through array and add to field 'choices'
	if( is_array($choices) ){
		
		foreach( $choices as $choice ){
			
			$field['choices'][ $choice ] = $choice;
			
		}
		
	}
    
	# return the field
	return $field;
    
}
#https://support.advancedcustomfields.com/forums/topic/dynamically-populate-a-select-subfield-in-a-repeater-field/
add_filter('acf/load_field/key=field_5a6b34f5d131e', 'acf_load_top_100_large_intake_company_name_field_choices');

###
# TOP 50 COMPANY
# we may need to run this hourly to ensure any new/old employers get added to the job alert drop down
###
function acf_load_top_50_company_name_field_choices( $field ) {

	# reset choices
	$field['choices'] = array();
	
	# get the employers from woocommerce, so target specific roles
	$args = array(
		'role__in'     	=> array('customer', 'subscriber'),
		'role__not_in' 	=> array('administrator'),
		'exclude'      	=> array(1),
		'orderby' 		=> 'meta_value',
		'meta_key'		=> 'company_name',
		'order' 		=> 'ASC',
		'meta_query' =>
		array(
			array(
				'relation' => 'AND', # OR						
				array(
					'key' => 'activate_membership',
					'value' => 'Yes',
					'compare' => "=",
				)
			)
		)						
	);	
	$users = get_users($args);	
	
	if ($users){

		# populate a blank option / add custom first option
		$choices[] = 'Search Companies';			
		
		foreach ( $users as $user ) {
			
			#$choices[] = get_user_meta( $user->ID, 'billing_company', true );
			$choices[] = get_user_meta( $user->ID, 'company_name', true );
			
		}
	}
	
	# loop through array and add to field 'choices'
	if( is_array($choices) ){
		
		foreach( $choices as $choice ){
			
			$field['choices'][ $choice ] = $choice;
			
		}
		
	}
    
	# return the field
	return $field;
    
}
#https://support.advancedcustomfields.com/forums/topic/dynamically-populate-a-select-subfield-in-a-repeater-field/
add_filter('acf/load_field/key=field_5a5de14478e57', 'acf_load_top_50_company_name_field_choices');

###
# TOP COMPANIES INDUSTRY SECTOR
# we may need to run this hourly to ensure any new/old employers get added to the job alert drop down
###
function acf_load_top_companies_industry_sector_company_name_field_choices( $field ) {

    # reset choices
    $field['choices'] = array();
    
    
    # get the textarea value from options page without any formatting
    $choices = get_field('industry', 'option', false);

    
    # explode the value so that each line is a new array piece
    $choices = explode("\n", $choices);

    
    # remove any unwanted white space
    $choices = array_map('trim', $choices);

    
    # loop through array and add to field 'choices'
    if( is_array($choices) ) {
        
        foreach( $choices as $choice ) {
            
            $field['choices'][ $choice ] = $choice;
            
        }
        
    }
    
	# return the field
	return $field;
    
}
#https://support.advancedcustomfields.com/forums/topic/dynamically-populate-a-select-subfield-in-a-repeater-field/
add_filter('acf/load_field/key=field_5a5de4bfc2929', 'acf_load_top_companies_industry_sector_company_name_field_choices');

###
# COMPANY REPORT
# used on the options>reports
###
function acf_load_company_report_field_choices( $field ) {

	# reset choices
	$field['choices'] = array();
	
	# populate a blank option / add custom first option
	$field['choices']['- Select All -'] = '- Select All -';
	
	# get the employers from woocommerce, so target specific roles	
	$args = array(
		'role__in'     	=> array('customer', 'subscriber'),
		'role__not_in' 	=> array('administrator'),
		'exclude'      	=> array(1),
		'orderby' 		=> 'meta_value',
		'meta_key'		=> 'company_name',
		'order' 		=> 'ASC',
		'meta_query' =>
		array(
			array(
				'relation' => 'AND', # OR						
				array(
					'key' => 'activate_membership',
					'value' => 'Yes',
					'compare' => "=",
				)
			)
		)						
	);	
	$users = get_users($args);	
	
	if ($users){
	
		foreach ( $users as $user ) {
			
			#$choices[] = get_user_meta( $user->ID, 'billing_company', true );
			$choices[] = get_user_meta( $user->ID, 'company_name', true );
			
		}
	}
	
	# loop through array and add to field 'choices'
	if( is_array($choices) ){
		
		foreach( $choices as $choice ){
			
			$field['choices'][ $choice ] = $choice;
			
		}
		
	}
    
	# return the field
	return $field;
    
}

add_filter('acf/load_field/name=company_report', 'acf_load_company_report_field_choices');

###
# COMPANY REPORT
# this is used in options > reports (repeater)
###
/*
function my_acf_prepare_field( $field ) {

	# reset choices
	$field['choices'] = array();
	
	# populate a blank option / add custom first option
	$field['choices'][''] = 'All Companies';
	
	# get the employers from woocommerce, so target specific roles
	$args = array(
		'role__in'     	=> array('customer', 'subscriber'),
		'role__not_in' 	=> array('administrator'),
		'exclude'      	=> array(1),
		'orderby' 		=> 'meta_value',
		'meta_key'		=> 'company_name',
		'order' 		=> 'ASC',
		'meta_query' =>
		array(
			array(
				'relation' => 'AND', # OR						
				array(
					'key' => 'activate_membership',
					'value' => 'Yes',
					'compare' => "=",
				)
			)
		)						
	);	
	$users = get_users($args);	
	
	if ($users){
	
		foreach ( $users as $user ) {
			
			#$choices[] = get_user_meta( $user->ID, 'billing_company', true );
			$choices[] = get_user_meta( $user->ID, 'company_name', true );
			
		}
	}
	
	# loop through array and add to field 'choices'
	if( is_array($choices) ){
		
		foreach( $choices as $choice ){
			
			#$field['choices'][ $choice ] = $choice;
			$row = array(				
				'field_59e4ce48af420'	=> $choice,		#company
				'field_59e4ce51af421' 	=> ''			#enhanced message				
			);			
			$i = add_row('field_59e4ce34af41f', $row, 'option');				
			
		}
		
	}
    
	# return the field
	return $field;
    
}

add_filter('acf/prepare_field/key=field_59e4ce34af41f', 'my_acf_prepare_field');
*/

###
# REVIEW COMPANY (SINGLE)
# used on the review alerts
###
/*
function acf_load_review_company_field_choices( $field ) {

	# reset choices
	$field['choices'] = array();
	
	# populate a blank option / add custom first option
	$field['choices'][''] = 'Search Companies';
	
	# get the employers from woocommerce, so target specific roles	
	$args = array(
		'role__in'     	=> array('customer', 'subscriber'),
		'role__not_in' 	=> array('administrator'),
		'exclude'      	=> array(1),
		'orderby' 		=> 'meta_value',
		'meta_key'		=> 'company_name',
		'order' 		=> 'ASC',
		'meta_query' =>
		array(
			array(
				'relation' => 'AND', # OR						
				array(
					'key' => 'activate_membership',
					'value' => 'Yes',
					'compare' => "=",
				)
			)
		)						
	);	
	$users = get_users($args);	
	
	if ($users){
	
		foreach ( $users as $user ) {
			
			#$choices[] = get_user_meta( $user->ID, 'billing_company', true );
			$choices[] = get_user_meta( $user->ID, 'company_name', true );
			
		}
	}
	
	# loop through array and add to field 'choices'
	if( is_array($choices) ){
		
		foreach( $choices as $choice ){
			
			$field['choices'][ $choice ] = $choice;
			
		}
		
	}
    
	# return the field
	return $field;
    
}

add_filter('acf/load_field/name=review_company', 'acf_load_review_company_field_choices');
*/
###
# REVIEW COMPANY (REPEATER)
# used on the review alerts
###
function acf_load_test_review_alert_company_field_choices( $field ) {

	# reset choices
	$field['choices'] = array();
	
	# get the employers from woocommerce, so target specific roles	
	$args = array(
		'role__in'     	=> array('customer', 'subscriber'),
		'role__not_in' 	=> array('administrator'),
		'exclude'      	=> array(1),
		'orderby' 		=> 'meta_value',
		'meta_key'		=> 'company_name',
		'order' 		=> 'ASC',
		'meta_query' =>
		array(
			array(
				'relation' => 'AND', # OR						
				array(
					'key' => 'activate_membership',
					'value' => 'Yes',
					'compare' => "=",
				)
			)
		)						
	);	
	$users = get_users($args);	
	
	if ($users){

	
	# populate a blank option / add custom first option
	$choices[] = 'Search Companies';		
		
		foreach ( $users as $user ) {
			
			#$choices[] = get_user_meta( $user->ID, 'billing_company', true );
			$choices[] = get_user_meta( $user->ID, 'company_name', true );
			
		}
	}
	
	# loop through array and add to field 'choices'
	if( is_array($choices) ){
		
		foreach( $choices as $choice ){
			
			$field['choices'][ $choice ] = $choice;
			
		}
		
	}
    
	# return the field
	return $field;
    
}

add_filter('acf/load_field/key=field_59cb905f8c9a2', 'acf_load_test_review_alert_company_field_choices');

##########################
# IMPORT EMPLOYER CSV FILE
##########################

function import_additional_recipients_csv_list( $post_id ) {   
	
	# add conditional to check form was triggered
	if(($_POST['acf']['survey_import_additional_recipients'] == 'true')) {
	
		# bail early if editing in admin
		if( is_admin() ) {		
			return;		
		}
		# get the values we need		
		$file = get_field('additional_recipients', $post_id);
		$url = $file['url'];		
		$skip_first_row = get_field('skip_first_row', $post_id);
		
		if( $file ):

			$csvFile = fopen($file, 'r');	

			#skip first line if it has column headings
			if ($skip_first_row == 1){
				fgetcsv($csvFile);
			}

			#parse data from csv file line by line
			#$last_row = count( get_field( 'recipients', $post_id ) ); #set counter
			#$x = ($last_row + 3);

			#https://support.advancedcustomfields.com/forums/topic/get-specific-row-from-repeater-field/
			#run the repeater	
			#$post_id was 1078 - may have been for testing
			while( have_rows('recipients', $post_id) ): the_row();
				$my_data[] = get_row();
			endwhile;

			#get the last row on the list
			$total_rows = count( get_field( 'recipients', $post_id ) );
			#echo '<p>Total rows: '. $total_rows.'</p>';

			#need to remove one due to indexing discrepancies
			$row_index = ($total_rows - 1);
			#echo '<p>New total: '. $row_index.'</p>';

			#$row_index = 20;
			$import_id = $my_data[$row_index][field_5996a05bc0d7d];
			$forename = $my_data[$row_index][field_5947dbd28c36e];
			$email = $my_data[$row_index][field_5947dbdc8c370];
			/*
			echo '<p>import_id: '. $import_id.'</p>';
			echo '<p>forename: '. $forename.'</p>';
			echo '<p>email: '. $email.'</p>';
			*/

			#need to get the new row number to prevent issues should one be removed
			$row 		= explode("-", $import_id);
			$list_id	= $row[0];	
			$row_id		= $row[1];	

			$survey_type = get_field('survey_type',$post_id);
			$survey_type = get_field('survey_type',$post_id);
			if ($survey_type == 'Graduate') :
				$survey = 'student';
			else:
				$survey = 'apprentice';
			endif;

			#echo '<p>list_id: '. $list_id.'</p>';
			#echo '<p>row_id: '. $row_id.'</p>';

			#$new_import_id = ($row_id + 1);
			#echo '<p>new_import_id: '. $new_import_id.'</p>';

			while(($line = fgetcsv($csvFile)) !== FALSE){	

				$row_id++; #increment counter	

				#we need to create the user accounts on import

				#lets construct the user login
				$initial = substr($line[0], 0, 1);
				$user_login = $initial.$line[1];

				#we can also auto generate a password
				$password = wp_generate_password( 12, false ); 

				#set up the user meta
				$userdata = array(
					'first_name'	=> $line[0],
					'last_name'		=> $line[1],	
					'user_email'	=> $line[2],
					'user_login'	=> $user_login,	
					'role'			=> $survey_type,
					'user_pass'		=> $password
				);

				$user_id[$i] = wp_insert_user( $userdata ) ;

				# On success
				if ( ! is_wp_error( $user_id[$i] ) ) {
					$user_status =  "User created";
					#wp_mail( $email_address, 'Welcome!', 'Your Password: ' . $password );
					#wp_mail( $line[2], 'Welcome!', 'Your Password: ' . $password );
				} else {
					$user_status =  "User exists";	
				}			

				$row = array(

					'field_5947dbd28c36e'	=> $line[0],			#forename
					'field_5947dbd78c36f'	=> $line[1],			#surname
					'field_5947dbdc8c370'	=> $line[2],			#email address
					'field_5996cd0d95b6c' 	=> '',					#partially completed
					'field_5947dbe48c371' 	=> '',					#completed				
					'field_5996a05bc0d7d'	=> $post_id.'-'.$row_id,#import_id	
					'field_59e0b97b6788b' 	=> $user_status			#user_status	

				);
				
				
				$i = add_row('field_5947dbbd8c36d', $row, $post_id);

			}

			#close opened csv file
			fclose($csvFile);
					
		endif; #endif $file
	
	}	
	    
}
#acf/save_post action after ACF has saved the $_POST data. This is possible by using a priority greater  than 1.
add_action('acf/save_post', 'import_additional_recipients_csv_list', 20); 

##########################
# ADD SINGLE RECIPIENT
##########################

function import_additional_singular_recipient( $post_id ) {   
	
	# add conditional to check form was triggered
	if(($_POST['acf']['survey_additional_single_recipient'] == 'true')) {
	
		# bail early if editing in admin
		if( is_admin() ) {		
			return;		
		}

			#we need to create the user accounts on import
			$survey_type = get_field('survey_type',$post_id);
			$survey_type = get_field('survey_type',$post_id);
			if ($survey_type == 'Graduate') :
				$survey = 'student';
			else:
				$survey = 'apprentice';
			endif;		
		
			$forename = $_POST['acf']['field_5947dbd28c36e'];
			$surname = $_POST['acf']['field_5947dbd78c36f'];
			$email = $_POST['acf']['field_5947dbdc8c370'];

			#lets construct the user login
			$initial = substr($forename, 0, 1);
			$user_login = $initial.$surname;

			#we can also auto generate a password
			$password = wp_generate_password( 12, false ); 

			#set up the user meta
			$userdata = array(
				'first_name'	=> $forename,
				'last_name'		=> $surname,	
				'user_email'	=> $email,
				'user_login'	=> $user_login,	
				'role'			=> $survey_type,
				'user_pass'		=> $password
			);

			$user_id = wp_insert_user( $userdata ) ;
		
			#the field name of the repeater field
			$repeater = 'recipients'; 	
		
			#get the total row count so we know where to add
			$count = intval(get_post_meta($post_id, $repeater, true)); 
			$next_row = $count+1;

			# On success
			if ( ! is_wp_error( $user_id ) ) {
				$user_status =  "User created";

				/*
				$to			= get_option( 'admin_email' );
				$subject	= "New single recipient added {$post_id}";
				$message	= "forename: {$forename} <br/>";
				$message	.= "surname: {$surname} <br/>";
				$message	.= "email: {$email} <br/>";
				$message	.= "user_login: {$user_login} <br/>";
				$message	.= "survey_type: {$survey_type} <br/>";
				$message	.= "user_status: {$user_status} <br/>";
				$message	.= "count: {$count} <br/>";
				$message	.= "next_row: {$next_row} <br/>";
				$headers = array(
					'Content-Type: text/html; charset=UTF-8',
					'From: '.get_option( 'blogname' ).' <info@thejobcrowd.com>'
				);
				wp_mail ($to, $subject, $message, $headers);	
				*/
				
				
			} else {
				$user_status =  "User exists";	
			}	
		
			#create our array
			$row = array(

				'field_5947dbd28c36e'	=> $forename,			#forename
				'field_5947dbd78c36f'	=> $surname,			#surname
				'field_5947dbdc8c370'	=> $email,				#email address
				'field_5996cd0d95b6c' 	=> '',					#partially completed
				'field_5947dbe48c371' 	=> '',					#completed				
				'field_5996a05bc0d7d'	=> $post_id.'-'.$next_row,#import_id	
				'field_59e0b97b6788b' 	=> $user_status			#user_status	

			);
			
			#insert our new row
			$i = add_row('field_5947dbbd8c36d', $row, $post_id);		
	
	}	
	    
}
#acf/save_post action after ACF has saved the $_POST data. This is possible by using a priority greater  than 1.
add_action('acf/save_post', 'import_additional_singular_recipient', 20); 

###
# NOW CLEAR THE SINGLE FORM TO PREVENT PREVIOUS RECIPIENTS SHOWING
### 
function clear_single_recipient_input_acf_save_post( $post_id ) {
    
	# add conditional to check form was triggered
	if(($_POST['acf']['survey_additional_single_recipient'] == 'true')) {
	
		# bail early if editing in admin
		if( is_admin() ) {		
			return;		
		}	
				
		
		$meta_key_1 = 'forename';		
		$meta_key_2 = 'surname';
		$meta_key_3 = 'email_address';
		$meta_value = '';				
		delete_post_meta ( $post_id, $meta_key_1, $meta_value );
		delete_post_meta ( $post_id, $meta_key_2, $meta_value );
		delete_post_meta ( $post_id, $meta_key_3, $meta_value );
	
	}
    
}

add_action('acf/save_post', 'clear_single_recipient_input_acf_save_post', 30);

##########################
# SET THE CORRECT No OF ROWS FOR THE AD SPACE
##########################

function prg_set_ad_space_row_limit( $field ) {
  
	# reset choices
	$field['choices'] = array();
        
	# get the textarea value from options page without any formatting
	$choices = get_field('industry', 'option', false);
    
	# explode the value so that each line is a new array piece
	$choices = explode("\n", $choices);
    
	# remove any unwanted white space
	$choices = array_map('trim', $choices);
	
	$total_industries = count($choices);  
	
	#reduce by 1 to exclude - Select Industry -
	$field['max'] = ($total_industries-1);
	$field['min'] = ($total_industries-1);
     
	# loop through array and add to field 'choices'
	if( is_array($choices) ) {
        
		foreach( $choices as $choice ) {
			
            if($choice === '- Select Industry -') continue; #skip first item  
			$field['choices'][ $choice ] = $choice;
 
		}
        
	}   

  return $field;
  
}

add_filter('acf/prepare_field/key=field_59df4a5879334', 'prg_set_ad_space_row_limit');


##########################
# COPY THEMES>JOB ALERTS>INDUSTRY TO AD SPACE REPEATER
##########################

function prg_replicate_industries_for_ad_space_location( $value, $field ){

	# get the textarea value from options page without any formatting
	$choices = get_field('industry', 'option', false);
	
	# explode the value so that each line is a new array piece
	$choices = explode("\n", $choices);
    
	# remove any unwanted white space
	$choices = array_map('trim', $choices);

    $i = 0;
	
	# loop through array and add to field 'choices'
	if( is_array($choices) ) {
		
		foreach( $choices as $choice ){
			
			if($choice === '- Select Industry -') continue; #skip first item  
			$value[$i]['field_59df550bfd58e'] = $choice;	
			$i++;
	
		}

	} 
	return $value;

}

add_filter('acf/load_value/key=field_59df4a5879334', 'prg_replicate_industries_for_ad_space_location', 10, 3);

##########################
# ACF DISABLE FIELDS (ADMIN REF ONLY)
##########################

function prg_disable_acf_fields( $field ) {
	#if( !is_admin() ){
		$field['disabled'] = 1;
	
	#}
	return $field;
}
add_filter('acf/load_field/key=field_5996a05bc0d7d', 'prg_disable_acf_fields');	#import_id
add_filter('acf/load_field/key=field_59e0b97b6788b', 'prg_disable_acf_fields');	#user_status
add_filter('acf/load_field/key=field_599afa7c03cbc', 'prg_disable_acf_fields');	#survey_type
add_filter('acf/load_field/key=field_59e4de0e8500d', 'prg_disable_acf_fields');	#count - used on options>report>refresh :: Above are all used on survey imports

#the following disable the review scores on the grad and app cpt
add_filter('acf/load_field/key=field_5a255887d2cf9', 'prg_disable_acf_fields');
add_filter('acf/load_field/key=field_5a25589cd2cfa', 'prg_disable_acf_fields');
add_filter('acf/load_field/key=field_5a2558a9d2cfb', 'prg_disable_acf_fields');
add_filter('acf/load_field/key=field_5a2558b6d2cfc', 'prg_disable_acf_fields');
add_filter('acf/load_field/key=field_5a2558c6d2cfd', 'prg_disable_acf_fields');
add_filter('acf/load_field/key=field_5a2558d7d2cfe', 'prg_disable_acf_fields');
add_filter('acf/load_field/key=field_5a2558e2d2cff', 'prg_disable_acf_fields');
add_filter('acf/load_field/key=field_5a2558e9d2d00', 'prg_disable_acf_fields');
add_filter('acf/load_field/key=field_5a2558fcd2d01', 'prg_disable_acf_fields');
add_filter('acf/load_field/key=field_5a25590dd2d02', 'prg_disable_acf_fields');
add_filter('acf/load_field/key=field_5a255917d2d03', 'prg_disable_acf_fields');

#the following disable the review scores on the users page
add_filter('acf/load_field/key=field_59affbf5c2f22', 'prg_disable_acf_fields');
add_filter('acf/load_field/key=field_59b00a4060775', 'prg_disable_acf_fields');
add_filter('acf/load_field/key=field_59b00a5e60776', 'prg_disable_acf_fields');
add_filter('acf/load_field/key=field_59affc23c2f25', 'prg_disable_acf_fields');
add_filter('acf/load_field/key=field_59b00a9260777', 'prg_disable_acf_fields');
add_filter('acf/load_field/key=field_59b00a9e60778', 'prg_disable_acf_fields');
add_filter('acf/load_field/key=field_59affbd7c2f21', 'prg_disable_acf_fields');
add_filter('acf/load_field/key=field_59b00ab660779', 'prg_disable_acf_fields');
add_filter('acf/load_field/key=field_59b00ac36077a', 'prg_disable_acf_fields');
add_filter('acf/load_field/key=field_59affc2fc2f26', 'prg_disable_acf_fields');
add_filter('acf/load_field/key=field_59b00aec07daa', 'prg_disable_acf_fields');
add_filter('acf/load_field/key=field_59b00afb07dab', 'prg_disable_acf_fields');
add_filter('acf/load_field/key=field_59affbc9c2f20', 'prg_disable_acf_fields');
add_filter('acf/load_field/key=field_59b00b1707dac', 'prg_disable_acf_fields');
add_filter('acf/load_field/key=field_59b00b2107dad', 'prg_disable_acf_fields');
add_filter('acf/load_field/key=field_59affb77c2f1e', 'prg_disable_acf_fields');
add_filter('acf/load_field/key=field_59b00b3807dae', 'prg_disable_acf_fields');
add_filter('acf/load_field/key=field_59b00b4307daf', 'prg_disable_acf_fields');
add_filter('acf/load_field/key=field_59affb9cc2f1f', 'prg_disable_acf_fields');
add_filter('acf/load_field/key=field_59b00b5907db0', 'prg_disable_acf_fields');
add_filter('acf/load_field/key=field_59b00b6307db1', 'prg_disable_acf_fields');
add_filter('acf/load_field/key=field_59affc05c2f23', 'prg_disable_acf_fields');
add_filter('acf/load_field/key=field_59b00b922d153', 'prg_disable_acf_fields');
add_filter('acf/load_field/key=field_59b00b9d2d154', 'prg_disable_acf_fields');
add_filter('acf/load_field/key=field_59affc0fc2f24', 'prg_disable_acf_fields');
add_filter('acf/load_field/key=field_59b00bb22d155', 'prg_disable_acf_fields');
add_filter('acf/load_field/key=field_59b00bbf2d156', 'prg_disable_acf_fields');
add_filter('acf/load_field/key=field_59b149ff92119', 'prg_disable_acf_fields');
add_filter('acf/load_field/key=field_59b14a1c9211a', 'prg_disable_acf_fields');
add_filter('acf/load_field/key=field_59b00a0487f89', 'prg_disable_acf_fields');
add_filter('acf/load_field/key=field_59c8d24e403f6', 'prg_disable_acf_fields');

#the following disable the account ownership on the users page
add_filter('acf/load_field/key=field_5a65cf8e05d8a', 'prg_disable_acf_fields');
add_filter('acf/load_field/key=field_5a65cfa905d8c', 'prg_disable_acf_fields');
add_filter('acf/load_field/key=field_5a65cfd205d8d', 'prg_disable_acf_fields');

#the following disable the advertising sectors under the options page
add_filter('acf/load_field/key=field_59df550bfd58e', 'prg_disable_acf_fields');


##############################################################################
# 3) WOOCOMMERCE
##############################################################################

##########################
# CREATE CUSTOM WOOCOMMERCE END POINTS
##########################

# Insert the new endpoint into the My Account menu.
function prg_custom_endpoints() {
	add_rewrite_endpoint( 'case-study-submission', EP_ROOT | EP_PAGES );
	add_rewrite_endpoint( 'day-in-life-submission', EP_ROOT | EP_PAGES );
	add_rewrite_endpoint( 'your-notes', EP_ROOT | EP_PAGES );
	add_rewrite_endpoint( 'job-alert', EP_ROOT | EP_PAGES );
	add_rewrite_endpoint( 'review-alert', EP_ROOT | EP_PAGES );
	add_rewrite_endpoint( 'your-jobs', EP_ROOT | EP_PAGES );
	add_rewrite_endpoint( 'job-submission', EP_ROOT | EP_PAGES );
	add_rewrite_endpoint( 'stored-reviews', EP_ROOT | EP_PAGES );
	add_rewrite_endpoint( 'register-apprentices', EP_ROOT | EP_PAGES );
	add_rewrite_endpoint( 'register-graduates', EP_ROOT | EP_PAGES );
	#add_rewrite_endpoint( 'send-survey', EP_ROOT | EP_PAGES );
	add_rewrite_endpoint( 'your-surveys', EP_ROOT | EP_PAGES );
	add_rewrite_endpoint( 'enhanced-user-profile', EP_ROOT | EP_PAGES );
}

add_action( 'init', 'prg_custom_endpoints' );

function prg_custom_query_vars( $vars ) {
	$vars[] = 'case-study-submission';
	$vars[] = 'day-in-life-submission';
	$vars[] = 'your-notes';
	$vars[] = 'job-alert';
	$vars[] = 'review-alert';
	$vars[] = 'your-jobs';
	$vars[] = 'job-submission';
	$vars[] = 'stored-reviews';
	$vars[] = 'register-apprentices';
	$vars[] = 'register-graduates';
	#$vars[] = 'send-survey';
	$vars[] = 'your-surveys';	
	$vars[] = 'enhanced-user-profile';	
	$vars[] = "company"; #this is a non-woo endpoint but exists to view company details
    return $vars;
}

add_filter( 'query_vars', 'prg_custom_query_vars', 0 );

function prg_custom_flush_rewrite_rules() {
    flush_rewrite_rules();
}

add_action( 'after_switch_theme', 'prg_custom_flush_rewrite_rules' );

# Set position in My Account menu
function prg_custom_my_account_menu_items( $items ) {
	
	#create all end points
	/*
	$items['case-study-submission']	= 'Case Study Submission';
	$items['day-in-life-submission'] = 'A Day In The Life Submission';
	$items['your-notes']			= 'Your Notes';
	$items['job-alert']				= 'Job Alerts';
	$items['review-alert']			= 'Review Alerts';
	$items['stored-reviews']		= 'Stored Reviews';
	$items['your-jobs']				= 'Jobs';	
	$items['register-apprentices']	= 'Register Apprentices';
	$items['register-graduates']	= 'Register Graduates';
	#$items['send-survey']			= 'Send Survey'; #now surplus to requirements as register-app and register-grad superceed this
	$items['your-surveys']			= 'Your Surveys';
	$items['enhanced-user-profile']	= 'Enhanced User Profile';
	*/
	
	#check if a user is part of a specific group
	#http://docs.itthinx.com/document/groups/api/examples/
	$groups_user = new Groups_User( get_current_user_id() );
	
	#remove default items in order to rearrange our menu
	unset( $items['customer-logout'] );
	unset( $items['orders'] );
	unset( $items['members-area'] );
	unset( $items['subscriptions'] );
	unset( $items['edit-address'] );
	unset( $items['edit-account'] );
	unset($items['account-funds']);


	if ($groups_user->can( 'basic_apprentice' )==1) {
		$items['register-apprentices']	= 'Register Apprentices';
	}

	if ($groups_user->can( 'basic_both' )==1) {
		$items['register-apprentices']	= 'Register Apprentices';
		$items['register-graduates']	= 'Register Graduates';
	}
	
	if ($groups_user->can( 'basic_graduate' )==1) {
		$items['register-graduates']	= 'Register Graduates';		
	}	
		
	if ($groups_user->can( 'enhanced_apprentice' )==1) {
		$items['register-apprentices']	= 'Register Apprentices';
		$items['enhanced-user-profile']	= 'Enhanced User Profile';
	}		
	
	if ($groups_user->can( 'enhanced_both' )==1) {
				
		$items['register-apprentices']	= 'Register Apprentices';
		$items['register-graduates']	= 'Register Graduates';
		#$items['enhanced-user-profile']	= 'Enhanced User Profile';
		
	}		
	
	if ($groups_user->can( 'enhanced_graduate' )==1) {
		$items['register-graduates']	= 'Register Graduates';
		#$items['enhanced-user-profile']	= 'Enhanced User Profile';
	}		
	
	if ( current_user_can('subscriber')  ) {
		$items['your-surveys']			= 'Your Surveys';	
	}	
	
	if ($groups_user->can( 'enhanced_both' )==1) {				
		$items['enhanced-user-profile']	= 'Enhanced User Profile';		
	}		
	
	if ($groups_user->can( 'enhanced_graduate' )==1) {
		$items['enhanced-user-profile']	= 'Enhanced User Profile';
	}		
	
	if ($groups_user->can( 'jobs' )==1) {
		$items['your-jobs']				= 'Jobs';
	}	
	
	#check what role a user has and display the additional menu options
	if ( current_user_can('administrator')  ) {
	
	}
	
	if ( current_user_can('apprentice') ) {
		unset($items['account-funds']);
		$items['case-study-submission']	= 'Case Study Submission';
		$items['day-in-life-submission'] = 'A Day In The Life Submission';	
		$items['job-alert']				= 'Job Alerts';
		$items['review-alert']			= 'Review Alerts';		
	}	
	
	if ( current_user_can('customer') ) {

	}
	
	if ( current_user_can('student')  ) {
		unset($items['account-funds']);
		$items['your-notes']			= 'Your Notes';
		$items['job-alert']				= 'Job Alerts';
		$items['review-alert']			= 'Review Alerts';
		$items['stored-reviews']		= 'Stored Reviews';		
	}	
	
	if ( current_user_can('subscriber')  ) {
		$items['account-funds'] 		= 'Account funds';	
	}	
	
	#reinstate default items	
	$items['orders'] = 'Orders';
	#$items['members-area'] = 'Membership';
	$items['subscriptions'] = 'Subscriptions';
	$items['edit-address'] = 'Addresses';
	$items['edit-account'] = 'Account details';
	#$items['account-funds'] = 'Account funds';	
	$items['customer-logout'] = 'Logout';
	
    return $items;
}

add_filter( 'woocommerce_account_menu_items', 'prg_custom_my_account_menu_items' );

###
# Specify template to call (one per endpoint)
###

# Article Submission (apprentice)
function prg_custom_endpoint_case_study_submission_content() {
    include 'woocommerce/my-account/case-study-submission.php'; 
}
add_action( 'woocommerce_account_case-study-submission_endpoint', 'prg_custom_endpoint_case_study_submission_content' );

# Notes (student)
function prg_custom_endpoint_your_notes_content() {
    include 'woocommerce/my-account/your-notes.php'; 
}
add_action( 'woocommerce_account_your-notes_endpoint', 'prg_custom_endpoint_your_notes_content' );

# Job Alert (student and apprentice)
function prg_custom_endpoint_job_alert_content() {
    include 'woocommerce/my-account/job-alert.php'; 
}
add_action( 'woocommerce_account_job-alert_endpoint', 'prg_custom_endpoint_job_alert_content' );

# Review Alert (student and apprentice)
function prg_custom_endpoint_review_alert_content() {
    include 'woocommerce/my-account/review-alert.php'; 
}
add_action( 'woocommerce_account_review-alert_endpoint', 'prg_custom_endpoint_review_alert_content' );

# Jobs (subscriber)
function prg_custom_endpoint_your_jobs_content() {
    include 'woocommerce/my-account/your-jobs.php'; 
}
add_action( 'woocommerce_account_your-jobs_endpoint', 'prg_custom_endpoint_your_jobs_content' );

# Job Submission (subscriber)
function prg_custom_endpoint_job_submission_content() {
    include 'woocommerce/my-account/job-submission.php'; 
}
add_action( 'woocommerce_account_job-submission_endpoint', 'prg_custom_endpoint_job_submission_content' );

# Stored Reviews (student and apprentice)
function prg_custom_endpoint_stored_reviews_content() {
    include 'woocommerce/my-account/stored-reviews.php'; 
}
add_action( 'woocommerce_account_stored-reviews_endpoint', 'prg_custom_endpoint_stored_reviews_content' );

# Edit Job (subscriber)
function prg_custom_endpoint_register_apprentices_content() {
    include 'woocommerce/my-account/register-apprentices.php'; 
}
add_action( 'woocommerce_account_register-apprentices_endpoint', 'prg_custom_endpoint_register_apprentices_content' );

# Edit Job (subscriber)
function prg_custom_endpoint_register_graduates_content() {
    include 'woocommerce/my-account/register-graduates.php'; 
}
add_action( 'woocommerce_account_register-graduates_endpoint', 'prg_custom_endpoint_register_graduates_content' );

# Edit Survey (subscriber/employer)
/* #superceeded by register-add and register-grad
function prg_custom_endpoint_send_survey_content() {
    include 'woocommerce/my-account/send-survey.php'; 
}
add_action( 'woocommerce_account_send-survey_endpoint', 'prg_custom_endpoint_send_survey_content' );
*/

# All Surveys (subscriber/employer)
function prg_custom_endpoint_your_surveys_content() {
    include 'woocommerce/my-account/your-surveys.php'; 
}
add_action( 'woocommerce_account_your-surveys_endpoint', 'prg_custom_endpoint_your_surveys_content' );

# Day In The Life Of (apprentice)
function prg_custom_endpoint_day_in_life_submission_content() {
    include 'woocommerce/my-account/day-in-life-submission.php'; 
}
add_action( 'woocommerce_account_day-in-life-submission_endpoint', 'prg_custom_endpoint_day_in_life_submission_content' );

# Enhanced User Profile (employer)
function prg_custom_endpoint_enhanced_user_profile_submission_content() {
    include 'woocommerce/my-account/enhanced-user-profile.php'; 
}
add_action( 'woocommerce_account_enhanced-user-profile_endpoint', 'prg_custom_endpoint_enhanced_user_profile_submission_content' );

##########################
# REORDER/RENAME WOO ENDPOINTS
##########################
/*
function prg_woo_my_account_endpoint_order() {
	$myorder = array(
		'my-custom-endpoint' => __( 'My Stuff', 'woocommerce' ),
		'edit-account'       => __( 'Change My Details', 'woocommerce' ),
		'dashboard'          => __( 'Dashboard', 'woocommerce' ),
		'orders'             => __( 'Orders', 'woocommerce' ),
		'downloads'          => __( 'Download MP4s', 'woocommerce' ),
		'edit-address'       => __( 'Addresses', 'woocommerce' ),
		'payment-methods'    => __( 'Payment Methods', 'woocommerce' ),
		'customer-logout'    => __( 'Logout', 'woocommerce' ),
	);
	return $myorder;
}
add_filter ( 'woocommerce_account_menu_items', 'prg_woo_my_account_endpoint_order' );
*/

##########################
# AMEND CHECKOUT WORDING
##########################
add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

// Our hooked in function - $fields is passed via the filter!
function custom_override_checkout_fields( $fields ) {
     $fields['billing']['billing_address_2']['placeholder'] = 'Address Line 2 (optional)';
	 $fields['shipping']['shipping_address_2']['placeholder'] = 'Address Line 2 (optional)';
     return $fields;
}

##########################
# REMOVE DOWNLOADS FROM MY ACCOUNT
##########################
function prg_my_account_menu_items( $items ) {
	unset($items['downloads']);
	return $items;
}
add_filter( 'woocommerce_account_menu_items', 'prg_my_account_menu_items' );

##########################
# ONLY ONE ITEM PER ORDER
# SAVES ISSUES WITH MULTIPLE SUBS ON ONE ORDER ID
##########################
add_filter( 'woocommerce_add_cart_item_data', 'woo_custom_add_to_cart' );

function woo_custom_add_to_cart( $cart_item_data ) {

	global $woocommerce;
	$woocommerce->cart->empty_cart();

	#do nothing with the data and return
	return $cart_item_data;
}

##########################
# CUSTOMISE THE EXPORTED ORDERS REPORT
# WooCommerce Customer/Order CSV Export: Renaming / Removing / Reordering Columns
# https://gist.github.com/woogist/8838110
##########################

###
# rename a column
###
function wc_csv_export_rename_column( $column_headers ) {
	# rename the order_notes column to notes
	# make sure to not change the key (`order_notes`) in this case
	# as this matches the column to the relevant data
	# simply change the value of the array to change the column header that's exported
	$column_headers['created_at'] = 'order_date'; 
 
	return $column_headers;
}
add_filter( 'wc_customer_order_csv_export_order_headers', 'wc_csv_export_rename_column' );

###
# remove a column
###
function wc_csv_export_remove_column( $column_headers ) {
	
	# the list of column keys can be found in includes/class-wc-customer-order-csv-export-generator.php 
	# you need to repeat "unset( $column_headers['coupon_items'] );" per column that you want to remove
	
	#unset( $column_headers[	'order_id' ] );
	#unset( $column_headers[	'order_number_formatted' ] );
	#unset( $column_headers[	'order_number' ] );
	#unset( $column_headers[	'order_date' ] );
	#unset( $column_headers[	'status' ] );
	unset( $column_headers[	'shipping_total' ] );
	unset( $column_headers[	'shipping_tax_total' ] );
	unset( $column_headers[	'fee_total' ] );
	unset( $column_headers[	'fee_tax_total' ] );
	unset( $column_headers[	'tax_total' ] );
	unset( $column_headers[	'discount_total' ] );
	unset( $column_headers[	'order_total' ] );
	unset( $column_headers[	'refunded_total' ] );
	unset( $column_headers[	'order_currency' ] );
	unset( $column_headers[	'payment_method' ] );
	unset( $column_headers[	'shipping_method' ] );
	#unset( $column_headers[	'customer_id' ] );
	#unset( $column_headers[	'billing_first_name' ] );
	#unset( $column_headers[	'billing_last_name' ] );
	#unset( $column_headers[	'billing_full_name' ] );
	#unset( $column_headers[	'billing_company' ] );
	#unset( $column_headers[	'billing_email' ] );
	#unset( $column_headers[	'billing_phone' ] );
	#unset( $column_headers[	'billing_address_1' ] );
	#unset( $column_headers[	'billing_address_2' ] );
	#unset( $column_headers[	'billing_postcode' ] );
	#unset( $column_headers[	'billing_city' ] );
	#unset( $column_headers[	'billing_state' ] );
	#unset( $column_headers[	'billing_state_code' ] );
	#unset( $column_headers[	'billing_country' ] );
	unset( $column_headers[	'shipping_first_name' ] );
	unset( $column_headers[	'shipping_last_name' ] );
	unset( $column_headers[	'shipping_full_name' ] );
	unset( $column_headers[	'shipping_company' ] );
	unset( $column_headers[	'shipping_address_1' ] );
	unset( $column_headers[	'shipping_address_2' ] );
	unset( $column_headers[	'shipping_postcode' ] );
	unset( $column_headers[	'shipping_city' ] );
	unset( $column_headers[	'shipping_state' ] );
	unset( $column_headers[	'shipping_state_code' ] );
	unset( $column_headers[	'shipping_country' ] );
	unset( $column_headers[	'customer_note' ] );
	unset( $column_headers[	'shipping_items' ] );
	unset( $column_headers[	'fee_items' ] );
	unset( $column_headers[	'tax_items' ] );
	unset( $column_headers[	'coupon_items' ] );
	unset( $column_headers[	'refunds' ] );
	unset( $column_headers[	'order_notes' ] );
	#unset( $column_headers[	'download_permissions' ] );

	return $column_headers;
}
add_filter( 'wc_customer_order_csv_export_order_headers', 'wc_csv_export_remove_column' );


##########################
# SET ALL PRODUCTS TO VIRTUAL
# THIS PREVENTS THE SHIPPING INFO
##########################
/*
function prg_default_wc_product_type_options( $product_type_options ) {
    $product_type_options['virtual']['default'] = 'yes';
    #$product_type_options['downloadable']['default'] = 'yes';
    return $product_type_options;
}
add_filter( 'product_type_options', 'prg_default_wc_product_type_options' );
*/
##########################
# DISABLE WOOCOMMERCE REVIEWS
##########################
add_filter( 'woocommerce_product_tabs', 'prg_woo_remove_reviews_tab', 98 );
	function prg_woo_remove_reviews_tab($tabs) {
	unset($tabs['reviews']);
	unset($tabs['additional_information']);  	
	return $tabs;
}

##########################
# WOOCOMMERCE ADD PRICE TO DROP DOWN
##########################

add_filter( 'woocommerce_variation_option_name', 'prg_display_price_in_variation_option_name' );

function prg_display_price_in_variation_option_name( $term ) {
	
	global $wpdb, $product, $woocommerce;;
	
	$result = $wpdb->get_col( "SELECT slug FROM {$wpdb->prefix}terms WHERE name = '$term'" );
	
	$term_slug = ( !empty( $result ) ) ? $result[0] : $term;
	
	$query = "
		SELECT postmeta.post_id AS product_id
		FROM {$wpdb->prefix}postmeta AS postmeta
		LEFT JOIN {$wpdb->prefix}posts AS products ON ( products.ID = postmeta.post_id )
		WHERE postmeta.meta_key LIKE 'attribute_%'
		AND postmeta.meta_value = '$term_slug'		
		AND products.post_parent = '$product->id'
	";
	
	$variation_id = $wpdb->get_col( $query );
	
	$parent = wp_get_post_parent_id( $variation_id[0] );
	
	if ( $parent > 0 ) {
		$_product = new WC_Product_Variation( $variation_id[0] );
		
		$itemPrice = strip_tags (woocommerce_price( $_product->get_price() )); #price ex VAT
		#$itemPrice = strip_tags (woocommerce_price( $_product->get_price_including_tax() ));
		# this is where we can actually customise how the price that's displayed
		return $term . ' (' . $itemPrice . ') plus VAT';
	}
	return $term;

}  

##########################
# WOOCOMMERCE REMOVE CONTINUE SHOPPING BUTTON
##########################

add_filter( 'wc_add_to_cart_message', 'custom_add_to_cart_message' );
function custom_add_to_cart_message() {
	global $woocommerce;

	$return_to  = get_permalink(woocommerce_get_page_id('shop'));
	#$message    = sprintf('<a href="%s" class="button wc-forwards">%s</a> %s', $return_to, __('Continue Shopping', 'woocommerce'), __('Product successfully added to your cart.', 'woocommerce') );	
	$message    = 'Product successfully added to your basket.';	
	
	return $message;
}

##########################
# INCLUDE CUSTOM JS
##########################

add_action('admin_enqueue_scripts','prg_woo_order_filter');
function prg_woo_order_filter( $hook ){
    wp_enqueue_script( 
        'prg_woo_order_filter_script', 										#unique handle
        get_template_directory_uri().'/includes/woo-checkout-images.js',	#location
        array('jquery')														#dependencies
     );
}

##########################
# MIGRATE ORDER DETAILS TO USERS
# USED WHEN AN EMPLOYER REGISTERS TO CREATE USER META
##########################

add_action( 'woocommerce_thankyou', 'prg_update_usermeta_from_order');
 
function prg_update_usermeta_from_order( $order_id ) {

	#get the order details
	$order = new WC_Order( $order_id );
	
	#get the user id from the order
	$user_id = $order->user_id;    
	
	#get the items from the order
	$items = $order->get_items();	
	
	#get the product id from the order
	foreach ( $items as $item ) {
		$product_id = $item['product_id'];
	}	
	
	#we only want to update the user meta if a certain item has been purchased (employer registration)
	#id 600 = basic profile // id 1558 = enhanced profile
	if ($product_id == 600 || $product_id == 1558){		
		
		$company_name = get_post_meta( $order_id, 'company_name', $single = true);	
		$main_address = get_post_meta( $order_id, 'main_address', $single = true);
		
		#use geocoding to get the necessary details
		$geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?sensor=false&address=' . urlencode($main_address) . '&key=AIzaSyAj_Xc1gEDKeDqLIMFHgok9hhWgyf16TK0');
		$output = json_decode($geocode);
		$formatted_address = $output->results[0]->formatted_address;
		$latitude = $output->results[0]->geometry->location->lat;
		$longitude = $output->results[0]->geometry->location->lng;
		
		#convert into ACF Google Map
		$address = array(
			"address" => $formatted_address, 
			"lat" => $latitude, 
			"lng" => $longitude, 
			"zoom" => 14
		);	
		
		$industry = get_post_meta( $order_id, 'employer_registration_industry', $single = true);
		$number_of_employees = get_post_meta( $order_id, 'number_of_employees', $single = true);
		$graduates = get_post_meta( $order_id, 'graduates', $single = true);		
		$apprentices = get_post_meta( $order_id, 'apprentices', $single = true);
		$number_of_graduates_hiredyear = get_post_meta( $order_id, 'number_of_graduates_hiredyear', $single = true);
		$number_of_apprentices_hiredyear = get_post_meta( $order_id, 'number_of_apprentices_hiredyear', $single = true);
		
		$primary_name = get_post_meta( $order_id, 'primary_name', $single = true);
		$primary_position_held = get_post_meta( $order_id, 'primary_position_held', $single = true);
		$primary_email = get_post_meta( $order_id, 'primary_email', $single = true);
		$primary_telephone_number = get_post_meta( $order_id, 'primary_telephone_number', $single = true);
		
		$secondary_name = get_post_meta( $order_id, 'secondary_name', $single = true);
		$secondary_position_held = get_post_meta( $order_id, 'secondary_position_held', $single = true);
		$secondary_email = get_post_meta( $order_id, 'secondary_email', $single = true);
		$secondary_telephone_number = get_post_meta( $order_id, 'secondary_telephone_number', $single = true);
		
		$add_another_contact = get_post_meta( $order_id, 'add_another_contact', $single = true);
		$additional_name = get_post_meta( $order_id, 'additional_name', $single = true);
		$additional_position_held = get_post_meta( $order_id, 'additional_position_held', $single = true);
		$additional_email = get_post_meta( $order_id, 'additional_email', $single = true);
		$additional_telephone_number = get_post_meta( $order_id, 'additional_telephone_number', $single = true);	
		/*
		$free_employer_reviews_published_online = get_post_meta( $order_id, 'free_employer_reviews_published_online', $single = true);
		$free_employer_profile_online = get_post_meta( $order_id, 'free_employer_profile_-_online', $single = true);
		$top_companies_ranking_print_and_online = get_post_meta( $order_id, 'top_companies_ranking_-_print_and_online', $single = true);
		$top_companies_industry_sector_sponsorship_print = get_post_meta( $order_id, 'top_companies_industry_sector_sponsorship_-_print', $single = true);
		$top_companies_guide_advert_print = get_post_meta( $order_id, 'top_companies_guide_advert_-_print', $single = true);
		$top_companies_guide_advertorial_print = get_post_meta( $order_id, 'top_companies_guide_advertorial_-_print', $single = true);
		#$enhanced_employer_profile_online = get_post_meta( $order_id, 'enhanced_employer_profile_-_online', $single = true);
		$job_listing_online = get_post_meta( $order_id, 'job_listing_-_online', $single = true);
		$job_listing_ppc_campaigns_online = get_post_meta( $order_id, 'job_listing_ppc_campaigns_-_online', $single = true);
		$display_advertising_online = get_post_meta( $order_id, 'display_advertising_-_online', $single = true);
		$social_media_promotion_facebook_twitter_linkedin = get_post_meta( $order_id, 'social_media_promotion_-_facebook_twitter_linkedin', $single = true);
		$newsletter_promotion_online = get_post_meta( $order_id, 'newsletter_promotion_-_online', $single = true);
		$tickets_to_annual_awards_show = get_post_meta( $order_id, 'tickets_to_annual_awards_show', $single = true);
		$sponsorship_of_annual_awards_show = get_post_meta( $order_id, 'sponsorship_of_annual_awards_show', $single = true);
		$employer_benchmark_reports = get_post_meta( $order_id, 'employer_benchmark_reports', $single = true);
		$yes_please_enter_me_into_the_free_prize_draw = get_post_meta( $order_id, 'yes_please_enter_me_into_the_free_prize_draw', $single = true);
		*/
		$use_billing_details_as_primary_contact = get_post_meta( $order_id, 'use_billing_details_as_primary_contact', $single = true);
		###
		
		if ($use_billing_details_as_primary_contact== '1'){
			# if this is selected, we need to set the billing details as the primary contact information
			$billing_first_name = get_post_meta( $order_id, '_billing_first_name', true );
			$billing_last_name = get_post_meta( $order_id, '_billing_last_name', true );
			$billing_company = get_post_meta( $order_id, '_billing_company', true );
			$billing_email = get_post_meta( $order_id, '_billing_email', true );
			$billing_phone = get_post_meta( $order_id, '_billing_phone', true );
			
			update_user_meta( $user_id, 'company_name', $billing_company );
			
			update_user_meta( $user_id, 'primary_name', $billing_first_name.' '.$billing_last_name );
			update_user_meta( $user_id, 'primary_email', $billing_email );
			update_user_meta( $user_id, 'primary_telephone_number', $billing_phone );	
			
		} else {
			
			update_user_meta( $user_id, 'company_name', $company_name );
			
			update_user_meta( $user_id, 'primary_name', $primary_name );
			update_user_meta( $user_id, 'primary_email', $primary_email );
			update_user_meta( $user_id, 'primary_telephone_number', $primary_telephone_number );			
			
		}
		 
		
		#update_user_meta( $user_id, 'company_name', $company_name );
		update_user_meta( $user_id, 'main_address', $main_address );
		update_user_meta( $user_id, 'google_map', $address ); # we use this as an additional because address data is an array
		update_user_meta( $user_id, 'employer_registration_industry', $industry );
		update_user_meta( $user_id, 'number_of_employees', $number_of_employees );
		update_user_meta( $user_id, 'graduates', $graduates );		
		update_user_meta( $user_id, 'apprentices', $apprentices );						
		update_user_meta( $user_id, 'number_of_graduates_hiredyear', $number_of_graduates_hiredyear );
		update_user_meta( $user_id, 'number_of_apprentices_hiredyear', $number_of_apprentices_hiredyear );
		
		#update_user_meta( $user_id, 'primary_name', $primary_name );
		update_user_meta( $user_id, 'primary_position_held', $primary_position_held );
		#update_user_meta( $user_id, 'primary_email', $primary_email );
		#update_user_meta( $user_id, 'primary_telephone_number', $primary_telephone_number );
		
		update_user_meta( $user_id, 'secondary_name', $secondary_name );
		update_user_meta( $user_id, 'secondary_position_held', $secondary_position_held );
		update_user_meta( $user_id, 'secondary_email', $secondary_email );
		update_user_meta( $user_id, 'secondary_telephone_number', $secondary_telephone_number );	
				
		update_user_meta( $user_id, 'add_another_contact', $add_another_contact );			
		update_user_meta( $user_id, 'additional_name', $additional_name );
		update_user_meta( $user_id, 'additional_position_held', $additional_position_held );
		update_user_meta( $user_id, 'additional_email', $additional_email );
		update_user_meta( $user_id, 'additional_telephone_number', $additional_telephone_number );		
		/*
		update_user_meta( $user_id, 'free_employer_reviews_published_online', $free_employer_reviews_published_online );	
		update_user_meta( $user_id, 'free_employer_profile_-_online', $free_employer_profile_online );	
		update_user_meta( $user_id, 'top_companies_ranking_-_print_and_online', $top_companies_ranking_print_and_online );	
		update_user_meta( $user_id, 'top_companies_industry_sector_sponsorship_-_print', $top_companies_industry_sector_sponsorship_print );	
		update_user_meta( $user_id, 'top_companies_guide_advert_-_print', $top_companies_guide_advert_print );	
		update_user_meta( $user_id, 'top_companies_guide_advertorial_-_print', $top_companies_guide_advertorial_print );	
		#update_user_meta( $user_id, 'enhanced_employer_profile_-_online', $enhanced_employer_profile_online );	
		update_user_meta( $user_id, 'job_listing_-_online', $job_listing_online );	
		update_user_meta( $user_id, 'job_listing_ppc_campaigns_-_online', $job_listing_ppc_campaigns_online );	
		update_user_meta( $user_id, 'display_advertising_-_online', $display_advertising_online );	
		update_user_meta( $user_id, 'social_media_promotion_-_facebook_twitter_linkedin', $social_media_promotion_facebook_twitter_linkedin );	
		update_user_meta( $user_id, 'newsletter_promotion_-_online', $newsletter_promotion_online );	
		update_user_meta( $user_id, 'tickets_to_annual_awards_show', $tickets_to_annual_awards_show );	
		update_user_meta( $user_id, 'sponsorship_of_annual_awards_show', $sponsorship_of_annual_awards_show );	
		update_user_meta( $user_id, 'employer_benchmark_reports', $employer_benchmark_reports );	
		update_user_meta( $user_id, 'yes_please_enter_me_into_the_free_prize_draw', $yes_please_enter_me_into_the_free_prize_draw );	
		*/
		###
		# activate the membership, can amend via wp-admin > users > edit user
		# adding this to a user will add them into all areas of the site by searching users with an active membership
		###
		
		$activate_membership = 'Yes';
		update_user_meta( $user_id, 'activate_membership', $activate_membership );							
	
	}
 
}

##########################
# WOOCOMMERCE AMEND CHECKOUT FIELD LABELS
##########################
add_filter( 'woocommerce_checkout_fields' , 'prg_override_checkout_fields' );

function prg_override_checkout_fields( $fields ) {
     $fields['billing']['billing_address_2']['placeholder'] = 'Address Line 2 (optional)';
	 $fields['shipping']['shipping_address_2']['placeholder'] = 'Address Line 2 (optional)';
     return $fields;
}

##########################
# REMOVE ADDITIONAL CHECKOUT FIELDS FOR NON MEMBERSHIP PRODUCTS
##########################

add_filter( 'woocommerce_checkout_fields' , 'remove_fields', 2000 );

function remove_fields( $fields ) { 

	global $woocommerce;
	
	#get the category ids for e-guide and job subscription
	$category_IDs = array(20,22);
	
	#loop through all the products in the basket
	foreach ($woocommerce->cart->cart_contents as $key => $values ) {
		$terms = get_the_terms( $values['product_id'], 'product_cat' );    
		foreach ($terms as $term) {        
			if(in_array($term->term_id, $category_IDs)){
				
				unset($fields['order']['order_comments']);
				
				unset($fields['order']['use_billing_details_as_primary_contact']);
				
				unset($fields['order']['company_name']);
				unset($fields['order']['main_address']);
				unset($fields['order']['employer_registration_industry']);				
				unset($fields['order']['number_of_employees']);
				unset($fields['order']['graduates']);
				unset($fields['order']['apprentices']);
				unset($fields['order']['number_of_graduates_hiredyear']);
				unset($fields['order']['number_of_apprentices_hiredyear']);
				
				unset($fields['order']['primary_name']);
				unset($fields['order']['primary_position_held']);
				unset($fields['order']['primary_email']);
				unset($fields['order']['primary_telephone_number']);
				
				unset($fields['order']['secondary_name']);
				unset($fields['order']['secondary_position_held']);
				unset($fields['order']['secondary_email']);
				unset($fields['order']['secondary_telephone_number']);
				
				unset($fields['order']['add_another_contact']);
				unset($fields['order']['additional_name']);
				unset($fields['order']['additional_position_held']);
				unset($fields['order']['additional_email']);
				unset($fields['order']['additional_telephone_number']);
				/*
				unset($fields['order']['free_employer_reviews_published_online']);
				unset($fields['order']['free_employer_profile_-_online']);
				unset($fields['order']['top_companies_ranking_-_print_and_online']);
				unset($fields['order']['top_companies_industry_sector_sponsorship_-_print']);
				unset($fields['order']['top_companies_guide_advert_-_print']);
				unset($fields['order']['top_companies_guide_advertorial_-_print']);
				unset($fields['order']['enhanced_employer_profile_-_online']);
				unset($fields['order']['job_listing_-_online']);
				unset($fields['order']['job_listing_ppc_campaigns_-_online']);
				unset($fields['order']['display_advertising_-_online']);
				unset($fields['order']['social_media_promotion_-_facebook_twitter_linkedin']);
				unset($fields['order']['newsletter_promotion_-_online']);
				unset($fields['order']['tickets_to_annual_awards_show']);
				unset($fields['order']['sponsorship_of_annual_awards_show']);
				unset($fields['order']['employer_benchmark_reports']);
				unset($fields['order']['yes_please_enter_me_into_the_free_prize_draw']);			
				*/
				break;
			}
			break;
		}
	 }
    
	unset($fields['order']['order_comments']); #remove order notes field
	return $fields;

}

##########################
# E-GUIDES/PRINTED DIRS SPECIFIC PAYMENT GATEWAY
# only allow paypal, remove all other options
##########################

function prg_directories_specific_payment($gateways){

	global $woocommerce;

	foreach ($woocommerce->cart->cart_contents as $key => $values ) {

		# ID(s) of the category we want to remove gateways from
		$category_ids = array( 22 );

		# Get the terms, i.e. category list using the ID of the product
		$terms = get_the_terms( $values['product_id'], 'product_cat' );

		# Because a product can have multiple categories, we need to iterate through the list of the products category for a match
		foreach ($terms as $term) {

			if(in_array($term->term_id,$category_ids)){

				# If you have additional items from a non-restricted category in the cart with a restricted category product the only payment option available for all cart items will be the gateway(s) that haven't been unset.
				# WARNING: If you unset all the gateways users will get a "Sorry, it seems that there are no available payment methods for your state. Please contact us if you require assistance or wish to make alternate arrangements." when trying to checkout!
				#unset($gateways['cod']);
				#unset($gateways['bacs']);
				unset($gateways['cheque']);
				unset($gateways['accountfunds']);
				break;
			}
		break;
        }

	}
	return $gateways;
}
add_filter('woocommerce_available_payment_gateways','prg_directories_specific_payment');

##########################
# CREATE JOB SUBSCRIPTION UPON SIGNUP
# https://gist.github.com/corsonr/d661b88408f6ea373dcee62d310de359#file-create-woocommerce-order-dynamically-php
##########################

add_action( 'woocommerce_thankyou', 'prg_create_free_job_subscription_from_order');
function prg_create_free_job_subscription_from_order( $order_id ) {
			
	#create a flag we can pass to our email function
	global $disableOrderCompleteEmail;		

	#set flag in order to disable email
	$disableOrderCompleteEmail = 'false';	
	
	#get an instance of the WC_Order object
	#using get_data() method allow us to access to the protected data (associative array mode) 
	$order = wc_get_order( $order_id );

	#get the order data
	$order_data = $order->get_data();

	#get the order details
	$order = new WC_Order( $order_id );

	#get the user id from the order
	$user_id = $order->user_id;    

	#get the items from the order
	$items = $order->get_items();	

	#get the product id from the order
	foreach ( $items as $item ) {
		$product_id = $item['product_id'];
	}	
	
	#get our conditional - prevents the order page being refreshed and generating more job subscriptions
	$free_jobs_created = get_field( "free_jobs_created", $order_id );	
	
	if ($free_jobs_created !='1'){
		
		#true/false not yet set, so we must update the field
		#use the acf key as the value doesn't exist
		update_field('field_5a5341a9180f4', 1, $order_id);
		
		if ($product_id == 600){		

			$maximum = 0;
			#require get_template_directory() . '/includes/create-job-subscription-order.php';	

			for ($x = 0; $x <= $maximum; $x++) {
				require get_template_directory() . '/includes/create-job-subscription-order.php';
			}		

		} elseif ($product_id == 1558){	

			$maximum = 3; #counter starts at 0! 
			#require get_template_directory() . '/includes/create-job-subscription-order.php';

			for ($x = 0; $x <= $maximum; $x++) {
				require get_template_directory() . '/includes/create-job-subscription-order.php';
			}			

		} else {

			#no further action required

		}							
		
	}
			
}	



##########################
# DISABLE WOOCOMMERCE EMAILS FOR GENERATED ORDERS
# https://docs.woocommerce.com/document/unhookremove-woocommerce-emails/
##########################

add_action( 'woocommerce_email', 'prg_disable_emails' );
function prg_disable_emails( $email_class ) {
	
	#get our global variable
	global $disableOrderCompleteEmail;

	$subject = 'Get Global Var';
	$message = 'Global Var: '.$disableOrderCompleteEmail;
	$email_address = 'ian@prgltd.co.uk';
	#wp_mail( $email_address, $subject, $message );		
	
	if ($disableOrderCompleteEmail == 'true') {
		remove_action( 'woocommerce_order_status_completed_notification', array( $email_class->emails['WC_Email_Customer_Completed_Order'], 'trigger' ) );		
	}

}

##########################
# RENAME COUPON TO VOUCHER
########################## 

#rename the coupon field on the cart page
function woocommerce_rename_coupon_field_on_cart( $translated_text, $text, $text_domain ) {
	#bail if not modifying frontend woocommerce text
	if ( is_admin() || 'woocommerce' !== $text_domain ) {
		return $translated_text;
	}
	if ( 'Coupon:' === $text ) {
		$translated_text = 'Voucher Code:';
	}
	return $translated_text;
}
add_filter( 'gettext', 'woocommerce_rename_coupon_field_on_cart', 10, 3 );

#rename the "Have a Coupon?" message on the checkout page
function woocommerce_rename_coupon_message_on_checkout() {
	return 'Have a Voucher Code?' . ' <a href="#" class="showcoupon">' . __( 'Click here to enter your code', 'woocommerce' ) . '</a>';
}
add_filter( 'woocommerce_checkout_coupon_message', 'woocommerce_rename_coupon_message_on_checkout' );
#rename the coupon field on the checkout page
function woocommerce_rename_coupon_field_on_checkout( $translated_text, $text, $text_domain ) {
	// bail if not modifying frontend woocommerce text
	if ( is_admin() || 'woocommerce' !== $text_domain ) {
		return $translated_text;
	}
	if ( 'Coupon code' === $text ) {
		$translated_text = 'Voucher Code';
	
	} elseif ( 'Apply coupon' === $text ) {
		$translated_text = 'Apply Code';
	}
	return $translated_text;
}
add_filter( 'gettext', 'woocommerce_rename_coupon_field_on_checkout', 10, 3 );


##########################
# AUTO COMPLETE BACS ORDERS IN WOOCOMMERCE
########################## 

add_action( 'woocommerce_thankyou', 'prg_woocommerce_auto_complete_bacs_order', 10, 1 );
function prg_woocommerce_auto_complete_bacs_order( $order_id ) {
	if ( ! $order_id )
	return;

	$order = wc_get_order( $order_id );

    # No updated status for orders delivered with Bank wire, Cash on delivery and Cheque payment methods.
    if ( ( 'bacs' == get_post_meta($order_id, '_payment_method', true) ) || ( 'cod' == get_post_meta($order_id, '_payment_method', true) ) || ( 'cheque' == get_post_meta($order_id, '_payment_method', true) ) ) {

        $order->update_status( 'completed' );
    }
}

##########################
# REMOVE RELATED PRODUCTS
##########################
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );



########################## 
# REMOVE WOO MEMBERSHIP THANK YOU 
#https://github.com/skyverge/wc-plugins-snippets/blob/master/woocommerce-memberships/remove-thankyou-memberships-notice.php
########################## 
# Removes Memberships "thank you" message from thank you page
function sv_wc_memberships_remove_email_thankyou() {
	if ( function_exists( 'wc_memberships' ) ) {
		remove_action( 'woocommerce_thankyou', array( wc_memberships()->get_frontend_instance(), 'maybe_render_thank_you_content' ), 9 );
	}
}
add_action( 'init', 'sv_wc_memberships_remove_email_thankyou' );


########################## 
# REMOVE WOO SUBSCRIPTION THANK YOU 
#https://nicola.blog/2015/01/21/customize-thank-you-message-woocommerce-subscriptions/
########################## 
function custom_subscription_thank_you( $order_id ){

    if( WC_Subscriptions_Order::order_contains_subscription( $order_id ) ) {
        $thank_you_message = sprintf( __( '%sThank you for purchasing our subscription! Visit %syour account%s page to know its status.%s', 'woocommerce-subscriptions' ), '<p>', '<a href="' . get_permalink( wc_get_page_id( 'myaccount' ) ) . '">', '</a>','</p>' );

        return $thank_you_message;
    }

}
add_filter( 'woocommerce_subscriptions_thank_you_message', 'custom_subscription_thank_you');

########################## 
# ADD TO CART REDIRECT
# used for claiming account
########################## 
/*
function custom_add_to_cart_redirect() { 
    return 'https://www.thejobcrowds.com/migrationTest/claim-account/'; 
}
add_filter( 'woocommerce_add_to_cart_redirect', 'custom_add_to_cart_redirect' );
*/

add_filter ('add_to_cart_redirect', 'prg_redirect_to_claim_account');
function prg_redirect_to_claim_account() {
	
	global $woocommerce;

	#get product ID
	$product_id = (int) apply_filters('woocommerce_add_to_cart_product_id', $_POST['product_id']);

	#check if product ID is in a certain category
	if( has_term( 'directory', 'product_cat', $product_id ) ){
		
		#set redirect URL
		$url = site_url();
		#$checkout_url = get_permalink(get_option('woocommerce_checkout_page_id'));
		$checkout_url = $url.'/sign-up/';
		
		#return the new URL
		return $checkout_url;
		
	};
	
}


##########################
# RENAME PROCEED TO CHECKOUT
##########################
remove_action( 'woocommerce_proceed_to_checkout', 'woocommerce_button_proceed_to_checkout', 20 ); 
add_action('woocommerce_proceed_to_checkout', 'prg_woo_custom_checkout_button_text',20);

function prg_woo_custom_checkout_button_text() {
	global $woocommerce;
	$checkout_url = WC()->cart->get_checkout_url();
	if( has_term( 'directory', 'product_cat', $product_id ) ){
	?>
		<a href="<?php echo $checkout_url; ?>" class="checkout-button button alt wc-forward" title="Proceed to register"><?php  _e( 'Proceed to register', 'woocommerce' ); ?></a> 
	<?php
	}else{
	?>
		<a href="<?php echo $checkout_url; ?>" class="checkout-button button alt wc-forward" title="Proceed to checkout"><?php  _e( 'Proceed to checkout', 'woocommerce' ); ?></a> 
	<?php		
	}
} 

########################## 
# AMEND PRICE FROM ON PRODUCT PAGE
########################## 
/*
add_filter( 'woocommerce_variable_price_html', 'prg_woo_single_product_page_price_from_text', 10, 2 );
 
function prg_woo_single_product_page_price_from_text( $price, $product ) {
 
	# 1. Find the minimum regular and sale prices
	#$min_var_reg_price = $product->get_variation_regular_price( 'min', true );
	$min_var_reg_price = $product->get_price(); #price ex VAT
	#$min_var_reg_price = $product->get_price_including_tax(); #price inc VAT
	$min_var_sale_price = $product->get_variation_sale_price( 'min', true );

	# 2. New $price
	if ( $min_var_sale_price < $min_var_reg_price ) {
		$price = sprintf( __( 'From: <del>%1$s</del><ins>%2$s</ins>', 'woocommerce' ), wc_price( $min_var_reg_price ), wc_price( $min_var_sale_price ) );
	} else {
		$price = sprintf( __( 'From: %1$s plus VAT', 'woocommerce' ), wc_price( $min_var_reg_price ) );
	}

	# 3. Return edited $price
	return $price;
	
}
*/

########################## 
# WOO CUSTOM PHONE VALIDATION
########################## 
add_action('woocommerce_checkout_process', 'prg_custom_validate_billing_phone');
function prg_custom_validate_billing_phone() {
	$is_correct = preg_match('/^[0-9]{6,20}$/', $_POST['billing_phone']);
	if ( $_POST['billing_phone'] && !$is_correct) {
		wc_add_notice( __( 'The Phone field should be <strong>between 6 and 20 digits <u>without</u> spaces</strong>.' ), 'error' );
	}
}

##############################################################################
# 4) GRAVITY FORMS
##############################################################################

##########################
# SET MUTLI-STEP FORM TO ZERO
##########################

add_filter( 'gform_progressbar_start_at_zero', '__return_true' );

##########################
# GF MIN CHARACTER LINIT 
##########################

require get_template_directory() . '/includes/gravity-whizz-GW_Minimum_Characters.php';

# Configuration
new GW_Minimum_Characters( array( 
    'form_id' => 4,
    'field_id' => 7,
    'min_chars' => 20,
    'max_chars' => 500,
    'min_validation_message' => __( 'Oops! You need to enter at least %s characters.' ),
    'max_validation_message' => __( 'Oops! You can only enter %s characters.' )
) );

##########################
# GF LIMIT NUMBER OF CHECKBOXES
# https://gravitywiz.com/limiting-how-many-checkboxes-can-be-checked/
##########################

require get_template_directory() . '/includes/gravity-whizz-GFLimitCheckboxes.php';

# Configuration

###
# graduate review
###
new GFLimitCheckboxes(5, array(
	18 => array(
		'min' => 0, 
		'max' => 3
	),
	20 => array(
		'min' => 0, 
		'max' => 3
	),
	55 => array(
		'min' => 0, 
		'max' => 3
	),
	87 => array(
		'min' => 0, 
		'max' => 3
	),
	96 => array(
		'min' => 0, 
		'max' => 3
	),
	103 => array(
		'min' => 0, 
		'max' => 3
	),
	117 => array(
		'min' => 0, 
		'max' => 3
	)	
));

###
# apprentice review
###
new GFLimitCheckboxes(6, array(
	11 => array(
		'min' => 0, 
		'max' => 3
	),
	13 => array(
		'min' => 0, 
		'max' => 3
	),
	66 => array(
		'min' => 0, 
		'max' => 3
	),
	75 => array(
		'min' => 0, 
		'max' => 3
	),
	82 => array(
		'min' => 0, 
		'max' => 3
	),
	89 => array(
		'min' => 0, 
		'max' => 3
	),
	96 => array(
		'min' => 0, 
		'max' => 3
	)	
));


##########################
# GF DYNAMICALLY POPULATE GRADUATE REVIEW FORM
##########################

###
# Employers
###

add_filter( 'gform_pre_render', 'graduate_review_employer' );
add_filter( 'gform_pre_validation', 'graduate_review_employer' );
add_filter( 'gform_admin_pre_render', 'graduate_review_employer' );
add_filter( 'gform_pre_submission_filter', 'graduate_review_employer' );

function graduate_review_employer( $form ) {

    # only populate select buttons for form id 5
    if ( $form['id'] != 5 ) {
       return $form;
    }

    # get the values from the select dynamic_filled_industry in group ID 206
	# this is populated from the main ACF options page
	$acf_field_key = "field_5964f3ad56407";
	$acf_field = get_field_object($acf_field_key);	

    # creating item array.
    $items = array();

	foreach( $acf_field['choices'] as $k => $v ){
		$items[] = array("value" => $k , "text" => $v );				
	}
	
    #add option for 'other'
    $items[] = array( 'text' => 'Other (please specify)', 'value' => 'Other (please specify)' );	

    # adding items to field id 12.
    foreach ( $form['fields'] as &$field ) {
        if ( $field->id == 12 ) {            
            $field->choices = $items;
        }
    }

    return $form;
}

###
# INDUSTRY
###

add_filter( 'gform_pre_render', 'graduate_review_industry' );
add_filter( 'gform_pre_validation', 'graduate_review_industry' );
add_filter( 'gform_admin_pre_render', 'graduate_review_industry' );
add_filter( 'gform_pre_submission_filter', 'graduate_review_industry' );

function graduate_review_industry( $form ) {

    # only populate select buttons for form id 5
    if ( $form['id'] != 5 ) {
       return $form;
    }

    # get the values from the select dynamic_filled_industry in group ID 206
	# this is populated from the main ACF options page
	$acf_field_key = "field_5964f3ad5658a";
	$acf_field = get_field_object($acf_field_key);	

    # creating item array.
    $items = array();

	foreach( $acf_field['choices'] as $k => $v ){
		$items[] = array("value" => $k , "text" => $v );				
	}

    # adding items to field id 14.
    foreach ( $form['fields'] as &$field ) {
        if ( $field->id == 14 ) {            
            $field->choices = $items;
        }
    }

    return $form;
}

##########################
# GF DYNAMICALLY POPULATE APPRENTICE REVIEW FORM
##########################

###
# Employers
###

add_filter( 'gform_pre_render', 'apprentice_review_employer' );
add_filter( 'gform_pre_validation', 'apprentice_review_employer' );
add_filter( 'gform_admin_pre_render', 'apprentice_review_employer' );
add_filter( 'gform_pre_submission_filter', 'apprentice_review_employer' );

function apprentice_review_employer( $form ) {

    # only populate select buttons for form id 5
    if ( $form['id'] != 6 ) {
       return $form;
    }

    # get the values from the select dynamic_filled_industry in group ID 206
	# this is populated from the main ACF options page
	$acf_field_key = "field_5964f3ad56407";
	$acf_field = get_field_object($acf_field_key);	

    # creating item array.
    $items = array();

	foreach( $acf_field['choices'] as $k => $v ){
		$items[] = array("value" => $k , "text" => $v );				
	}

    #add option for 'other'
    $items[] = array( 'text' => 'Other (please specify)', 'value' => 'Other (please specify)' );			
	
    # adding items to field id 12.
    foreach ( $form['fields'] as &$field ) {
        if ( $field->id == 5 ) {            
            $field->choices = $items;
        }
    }

    return $form;
}

###
# INDUSTRY
###

add_filter( 'gform_pre_render', 'apprentice_review_industry' );
add_filter( 'gform_pre_validation', 'apprentice_review_industry' );
add_filter( 'gform_admin_pre_render', 'apprentice_review_industry' );
add_filter( 'gform_pre_submission_filter', 'apprentice_review_industry' );

function apprentice_review_industry( $form ) {

    # only populate select buttons for form id 5
    if ( $form['id'] != 6 ) {
       return $form;
    }

    # get the values from the select dynamic_filled_industry in group ID 206
	# this is populated from the main ACF options page
	$acf_field_key = "field_5964f3ad5658a";
	$acf_field = get_field_object($acf_field_key);	

    # creating item array.
    $items = array();

	foreach( $acf_field['choices'] as $k => $v ){
		$items[] = array("value" => $k , "text" => $v );				
	}

    # adding items to field id 14.
    foreach ( $form['fields'] as &$field ) {
        if ( $field->id == 7 ) {            
            $field->choices = $items;
        }
    }

    return $form;
}

##########################
# UPDATE SURVEY LIST IF SURVEY IS PARTIAL COMPLETE
##########################

function prg_update_recipient_list_partially_complete($submission, $resume_token, $form, $entry) {
	
	#target form ID 8
	if($form["id"] != 8) {
		#stop here for forms which don't have the id 8
		return;
	}
	
	#get the import id
	$iid = rgar( $entry, '5' );
	
	#split the import id. 
	#first part is the post ID
	#second part is the row no.
	$row 		= explode("-", $iid);
	$list_id	= $row[0];	
	$row_id		= $row[1];	

	#run the repeater to get the row we neeed
	if(get_field('recipients',$list_id)) {
		$i=0;
		while(has_sub_field('recipients',$list_id)) {
		$i++;
			$import_id = get_sub_field('import_id');
			$forename = get_sub_field('forename');
			$surname = get_sub_field('surname');
			$email_address = get_sub_field('email_address');
			
			if( $import_id == $iid ){
				
				#isolate the specific row and change the completed value (its a true/false so saves as 1/0 respectively)
				#$selector = array('recipients', $row_id, 'partially_completed');
				$selector = array('recipients', $i, 'partially_completed'); #changed to $i as this is the row, otherwise, if a row is removed in wp-admin, they're not in sync
				$value = '1';
				update_sub_field( $selector, $value, $list_id );
				
				$email = $email_address; #can remove this as exists for test/debug

				break;
			}
			
		}
	}		
}
add_action( 'gform_incomplete_submission_post_save', 'prg_update_recipient_list_partially_complete', 10, 4 );


##########################
# UPDATE SURVEY LIST IF SURVEY IS COMPLETE
##########################

#set the form value within the gform_after_submission hook to target that form specifically
add_action( 'gform_after_submission_8', 'prg_update_recipient_list_complete', 10, 2 );
function prg_update_recipient_list_complete( $entry, $form ) {

	#get the import id
	$iid = rgar( $entry, '5' );
	
	#split the import id. 
	#first part is the post ID
	#second part is the row no.
	$row 		= explode("-", $iid);
	$list_id	= $row[0];	
	$row_id		= $row[1];	

	#run the repeater to get the row we neeed
	if(get_field('recipients',$list_id)) {
		$i=0;
		while(has_sub_field('recipients',$list_id)) {
		$i++;
			$import_id = get_sub_field('import_id');
			$forename = get_sub_field('forename');
			$surname = get_sub_field('surname');
			$email_address = get_sub_field('email_address');
			
			if( $import_id == $iid ){
				
				#isolate the specific row and change the completed value (its a true/false so saves as 1/0 respectively)
				#$selector = array('recipients', $row_id, 'completed');
				$selector = array('recipients', $i, 'completed');
				$value = '1';
				update_sub_field( $selector, $value, $list_id );
				
				$email = $email_address; #can remove this as exists for test/debug

				break;
			}
	
		}
	}	
}

##########################
# GDPR :: DELETE DATA ENTRY AFTER SUBMIISION
# https://docs.gravityforms.com/delete-entry-data-after-submission/
##########################
/*
# Target submissions from form ID  1.
# Change gform_after_submission_1 to reflect your target form ID, or use gform_after_submission to target all forms.
add_action( 'gform_after_submission', 'remove_form_entry' );
function remove_form_entry( $entry ) {
	GFAPI::delete_entry( $entry['id'] );
}
*/

##########################
# GENERATE EMAIL WHEN REVIEW CREATED
########################## 
add_action( 'gform_postcreation_post_after_creation_5', 'gf_send_notification_for_drafts'); #grad reviews
add_action( 'gform_postcreation_post_after_creation_6', 'gf_send_notification_for_drafts'); #app reviews
function gf_send_notification_for_drafts( $post_id, $feed, $entry, $form ) {

	#get the post type from the ID
	$post_type = get_post_type( $post_id );
	if ($post_type == 'graduate_reviews'){
		$type = 'graduate';
	} else {
		$type = 'apprentice';
	}
	
	
	# get the review details
	$title = wp_strip_all_tags(get_the_title($post_id));	
	$url = home_url( '/' );
	$edit_post = $url.'wp-admin/post.php?post='.$post_id.'&action=edit';
	
	# create the mail function and notify the admin
	#$to			= get_option( 'admin_email' );
	$to			= 'jo@thejobcrowd.com';
	$subject	= "New {$type} review added";
	$message	= "A new {$type} review has been submitted for approval. <br/>";
	#$message	.= "Post ID: {$post_id} <br/>";
	$message	.= "Login to approve or edit the review: <a href=\"{$edit_post}\">{$title}</a>";
	
	
	$headers = array(
		'Content-Type: text/html; charset=UTF-8',
		'Cc:' . 'publishingteam@thejobcrowd.com',
		'Bcc:' . 'ian@prgltd.co.uk',
		'From: '.get_option( 'blogname' ).' <info@thejobcrowd.com>'
	);
	wp_mail ($to, $subject, $message, $headers);	
	
}

/*
add_action( 'gform_postcreation_post_after_creation', function( $post_id, $feed, $entry, $form ) {
#email code here
} );
*/
##############################################################################
# 5) AJAX
##############################################################################

##########################
# STORE STUDENT REVIEW 
##########################

# Enqueue our Scripts and Styles Properly
function theme_enqueue() {

	$theme_url  = get_template_directory_uri();		# Used to keep our Template Directory URL
	$ajax_url   = admin_url( 'admin-ajax.php' );	# Localized AJAX URL

    # Register Our Script for Localization
    wp_register_script(
		'store-review',								# Our Custom Handle
		"{$theme_url}/includes/store-review.js",			# Script URL, this script is located for me in `theme-name/scripts/um-modifications.js`
		array( 'jquery' ),							# Dependant Array
		'1.0',										# Script Version ( Arbitrary )
		true										# Enqueue in Footer
    );

	# Localize Our Script so we can use `ajax_url`
	wp_localize_script(
		'store-review',
		'ajax_url',
		$ajax_url
	);

	# Finally enqueue our script
	wp_enqueue_script( 'store-review' );
}
add_action( 'wp_enqueue_scripts', 'theme_enqueue' );


###
# AJAX Callback
# Always Echos and Exits
###

function prgStoreReview_modifications_callback() {

    # Ensure we have the data we need to continue
    if( ! isset( $_POST ) || empty( $_POST ) || ! is_user_logged_in() ) {

        # If we don't - return custom error message and exit
        header( 'HTTP/1.1 400 Empty POST Values' );
        echo 'Could Not Verify POST Values.';
        exit;
    }

    $user_id		= sanitize_text_field( $_POST['uid'] );		# Sanitize our user ID
	$review_id		= sanitize_text_field( $_POST['rid'] );		# Sanitize our review ID
    $review_title	= sanitize_text_field( $_POST['title'] );	# Sanitize our review title
	
	# Insert data into the repeater
	$row = array(
		'field_595ceca2bddf1'	=> $review_id,
		'field_595cecbabddf2'	=> $review_title
	);
	$i = add_row('field_595cec87bddf0', $row, 'user_'.$user_id);

    exit;
}
add_action( 'wp_ajax_nopriv_prgStoreReview', 'prgStoreReview_modifications_callback' );
add_action( 'wp_ajax_prgStoreReview', 'prgStoreReview_modifications_callback' );



##############################################################################
# 6) ALERTS / CRON
##############################################################################

##########################
# POST JOB ID TO ORDER ID
# THIS WILL PREVENT MULTIPLE JOB SUBMISSIONS PER SUBSCRIPTION
# ALSO UPDATE ADMIN NEW JOB SUBMISSION 
##########################

function one_job_submission_per_subscription( $post_id ) {   
	
	# add conditional to check form was triggered		
	if(($_POST['acf'] != '')) {
		
		if(($_POST['acf']['field_595f933c6699e'] != '')) {
	
		# bail early if editing in admin
		if( is_admin() ) {		
			return;		
		}		
		
		# get the value from our single form		
		$job_subscription_order_id = $_POST['acf']['field_595f933c6699e'];					

		# add the detail to the order
		add_post_meta($job_subscription_order_id, "job_subscription_order_id", $post_id ); 
		
		# get the job post details
		$title = wp_strip_all_tags(get_the_title($post_id));
		#$url = get_permalink($post_id);				
		$url = home_url( '/' );		
		$edit_post = $url.'wp-admin/post.php?post='.$post_id.'&action=edit';
		$view_post = $url.'?p='.$post_id;		
		
		# create the mail function and notify the admin
		#$to			= get_option( 'admin_email' );
		$to			= 'info@thejobcrowd.com';	
		$subject	= "New job published: {$title}";
		$message	= "A new job has been published: <br/>";
		$message	.= "Post ID: {$post_id} <br/>";
		$message	.= "Login to edit the job: <a href=\"{$edit_post}\">{$title}</a>";
		$headers = array(
			'Content-Type: text/html; charset=UTF-8',
			'Cc:' . 'publishingteam@thejobcrowd.com',
			#'Bcc:' . 'ian@prgltd.co.uk',
			'From: '.get_option( 'blogname' ).' <info@thejobcrowd.com>'
		);
		
		wp_mail ($to, $subject, $message, $headers);		
		
		}
		
	}	
	    
}
add_action('acf/save_post', 'one_job_submission_per_subscription', 1);

##########################
# NOTIFICATION OF REVIEW ALERT
# removed as this triggers an email when a review alert is created/form triggered
# so not required!
##########################
/*
function review_alert_form( $post_id ) {  

	# add conditional to check form was triggered		
	if(($_POST['acf']['add_review_alert'] == 'true')) {
			
		# bail early if editing in admin
		if( is_admin() ) {		
			return;		
		}		
		
		#remove user_ from the data to leave just the user ID
		$user_id = preg_replace("/[^0-9]/","",$post_id);	
		
		#with a clean user ID we can now get various user meta
		$user_info	= get_userdata($user_id);
		$name		= $user_info->first_name.' '.$user_info->last_name;
		$email		= $user_info->user_email;
		
		
		# create the mail function and notify the admin
		$to			= get_option( 'admin_email' );
		$subject	= "Updated Review Alert!:";
		$message	= "Just been triggered: <br/>";
		$message	.= "By: {$name} (User ID: {$user_id}) <br/>";

		$headers = array(
			'Content-Type: text/html; charset=UTF-8',
			'From: '.get_option( 'blogname' ).' <info@thejobcrowd.com>'
			'CC: '.$email
		);
		
		wp_mail ($to, $subject, $message, $headers);		
				
	}	

}
add_action('acf/save_post', 'review_alert_form', 1);
*/


##########################
# NOTIFICATION OF CASE STUDY SUBMISSION
##########################

function case_study_submission_form( $post_id ) {  

	# add conditional to check form was triggered		
	if(($_POST['acf']['add_case_study_submission'] == 'true')) {
			
		# bail early if editing in admin
		if( is_admin() ) {		
			return;		
		}		

		# get the case study details
		$title = wp_strip_all_tags(get_the_title($post_id));			
		$url = home_url( '/' );		
		$edit_post = $url.'wp-admin/post.php?post='.$post_id.'&action=edit';

		# create the mail function and notify the admin
		#$to			= get_option( 'admin_email' );
		$to			= 'info@thejobcrowd.com';
		$subject	= "New Case Study Created:";
		$message	= "A new case study has been submitted: <br/>";
		#$message	.= "Post ID: {$post_id} <br/>";
		$message	.= "Login to edit the case study: <a href=\"{$edit_post}\">{$title}</a>";

		$headers = array(
			'Content-Type: text/html; charset=UTF-8',
			'Bcc:' . 'ian@prgltd.co.uk',
			'From: '.get_option( 'blogname' ).' <info@thejobcrowd.com>'
		);
		
		wp_mail ($to, $subject, $message, $headers);		
				
	}	

}
add_action('acf/save_post', 'case_study_submission_form', 1);

##########################
# NOTIFICATION OF DAY IN LIFE SUBMISSION
##########################

function day_in_the_life_submission_form( $post_id ) {  

	# add conditional to check form was triggered		
	if(($_POST['acf']['add_day_in_life_submission'] == 'true')) {
			
		# bail early if editing in admin
		if( is_admin() ) {		
			return;		
		}		

		# get the case study details
		$title = wp_strip_all_tags(get_the_title($post_id));			
		$url = home_url( '/' );		
		$edit_post = $url.'wp-admin/post.php?post='.$post_id.'&action=edit';

		# create the mail function and notify the admin
		#$to			= get_option( 'admin_email' );
		$to			= 'info@thejobcrowd.com';
		$subject	= "New Day In The Life Of Article Created:";
		$message	= "A new day in the life of has been submitted: <br/>";
		#$message	.= "Post ID: {$post_id} <br/>";
		$message	.= "Login to edit the article: <a href=\"{$edit_post}\">{$title}</a>";

		$headers = array(
			'Content-Type: text/html; charset=UTF-8',
			'Bcc:' . 'ian@prgltd.co.uk',
			'From: '.get_option( 'blogname' ).' <info@thejobcrowd.com>'
		);
		
		wp_mail ($to, $subject, $message, $headers);		
				
	}	

}
add_action('acf/save_post', 'day_in_the_life_submission_form', 1);

##########################
# UPDATE THE SURVEY TITLE
##########################

function survey_change_content_and_subject( $post_id ) {  

	# add conditional to check form was triggered		
	if(($_POST['acf']['survey_content_change'] == 'true')) {
			
		# bail early if editing in admin
		if( is_admin() ) {		
			return;		
		}		

		# check its not a new post
		if( $post_id != 'new' ) {			
			
			$postData = array(
				'post_title' => $_POST['acf']['field_5a05c296218c7'],
				'post_name' => '' #need to update the slug
			);

			wp_update_post($postData);

			return $post_id;
		
		}			
				
	}	

}
add_action('acf/save_post', 'survey_change_content_and_subject', 1);

##########################
# JOB ALERT EMAIL
# SEND AN EMAIL IF A STUDENTS 
# CRITERIA MATCHES NEW JOB POST
##########################

add_action( 'transition_post_status', 'send_job_alerts_on_publish', 10, 3 );

function send_job_alerts_on_publish( $new_status, $old_status, $post ) {
	
	if ( 'publish' !== $new_status or 'publish' === $old_status || 'jobs' !== get_post_type( $post ) )
	return;

	# get the job post details
    $title = wp_strip_all_tags(get_the_title($post->ID));
    #$url = get_permalink($post->ID);
	$url = home_url( '/' );		
	$edit_post = $url.'wp-admin/post.php?post='.$post->ID.'&action=edit';
	$view_post = $url.'?p='.$post->ID;			
	
	# get the alert details
	$company 	= get_field( 'dynamic_filled_company', $post->ID );
	$salary_min	= get_field( 'dynamic_filled_salary_min', $post->ID );
	$salary_max	= get_field( 'dynamic_filled_salary_max', $post->ID );
	$industry	= get_field( 'dynamic_filled_industry', $post->ID );
	$type		= get_field( 'dynamic_filled_type', $post->ID );
	$careers	= get_field( 'dynamic_filled_careers', $post->ID );
	$region		= get_field( 'dynamic_filled_region', $post->ID );
	$county		= get_field( 'dynamic_filled_county', $post->ID );
	
	#get author ID from the post ID
	#$post_author_id = get_post_field( 'post_author', $post_id );
	$post_author_id = get_post_field( 'post_author', $post->ID );
	
	#we can now get the company
	#$company = get_user_meta( $post_author_id, 'billing_company', true );
	$company = get_user_meta( $post_author_id, 'company_name', true );
		
	# create an array of email addresses
	$emails = array ();	
	
	$args = array(
		#'role' => 'student',
		'role__in' => array('apprentice', 'student'),
		'meta_query' =>
	
		array(
		
			array(
				'relation' => 'OR', # OR
				
				/*	
				#we can also utilise the IN
				array(
					'key' => 'acf_field_name',
					'value' => array('value1', 'value2'),
					'compare' => 'IN'
				),
				*/				

				array(
					'key' => 'dynamic_filled_company',
					'value' => $company,
					'compare' => "=",
				),

				array(
					'key' => 'salary_min',
					'value' => $salary_min,
					'compare' => ">=",
				),
			
				array(
					'key' => 'salary_max',
					'value' =>  $salary_max,
					'compare' => "<="
				),
			
				array(
					'key' => 'industry',
					'value' =>  $industry,
					'compare' => "IN"
				),
			
				array(
					'key' => 'type',
					'value' =>  $type,
					'compare' => "="
				),
				/*
				array(
					'key' => 'region',
					'value' =>  $region,
					'compare' => "="
				),
				*/
				array(
					'key' => 'region',
					'value' =>  $region,
					'compare' => "IN"
				),				
				/*
				array(
					'key' => 'careers',
					'value' =>  $careers,
					'compare' => "="
				),	
				*/
				array(
					'key' => 'careers',
					'value' => $careers,
					'compare' => 'IN'
				),				
				/*
				array(
					'key' => 'county',
					'value' =>  $county,
					'compare' => "="
				)	
				*/
				array(
					'key' => 'county',
					'value' =>  $county,
					'compare' => "IN"
				)					
				
			)
		)
			 
	);
	
	$users = get_users( $args );
	
	if ($users){
	
		foreach ( $users as $user ) {
			
			# add email addresses to the array
			#$emails[] = $user->user_email;
			$to			= $user->user_email;
			$subject	= "Job Alert: {$title}";
			$message	= "Link to job: <br/>";
			$message	.= "The following job met one or more of your criteria: <br/>";
			$message	.= "<a href=\"{$view_post}\">{$title}</a>";
			$headers = array(
				'Content-Type: text/html; charset=UTF-8',
				'From: '.get_option( 'blogname' ).' <info@thejobcrowd.com>'
			);

			wp_mail ($to, $subject, $message, $headers);
			
		}
		
		# create the mail function and notify all students with matching requirements
		/*
		$to			= get_option( 'admin_email' );
		$subject	= "Job Alert: {$title}";
		$message	= "Link to job: <br/>";
		$message	.= "The following job met one or more of your criteria: <br/>";
		$message	.= "<a href=\"{$view_post}\">{$title}</a>";
		$headers = array(
			'Content-Type: text/html; charset=UTF-8',
			'Bcc:' . implode( ",", $emails ),
			'From: '.get_option( 'blogname' ).' <info@thejobcrowd.com>'
		);
		
		wp_mail ($to, $subject, $message, $headers);	
		*/
	
	} #endif $users;	
	
}

##########################
# EXPIRE JOBS
# WHEN A JOB HAS A DATE SET
##########################

# moved to cron/cron-expire-job-posts-by-date.php

##########################
# EXPIRE JOBS
# ON SUBSCRIPTION END
##########################

# moved to cron/cron-expire-job-posts-subscription-end.php

##########################
# GRADUATE REVIEW ALERT EMAIL
# SEND AN EMAIL IF A STUDENTS 
# CRITERIA MATCHES NEW REVIEW POST
##########################

add_action( 'transition_post_status', 'graduate_reviews_alerts_on_publish', 10, 3 );

function graduate_reviews_alerts_on_publish( $new_status, $old_status, $post ) {
	
	if ( 'publish' !== $new_status or 'publish' === $old_status || 'graduate_reviews' !== get_post_type( $post )  )
	return;

	# get the review post details
    $title = wp_strip_all_tags(get_the_title($post->ID));
    #$url = get_permalink($post->ID);
	$url = home_url( '/' );		
	$edit_post = $url.'wp-admin/post.php?post='.$post->ID.'&action=edit';
	$view_post = $url.'?p='.$post->ID;			
	
	# get the alert details
	$company 	= get_field( 'graduate_review_employer', $post->ID );
	
	#get author ID from the post ID
	#$post_author_id = get_post_field( 'post_author', $post->ID );
	
	#we can now get the company
	#$company = get_user_meta( $post_author_id, 'billing_company', true );
	#$company = get_user_meta( $post_author_id, 'company_name', true );
		
	# create an array of email addresses
	$emails = array ();
	
	#we need to get the row number from the post ID
	global $wpdb;
				
	$users = $wpdb->get_results(
		$wpdb->prepare( 		
					   
		"
			SELECT *
			FROM $wpdb->usermeta
			WHERE meta_key LIKE %s
			AND meta_value = %s
		",
		'reviews_%_company',
		$company		
		
		)
	);	
	
	if ($users){
	
		foreach ( $users as $user ) {
			
			# get the user id
			$user_id	= $user->user_id;
			
			#we can now get the user meta
			$user_info = get_userdata($user_id);
			$user_info->first_name .  " " . $user_info->last_name;
			
			# add email addresses to the array, this is used in the BCC field
			#$emails[] = $user_info->user_email;
			
			# create the mail function and notify all students with matching requirements
			$to			= $user_info->user_email;
			$subject	= "Review Alert: {$title}";
			$message	= "The following review met your criteria: <br/>";
			$message	.= "<a href=\"{$view_post}\">{$title}</a>";
			$headers = array(
				'Content-Type: text/html; charset=UTF-8',
				'From: '.get_option( 'blogname' ).' <info@thejobcrowd.com>'
			);

			wp_mail ($to, $subject, $message, $headers);			
			
		}
		
		# create the mail function and notify all students with matching requirements
		/*
		$to			= get_option( 'admin_email' );
		$subject	= "Review Alert: {$title}";
		$message	= "The following review met your criteria: <br/>";
		$message	.= "<a href=\"{$view_post}\">{$title}</a>";
		$headers = array(
			'Content-Type: text/html; charset=UTF-8',
			'Bcc:' . implode( ",", $emails ),
			'From: '.get_option( 'blogname' ).' <info@thejobcrowd.com>'
		);
		
		wp_mail ($to, $subject, $message, $headers);
		*/
	
	} #endif $users;	
				
}

##########################
# APPRENTICE REVIEW ALERT EMAIL
# SEND AN EMAIL IF A STUDENTS 
# CRITERIA MATCHES NEW REVIEW POST
##########################

add_action( 'transition_post_status', 'apprentice_reviews_alerts_on_publish', 10, 3 );

function apprentice_reviews_alerts_on_publish( $new_status, $old_status, $post ) {
	
	if ( 'publish' !== $new_status or 'publish' === $old_status || 'apprentice_reviews' !== get_post_type( $post )  )
	return;

	# get the review post details
    $title = wp_strip_all_tags(get_the_title($post->ID));
    #$url = get_permalink($post->ID);
	$url = home_url( '/' );		
	$edit_post = $url.'wp-admin/post.php?post='.$post->ID.'&action=edit';
	$view_post = $url.'?p='.$post->ID;			
	
	# get the alert details
	$company 	= get_field( 'apprentice_review_employer', $post->ID );
		
	# create an array of email addresses
	$emails = array ();	
	
	#we need to get the row number from the post ID
	global $wpdb;
				
	$users = $wpdb->get_results(
		$wpdb->prepare( 		
					   
		"
			SELECT *
			FROM $wpdb->usermeta
			WHERE meta_key LIKE %s
			AND meta_value = %s
		",
		'reviews_%_company',
		$company		
		
		)
	);	
	
	if ($users){
	
		foreach ( $users as $user ) {
			
			# get the user id
			$user_id	= $user->user_id;
			
			#we can now get the user meta
			$user_info = get_userdata($user_id);
			$user_info->first_name .  " " . $user_info->last_name;
			
			# add email addresses to the array, this is used in the BCC field
			#$emails[] = $user_info->user_email;	
			# create the mail function and notify all students with matching requirements
			$to			= $user_info->user_email;
			$subject	= "Review Alert: {$title}";
			$message	= "The following review met your criteria: <br/>";
			$message	.= "<a href=\"{$view_post}\">{$title}</a>";
			$headers = array(
				'Content-Type: text/html; charset=UTF-8',
				'From: '.get_option( 'blogname' ).' <info@thejobcrowd.com>'
			);

			wp_mail ($to, $subject, $message, $headers);			
			
		}
		/*
		# create the mail function and notify all students with matching requirements
		$to			= get_option( 'admin_email' );
		$subject	= "Review Alert: {$title}";
		$message	= "The following review met your criteria: <br/>";
		$message	.= "<a href=\"{$view_post}\">{$title}</a>";
		$headers = array(
			'Content-Type: text/html; charset=UTF-8',
			'Bcc:' . implode( ",", $emails ),
			'From: '.get_option( 'blogname' ).' <info@thejobcrowd.com>'
		);
		
		wp_mail ($to, $subject, $message, $headers);
		*/
	
	} #endif $users;
				
}

##############################################################################
# 7) MEMBERSHIP
##############################################################################

###
# Do not grant membership access to purchasers if they already hold any membership, regardless of status
# https://hotexamples.com/examples/-/-/wc_memberships_get_user_memberships/php-wc_memberships_get_user_memberships-function-examples.html
###

function sv_wc_memberships_limit_to_one_membership($grant_access, $args) {
    # get all active memberships for the purchaser, regardless of status
    $memberships = wc_memberships_get_user_memberships($args['user_id']);
    # if there are any memberships returned, do not grant access from purchase
    if (!empty($memberships)) {
        return false;
    }
    return $grant_access;
}

##########################
# EXPIRED MEMBERSHIPS UPDATE USER ROLE/GROUP
#http://www.wpsuperstar.com/woocommerce-memberships-useful-functions-to-know/
#https://stackoverflow.com/questions/40161241/wc-memberships-get-user-memberships-in-the-customer-completed-order-mail
#https://hotexamples.com/examples/-/-/wc_memberships_get_user_memberships/php-wc_memberships_get_user_memberships-function-examples.html
##########################
/*
function prg_remove_expired_member_from_employee_group( $login ) {
	
    $user = get_user_by('login', $login);
	$user_id = $user->ID;
	$employer_registration = 630;
	$basic_legacy = 22396;
	$enhanced_legacy = 22397;
	
	#if( wc_memberships_is_user_active_member( $user_id, $plan_id ) ) {	
	if( wc_memberships_is_user_active_member( $user_id, $employer_registration ) || wc_memberships_is_user_active_member( $user_id, $basic_legacy ) || wc_memberships_is_user_active_member( $user_id, $enhanced_legacy ) ) {
		
		#user has active membership, useful if we wish to utilise this
		
	} else {
		
		#user is not an active member of this membership any more
	
		#remove them from the employer group
        $registered_group_id  =  5; #basic_apprentice
        $group_delete         =  new Groups_User_Group( $user_id, $group_id );
        $group_delete::delete ( $user_id, $registered_group_id );		
		
        $registered_group_id  =  6; #basic_graduate
        $group_delete         =  new Groups_User_Group( $user_id, $group_id );
        $group_delete::delete ( $user_id, $registered_group_id );
		
        $registered_group_id  =  7; #basic_both
        $group_delete         =  new Groups_User_Group( $user_id, $group_id );
        $group_delete::delete ( $user_id, $registered_group_id );
		
        $registered_group_id  =  8; #enhanced_apprentice
        $group_delete         =  new Groups_User_Group( $user_id, $group_id );
        $group_delete::delete ( $user_id, $registered_group_id );	
		
        $registered_group_id  =  9; #enhanced_graduate
        $group_delete         =  new Groups_User_Group( $user_id, $group_id );
        $group_delete::delete ( $user_id, $registered_group_id );	
		
        $registered_group_id  =  10; #enhanced_both
        $group_delete         =  new Groups_User_Group( $user_id, $group_id );
        $group_delete::delete ( $user_id, $registered_group_id );			
		
		#set the user role back to default of customer
		#only applicable to subscribers, so check current role			
		$user_meta=get_userdata( $user_id ); 
		$user_roles=$user_meta->roles; 
		if (in_array("subscriber", $user_roles)  ) {			
			#they're a subscriber, so change role to customer
			$role = 'customer';
			$user_id = wp_update_user( array( 'ID' => $user_id, 'role' => $role ) );	
		}	
	
	}	

}
add_action( 'wp_login', 'prg_remove_expired_member_from_employee_group' );
*/

##########################
# EXPIRED MEMBERSHIPS UPDATE USER ROLE/GROUP
# this is called via a daily cron
# saves being run on login
##########################

function prg_remove_from_group($user_id) {

	$employer_registration = 630;
	$basic_legacy = 22396;
	$enhanced_legacy = 22397;
	
	#if( wc_memberships_is_user_active_member( $user_id, $plan_id ) ) {	
	if( wc_memberships_is_user_active_member( $user_id, $employer_registration ) || wc_memberships_is_user_active_member( $user_id, $basic_legacy ) || wc_memberships_is_user_active_member( $user_id, $enhanced_legacy ) ) {
		
		#user has active membership, useful if we wish to utilise this
		
	} else {
		
		#user is not an active member of this membership any more
	
		#remove them from the employer group
        $registered_group_id  =  5; #basic_apprentice
        $group_delete         =  new Groups_User_Group( $user_id, $group_id );
        $group_delete::delete ( $user_id, $registered_group_id );		
		
        $registered_group_id  =  6; #basic_graduate
        $group_delete         =  new Groups_User_Group( $user_id, $group_id );
        $group_delete::delete ( $user_id, $registered_group_id );
		
        $registered_group_id  =  7; #basic_both
        $group_delete         =  new Groups_User_Group( $user_id, $group_id );
        $group_delete::delete ( $user_id, $registered_group_id );
		
        $registered_group_id  =  8; #enhanced_apprentice
        $group_delete         =  new Groups_User_Group( $user_id, $group_id );
        $group_delete::delete ( $user_id, $registered_group_id );	
		
        $registered_group_id  =  9; #enhanced_graduate
        $group_delete         =  new Groups_User_Group( $user_id, $group_id );
        $group_delete::delete ( $user_id, $registered_group_id );	
		
        $registered_group_id  =  10; #enhanced_both
        $group_delete         =  new Groups_User_Group( $user_id, $group_id );
        $group_delete::delete ( $user_id, $registered_group_id );			
		
		#set the user role back to default of customer
		#only applicable to subscribers, so check current role			
		$user_meta=get_userdata( $user_id ); 
		$user_roles=$user_meta->roles; 
		if (in_array("subscriber", $user_roles)  ) {			
			#they're a subscriber, so change role to customer
			$role = 'customer';
			$user_id = wp_update_user( array( 'ID' => $user_id, 'role' => $role ) );	
		}	
	
	}	
	
	
	# create the mail function and notify the admin
	$to		= get_option( 'admin_email' );
	$subject 	= 'Checking the function<br>';
	$message 	= 'Check a function<br/>';
	$message	.= "UID: {$user_id} <br/>";
	$message	.= "registered_group_id: {$registered_group_id} <br/>";

	$headers = array(
		'Content-Type: text/html; charset=UTF-8',
		'From: '.get_option( 'blogname' ).' <info@thejobcrowd.com>'
	);
		
	#wp_mail ($to, $subject, $message, $headers);	
	
}

##########################
# EXPIRED SUBSCRIPTION HOOK
#https://stackoverflow.com/questions/43108453/get-the-user-id-in-wooocommerce-subscriptions
#https://stackoverflow.com/questions/46264490/auto-change-woocommerce-subscriptions-status-to-on-hold-rather-than-active
#https://hotexamples.com/examples/-/WC_Subscription/update_status/php-wc_subscription-update_status-method-examples.html
#https://docs.woocommerce.com/document/subscriptions/develop/action-reference/
#https://stackoverflow.com/questions/35612520/hook-in-woocommerce-on-subscription-expiration
#http://docs.itthinx.com/document/groups/api/examples/
##########################


##############################################################################
# 8) MISC
##############################################################################

##########################
# ADD REVIEW SHORTLIST INCLUDE
##########################

if ( ! function_exists('custom_functions') ) :
	function custom_functions() {
		include(get_template_directory() . '/includes/shortlist-functions.php');
	}
endif;
# runs before 'init' hook
add_action( 'after_setup_theme', 'custom_functions' );


##########################
# POST COUNTER
# BASIC STATS COUNTER FOR EMPLOYERS
# http://wpsnipp.com/index.php/cache/track-post-views-without-a-plugin-using-post-meta/
##########################

function getPostViews($postID){
	$count_key = 'post_views_count';
	$count = get_post_meta($postID, $count_key, true);
	if($count==''){
		delete_post_meta($postID, $count_key);
		add_post_meta($postID, $count_key, '0');
		return "0 View";
	}
	#return $count.' Views';
	return $count;
}
function setPostViews($postID) {
	$count_key = 'post_views_count';
	$count = get_post_meta($postID, $count_key, true);
    if($count==''){
		$count = 0;
		delete_post_meta($postID, $count_key);
		add_post_meta($postID, $count_key, '0');
	}else{
		$count++;
		update_post_meta($postID, $count_key, $count);
		
		
		# get the job post details
		$title = wp_strip_all_tags(get_the_title($postID));				
		$url = home_url( '/' );		
		$edit_post = $url.'wp-admin/post.php?post='.$postID.'&action=edit';
		$view_post = $url.'?p='.$postID;	
		
		# get the author ID
		$author_id = get_post_field ('post_author', $postID);
		
		# we can now get the author meta/ email
		$user_email = get_the_author_meta( 'user_email', $author_id );
		
		# only notify job author once viewed 1,000 times
		if ($count > 999){
		
			# update job author every 100
			#if (($count + 1) % 100 == 0) { #include an offset
			if (($count) % 100 == 0) {
				
				# create the mail function and notify the job author
				$to			= $user_email;
				#$to		= get_option( 'admin_email' );
				$subject	= "Your Job: {$title} has a new view!";
				$message	= "Your job ({$title}) has been viewed {$count} times.<br/>";
				#$message	.= "Post ID: {$postID} <br/>";
				#$message	.= "Author ID: {$author_id} <br/>";
				#$message	.= "User email: {$user_email} <br/>";
				$message	.= "View the job: <a href=\"{$view_post}\">{$title}</a>";
				$headers = array(
					'Content-Type: text/html; charset=UTF-8',
					'From: '.get_option( 'blogname' ).' <info@thejobcrowd.com>'
				);
				
				wp_mail ($to, $subject, $message, $headers);	
				
			} #endif every 100
			
		} #endif every 1000
		
	}
}
# Remove issues with prefetching adding extra views
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

#add the below into the loop on single-jobs.php for example
#setPostViews(get_the_ID());
#echo 'Views: '.getPostViews(get_the_ID());


##########################
# CHECK A USER HAS PREVIOUSLY COMMENTED
########################## 
function prg_comment_counter($pid,$uid){

	global $wpdb;
	$query = "SELECT COUNT(comment_post_id) AS count FROM $wpdb->comments WHERE comment_approved = 1 AND comment_post_ID = $pid AND user_id = $uid AND comment_parent = 0";
	#var_dump( $wpdb->last_query );
	$parents = $wpdb->get_row($query);
	
	return $parents->count;
}


##########################
# GET TOTAL OF ALL RATINGS FOR THE POST
##########################

function prg_stats_counter($pid){

	global $wpdb;
	$stats_total = $wpdb->get_var("
	SELECT sum(meta_value)
	FROM $wpdb->commentmeta
	INNER JOIN $wpdb->comments ON $wpdb->commentmeta.comment_id = $wpdb->comments.comment_id
	WHERE $wpdb->commentmeta.meta_key =  'stars'
	AND $wpdb->comments.comment_post_ID = $pid");
	#var_dump( $wpdb->last_query );
	
	return $stats_total;
}

##########################
# GET TOTAL No OF REVIEWS WITH RATINGS (OTHERWISE SKEWS RESULTS)
##########################

function prg_total_reviews_counter($pid){

	global $wpdb;
	$reviews_total = $wpdb->get_var("
	SELECT count(meta_value)
	FROM $wpdb->commentmeta
	INNER JOIN $wpdb->comments ON $wpdb->commentmeta.comment_id = $wpdb->comments.comment_id
	WHERE $wpdb->commentmeta.meta_key =  'stars'
	AND $wpdb->comments.comment_post_ID = $pid");
	#var_dump( $wpdb->last_query );
	
	return $reviews_total;
}



##########################
# CREATE OUR OWN COMMENT CALLBACK
# Now we can include the stars and format as we need
##########################

function prg_custom_comment_layout($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment; ?>
	
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
	
	<div id="comment-<?php comment_ID(); ?>">
	
		<div class="comment-author vcard">
			<?php echo get_avatar($comment,$size='48',$default='<path_to_url>' ); ?>
		
			<?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
		</div>
		
		<?php if ($comment->comment_approved == '0') : ?>
			<em><?php _e('Your comment is awaiting moderation.') ?></em>
			<br />
		<?php endif; ?>
	  
		<?php 
		#Add the stars in per comment
		if(get_field('stars', $comment)) : ?>
			<img src="<?php echo get_stylesheet_directory_uri() ; ?>/images/stars_<?php the_field('stars', $comment); ?>.png" />
		<?php endif; ?>

		<div class="comment-meta commentmetadata">
			<a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php printf(__('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)'),'  ','') ?>
		</div>

		<?php comment_text() ?>

		<div class="reply">
			<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
		</div>
		
	</div>
<?php
}

##########################
# CHANGE EMAIL NOTIFICATION FOR COMMENTS
# Use this if it's not to be the site admin!
# http://www.sourcexpress.com/customize-wordpress-comment-notification-emails/
##########################

# Hook into comment_post as its an action triggered immediately after a comment is inserted into the database.
add_filter( 'comment_post', 'comment_notification' ); 

function comment_notification( $comment_ID, $comment_approved ) {

	# Remove if statement if you want to recive email even if it doesn't require moderation
	# Send email only when it's not approved
	if( $comment_approved == 0 ) {

		#get the post id 
		$cid = get_comment( $comment_ID ); 
    	$comment_post_id = $cid->comment_post_ID ;	
	
		# get the comment details
		$title = wp_strip_all_tags(get_the_title($comment_post_id));			
		$url = home_url( '/' );		
		$edit_post = $url.'wp-admin/comment.php?action=editcomment&c='.$comment_ID;

		# create the mail function and notify the admin
		#$to		= get_option( 'admin_email' );
		$to			= 'info@thejobcrowd.com';
		$subject 	= 'New Review Comment';
		$message 	= 'A new comment has been added on the website. Please login to approve or edit the comment.';
		#$message	.= "Comment ID: {comment_ID} <br/>";
		$message	.= "Login to view: <a href=\"{$edit_post}\">{$title}</a>";

		$headers = array(
			'Content-Type: text/html; charset=UTF-8',
			'Bcc:' . 'ian@prgltd.co.uk',
			'From: '.get_option( 'blogname' ).' <info@thejobcrowd.com>'
		);
		
		wp_mail ($to, $subject, $message, $headers);								
		
	}
}

##########################
# TITLE
##########################

/*
#https://code.tutsplus.com/tutorials/wordpress-shortcodes-the-right-way--wp-17165
#https://www.sitepoint.com/wordpress-shortcodes-tutorial/
*/


/*
##########################
# CHANGE POST TITLE ON GRAD/APP SURVEY FORMS
##########################
function my_acf_load_field( $field ) {
  $field['label'] = 'Subject';
  return $field;
}
add_filter('acf/load_field/name=_post_title', 'my_acf_load_field');
*/



##########################
# META OR TITLE QUERY IN WP
# activated through the '_meta_or_title' argument of WP_Query 
########################## 
/*
add_action( 'pre_get_posts', function( $q ){
									  
    if( $title = $q->get( '_meta_or_title' ) ){
		
        add_filter( 'get_meta_sql', function( $sql ) use ( $title ){
            global $wpdb;

            # Only run once:
            static $nr = 0; 
            if( 0 != $nr++ ) return $sql;

            # Modify WHERE part:
            $sql['where'] = sprintf(
                " AND ( %s OR %s ) ",
                $wpdb->prepare( "{$wpdb->posts}.post_title = '%s'", $title ),
                mb_substr( $sql['where'], 5, mb_strlen( $sql['where'] ) )
            );
            return $sql;
        });
    }
});
*/
