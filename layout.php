<?php

if( have_rows('layout') ):

     // loop through the rows of data
    while ( have_rows('layout') ) : the_row();

        if( get_row_layout() == 'one_column' ):

        	?>

        	<div class="row columns">
        		<?php the_sub_field('content'); ?>
        	</div>

        <?php elseif( get_row_layout() == 'two_columns' ): ?>

        	<div class="row">
        		<div class="medium-6 columns">
        			<?php the_sub_field('content_left'); ?>
        		</div>

        		<div class="medium-6 columns">
        			<?php the_sub_field('content_right'); ?>
        		</div>
        	</div>

        <?php elseif( get_row_layout() == 'three_columns' ): ?>

        	<div class="row">
        		<div class="medium-4 columns">
        			<?php the_sub_field('content_left'); ?>
        		</div>

        		<div class="medium-4 columns">
        			<?php the_sub_field('content_centre'); ?>
        		</div>

        		<div class="medium-4 columns">
        			<?php the_sub_field('content_right'); ?>
        		</div>
        	</div>


        <?php elseif( get_row_layout() == 'fullwidth_image' ): ?>

        	<div class="row margin-top">
        		<div class="columns text-center">

					<?php 

					$image = get_sub_field('image');

					if( !empty($image) ): ?>

						<img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" class="fullwidth" />

					<?php endif; ?>

        		</div>

        	</div>


        <?php elseif( get_row_layout() == 'coloured_box' ):

        	?>

        	<div class="row columns coloured_box">
        		<?php the_sub_field('content'); ?>
        	</div>       
       
        <?php elseif( get_row_layout() == 'gallery' ): ?>
        
        
        <div class="row margin-top">

            <div class="columns content-fullwidth padded text-center">
            
            <?php if(get_sub_field('gallery_title')): ?>
                <span class="section-title"><?php the_sub_field('gallery_title'); ?></span>
                <hr>
            <?php endif; ?>

                <?php 

                $images = get_sub_field('gallery');

                if( $images ): ?>

                <div class="row small-up-1 medium-up-3 large-up-5 image-gallery">
                <?php foreach( $images as $image ): ?>
                    <div class="column">
                    <a href="<?php echo $image['sizes']['large']; ?>" class="gallery">
                        <img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
                    </a>
                    <p><?php echo $image['caption']; ?></p>
                    </div>
                <?php endforeach; ?>
                </div>

                <?php endif; ?>

            </div>

        </div>   


        <?php elseif( get_row_layout() == 'list_of_services' ): ?>
        
        
        <div class="row margin-top">

            <div class="columns content-fullwidth padded text-center">
            
            <?php if(get_sub_field('title')): ?>
                <span class="section-title"><?php the_sub_field('title'); ?></span>
                <hr>
            <?php endif; ?>

            <?php 
                if(get_sub_field('services')) :
                    
                    echo '<ul class="services">';

                    while(has_sub_field('services')): ?>

                                <?php
                                $post_object = get_sub_field('service');
                                if( $post_object ): 
                                    $post = $post_object;
                                    setup_postdata( $post ); 

                                    ?>
                                        <span class="service"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></span>
                                     <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
                                <?php endif; ?>

                    <?php endwhile; 

                    echo '</ul>';

                endif; 

            ?>



            </div>

        </div>   
        
        <?php elseif( get_row_layout() == 'testimonials' ): ?>
        
        
        <div class="row margin-top">

            <div class="columns content-fullwidth padded text-center">
                
                <?php if(get_sub_field('title')): ?>
                    <span class="section-title"><?php the_sub_field('title'); ?></span>
                    <hr>
                <?php endif; ?>

                    <?php 
                        if(get_sub_field('testimonials')) :
                            
                            echo '<div class="services">';

                                while( have_rows('testimonials') ): the_row(); 
                                    $testimonial = get_sub_field('testimonial');
                                    $name = get_sub_field('name');
                                ?>
                                        
                                        <div class="testimonial">
                                           <?php echo $testimonial; ?>
                                            <span><?php echo $name; ?></span>
                                        </div>

                                <?php endwhile; 

                            echo '</div>';

                        endif; 

                    ?>

            </div>

        </div>   
        
        <?php elseif( get_row_layout() == 'downloads' ): ?> 
        
        <div class="row margin-top">

        	<div class="columns content-fullwidth padded">

				<?php 
				if(get_sub_field('downloads')) :
										
					echo '<ul class="downloads">';
										
					while(has_sub_field('downloads')) :						
						$file = get_sub_field('file');					
						if( $file ): ?>
	
							<li><a href="<?php echo $file['url']; ?>"><?php echo $file['title']; ?></a></li>

						<?php endif;
						
					endwhile;
									 
					echo '</ul>';
				endif; ?>

        	</div>

        </div>         
        
        <?php elseif( get_row_layout() == 'video' ): ?>

         <div class="row margin-top">

            <div class="columns padded-half content-fullwidth">
 
                    <?php the_sub_field('video'); ?>
               
            </div>

        </div>

        
        <?php elseif( get_row_layout() == 'call_to_action' ): ?>

         <div class="row margin-top">

            <div class="columns padded-half call-to-action content-fullwidth">
 
                    <?php the_sub_field('call_to_action'); ?>
               
            </div>

        </div>

        <?php
		else:
		
		endif;

    endwhile;

else :

    // no layouts found

endif;

?>
