<?php
acf_form_head(); #Add this to enable ACF form
$user_id = 'user_'.get_current_user_id(); #This is essential to ensure we update the user meta otherwise defaults to postmeta
$current_user = get_current_user_id(); #Get the current logged in user ID

$groups_user = new Groups_User( $current_user ); #Check which group the user belongs to

# Check the user role of the logged in user
# If it's not correct, redirect to main dashboard page
$user_meta=get_userdata( $current_user ); 
$user_roles=$user_meta->roles; 
if ( 
	(in_array("subscriber", $user_roles) && ($groups_user->can( 'jobs' )==1) ) || (in_array("customer", $user_roles) && ($groups_user->can( 'jobs' )==1) ) || in_array("administrator", $user_roles) ) :
	
	echo '<h1>JOBS</h1>';
	
	# Get all customers subscriptions
	$customer_subscriptions = get_posts( array(
		'numberposts'	=> -1,
		'meta_key'		=> '_customer_user', 
		'meta_value'	=> get_current_user_id(),	# Or $user_id
		'post_type'		=> 'shop_subscription',		# WC orders post type
		'post_status'	=> 'wc-active',				# Only orders with status "completed"
		'orderby'		=> 'post_date',
		'order' 		=> 'ASC'					# ACS or DESC
	) );
	
	if ($customer_subscriptions):
	?>
    <table id="example" class="display" cellspacing="0" width="100%">
	<thead>
    	<tr>
        	<th>Order No</th>
			<!--<th>Subscription Available</th>
            <th>Subscription Used</th>-->
			<th>Edit/Create Job</th>
        </tr>
    </thead>
	<tfoot>
    	<tr>
        	<th>Order No</th>
			<!--<th>Subscription Available</th>
            <th>Subscription Used</th>-->
			<th>Edit/Create Job</th>
        </tr>
    </tfoot>    
    <tbody>	
	<?php
		# Iterating through each post subscription object
		foreach( $customer_subscriptions as $customer_subscription ):
		
			# Getting an instance of the WC_Subscription object
			$subscription = new WC_Subscription($customer_subscription->ID);
			#echo '<p>Subscription: '.$subscription.'</p>';	
			#echo '<pre>';print_r($subscription);echo '</pre>';
		
			# Getting the related ORDER ID
			$order_id = $subscription->order->id;
			#echo '<p>Order #: '.$order_id.'</p>'; #debug
			
			# Getting an instance of the related WC_ORDER object  
			$order = $subscription->order;
			#echo '<p>Order: '.$order.'</p>'; #debug			
			
			$order = new WC_Order( $order_id );
			$items = $order->get_items();
			foreach ( $items as $item ) :
				$product_name = $item['name'];
				$product_id = $item['product_id'];
			endforeach;
			
			# product ID 117 is the job subscription. We add this conditional check here because a subscription is created
			# when a customer becomes an employer
			if ($product_id == '117') : 
			
				# an active subscription with the correct subscription product - show submission form
				
				if (WC_Subscriptions_Manager::user_has_subscription( $current_user, $product_id = '', $status = 'active')) :
				echo '<tr>';
					echo '<td>#'.$order_id.'</td>';
					
					$job_subscription_order_id = get_field( "job_subscription_order_id", $order_id );
		
					if( $job_subscription_order_id == '' ) :
					
						$nonce = wp_create_nonce( 'prg-add-job-nonce' );						
						$directory = get_site_url();
						
						#echo '<td><i class="fa fa-check" aria-hidden="true"></i></td>';							
						#echo '<td><i class="fa fa-times" aria-hidden="true"></i></td>';								
						echo "<td><i class='fa fa-info-circle' aria-hidden='true'></i> <a href='$directory/my-account/job-submission/?_wpnonce=$nonce&oid=$order_id'>Create Job</a></td>";
					
					else:
					
						#get todays date					
						$today = date('Y-m-d'); 
						#echo '<p>Today: '.$today.'</p>'; #debug
						
						#now calculate 6 months previously
						$date = strtotime($today.' -1 months');
						#echo date('Y-m-d', $date);
						#echo '<p>6 months ago: '.date('Y-m-d', $date).'</p>'; #debug
						
						#get the date of the order
						$order = new WC_Order($order_id);
						$order_date = $order->order_date;
						
						#remove the time aspect and convert date format
						$createDate = new DateTime($order_date);
						$cleanDate = $createDate->format('d-m-Y');										
						
						# need to remove jobs after 6 months to prevent a lengthy list
						if ($today < $date):	
						
							#echo '<td><i class="fa fa-times" aria-hidden="true"></i></td>';							
							#echo '<td><i class="fa fa-check" aria-hidden="true"></i></td>';						
						endif;
						
						if( $job_subscription_order_id == '' ) :
						
							echo '<td>no job ID</td>';
						
						#a job has been created
						else:
						
							$job_post_id = get_field( "job_subscription_order_id", $order_id );
							$url = get_permalink($job_post_id);
							$title = get_the_title($job_post_id);
							$status = get_post_status( $job_post_id );
															
							#get the job status and display results
							if ($status == 'draft'):
								echo '<td>Awaiting approval</td>';
							elseif ($status == 'publish'):
								echo '<td><a href="'.$url.'?&action=edit">'.$title.'</a></td>';
							else:
								echo '<td>&nbsp;</td>';
							endif;
							
						endif; #job_subscription_order_id
					
					echo '</tr>';
					endif; #endif job_subscription_order_id				
			
			else:
			
				# an active subscription with the incorrect subscription product - don't submission form
			
			endif; #endif check for correct product id/product subscription
			
				
			endif; #endif WC_Subscriptions_Manager					
		
		endforeach;
		#echo '<pre>';print_r($customer_subscriptions);echo '</pre>';
		?>
	</tbody>
	</table>
    <link href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet" />
    <script src="//code.jquery.com/jquery-1.12.4.js"></script>
    <script src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script>
	$(document).ready(function() {
		$('#example').DataTable();
	} ); 	
    </script> 		
		<?php
	else:
	
		echo "<p>You currently don't have any job subscriptions.</p>";
	
	endif; #endif $customer_subscriptions	

	
else:
	
	#echo get_permalink( get_option('woocommerce_myaccount_page_id') ); #debug	
	$redirect = get_permalink( get_option('woocommerce_myaccount_page_id') );
	header('Location: '.$redirect); die; 
	
endif;
?>