<?php
acf_form_head(); #Add this to enable ACF form
$user_id = 'user_'.get_current_user_id(); #This is essential to ensure we update the user meta otherwise defaults to postmeta

# Check the user role of the logged in user
# If it's not correct, redirect to main dashboard page
$user_meta=get_userdata( get_current_user_id() ); 
$user_roles=$user_meta->roles; 
if (in_array("student", $user_roles) || in_array("apprentice", $user_roles) || in_array("administrator", $user_roles) ) :
	
	echo '<h1>JOB ALERT</h1>';
	
	acf_form(array(
		'field_groups' => array(206), #use ACF group :: Student Notes Submission
		'post_id' => $user_id,
		'submit_value'	=> 'Set Job Alert',
		'updated_message' => __("Job Alert Set!", 'acf'),
		'html_after_fields' => '<input type="hidden" name="acf[add_job_alert]" value="true"/>',
	)); 	
	
else:
	
	#echo get_permalink( get_option('woocommerce_myaccount_page_id') ); #debug	
	$redirect = get_permalink( get_option('woocommerce_myaccount_page_id') );
	header('Location: '.$redirect); die; 
	
endif;
?>