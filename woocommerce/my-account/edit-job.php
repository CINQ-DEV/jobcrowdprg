<?php
wp_reset_query();
$current_user = get_current_user_id(); #Get the current logged in user ID

$groups_user = new Groups_User( $current_user ); #Check which group the user belongs to
		
if ($groups_user->can( 'jobs' )==1) :

global $post;
$args = array(
	'posts_per_page'	=> -1, 
	'post_type' 		=> 'jobs', 
	'author'			=> $current_user,
	'orderby'			=> 'post_date',
	'order'				=> 'ASC',
	'post_status'		=> 'publish'
);

$jobs = get_posts( $args );
if ($jobs) : ?>

	<h3>Your Latest Jobs</h3>
	<p>Please click a link below to edit.</p>
	<ul>
	
	<?php foreach ( $jobs as $post ) : setup_postdata( $post );
		#construct the URL with required variable
		$url = rtrim(get_permalink(),'/');
		
		#NOTE: This edits the job directly on single-jobs.php
		#Additional checks are handled by that file before editing can commence
		?>
	
		<li><a href="<?php echo $url.'?&action=edit'; ?>"><?php the_title(); ?></a></li>
		
	<?php endforeach; ?>
	
	</ul>

<?php else:
	echo "<p>You don't have any jobs.</p>";
endif;

else:

	echo "<p>You don't have an active job subscription</p>";

endif;

wp_reset_postdata();
?>
