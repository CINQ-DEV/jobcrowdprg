<?php
acf_form_head(); #Add this to enable ACF form
$user_id = 'user_'.get_current_user_id(); #This is essential to ensure we update the user meta otherwise defaults to postmeta

# Check the user role of the logged in user
# If it's not correct, redirect to main dashboard page
$user_meta=get_userdata( get_current_user_id() ); 
$user_roles=$user_meta->roles; 
if (in_array("apprentice", $user_roles) || in_array("administrator", $user_roles) ) :
	
	echo '<h1>DAY IN LIFE SUBMISSION</h1>';
	
	# https://www.advancedcustomfields.com/resources/acf_form/	
	acf_form(array(
		'post_id'		=> 'new_post',
		'post_title'	=> true,
		#'post_content'	=> true,
		'field_groups' => array(2450), #use ACF group :: Content
		'submit_value'	=> 'Add An Article',
		'updated_message' => __("<h3>A Day In The Life Of Created</h3><p>An administrator will review the case study and will then be published upon approval.</p>", 'acf'),
		'html_after_fields' => '<input type="hidden" name="acf[add_day_in_life_submission]" value="true"/>',
		'new_post'		=> array(
			'post_type'		=> 'day_in_life',
			'post_status'	=> 'draft'
		)
	));	
	
else:
	
	#echo get_permalink( get_option('woocommerce_myaccount_page_id') ); #debug	
	$redirect = get_permalink( get_option('woocommerce_myaccount_page_id') );
	header('Location: '.$redirect); die; 
	
endif;
?>