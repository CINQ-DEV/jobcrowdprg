<?php
acf_form_head(); #Add this to enable ACF form
$user_id = 'user_'.get_current_user_id(); #This is essential to ensure we update the user meta otherwise defaults to postmeta

# Check the user role of the logged in user
# If it's not correct, redirect to main dashboard page
$user_meta=get_userdata( get_current_user_id() ); 
$user_roles=$user_meta->roles; 
if (in_array("student", $user_roles) || in_array("administrator", $user_roles) ) :
	
	echo '<h1>YOUR NOTES</h1>';	
	
	if( have_rows('notes', $user_id) ): $i = 1; ?>
	
		<ul>
	
		<?php while( have_rows('notes', $user_id) ): the_row();  
	
			// vars
			$date_time = get_sub_field('date_time');
			$note = get_sub_field('note');
			
			$nonce = wp_create_nonce( 'prg-delete-note-nonce' );
			?>
	
			<li>
			
				<?php echo $date_time; ?> <a href="<?php echo get_stylesheet_directory_uri(); ?>/includes/delete-note.php?_wpnonce=<?php echo $nonce; ?>&row=<?php echo $i; ?>" title="Delete Note From <?php echo $date_time; ?>" class="delete-button">x</a><br />
	
				<?php echo $note; ?>											
				
				<hr />			
		  
			</li>
	
		<?php $i++; endwhile; ?>
	
		</ul>
	
	<?php endif; 
	
	acf_form(array(
		'field_groups' => array(73), #use ACF group :: Student Notes Submission
		'post_id' => $user_id,
		'submit_value'	=> 'Add Note',
		'updated_message' => __("Note Added", 'acf'),
		'html_after_fields' => '<input type="hidden" name="acf[add_note]" value="true"/>',
	)); 
	
	
else:
	
	#echo get_permalink( get_option('woocommerce_myaccount_page_id') ); #debug	
	$redirect = get_permalink( get_option('woocommerce_myaccount_page_id') );
	header('Location: '.$redirect); die; 
	
endif;
?>