<?php
acf_form_head(); #Add this to enable ACF form
$nonce = $_REQUEST['_wpnonce'];
get_header();

if ( ! wp_verify_nonce( $nonce, 'prg-add-job-nonce' ) ) :
	
	#nonce doesn't match or exist so terminate script!
	die( "<h1>Security Check!</h1><p>You're trying to access this page directly. If you believe you're seeing this page in error, please contact the administrator!</p>" ); 

else:

	$order_id = $_GET['oid'];
	
	$submitted = $_GET['updated'];
	
	if (!empty($_GET[updated])) : 

		echo '<h3>Job Now Created</h3><p>An administrator will review the vacancy and will then be published upon approval.</p>';
	
	else:	
	
		$job_subscription_order_id = get_field( "job_subscription_order_id", $order_id );
	
		if( $job_subscription_order_id == '' ) :	
		
			acf_form(array(
				'post_id'		=> 'new_post',
				'post_title'	=> true,
				
				# need to list fields from two groups, this allows us to contruct the job details and ensure we enter
				# the data to trigger the job aletts
				'fields' => array(
					#Group 96 :: Job Listing 
					'field_595b5f02d4a72',	#deadline
					'field_59675877b636a',	#deadline other info
					'field_595b5f27d4a75',	#qualifications
					'field_595b5f40d4a76',	#application
					'field_5964b890198cd',	#expiry date
					'field_5966469298f23',	#salary
					'field_5967545659cfe',	#additional salary
					
					#Group 206 :: Job Alert Search Criteria
					#'field_5964f3ad5648a',	#salary - min
					#'field_5964f3ad5650a',	#salary - max
					'field_5964f3ad5658a',	#industry
					'field_5964f3ad5660a',	#type
					'field_596883d81cc60',	#town
					'field_596755eff20e1',	#county
					'field_5964f3ad5668a',	#region
					'field_5a719e9c80f50',  #additional location
					'field_5967933a60835',	#careers
				
					#Group 96 :: Job Listing 
					'field_5a5e23f440353',	#display application form
					'field_5a5e28d043f0c',	#message
					'field_5a5e241940354',	#email address for form application
					'field_5a719f54ac3e7'	#apply now URL
				),
				
				'submit_value'	=> 'Add Job',
				'updated_message' => __("<h3>Job Created</h3><p>An administrator will review the vacancy and will then be published upon approval.</p>", 'acf'),
				'html_after_fields' => '<input type="hidden" name="acf[field_595f933c6699e]" value="'.$order_id.'" />',
				'new_post'		=> array(
					'post_type'		=> 'jobs',
					'post_status'	=> 'draft'
				)
			));	
		
		else:
		
			echo '<h3>Error!</h3><p>You have already created a job for this subscription.</p>';
			
		endif;	#endif job_subscription_order_id

	endif; #endif $_GET[updated]
	
endif; #endif $nonce

wp_footer();
?>