<?php
wp_reset_query();
$current_user = get_current_user_id(); #Get the current logged in user ID

$groups_user = new Groups_User( $current_user ); #Check which group the user belongs to
		
#if ($groups_user->can( 'employer' )==1) :

if ( $groups_user->can( 'basic_apprentice' )==1 || $groups_user->can( 'basic_graduate' )==1 || $groups_user->can( 'basic_both' )==1 || $groups_user->can( 'enhanced_apprentice' )==1 || $groups_user->can( 'enhanced_graduate' )==1 || $groups_user->can( 'enhanced_both' )==1 ) :

	global $post;
	$args = array(
		'posts_per_page'	=> -1, 
		'post_type' 		=> 'surveys', 
		'author'			=> $current_user,
		'orderby'			=> 'post_date',
		'order'				=> 'ASC',
		'post_status'		=> 'publish'
	);

	$lists = get_posts( $args );
	if ($lists) : ?>

		<h3>Your Surveys</h3>
		<p>Please click a link below to view.</p>
			<table id="example" class="display" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>View/Submit</th>
                        <th>Edit Email</th>               
                        <th>Add/Remove Recipients</th>
                        <th>Survey Type</th>
                    </tr>							
                </thead>
                <tfoot>
                    <tr>
                        <th>View/Submit</th>
                        <th>Edit Email</th>               
                        <th>Add/Remove Recipients</th>
                        <th>Survey Type</th>
                    </tr>
                </tfoot>    
                <tbody>	

				<?php foreach ( $lists as $post ) : setup_postdata( $post );
                    #construct the URL with required variable
                    $url = rtrim(get_permalink(),'/');
                    $nonce = wp_create_nonce( 'prg-survey-status-nonce' );
                    #NOTE: This edits the job directly on single-jobs.php
                    #Additional checks are handled by that file before editing can commence
                    ?>
                    <tr>
                    <td><a href="<?php echo $url."/?status=submit&_wpnonce=$nonce"; ?>">View/Submit <?php the_title(); ?></a></td>
                    <td><a href="<?php echo $url."/?status=email&_wpnonce=$nonce"; ?>">Edit Email</a></td>
                    <td><a href="<?php echo $url."/?status=recipients&_wpnonce=$nonce"; ?>">Add/Remove Recipients</a></td>
                    <td><?php echo the_field('survey_type'); ?></td>
                    </tr>
                <?php endforeach; ?>

                </tbody>
			</table>				
			<link href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet" />
			<script src="//code.jquery.com/jquery-1.12.4.js"></script>
			<script src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>	
			<script>
			jQuery(document).ready(function() {
				$('#example').DataTable();
			} );				
			</script>	

	<?php else:
		echo "<p>You don't have any survey lists. Please register a new survey before you can continue.</p>";
	endif;

else:

	echo "<p>You don't have an active membership</p>";

endif;

wp_reset_postdata();
?>
