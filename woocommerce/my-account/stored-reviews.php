<?php
acf_form_head(); #Add this to enable ACF form
$user_id = 'user_'.get_current_user_id(); #This is essential to ensure we update the user meta otherwise defaults to postmeta

# Check the user role of the logged in user
# If it's not correct, redirect to main dashboard page
$user_meta=get_userdata( get_current_user_id() ); 
$user_roles=$user_meta->roles; 
if (in_array("student", $user_roles) || in_array("administrator", $user_roles) ) :
	
	echo '<h1>STORED REVIEWS</h1>';
	
	if( have_rows('stored_reviews', $user_id) ): $i = 1; ?>
	
		<ul>
	
		<?php while( have_rows('stored_reviews', $user_id) ): the_row();  
	
			// vars
			$review_id = get_sub_field('review_id');
			$review_title = get_sub_field('review_title');
			
			$nonce = wp_create_nonce( 'prg-delete-review-nonce' );
			?>
	
			<li>
			
				<a href="<?php echo get_the_permalink( $review_id ); ?>" title="<?php echo $review_title; ?>"><?php echo $review_title; ?></a>							
				
				<a href="<?php echo get_stylesheet_directory_uri(); ?>/includes/delete-review.php?_wpnonce=<?php echo $nonce; ?>&row=<?php echo $i; ?>" title="Delete Review <?php echo $review_title; ?>"> <small>remove</small> </a>
				<hr />			
		  
			</li>
	
		<?php $i++; endwhile; ?>
	
		</ul>
	<?php else: ?>
    
    	<p>You don't have any stored reviews.</p>
    
	<?php endif; 	
	
else:
	
	#echo get_permalink( get_option('woocommerce_myaccount_page_id') ); #debug	
	$redirect = get_permalink( get_option('woocommerce_myaccount_page_id') );
	header('Location: '.$redirect); die; 
	
endif;
?>