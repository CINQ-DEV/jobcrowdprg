<?php
acf_form_head(); #Add this to enable ACF form
$user_id = 'user_'.get_current_user_id(); #This is essential to ensure we update the user meta otherwise defaults to postmeta

# Check the user role of the logged in user
# If it's not correct, redirect to main dashboard page
$user_meta=get_userdata( get_current_user_id() ); 
$user_roles=$user_meta->roles; 
$groups_user = new Groups_User( get_current_user_id() );
if ( ( in_array("subscriber", $user_roles) && ($groups_user->can( 'enhanced_apprentice' )==1) ) || ( in_array("subscriber", $user_roles) && ($groups_user->can( 'enhanced_graduate' )==1) ) || ( in_array("subscriber", $user_roles) && ($groups_user->can( 'enhanced_both' )==1) ) || in_array("administrator", $user_roles) ) :
	
	echo '<h1>ENHANCED USER PROFILE</h1>';
	
	# https://www.advancedcustomfields.com/resources/acf_form/	
	acf_form(array(
		'field_groups' => array(1505),
		'post_id' => 'user_'.get_current_user_id(),
		'submit_value'	=> 'Update Enhanced Profile',
		'updated_message' => __("Profile Details Updated", 'acf'),
		'html_after_fields' => '<input type="hidden" name="acf[enhanced_profile]" value="true"/>',
	)); 	
	
else:
	
	#echo get_permalink( get_option('woocommerce_myaccount_page_id') ); #debug	
	$redirect = get_permalink( get_option('woocommerce_myaccount_page_id') );
	header('Location: '.$redirect); die; 
	
endif;
?>