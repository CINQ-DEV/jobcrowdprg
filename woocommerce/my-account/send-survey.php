<?php
acf_form_head(); #Add this to enable ACF form
$user_id = 'user_'.get_current_user_id(); #This is essential to ensure we update the user meta otherwise defaults to postmeta
$current_user = get_current_user_id(); #Get the current logged in user ID


# Check the user role of the logged in user
# If it's not correct, redirect to main dashboard page
$user_meta=get_userdata( $current_user ); 
$user_roles=$user_meta->roles; 
if (in_array("subscriber", $user_roles) || in_array("administrator", $user_roles) ) :

	
	echo '<h1>SURVEY</h1>';	
	
	#use this method to restrict which fields are shown to the public
	#admins have more control via wp-admin > lists > add new
	acf_form(array(
		'post_id'		=> 'new_post',
		'post_title'	=> true,
		#'post_content'	=> true,
		'fields' => array(
			#Group 75 :: Survey Import Recipient List
			'field_599afa7c03cbc',	#survey type
			'field_5947dbf58c372',	#newsletter content
			'field_5995536793c74',	#skip first row
			'field_5947df23c2fd6'	#file upload
			
		),		
		'submit_value'	=> 'Create Survey',
		'updated_message' => __("Survey Created", 'acf'),
		'html_after_fields' => '<input type="hidden" name="acf[create_survey]" value="true"/>',
		'return' => '%post_url%',
		'new_post'		=> array(
			'post_type'		=> 'surveys',
			'post_status'	=> 'publish'
		)
	));


	#check whether a new list has been created
	$submitted = $_GET['updated'];
	
	if ($submitted != 'true'):
	
		echo '<p></p>';
		echo "<p>Once you've uploaded your mailing list, you will be able to send your survey email.</p>";
	
	endif;
	
else:
	
	#echo get_permalink( get_option('woocommerce_myaccount_page_id') ); #debug	
	$redirect = get_permalink( get_option('woocommerce_myaccount_page_id') );
	header('Location: '.$redirect); die; 
	
endif;
?>