<?php if(isset($_GET['post_type'])) : $type = $_GET['post_type']; endif; ?>

<?php if ((!is_front_page() && is_home())  || is_singular('post') || $type == 'post'): ?>


	<div class="medium-7 columns page-header-search">


		<form class="hero--search" role="search" method="get" id="search-reviews" action="<?php echo home_url( '/' ); ?>">

			<span class="hero--search--subject">
				<input class="input--search-subject" type="text" value="" name="s" id="s" >
				<label class="input--search-subject---label">
					Search blog posts</strong>
				</label>
			</span>

			<input type="hidden" name="post_type" value="post">
	 
			<span class="hero--search--button">
				<input class="input--search-button button" type="submit" id="searchsubmit" value="Search" />
			</span>

		</form>


	</div><!-- page-header-search -->

<?php else: ?>

	<div class="medium-7 columns page-header-search">


		<form class="hero--search" role="search" method="get" id="search-reviews" action="<?php echo home_url( '/' ); ?>">

			<span class="hero--search--subject">
				<input class="input--search-subject" type="text" value="" name="s" id="s" >
				<label class="input--search-subject---label">
					Search by <strong>Company name</strong>
				</label>
			</span>
						 
			<input type="hidden" value="reviews" name="post_type" />
			<span class="hero--search--button">
				<input class="input--search-button button" type="submit" id="searchsubmit" value="Search" />
			</span>

		</form>


	</div><!-- page-header-search -->

<?php endif; ?>