<?php
#get the values from the form using PHP GET
$s = $_GET['s'];
#echo 'Searching for '.$s.'<br/>'; #debug

$args = array(
	'role__in' => array('customer', 'subscriber'),
	'meta_query' =>
	array(		
		array(
			'relation' => 'AND', # OR

			array(
				'key' => 'activate_membership',
				'value' => 'Yes',
				'compare' => '='
			),
							

			array(
				'key' => 'company_name',
				'value' => $s,
				'compare' => "LIKE",
			)								
		)
	)			 
);
	
$users = get_users( $args );
	
if ($users):

	echo '<div class="row medium-up-2 reviews-items">';

	foreach ( $users as $user ) :
		#echo $user->ID;
		$company_name = get_field('company_name', 'user_'.$user->ID);

		$hyphenate_company_name = str_replace(" ", "-", $company_name); 

		$ind = get_field('employer_registration_industry', 'user_'.$user->ID);

		#we can now get the company rating as it's stored against the user
		$overall_rating = get_field('overall_rating', 'user_'.$user->ID);
		$overall_rating_new = get_field('star_rating');
				
		$overall_rating_percent = $overall_rating_new * 20;

		#get the location (only applicable to enhnaced user profiles)
		$locations = get_field('locations', 'user_'.$user->ID);				
				
		#get the main_address
		$main_address = get_field('main_address', 'user_'.$user->ID);
				
		#use geocoding to get the necessary details
		$geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?sensor=false&address=' . urlencode($main_address) . '&key=AIzaSyAj_Xc1gEDKeDqLIMFHgok9hhWgyf16TK0');
		$output = json_decode($geocode);	
				
		#get the address components
		$address_data = $output->results[0]->address_components;
		$street = str_replace('Dr', 'Drive', $address_data[1]->long_name);
		$town = $address_data[2]->long_name;
		?>
		<div id="<?php the_ID(); ?>" class="column column-block review">
			<a href="<?php echo esc_url( home_url( '/' ) ).'companies/details/?company='.$hyphenate_company_name.'&cid='.$user->ID; ?>" title="View the full details on <?php echo $company_name; ?>">

			<span class="employer-title"><?php echo $company_name; ?></span>

			<div class="stars">
				<div class="stars-gold" style="width: <?php echo $overall_rating_percent; ?>%;"> &nbsp; </div>
				<div class="stars-white"> &nbsp; </div>
			</div>

			<div class="review-bottom">

				<?php if($ind !== "- Select Industry -"): ?><span class="industry">Industry: <strong><?php echo implode(', ', $ind); ?></strong></span><?php endif; ?>
				<?php if($town): ?><span class="industry">Town: <strong><?php echo $town; ?></strong></span><?php endif; ?>	
			        
			</div><!-- review-bottom-->
			</a>

		</div><!--review -->		
		<?php			
					
	endforeach;

	echo '</div>';	

else:	

	echo '<p>No results for '.$s.' matched your criteria.</p>';

endif; #endif $users;	
?>