						<a class="sharethis" data-open="shareModal">
							<svg class="icon icon-share"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-share"></use></svg> Share this
						</a>

						<div class="reveal" id="shareModal" data-reveal>
						  <p class="lead">Share this content.</p>

							<a class="share-link" rel="nofollow" title="Share this article on Facebook " href="http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&t=<?php the_title(); ?>" target="blank">
								<svg class="icon icon-facebook"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-facebook"></use></svg>
							</a>

							<a class="share-link" rel="nofollow" title="Tweet this article" href="http://twitter.com/share?url=<?php the_permalink();?>&text=<?php the_title(); ?>" target="blank">
								<svg class="icon icon-twitter"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-twitter"></use></svg>
							</a>

							<a class="share-link" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>&title=<?php the_title(); ?>">
								<svg class="icon icon-linkedin"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-linkedin"></use></svg>
							</a>

							<a class="share-link" rel="nofollow" title="Share this article by email" href="mailto:?subject=<?php the_title(); ?>&amp;body=<?php the_permalink();?>">
								<svg class="icon icon-email"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-email"></use></svg>
							</a>


						  <button class="close-button" data-close aria-label="Close modal" type="button">
						    <span aria-hidden="true">&times;</span>
						  </button>
						</div><!--sharemodal-->
