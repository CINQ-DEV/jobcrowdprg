<?php
#get our search terms
$s = $_GET['s'];
$location = $_GET['location'];


#inform the user of what we searched
if(!empty($s) && !empty($location)):
	echo '<p>Searching for "'.$s.'" jobs in '. $location .'</p>';
elseif(!empty($location)) :
	echo '<p>Searching for jobs in '.$location.'</p>';
elseif(!empty($s)) :
	echo '<p>Searching for "'.$s.'"</p>';
endif;


###
# Search just the titles
###
$titles = get_posts(array( 
	'post_type' => 'jobs',
	's' => $s
));
#create an array
$title_result = array();
foreach($titles as $title) : 
	#echo $title->ID.'<br>';
	#pass the results into the array
	$title_result[] = $title->ID;
endforeach;


###
# Search just the careers
###
$careers = get_posts(array(
	'post_type' => 'jobs',		
	'meta_query' => array(
		array(
			'key' 	=> 'dynamic_filled_careers',
			'value' => $s,
			'compare' => 'LIKE'
		)
	)	
	
));
#create an array
$career_result = array();
foreach($careers as $career) : 
	#echo $career->ID.'<br>';
	#pass the results into the array
	$career_result[] = $career->ID;
endforeach;


###
# Search just the companies
###
$companies = get_posts(array(
	'post_type' => 'jobs',		
	'meta_query' => array(
		array(
			'key' 	=> 'dynamic_filled_company',
			'value' => $s,
			'compare' => 'LIKE'
		)
	)	
	
));
#create an array
$company_result = array();
foreach($companies as $company) : 
	#echo $company->ID.'<br>';
	#pass the results into the array
	$company_result[] = $company->ID;
endforeach;


###
# Search just the locations
###
$locations = get_posts(array(
	'post_type' => 'jobs',		
	'meta_query' => array(
		array(
			'key' 	=> 'dynamic_filled_town',
			'value' => $location,
			'compare' => 'LIKE'
		)
	)	
	
));
#create an array
$location_result = array();
foreach($locations as $loc) : 
	#echo $loc->ID.'<br>';
	#pass the results into the array
	$location_result[] = $loc->ID;
endforeach;

#echo '<p>Your results</p>';

###
# ONLY THE WILDCARD
###
if ($s != '' && $location == ''):

	#combine all results and remove duplicates
	$unique = array_unique( array_merge( $title_result, $career_result, $company_result ) );

###
# ONLY THE LOCATION
###
elseif ($s == '' && $location != ''):

	#combine all results and remove duplicates
	$unique = array_unique( array_merge( $location_result ) );

###
# BOTH
###
elseif ($s != '' && $location != ''):

	#combine all results from the OR criteria (title,company,career/) and remove duplicates
	$combine = array_unique( array_merge( $title_result, $career_result, $company_result ) );

	#now compare the combined results of the OR search with the AND value (location)
	$unique = array_intersect( $combine,  $location_result);

###
# NOT A SEARCH SO SHOW A MESSAGE
###
else:

	echo '<p>Please enter your search criteria</p>';

endif;


#if we don't have any results
if(empty($unique)) :

	echo '<p>Unfortunately, no results matched your search criteria</p>';

else:
#we have results, so let's display them

	$args = array (
		'post_type' => 'jobs',
		'post__in'	=> $unique		
	);	
	
	$wp_query = new WP_Query( $args );

	$total = $wp_query->found_posts;
	#echo '<p>Results Matched: '.$total.'</p>';

	if ( have_posts() ) : ?>

		<?php while ( have_posts() ) : the_post(); 
		$post_id = get_the_ID();

			$dynamic_filled_company		= get_field('dynamic_filled_company',$post_id);
			$dynamic_filled_careers		= get_field('dynamic_filled_careers',$post_id);
			$dynamic_filled_industry	= get_field('dynamic_filled_industry',$post_id);
			$dynamic_filled_town		= get_field('dynamic_filled_town',$post_id);
			$dynamic_filled_county		= get_field('dynamic_filled_county',$post_id);
			$dynamic_filled_region		= get_field('dynamic_filled_region',$post_id);
			$post_id = get_the_ID();
			$employer = get_field('dynamic_filled_company', $post_id);


			$blogusers = get_users(array('meta_key' => 'company_name', 'meta_value' => $employer));
			foreach ( $blogusers as $user ) {
			#access the user meta
			#echo '<span>' . esc_html( $user->ID ) . '</span>'; #debug
			}	
			$author_id=$post->post_author; 
			#get the main_address
			$logo = get_field('logo', 'user_'.$user->ID);
			#echo $employer;
			
			$overall_rating = get_field('overall_rating', 'user_'.$user->ID);
			$overall_rating_percent = $overall_rating * 20;
			$number_of_reviews = get_field('average_total_reviews', 'user_'.$user->ID);

			?>
				<div id="<?php the_ID(); ?>" class="search-result-block item">

					<a href="<?php the_permalink(); ?>">
							<div class="search-results-block--logo">
								<div class="logo-container">
									<?php if($logo): ?>
										<img src="<?php echo $logo['sizes']['medium']; ?>" alt="Logo"> 
									<?php else: ?>
										<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/no-logo-uploaded.png" alt="No Logo Uploaded"> 
									<?php endif; ?>
								</div>
							</div>
					</a>

					<div class="search-results-block--details">
						
						<a href="<?php the_permalink(); ?>"><span><strong><?php the_title(); ?></strong><br/>
						<?php if($employer): ?>Company: <strong><?php echo $employer; ?></strong><br/><?php endif; ?>
						
						<?php if((implode(', ', $dynamic_filled_industry) != '- Select Industry -') && (implode(', ', $dynamic_filled_industry) != '')): ?>
							Industry: <strong><?php echo implode(', ', $dynamic_filled_industry); ?></strong><br/>
						<?php endif; ?>
												
						<?php $job_type = get_field('dynamic_filled_type'); 
						if ($job_type == '- Select Job Type -'):
							$job = '';
						else:
							$job = $job_type;
						endif;
						?>
						</span></a>

					</div>

					<div class="search-results-block--location">
						
						<span>
							<?php $jobtown = get_field('dynamic_filled_town');
									if($jobtown): ?>
										<?php if(implode(', ', $jobtown) != "- Select Town -"): ?>
											Location:   <strong><?php echo implode(', ', $jobtown); ?></strong><br/>
										<?php endif; 
									endif; ?>

							<?php if(get_field('salary')): ?>
								Salary:       <strong><?php $salary = get_field('salary'); echo '£'.number_format($salary,2); ?></strong>
							<?php endif; ?>

							<?php if(get_field('additional_salary')): ?>
								Additional Salary:       <strong><?php $additional_salary = get_field('additional_salary'); the_field('additional_salary'); //number_format($salary,2); ?></strong>
							<?php endif; ?>

						</span>

					</div>

					<div class="search-results-block--savethis">

						<a href="#" class="savethis btn action add" data-action="add">
							<svg class="icon icon-heart"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-heart"></use></svg>
							<svg class="icon icon-heart icon-heart-hidden"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-heart"></use></svg>
							<span class="save-message">Save this</span>
						</a>

				        <a href="#" class="btn action remove" data-action="remove">
				        	<i class="fa fa-minus-circle" aria-hidden="true"></i> Remove from shortlist
				        </a>

				       <a href="#" class="btn selected"><i class="fa fa-check" aria-hidden="true"></i></a>

					</div>

					<a href="<?php the_permalink(); ?>">
						<div class="search-results-block--moredetails">
							
							<div class="stars">
								<div class="stars-gold" style="width: <?php echo $overall_rating_percent; ?>%;"> &nbsp; </div>
								<div class="stars-white"> &nbsp; </div>
							</div>

							<span class="review-count"><?php if($number_of_reviews != ""): echo $number_of_reviews; else: echo '0'; endif; ?> Reviews</span>

							<span class="button blue">More details</span>

						</div>
					</a>


				</div><!--search-result-block-->













		<?php endwhile;

	endif;

endif;
?>