<?php
/*
 Template Name: Industries  
 */

get_header(); ?>

<?php
$query =  $_GET["industry"];
	
$find = ["-", "_"];
$replace   = [" ", "&amp;"];

$industry = str_replace($find, $replace, $query);                               
$adindustry = str_replace($find, $replace, $query);                               
?> 


<div class="page-header row">

    <div class="medium-5 columns">
    <?php if($industry): ?>
        <h1><?php echo $industry; ?></h1>
    <?php else: ?>
        <h1><?php the_title(); ?></h1>
    <?php endif; ?>
    </div>
    
    <?php get_template_part('template-parts/page-header-search'); ?>

</div><!-- page-header -->


<div class="main-content" data-equalizer="main-content">



    <div class="main-content-main" data-equalizer-watch="main-content">




                <div class="main-content-main--breadcrumbs" data-equalizer-watch="main-content-headers">
                
                            <?php
                            if ( function_exists('yoast_breadcrumb') ) {
                            yoast_breadcrumb('
                            <p id="breadcrumbs">','</p>
                            ');
                            }
                            ?>
                            
                    <div class="save-share">

                        <!--<a href="#" class="savethis">
                            <svg class="icon icon-heart"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-heart"></use></svg> Save this
                        </a>-->

                        <?php get_template_part('template-parts/share-button'); ?>

                    </div>

                </div><!-- main-content-main-breadcrumbs -->

        <div class="padded">
                    
<!-- Nav tabs -->
<ul class="tabs" role="tablist" data-deep-link="true" data-update-history="true" data-tabs id="industriesTabs">
	<li role="presentation" class="tabs-title is-active"><a href="#companies" aria-controls="companies" role="tab" data-toggle="tab">Companies</a></li>
	<li role="presentation" class="tabs-title"><a href="#jobs" aria-controls="jobs" role="tab" data-toggle="tab">Jobs</a></li>
    <li role="presentation" class="tabs-title"><a href="#grad" aria-controls="grad" role="tab" data-toggle="tab">Graduate Reviews</a></li>
    <li role="presentation" class="tabs-title"><a href="#app" aria-controls="app" role="tab" data-toggle="tab">Apprentice Reviews</a></li>
</ul>

<!-- Tab panes -->
<div class="tab-content" id="industriesTabsContent" data-tabs-content="industriesTabs">
	<div role="tabpanel" class="tabs-panel is-active" id="companies">
    
	<?php  
    $args = array(
        #'role__in'		=> array('employer', 'subscriber'),
        'role__in'		=> array('customer', 'subscriber'),
        'role__not_in'	=> array('administrator'),
        'exclude'		=> array(1),
        'orderby'		=> 'ID',
        'order'			=> 'ASC',

        'meta_query' => array(
        'relation' => 'AND',
        'industry' => array(
            'key' => 'employer_registration_industry',
            'value' => $industry,
            'compare' => 'LIKE',

        ),
        'rating' => array(
            'key' => 'overall_rating',
            'compare' => 'EXISTS',
        ), 
    ),
    'orderby' => array( 
        'rating' => 'DESC',
    ),
    );
    $users = get_users($args);
    if ( $users ) :
        foreach ($users as $user) : 
        $company_name = get_field('company_name', 'user_'.$user->ID);
        $hyphenate_company_name = str_replace(" ", "-", $company_name); 
        $average_total_reviews = get_field('average_total_reviews', 'user_'.$user->ID);
        $overall_rating = get_field('overall_rating', 'user_'.$user->ID);
        $overall_rating_percent = $overall_rating * 20;
        $employer_registration_industry = get_field('employer_registration_industry', 'user_'.$user->ID);
        $number_of_employees = get_field('number_of_employees', 'user_'.$user->ID);
    ?>
        <div class="job-reviews-by-company--job-review">
                <a href="<?php echo esc_url( home_url( '/' ) ).'companies/details/?company='.$hyphenate_company_name.'&cid='.$user->ID; ?>" title="View the full details on <?php echo $company_name; ?>">
                    
                                    <div class="job-review--header">
                                        
                                                <span class="job-review-title"><?php echo $company_name; ?></span>
                                        
                                                 <?php if ($average_total_reviews != '0'): ?>
                                                    
                                                <div class="stars">
                                                    <div class="stars-gold" style="width: <?php echo $overall_rating_percent; ?>%;"> &nbsp; </div>
                                                    <div class="stars-white"> &nbsp; </div>
                                                </div>
                                                    <?php #$review = '<img src="'.get_template_directory_uri().'/img/stars_'.$overall_rating.'.png" /> <small> based on</small> '.$average_total_reviews.' <small>reviews</small>'; 
                                                    #'<span>'.$average_total_reviews.' reviews</span>'; 
                                                else:   
                                                    $review = '<small>no reviews</small>'; 
                                                endif; ?>



                                    </div><!--job-review-header-->

                                    <div class="job-review--footer" data-equalizer-watch="job-review-footer">
                                        <?php if (implode(', ', $employer_registration_industry)): ?>
                                            <span>Industry: <strong><?php echo implode(', ', $employer_registration_industry); ?></strong></span>
                                        <?php endif; ?>

                                        <?php if ($number_of_employees): ?>
                                            <span>Number of employees: <strong><?php echo $number_of_employees; ?></strong></span>
                                        <?php endif; ?>

                                        <?php
                                        #get the main_address
                                        $main_address = get_field('main_address', 'user_'.$user->ID);
                                        if($main_address):
                                            #use geocoding to get the necessary details
                                            $geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?sensor=false&address=' . urlencode($main_address) . '&key=AIzaSyAj_Xc1gEDKeDqLIMFHgok9hhWgyf16TK0');
                                            $output = json_decode($geocode);    
                                            
                                            #get the address components
                                            $address_data = $output->results[0]->address_components;
                                            //$street = str_replace('Dr', 'Drive', $address_data[1]->long_name);
                                            $town = $address_data[2]->long_name;
                                            //$county = $address_data[3]->long_name;    
                                        ?>

                                            <?php if($town): ?>
                                                <span>
                                                    Location:  <strong><?php echo $town; ?></strong>
                                                </span>
                                            <?php endif; // if town?>

                                        <?php endif; // if has main address ?>

                                    </div><!--job-review-footer-->
                    
                </a>

            </div>

        <?php
        endforeach;
    else:
        echo '<p>No companies found in this industry.</p>';
    endif;
	wp_reset_query(); 
    ?>
    
	</div>
	<div role="tabpanel" class="tabs-panel" id="jobs">
   
	<?php  
    $args = array (
        'post_type' 	=> 'jobs',
        'post_status' 	=> 'publish',
        'orderby' 		=> 'ID',			
        'order' 		=> 'asc',
		'meta_query' 	=> array(
			array(
				'key' 		=> 'dynamic_filled_industry',
				'value' 	=> '"'.$industry.'"',
				'compare' 	=> 'LIKE'
			)
		)
    );		
		
    $wp_query = new WP_Query( $args );
                
    if ( have_posts() ) :
        while ( have_posts() ) : the_post();
        ?>
        
        <a href="<?php the_permalink(); ?>" title="View the full details on <?php the_title(); ?> &raquo;"><?php the_title(); ?></a><br>

        <?php
        endwhile;
    else:
        echo '<p>No jobs found in this industry.</p>';
    endif;
	wp_reset_query(); 	
    ?>
    
	</div>
    
    <div role="tabpanel" class="tabs-panel" id="grad">

    <?php 



    $args = array (
        'post_type' => 'graduate_reviews',
         'meta_query' => array(
                    array(
                        'key'   => 'graduate_review_industry',
                        'value' => $industry,
                        'compare' => 'LIKE'
                    )
                )   

    );  
    
    $wp_query = new WP_Query( $args );

    if ( $wp_query->have_posts() ) :    
        echo '<div class="row">';

        while ( $wp_query->have_posts() ) : $wp_query->the_post(); 
        
        #need the ascertain the post type, so we can now get the company that's been reviewed
        
        #need to exclude any results whereby the industry hasn't been specified
        #if ($industry != '- Select Industry -'):

            $post_id = get_the_ID();    
            #echo $post_id;
                $post_type = get_post_type( get_the_ID() ); 

                    $employer = get_field('graduate_review_employer', $post_id);
                    $ind = get_field('graduate_review_industry', $post_id);

                $blogusers = get_users(array('meta_key' => 'company_name', 'meta_value' => $employer));
                foreach ( $blogusers as $user ) {
                    #access the user meta
                    #echo '<span>' . esc_html( $user->ID ) . '</span>'; #debug
                }   
                
                #we can now get the company rating as it's stored against the user
                $overall_rating = get_field('overall_rating', 'user_'.$user->ID);
                $overall_rating_new = get_field('star_rating');
                
                $overall_rating_percent = $overall_rating_new * 20;


                #get the location (only applicable to enhnaced user profiles)
                $locations = get_field('locations', 'user_'.$user->ID);             
                
                #get the main_address
                $main_address = get_field('main_address', 'user_'.$user->ID);
                
                #use geocoding to get the necessary details
                $geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?sensor=false&address=' . urlencode($main_address) . '&key=AIzaSyAj_Xc1gEDKeDqLIMFHgok9hhWgyf16TK0');
                $output = json_decode($geocode);    
                
                #get the address components
                $address_data = $output->results[0]->address_components;
                $street = str_replace('Dr', 'Drive', $address_data[1]->long_name);
                $town = $address_data[2]->long_name;


        ?>
        <div class="job-reviews-by-company--job-review">

                <a href="<?php the_permalink(); ?>">
                    
                                    <div class="job-review--header">
                                        
                                                <span class="job-review-title"><?php echo $employer; ?></span>
                                        
                                                 <?php if ($average_total_reviews != '0'): ?>
                                                    
                                                <div class="stars">
                                                    <div class="stars-gold" style="width: <?php echo $overall_rating_percent; ?>%;"> &nbsp; </div>
                                                    <div class="stars-white"> &nbsp; </div>
                                                </div>
                                                    <?php #$review = '<img src="'.get_template_directory_uri().'/img/stars_'.$overall_rating.'.png" /> <small> based on</small> '.$average_total_reviews.' <small>reviews</small>'; 
                                                    #'<span>'.$average_total_reviews.' reviews</span>'; 
                                                else:   
                                                    $review = '<small>no reviews</small>'; 
                                                endif; ?>



                                    </div><!--job-review-header-->

                                    <div class="job-review--footer" data-equalizer-watch="job-review-footer">
                                        <?php if (implode(', ', $employer_registration_industry)): ?>
                                            <span>Industry: <strong><?php echo implode(', ', $employer_registration_industry); ?></strong></span>
                                        <?php endif; ?>

                                        <?php if ($number_of_employees): ?>
                                            <span>Number of employees: <strong><?php echo $number_of_employees; ?></strong></span>
                                        <?php endif; ?>

                                        <?php
                                        #get the main_address
                                        $main_address = get_field('main_address', 'user_'.$user->ID);
                                        if($main_address):
                                            #use geocoding to get the necessary details
                                            $geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?sensor=false&address=' . urlencode($main_address) . '&key=AIzaSyAj_Xc1gEDKeDqLIMFHgok9hhWgyf16TK0');
                                            $output = json_decode($geocode);    
                                            
                                            #get the address components
                                            $address_data = $output->results[0]->address_components;
                                            //$street = str_replace('Dr', 'Drive', $address_data[1]->long_name);
                                            $town = $address_data[2]->long_name;
                                            //$county = $address_data[3]->long_name;    
                                        ?>

                                            <?php if($town): ?>
                                                <span>
                                                    Location:  <strong><?php echo $town; ?></strong>
                                                </span>
                                            <?php endif; // if town?>

                                        <?php endif; // if has main address ?>

                                    </div><!--job-review-footer-->
                    
                </a>

            </div>
                


        <?php       
        #endif; #endif $industry
        endwhile; #wp_reset_query();    
        echo '</div>';  
            
    else:   
        
        echo '<h3>No graduate matched your criteria.</h3>';
        
    endif; #endif $users

?>

    </div>
    
    <div role="tabpanel" class="tabs-panel" id="app">



    <?php 



    $args = array (
        'post_type' => 'apprentice_reviews',
         'meta_query' => array(
                    array(
                        'key'   => 'apprentice_review_industry',
                        'value' => $industry,
                        'compare' => 'LIKE'
                    )
                )   

    );  
    
    $wp_query = new WP_Query( $args );

    if ( $wp_query->have_posts() ) :    
        echo '<div class="row">';

        while ( $wp_query->have_posts() ) : $wp_query->the_post(); 
        
        #need the ascertain the post type, so we can now get the company that's been reviewed
        
        #need to exclude any results whereby the industry hasn't been specified
        #if ($industry != '- Select Industry -'):

            $post_id = get_the_ID();    
            #echo $post_id;
                $post_type = get_post_type( get_the_ID() ); 

                    $employer = get_field('apprentice_review_employer', $post_id);
                    $ind = get_field('apprentice_review_industry', $post_id);

                $blogusers = get_users(array('meta_key' => 'company_name', 'meta_value' => $employer));
                foreach ( $blogusers as $user ) {
                    #access the user meta
                    #echo '<span>' . esc_html( $user->ID ) . '</span>'; #debug
                }   
                
                #we can now get the company rating as it's stored against the user
                $overall_rating = get_field('overall_rating', 'user_'.$user->ID);
                $overall_rating_new = get_field('star_rating');
                
                $overall_rating_percent = $overall_rating_new * 20;


                #get the location (only applicable to enhnaced user profiles)
                $locations = get_field('locations', 'user_'.$user->ID);             
                
                #get the main_address
                $main_address = get_field('main_address', 'user_'.$user->ID);
                
                #use geocoding to get the necessary details
                $geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?sensor=false&address=' . urlencode($main_address) . '&key=AIzaSyAj_Xc1gEDKeDqLIMFHgok9hhWgyf16TK0');
                $output = json_decode($geocode);    
                
                #get the address components
                $address_data = $output->results[0]->address_components;
                $street = str_replace('Dr', 'Drive', $address_data[1]->long_name);
                $town = $address_data[2]->long_name;


        ?>
        <div class="job-reviews-by-company--job-review">

                <a href="<?php the_permalink(); ?>">
                    
                                    <div class="job-review--header">
                                        
                                                <span class="job-review-title"><?php echo $employer; ?></span>
                                        
                                                 <?php if ($average_total_reviews != '0'): ?>
                                                    
                                                <div class="stars">
                                                    <div class="stars-gold" style="width: <?php echo $overall_rating_percent; ?>%;"> &nbsp; </div>
                                                    <div class="stars-white"> &nbsp; </div>
                                                </div>
                                                    <?php #$review = '<img src="'.get_template_directory_uri().'/img/stars_'.$overall_rating.'.png" /> <small> based on</small> '.$average_total_reviews.' <small>reviews</small>'; 
                                                    #'<span>'.$average_total_reviews.' reviews</span>'; 
                                                else:   
                                                    $review = '<small>no reviews</small>'; 
                                                endif; ?>



                                    </div><!--job-review-header-->

                                    <div class="job-review--footer" data-equalizer-watch="job-review-footer">
                                        <?php if (implode(', ', $employer_registration_industry)): ?>
                                            <span>Industry: <strong><?php echo implode(', ', $employer_registration_industry); ?></strong></span>
                                        <?php endif; ?>

                                        <?php if ($number_of_employees): ?>
                                            <span>Number of employees: <strong><?php echo $number_of_employees; ?></strong></span>
                                        <?php endif; ?>

                                        <?php
                                        #get the main_address
                                        $main_address = get_field('main_address', 'user_'.$user->ID);
                                        if($main_address):
                                            #use geocoding to get the necessary details
                                            $geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?sensor=false&address=' . urlencode($main_address) . '&key=AIzaSyAj_Xc1gEDKeDqLIMFHgok9hhWgyf16TK0');
                                            $output = json_decode($geocode);    
                                            
                                            #get the address components
                                            $address_data = $output->results[0]->address_components;
                                            //$street = str_replace('Dr', 'Drive', $address_data[1]->long_name);
                                            $town = $address_data[2]->long_name;
                                            //$county = $address_data[3]->long_name;    
                                        ?>

                                            <?php if($town): ?>
                                                <span>
                                                    Location:  <strong><?php echo $town; ?></strong>
                                                </span>
                                            <?php endif; // if town?>

                                        <?php endif; // if has main address ?>

                                    </div><!--job-review-footer-->
                    
                </a>

            </div>
                


        <?php       
        #endif; #endif $industry
        endwhile; #wp_reset_query();    
        echo '</div>';  
            
    else:   
        
        echo '<h3>No apprentice reviews matched your criteria.</h3>';
        
    endif; #endif $users

?>



    </div>
    
</div>		        





		</div><!-- #main -->

</div>
    <div class="sidebar-left" data-equalizer-watch="main-content">


            <div class="sidebar--header" data-equalizer-watch="main-content-headers">
                <h3>Filter Industries</h3> <svg class="icon icon-details"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-details"></use></svg>
            </div>

            <div class="padded">

                <?php
                # we need to get a list of all industries from the options page

                # reset choices
                $field['industries'] = array();
                        
                # get the textarea value from options page without any formatting
                $industries = get_field('industry', 'option', false);
                    
                # explode the value so that each line is a new array piece
                $industries = explode("\n", $industries);
                    
                # remove any unwanted white space
                $industries = array_map('trim', $industries);
                $url = get_site_url();

                # loop through array and add to field 'choices'
                if( is_array($industries) ) :
                    foreach( $industries as $industry ) :
                        if($industry === '- Select Industry -') continue; #skip first item      
                        #$industry2 = str_replace("&","&amp;",$industry);       
				
				

						$find = [" ", "&amp;"];
						$replace   = ["-", "_"];
						$cleaned_industry = str_replace($find, $replace, $industry);
					
                       	echo '<a href="' . $url . '/industries/?industry='.$cleaned_industry.'">'.$industry.'</a>';
                            
                    endforeach;
                endif; ?>

            </div>








    </div><!--sidebar-left -->








    <div class="sidebar-right" data-equalizer-watch="main-content">


<?php 
#get the post id
wp_reset_query();
$id = get_the_id();

#we can now get the specific industry we're in
$dynamic_filled_industry = $adindustry;
#echo $dynamic_filled_industry;

if( !empty($dynamic_filled_industry) ):
    echo '<div class="padded">';
                #echo '<h4><a href="/industries/?industry='.$dynamic_filled_industry.'" title="'.$dynamic_filled_industry.'">'.$dynamic_filled_industry.'</a></h4>';

                #handle the & / &amp; issue as the DB sotres as &amp; so need to convert & in order for the query to work
                $industry = htmlspecialchars($dynamic_filled_industry, ENT_QUOTES);
                #echo $industry;

                #lets search for the relevant row with our advert
                global $wpdb;
                
                #pass in the industry/sector name
                #$advert = 'IT Development &amp; Consulting';
                #$advert = 'Accountancy &amp; Insurance';
                                                                                
                $adverts = $wpdb->get_results(
                                $wpdb->prepare( 
                                "
                                SELECT *
                                FROM $wpdb->options
                                WHERE option_name LIKE %s
                                AND option_value = %s
                                ",
                                'options_advertisement_%_sector',
                                $dynamic_filled_industry             
                                )
                );
                #echo $wpdb->last_query.'<br>';             #debug 
                
                #we can now obtain the specific repeater row details
                foreach ( $adverts as $advert ):
                
                                $option_id                    = $advert->option_id;
                                $option_name                  = $advert->option_name;
                                $option_value                 = $advert->option_value;
                                /*           
                                echo '<p>Option ID: '.$advert->option_id.'</p>';
                                echo '<p>Option Name: '.$advert->option_name.'</p>';
                                echo '<p>Option Value: '.$advert->option_value.'</p>';
                                */
                endforeach;
                
                #identify the row number
                $row = preg_replace("/[^0-9]/","",$option_name);
                
                #we can now get the specific advert from the row
                $image_id = get_option( 'options_advertisement_'.$row.'_advert' );
                
                #finally, we can get the image by attachment ID
                $image = wp_get_attachment_image_src( $image_id, 'full' );
                # url = $image[0];
                # width = $image[1];
                # height = $image[2];
                ?>
                            <img src="<?php echo $image['0']; ?>" />

            <?php     
            echo '</div>';
            endif; #!empty($dynamic_filled_industry) ?>


<!--
        <div class="sidebar--header" data-equalizer-watch="main-content-headers">
            <h3>Your saved jobs</h3>
        </div>

        <div class="saved-jobs">

            <?php //get_template_part("sidebar-saved-jobs"); ?>

        </div>

    </div>
-->



</div> <!-- main-content -->



<?php
#get_sidebar();
get_footer();
