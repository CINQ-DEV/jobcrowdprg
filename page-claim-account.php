<?php
/*
Template Name: Claim Account
*/

get_header(); ?>

<div class="page-header row">

	<div class="medium-5 columns">
		<h1>Sign Up</h1>
	</div>
	
	<?php get_template_part('template-parts/page-header-search'); ?>

</div><!-- page-header -->



<div class="main-content fullwidth" data-equalizer="main-content">

	<div class="main-content-main" data-equalizer-watch="main-content">

				<div class="main-content-main--breadcrumbs" data-equalizer-watch="main-content-headers">
							<?php
							if ( function_exists('yoast_breadcrumb') ) {
							yoast_breadcrumb('
							<p id="breadcrumbs">','</p>
							');
							}
							?>
					<div class="save-share">

						<!--<a href="#" class="savethis">
							<svg class="icon icon-heart"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-heart"></use></svg> Save this
						</a>-->

						<?php get_template_part('template-parts/share-button'); ?>

					</div><!--save-share-->

				</div><!-- main-content-main-breadcrumbs -->
				
				<div class="padded">
					
					<div class="row">
						<div class="medium-12 columns">	
						<?php get_template_part('layout'); ?>
						</div> <!-- medium-12 -->
					</div> <!-- row -->								

				</div><!--padded-->
				
				<div class="padded">
					
					<div class="row">

					<div class="medium-6 columns">
					<h3>Have an Account?</h3>
					<?php 
					if (is_user_logged_in()):	
						#echo '<p>Complete your order</p>';
					else:
						$existing_user_introduction = get_field('existing_user_introduction'); 
						if ($existing_user_introduction):
							echo $existing_user_introduction;
						endif;
					endif;	
					?>
					<?php 
					#create a conditional widget area to embed a woo login form	
					if (is_user_logged_in()):
						global $woocommerce;
						$checkout_url = $woocommerce->cart->get_checkout_url();						
						echo '<a href="'.$checkout_url.'" class="button">Proceed to register</a>';
					else:	
						#not logged in, so show form
						if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("WooCommerce widgets") ) : endif;
					endif; 						
					?>						
					</div> <!-- medium-6 -->									

					<div class="medium-6 columns">
					<?php 
					echo '<h3>Need an Account?</h3>';
					$claim_account_introduction = get_field('claim_account_introduction'); 
					if ($claim_account_introduction):
						echo $claim_account_introduction;
					endif;
					?>					
					<style>
					.error {color: #FF0000;}
					.success {color: #ec008c;}
					</style>					
					<?php				
					#define variables and set to empty values
					$emailErr = "";
					$companyErr = "";
					$emailSuc = "";
					$companySuc = "";
					$successMsg = "";
					$email = "";
					$company = "";
					if ($_SERVER["REQUEST_METHOD"] == "POST") :

						#we need to check if the email has been previously registered						
						$exists = email_exists( $_POST["email"] );
						if ( $exists ) :

							echo '<h3>Sorry!</h3>';
							echo '<p>That email is already registered with us.</p>';

						else:

							if (empty($_POST["email"])) :

								$emailErr = "Email is required";

							else:

								$email = test_input($_POST["email"]);
								# check if e-mail address is well-formed
								if (!filter_var($email, FILTER_VALIDATE_EMAIL)) :

									$emailErr = "Invalid email format"; 

								else:

									$emailSuc = 'True';	

								endif;

							endif; #endif; $_POST["email"] 						

						endif;	#endif $email_exists					



						if (empty($_POST["company"])) :		

							$companyErr = "Company is required";

						else:	

							$company = $_POST["company"];				
							$blogusers = get_users(array('meta_key' => 'company_name', 'meta_value' => $company));
							foreach ( $blogusers as $user ) :
								$uID = $user->ID;
								$company_name = get_field('company_name', 'user_'.$user->ID);		
							endforeach;

							$companySuc = 'True';				

						endif; 	#endif 	$_POST["company"]	

						if ( ($emailSuc == "True") && ($companySuc == "True") ) :

							$hash = md5( rand(0,1000) ); 			
							update_user_meta( $uID, 'hash', $hash );
							#update_user_meta( $uID, 'account_verified', '0' );

							#for reference, copy the old username so we can track it if needs be
							$user = get_user_by('id',$uID);
							if($user):
								update_user_meta( $uID, 'existing_email_address', $user->user_login );
							endif;							

							echo '<p>Please check your email to verify the account.</p>';													

							#$to		= get_option( 'admin_email' );
							$to			= $email;				
							$subject	= "The Job Crowd :: Account Ownership Verification";
							$message	= "Hi {$email} , <br/><br/>";
							$message	.= "You're receiving this email as someone used your email address ({$email}) to claim the account at https://www.thejobcrowds.com.<br/><br/>";
							#$message	.= "Email Address: {$email} <br/>";
							#$message	.= "Company: {$company_name} <br/>";	
							#$message	.= "uID: {$uID} <br/>";			
							#$message	.= "hash: {$hash} <br/>";	
							$message	.= "Please click this link to verify your account: <br/><br/>";					
							$message	.= "https://www.thejobcrowd.com/sign-up/verified/?email='.$email.'&hash='.$hash.'";
							$message	.= "<br/><br/>";
							$message	.= "If this wasn't you, please contact us: https://www.thejobcrowd.com/contact-us/  <br/>";	
							$headers = array(
								'Content-Type: text/html; charset=UTF-8',
								'From: '.get_option( 'blogname' ).' <info@thejobcrowd.com>'
							);
							wp_mail ($to, $subject, $message, $headers);	
							
							$successMsg = "<p>An email has been sent to your address. Please also check your spam/junk folder if it's not received.</p>";

						endif;

					endif; #endif POST

					function test_input($data) {
						$data = trim($data);
						$data = stripslashes($data);
						$data = htmlspecialchars($data);
						return $data;
					}
					?>

					<?php
					#get all the companies, pass company into predictive search						
					$companies = array(
						array('customer', 'subscriber'),
						'number' => -1,
						'meta_query' =>
						array(

							array(
								'relation' => 'AND', # OR						

								array(
									'key' => 'activate_membership',
									'value' => 'Yes',
									'compare' => "=",
								),
								array(
									'key' => 'account_verified',
									'value' => '0',
									'compare' => "=",
								)			
							)
						)

					);						

					$all_users = get_users( $companies );


					if ($all_users):
						$company = array();
						foreach ( $all_users as $user ) :		

							#$company[] = '"'.get_user_meta( $user->ID, 'billing_company', true ).'"';
							$company[] = '"'.get_user_meta( $user->ID, 'company_name', true ).'"';				

						endforeach;

					endif;

					?>	
					<!-- /include the jquery auto complete files -->
					<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
					<link rel="stylesheet" href="/resources/demos/style.css">
					<script src="//code.jquery.com/jquery-1.12.4.js"></script>
					<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
			
					<form action="https://www.thejobcrowd.com/sign-up/" method="post" class="form-border">
					<?php echo '<h5 class="success">'.$successMsg.'</h5>'; ?> 
						<p class="login-password">
						<?php
						# convert array into comma-separated string
						$companies = implode(',', $company );	
						#replace apostrophe's as this breaks the results	
						$cleaned_companies = str_replace("'","",$companies);	
						?>	
						<script>
						$( function() {
							var availableCompanies = [
								<?php echo $cleaned_companies; #output the companies as a comma-separated list ?>
							];
							$( "#companies" ).autocomplete({
								source: availableCompanies
							});
						});
						</script>
						<label for="company">Company:</label><br/>
						<input id="companies" name="company" value="">	    
						<span class="error">* <?php echo $companyErr;?></span>
						</p> 					
						
						<?php	
						$claim_account_instructions = get_field('claim_account_instructions'); 
						if ($claim_account_instructions):
							echo $claim_account_instructions;
						endif;
						?>		
						
						<p class="login-username">
						<label for="email">Email:</label><br/>
						<input type="text" name="email">
						<span class="error">* <?php echo $emailErr;?></span>      
						</p>
							
						<p class="login-submit">          
						<input type="submit"  class="button alt" value="Verify Account" />
						</p> 																															
						
					</form>	
							
					<?php $claim_account_note = get_field('claim_account_note'); 
					if ($claim_account_note):
						echo $claim_account_note;
					endif;
					?>		
                    <p>&nbsp;</p>				
					<?php 
					#create a conditional widget area to embed a woo login form	
					if (is_user_logged_in()):
						global $woocommerce;
						$checkout_url = $woocommerce->cart->get_checkout_url();
						#echo '<p>Complete your order</p>';
						#echo '<a href="'.$checkout_url.'" class="button">Proceed to register</a>';
					else:	
						$new_user_introduction = get_field('new_user_introduction'); 
						if ($new_user_introduction):
							echo $new_user_introduction;
						endif;						
						#not logged in, so show form
						#echo do_shortcode('[woocommerce_simple_registration]');
					endif; 
					?>	
					</div> <!-- medium-6 -->
					
					</div> <!-- row -->
					<div class="row">
					

					</div> <!-- row -->

				</div> <!-- padded -->
			
	</div><!--main-content-main -->



</div> <!-- main-content -->

<?php get_footer();