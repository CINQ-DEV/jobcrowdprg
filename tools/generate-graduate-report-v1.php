<?php
# Our include
require_once('../../../../wp-load.php');

#https://www.thejobcrowd.com/wp-content/themes/jobcrowdprg/tools/generate-graduate-report-v1.php

$graduate_reviews = array (
	'post_type' 	=> 'graduate_reviews',
	'post_status'	=> array('publish','draft')
);	
$wp_gr_query = new WP_Query( $graduate_reviews );

$total_graduate_reviews = $wp_gr_query->found_posts;
echo '<p>total graduate reviews: '.$total_graduate_reviews.'</p>';

$rows_per_file = '1000';
#$rows_per_file = '10';
#echo '<p>max rows per file: '.$rows_per_file.'</p>';

$no_of_csv_files = ceil($total_graduate_reviews/$rows_per_file);
#$no_of_csv_files = '2';
echo '<p>no of files to create: '.$no_of_csv_files.'</p>';

for ($x = 0; $x <= $no_of_csv_files; $x++) {
	
	#calculate the offset
	$offset = ($rows_per_file * $x);	

	echo '<p>Offset: '.$offset.'</p>';		
	echo '<p>Iteration: '.$x.'</p>';
	
		$args = array(
			'posts_per_page'	=> $rows_per_file,
			'offset'			=> $offset,
			'post_status'		=> array('publish','draft'),
			'post_type'			=> 'graduate_reviews',
			'orderby'			=> 'date',
			'order'				=> 'DESC',
			'fields'			=> 'ids'
				
		);
		$data = array();
		$loop = new WP_Query($args);
		if( $loop->have_posts() ):
	
			#create column headers
			$data[] = array(
"Review ID",
"Title",
"Email",
"Gender",
"Ethnicity",
"What is your highest level of qualification?",
"More details (optional)",
"University Attended",
"Degree Subject",
"Degree Level",
"Degree Grade",
"Employer",
"Employer Other",
"Industry",
"Department",
"Job Title",
"How long have you been at the company?",
"Which of the following are most important to you?",
"What most attracted you to this company?",
"What most attracted you to this company? Other",
"What most attracted you to the role?",
"Application Advice",
"Interview Advice",
"What are the key skills that you use in your day-to-day work life?",
"Best",
"Worst",
"How long do you intend to stay at this company?",
"What do you think would be the reason for leaving?",
"What are the Best and Worst things about your company? Best",
"What are the Best and Worst things about your company? Worst",
"Manage people",
"50% pay rise relative to starting salary",
"100% pay rise relative to starting salary",
"International travel",
"International relocation",
"Ownership of specific project/business area",
"First promotion",
"Second promotion",
"Control over your work hours",
"Professional Qualification",
"Do you hope to progress within the company you are currently work for?",
"How would you rate the career progression at the company you work for?",
"Are career paths within your company clear to you?",
"Have you already progressed in any way?",
"Which of the following exist at your company?1",
"Can you think of anything to improve the Career Progression (not including promoting you to CEO!)",
"How would you rate the colleagues in the department you work in?",
"Is there diversity within the team you work in?",
"Are there clearly defined processes for dealing with difficult colleagues?",
"Did you start at the same time as any other graduates?",
"Which of the following exist at your company?2",
"Can you think of anything to improve the Colleagues (not including hiring all of your friends!)",
"How would you rate the company & office culture?",
"Are you familiar with the company values?",
"Are you familiar with the company values? Please specify",
"Is there a welcoming environment?",
"Do you know your departments’ mission statements?",
"Do you know your departments’ mission statements? Please specify",
"Which of the following statements are true of your company and office culture?",
"Can you think of anything to improve the Company Culture (not including onesie Wednesdays!)",
"When you first joined the company how much did you enjoy your role?",
"Now, in general, how much do you enjoy your day-to-day work?",
"Would you enjoy another role in the company more than what you currently do?",
"Would you enjoy another role in the company more than what you currently do? Please specify",
"In general, how much does the team you work in seem to enjoy their work?",
"Which of the following contribute to enjoyment of your work?",
"Can you think of anything to improve the Enjoyment of your work (not including bouncy castle breaks!)",
"Do you think your company is ethically & environmentally aware?",
"How would you rate the Ethical & Environmental Awareness of your company?",
"Have you been directly involved in any Ethical and Environmental activities at your company?",
"Is there an individual or department, dedicated to Ethical and Environmental issues?",
"Which of the following exist at your company?3",
"Can you think of anything to improve the Ethical and Environmental awareness?",
"What is your favourite responsibility?",
"What is your favourite responsibility? Why",
"What responsibility do you enjoy less?",
"What responsibility do you enjoy less? Why",
"Would you like more responsibility?",
"How would you rate the level of responsibility you have?",
"Which of the following are true statements about the Responsibilities you have in your role?",
"Can you think of anything to improve your Responsibilities (not including removing all of them!)",
"Did you receive training when you first started?",
"How would you rate the usefulness of your training?",
"Have you had other training opportunities?",
"How would you rate the enjoyment of the training?",
"Which of the following statements are true of the training at the company you work for?",
"Can you think of anything to improve the Training (not including getting training on how to catch dragons!)",
"Do you feel confident to ask for support, if you need it?",
"How would you rate the support you receive?",
"Are you responsible for supporting anybody?",
"Have you required support in your role?",
"Which of the following statements are true of the support at the company you work for?",
"Can you think of anything to improve the Support (not including getting someone else to do the work for you!)",
"What time do you normally start and finish work? Start time",
"What time do you normally start and finish work? Finish time",
"How would you rate your work/life balance?",
"Which of the following exist at the company you work for?",
"Do you have to take your work home with you?",
"Do your colleagues take an interest in your personal matters?",
"Can you think of anything to improve the Work/Life Balance (not including 5 day weekends!)",
"Did you receive a sign on bonus?",
"Do you receive performance based bonuses?",
"Does your office building have perks, (such as gym facilities, relaxation areas etc)?",
"Does the company you work for, subscribe to any consumer benefits programme (internally /externally)?",
"How would you rate the benefits package?",
"Can you think of anything to improve the Benefits (not including introducing free drinks every day at 5pm!)",
"Is your role predominantly office based?",
"How satisfied are you with the working location?",
"Does the company you work for have other offices in the UK?",
"Does the company you work for have other offices abroad?",
"Can you think of anything to improve the Location (not including moving the offices to your back garden!)",
"Would you like to be offered international opportunities?",
"Do you speak any other languages?",
"How satisfied are you with the international opportunities available to you?",
"Would you be happy to permanently relocate abroad?",
"Can you think of anything to improve the International Opportunities (not including offering free holidays to everyone!)",
"How would you rate the senior management of the company?",
"How would you rate the management in your department?",
"How often do you have 1-2-1’s with your management?",
"Do you feel comfortable to talk to your management about issues you have within your role?",
"Do you feel that your management care about your day-to-day activities?",
"Can you think of anything to improve the Management (not including firing any of them!)",
"STARTING Salary",
"CURRENT Salary",
"How satisfied are you with your current salary level?",
"Has this company ever offered you a pay rise?",
"How much do you think you are worth?",
"Can you think of anything to improve the Salary (not including increasing them!)",
"All in all, what one thing could be done to improve the company and job?",
"Company ID",
			); 			
	
			while( $loop->have_posts() ): $loop->the_post();
				
				#get the post ID 
				$id = get_the_ID();	
				$title = get_the_title( $id ); 
			
				$graduate_review_how_would_you_rate_the_career_progression_at_the_company_you_work_for = get_field("graduate_review_how_would_you_rate_the_career_progression_at_the_company_you_work_for", $id);
				if ($graduate_review_how_would_you_rate_the_career_progression_at_the_company_you_work_for == 'Poor'):
					$career_progression = '1';
				elseif ($graduate_review_how_would_you_rate_the_career_progression_at_the_company_you_work_for == 'Fair'):
					$career_progression = '2';
				elseif ($graduate_review_how_would_you_rate_the_career_progression_at_the_company_you_work_for == 'Good'):
					$career_progression = '3';
				elseif ($graduate_review_how_would_you_rate_the_career_progression_at_the_company_you_work_for == 'Very Good'):
					$career_progression = '4';
				else:
					$career_progression = '5';
				endif;

				$graduate_review_how_would_you_rate_the_colleagues_in_the_department_you_work_in = get_field("graduate_review_how_would_you_rate_the_colleagues_in_the_department_you_work_in", $id);

				if ($graduate_review_how_would_you_rate_the_colleagues_in_the_department_you_work_in == 'Poor'):
					$colleagues = '1';
				elseif ($graduate_review_how_would_you_rate_the_colleagues_in_the_department_you_work_in == 'Fair'):
					$colleagues = '2';
				elseif ($graduate_review_how_would_you_rate_the_colleagues_in_the_department_you_work_in == 'Good'):
					$colleagues = '3';
				elseif ($graduate_review_how_would_you_rate_the_colleagues_in_the_department_you_work_in == 'Very Good'):
					$colleagues = '4';
				else:
					$colleagues = '5';
				endif;

				$graduate_review_how_would_you_rate_the_company_office_culture = get_field("graduate_review_how_would_you_rate_the_company_office_culture", $id);
				if ($graduate_review_how_would_you_rate_the_company_office_culture == 'Poor'):
					$company_office_culture = '1';
				elseif ($graduate_review_how_would_you_rate_the_company_office_culture == 'Fair'):
					$company_office_culture = '2';
				elseif ($graduate_review_how_would_you_rate_the_company_office_culture == 'Good'):
					$company_office_culture = '3';
				elseif ($graduate_review_how_would_you_rate_the_company_office_culture == 'Very Good'):
					$company_office_culture = '4';
				else:
					$company_office_culture = '5';
				endif;

				$graduate_review_how_would_you_rate_the_ethical_environmental_awareness_of_your_company = get_field("graduate_review_how_would_you_rate_the_ethical_environmental_awareness_of_your_company", $id);
				if ($graduate_review_how_would_you_rate_the_ethical_environmental_awareness_of_your_company == 'Poor'):
					$environmental_awareness = '1';
				elseif ($graduate_review_how_would_you_rate_the_ethical_environmental_awareness_of_your_company == 'Fair'):
					$environmental_awareness = '2';
				elseif ($graduate_review_how_would_you_rate_the_ethical_environmental_awareness_of_your_company == 'Good'):
					$environmental_awareness = '3';
				elseif ($graduate_review_how_would_you_rate_the_ethical_environmental_awareness_of_your_company == 'Very Good'):
					$environmental_awareness = '4';
				else:
					$environmental_awareness = '5';
				endif;

				$graduate_review_how_would_you_rate_the_level_of_responsibility_you_have = get_field("graduate_review_how_would_you_rate_the_level_of_responsibility_you_have", $id);
				if ($graduate_review_how_would_you_rate_the_level_of_responsibility_you_have == 'Poor'):
					$responsibility = '1';
				elseif ($graduate_review_how_would_you_rate_the_level_of_responsibility_you_have == 'Fair'):
					$responsibility = '2';
				elseif ($graduate_review_how_would_you_rate_the_level_of_responsibility_you_have == 'Good'):
					$responsibility = '3';
				elseif ($graduate_review_how_would_you_rate_the_level_of_responsibility_you_have == 'Very Good'):
					$responsibility = '4';
				else:
					$responsibility = '5';
				endif;

				$graduate_review_how_would_you_rate_the_usefulness_of_your_training = get_field("graduate_review_how_would_you_rate_the_usefulness_of_your_training", $id);
				if ($graduate_review_how_would_you_rate_the_usefulness_of_your_training == 'Poor'):
					$training = '1';
				elseif ($graduate_review_how_would_you_rate_the_usefulness_of_your_training == 'Fair'):
					$training = '2';
				elseif ($graduate_review_how_would_you_rate_the_usefulness_of_your_training == 'Good'):
					$training = '3';
				elseif ($graduate_review_how_would_you_rate_the_usefulness_of_your_training == 'Very Good'):
					$training = '4';
				else:
					$training = '5';
				endif;

				$graduate_review_how_would_you_rate_the_enjoyment_of_the_training = get_field("graduate_review_how_would_you_rate_the_enjoyment_of_the_training", $id);
				if ($graduate_review_how_would_you_rate_the_enjoyment_of_the_training == 'Poor'):
					$enjoyment = '1';
				elseif ($graduate_review_how_would_you_rate_the_enjoyment_of_the_training == 'Fair'):
					$enjoyment = '2';
				elseif ($graduate_review_how_would_you_rate_the_enjoyment_of_the_training == 'Good'):
					$enjoyment = '3';
				elseif ($graduate_review_how_would_you_rate_the_enjoyment_of_the_training == 'Very Good'):
					$enjoyment = '4';
				else:
					$enjoyment = '5';
				endif;

				$graduate_review_how_would_you_rate_your_worklife_balance = get_field("graduate_review_how_would_you_rate_your_worklife_balance", $id);
				if ($graduate_review_how_would_you_rate_your_worklife_balance == 'Poor'):
					$worklife_balance = '1';
				elseif ($graduate_review_how_would_you_rate_your_worklife_balance == 'Fair'):
					$worklife_balance = '2';
				elseif ($graduate_review_how_would_you_rate_your_worklife_balance == 'Good'):
					$worklife_balance = '3';
				elseif ($graduate_review_how_would_you_rate_your_worklife_balance == 'Very Good'):
					$worklife_balance = '4';
				else:
					$worklife_balance = '5';
				endif;

				$graduate_review_how_would_you_rate_the_benefits_package = get_field("graduate_review_how_would_you_rate_the_benefits_package", $id);
				if ($graduate_review_how_would_you_rate_the_benefits_package == 'Poor'):
					$benefits_package = '1';
				elseif ($graduate_review_how_would_you_rate_the_benefits_package == 'Fair'):
					$benefits_package = '2';
				elseif ($graduate_review_how_would_you_rate_the_benefits_package == 'Good'):
					$benefits_package = '3';
				elseif ($graduate_review_how_would_you_rate_the_benefits_package == 'Very Good'):
					$benefits_package = '4';
				else:
					$benefits_package = '5';
				endif;
	
				$data[] = array(
"Review ID" => $id,
"Title" =>  $title,				
"Email" => get_field("graduate_review_email", $id),
"Gender" => get_field("graduate_review_gender", $id),
"Ethnicity" => get_field("graduate_review_ethnicity", $id),
"What is your highest level of qualification?" => get_field("graduate_review_what_is_your_highest_level_of_qualification", $id),
"More details (optional)" => get_field("graduate_review_more_details_optional", $id),
"University Attended" => get_field("graduate_review_university_attended", $id),
"Degree Subject" => get_field("graduate_review_degree_subject", $id),
"Degree Level" => get_field("graduate_review_degree_level", $id),
"Degree Grade" => get_field("graduate_review_degree_grade", $id),
"Employer" => get_field("graduate_review_employer", $id),
"Employer Other" => get_field("graduate_review_employer_other", $id),
"Industry" => get_field("graduate_review_industry", $id),
"Department" => get_field("graduate_review_department", $id),
"Job Title" => get_field("graduate_review_job_title", $id),
"How long have you been at the company?" => get_field("graduate_review_how_long_have_you_been_at_the_company", $id),
"Which of the following are most important to you?" => get_field("graduate_review_which_of_the_following_are_most_important_to_you", $id),
"What most attracted you to this company?" => get_field("graduate_review_what_most_attracted_you_to_this_company", $id),
"What most attracted you to this company? Other" => get_field("graduate_review_what_most_attracted_you_to_this_company_other", $id),
"What most attracted you to the role?" => get_field("graduate_review_what_most_attracted_you_to_the_role", $id),
"Application Advice" => get_field("graduate_review_application_advice", $id),
"Interview Advice" => get_field("graduate_review_interview_advice", $id),
"What are the key skills that you use in your day-to-day work life?" => get_field("graduate_review_what_are_the_key_skills_that_you_use_in_your_day-to-day_work_life", $id),
"Best" => get_field("graduate_review_best", $id),
"Worst" => get_field("graduate_review_worst", $id),
"How long do you intend to stay at this company?" => get_field("graduate_review_how_long_do_you_intend_to_stay_at_this_company", $id),
"What do you think would be the reason for leaving?" => get_field("graduate_review_what_do_you_think_would_be_the_reason_for_leaving", $id),
"What are the Best and Worst things about your company? Best" => get_field("graduate_review_what_are_the_best_and_worst_things_about_your_company_best", $id),
"What are the Best and Worst things about your company? Worst" => get_field("graduate_review_what_are_the_best_and_worst_things_about_your_company_worst", $id),
"Manage people" => get_field("graduate_review_manage_people", $id),
"50% pay rise relative to starting salary" => get_field("graduate_review_50%_pay_rise_relative_to_starting_salary", $id),
"100% pay rise relative to starting salary" => get_field("graduate_review_100%_pay_rise_relative_to_starting_salary", $id),
"International travel" => get_field("graduate_review_international_travel", $id),
"International relocation" => get_field("graduate_review_international_relocation", $id),
"Ownership of specific project/business area" => get_field("graduate_review_ownership_of_specific_projectbusiness_area", $id),
"First promotion" => get_field("graduate_review_first_promotion", $id),
"Second promotion" => get_field("graduate_review_second_promotion", $id),
"Control over your work hours" => get_field("graduate_review_control_over_your_work_hours", $id),
"Professional Qualification" => get_field("graduate_review_professional_qualification", $id),
"Do you hope to progress within the company you are currently work for?" => get_field("graduate_review_do_you_hope_to_progress_within_the_company_you_are_currently_work_for", $id),
"How would you rate the career progression at the company you work for?" => $career_progression,				
"Are career paths within your company clear to you?" => get_field("graduate_review_are_career_paths_within_your_company_clear_to_you", $id),
"Have you already progressed in any way?" => get_field("graduate_review_have_you_already_progressed_in_any_way", $id),
"Which of the following exist at your company?1" => get_field("graduate_review_which_of_the_following_exist_at_your_company_1", $id),
"Can you think of anything to improve the Career Progression (not including promoting you to CEO!)" => get_field("graduate_review_can_you_think_of_anything_to_improve_the_career_progression_not_including_promoting_you_to_ceo", $id),
"How would you rate the colleagues in the department you work in?" => $colleagues,
				
"Is there diversity within the team you work in?" => get_field("graduate_review_is_there_diversity_within_the_team_you_work_in", $id),
				
"Are there clearly defined processes for dealing with difficult colleagues?" => get_field("graduate_review_are_there_clearly_defined_processes_for_dealing_with_difficult_colleagues", $id),
				
"Did you start at the same time as any other graduates?" => get_field("graduate_review_did_you_start_at_the_same_time_as_any_other_graduates", $id),
				
"Which of the following exist at your company?2" => get_field("graduate_review_which_of_the_following_exist_at_your_company_2", $id),
				
"Can you think of anything to improve the Colleagues (not including hiring all of your friends!)" => get_field("graduate_review_can_you_think_of_anything_to_improve_the_colleagues_not_including_hiring_all_of_your_friends", $id),
				
"How would you rate the company & office culture?" => $company_office_culture,
				
"Are you familiar with the company values?" => get_field("graduate_review_are_you_familiar_with_the_company_values", $id),
"Are you familiar with the company values? Please specify" => get_field("graduate_review_are_you_familiar_with_the_company_values_please_specify", $id),
"Is there a welcoming environment?" => get_field("graduate_review_is_there_a_welcoming_environment", $id),
"Do you know your departments’ mission statements?" => get_field("graduate_review_do_you_know_your_departments_mission_statements", $id),
"Do you know your departments’ mission statements? Please specify" => get_field("graduate_review_do_you_know_your_departments_mission_statements_please_specify", $id),
"Which of the following statements are true of your company and office culture?" => get_field("graduate_review_which_of_the_following_statements_are_true_of_your_company_and_office_culture", $id),
"Can you think of anything to improve the Company Culture (not including onesie Wednesdays!)" => get_field("graduate_review_can_you_think_of_anything_to_improve_the_company_culture_not_including_onesie_wednesdays", $id),
"When you first joined the company how much did you enjoy your role?" => get_field("graduate_review_when_you_first_joined_the_company_how_much_did_you_enjoy_your_role", $id),
"Now, in general, how much do you enjoy your day-to-day work?" => get_field("graduate_review_now_in_general_how_much_do_you_enjoy_your_day-to-day_work", $id),
"Would you enjoy another role in the company more than what you currently do?" => get_field("graduate_review_would_you_enjoy_another_role_in_the_company_more_than_what_you_currently_do", $id),
"Would you enjoy another role in the company more than what you currently do? Please specify" => get_field("graduate_review_would_you_enjoy_another_role_in_the_company_more_than_what_you_currently_do_please_specify", $id),
"In general, how much does the team you work in seem to enjoy their work?" => get_field("graduate_review_in_general_how_much_does_the_team_you_work_in_seem_to_enjoy_their_work", $id),
"Which of the following contribute to enjoyment of your work?" => get_field("graduate_review_which_of_the_following_contribute_to_enjoyment_of_your_work", $id),
"Can you think of anything to improve the Enjoyment of your work (not including bouncy castle breaks!)" => get_field("graduate_review_can_you_think_of_anything_to_improve_the_enjoyment_of_your_work_not_including_bouncy_castle_breaks", $id),
"Do you think your company is ethically & environmentally aware?" => get_field("graduate_review_do_you_think_your_company_is_ethically_environmentally_aware", $id),
"How would you rate the Ethical & Environmental Awareness of your company?" => $environmental_awareness,
"Have you been directly involved in any Ethical and Environmental activities at your company?" => get_field("graduate_review_have_you_been_directly_involved_in_any_ethical_and_environmental_activities_at_your_company", $id),
"Is there an individual or department, dedicated to Ethical and Environmental issues?" => get_field("graduate_review_is_there_an_individual_or_department_dedicated_to_ethical_and_environmental_issues", $id),
"Which of the following exist at your company?3" => get_field("graduate_review_which_of_the_following_exist_at_your_company_3", $id),
"Can you think of anything to improve the Ethical and Environmental awareness?" => get_field("graduate_review_can_you_think_of_anything_to_improve_the_ethical_and_environmental_awareness", $id),
"What is your favourite responsibility?" => get_field("graduate_review_what_is_your_favourite_responsibility", $id),
"What is your favourite responsibility? Why" => get_field("graduate_review_what_is_your_favourite_responsibility_why", $id),
"What responsibility do you enjoy less?" => get_field("graduate_review_what_responsibility_do_you_enjoy_less", $id),
"What responsibility do you enjoy less? Why" => get_field("graduate_review_what_responsibility_do_you_enjoy_less_why", $id),
"Would you like more responsibility?" => get_field("graduate_review_would_you_like_more_responsibility", $id),
"How would you rate the level of responsibility you have?" => $responsibility,
"Which of the following are true statements about the Responsibilities you have in your role?" => get_field("graduate_review_which_of_the_following_are_true_statements_about_the_responsibilities_you_have_in_your_role", $id),
"Can you think of anything to improve your Responsibilities (not including removing all of them!)" => get_field("graduate_review_can_you_think_of_anything_to_improve_your_responsibilities_not_including_removing_all_of_them", $id),
"Did you receive training when you first started?" => get_field("graduate_review_did_you_receive_training_when_you_first_started", $id),
"How would you rate the usefulness of your training?" => $training,
"Have you had other training opportunities?" => get_field("graduate_review_have_you_had_other_training_opportunities", $id),
"How would you rate the enjoyment of the training?" => $enjoyment,
"Which of the following statements are true of the training at the company you work for?" => get_field("graduate_review_which_of_the_following_statements_are_true_of_the_training_at_the_company_you_work_for", $id),
"Can you think of anything to improve the Training (not including getting training on how to catch dragons!)" => get_field("graduate_review_can_you_think_of_anything_to_improve_the_training_not_including_getting_training_on_how_to_catch_dragons", $id),
"Do you feel confident to ask for support, if you need it?" => get_field("graduate_review_do_you_feel_confident_to_ask_for_support_if_you_need_it", $id),
"How would you rate the support you receive?" => get_field("graduate_review_how_would_you_rate_the_support_you_receive", $id),
"Are you responsible for supporting anybody?" => get_field("graduate_review_are_you_responsible_for_supporting_anybody", $id),
"Have you required support in your role?" => get_field("graduate_review_have_you_required_support_in_your_role", $id),
"Which of the following statements are true of the support at the company you work for?" => get_field("graduate_review_which_of_the_following_statements_are_true_of_the_support_at_the_company_you_work_for", $id),
"Can you think of anything to improve the Support (not including getting someone else to do the work for you!)" => get_field("graduate_review_can_you_think_of_anything_to_improve_the_support_not_including_getting_someone_else_to_do_the_work_for_you", $id),
"What time do you normally start and finish work? Start time" => get_field("graduate_review_what_time_do_you_normally_start_and_finish_work_start_time", $id),
"What time do you normally start and finish work? Finish time" => get_field("graduate_review_what_time_do_you_normally_start_and_finish_work_finish_time", $id),
"How would you rate your work/life balance?" => $worklife_balance,
"Which of the following exist at the company you work for?" => get_field("graduate_review_which_of_the_following_exist_at_the_company_you_work_for", $id),
"Do you have to take your work home with you?" => get_field("graduate_review_do_you_have_to_take_your_work_home_with_you", $id),
"Do your colleagues take an interest in your personal matters?" => get_field("graduate_review_do_your_colleagues_take_an_interest_in_your_personal_matters", $id),
"Can you think of anything to improve the Work/Life Balance (not including 5 day weekends!)" => get_field("graduate_review_can_you_think_of_anything_to_improve_the_worklife_balance_not_including_5_day_weekends", $id),
"Did you receive a sign on bonus?" => get_field("graduate_review_did_you_receive_a_sign_on_bonus", $id),
"Do you receive performance based bonuses?" => get_field("graduate_review_do_you_receive_performance_based_bonuses", $id),
"Does your office building have perks, (such as gym facilities, relaxation areas etc)?" => get_field("graduate_review_does_your_office_building_have_perks_such_as_gym_facilities_relaxation_areas_etc", $id),
"Does the company you work for, subscribe to any consumer benefits programme (internally /externally)?" => get_field("graduate_review_does_the_company_you_work_for_subscribe_to_any_consumer_benefits_programme_internally_externally", $id),
"How would you rate the benefits package?" => $benefits_package,
"Can you think of anything to improve the Benefits (not including introducing free drinks every day at 5pm!)" => get_field("graduate_review_can_you_think_of_anything_to_improve_the_benefits_not_including_introducing_free_drinks_every_day_at_5pm", $id),
"Is your role predominantly office based?" => get_field("graduate_review_is_your_role_predominantly_office_based", $id),
"How satisfied are you with the working location?" => get_field("graduate_review_how_satisfied_are_you_with_the_working_location", $id),
"Does the company you work for have other offices in the UK?" => get_field("graduate_review_does_the_company_you_work_for_have_other_offices_in_the_uk", $id),
"Does the company you work for have other offices abroad?" => get_field("graduate_review_does_the_company_you_work_for_have_other_offices_abroad", $id),
"Can you think of anything to improve the Location (not including moving the offices to your back garden!)" => get_field("graduate_review_can_you_think_of_anything_to_improve_the_location_not_including_moving_the_offices_to_your_back_garden", $id),
"Would you like to be offered international opportunities?" => get_field("graduate_review_would_you_like_to_be_offered_international_opportunities", $id),
"Do you speak any other languages?" => get_field("graduate_review_do_you_speak_any_other_languages", $id),
"How satisfied are you with the international opportunities available to you?" => get_field("graduate_review_how_satisfied_are_you_with_the_international_opportunities_available_to_you", $id),
"Would you be happy to permanently relocate abroad?" => get_field("graduate_review_would_you_be_happy_to_permanently_relocate_abroad", $id),
"Can you think of anything to improve the International Opportunities (not including offering free holidays to everyone!)" => get_field("graduate_review_can_you_think_of_anything_to_improve_the_international_opportunities_not_including_offering_free_holidays_to_everyone", $id),
"How would you rate the senior management of the company?" => get_field("graduate_review_how_would_you_rate_the_senior_management_of_the_company", $id),
"How would you rate the management in your department?" => get_field("graduate_review_how_would_you_rate_the_management_in_your_department", $id),
"How often do you have 1-2-1’s with your management?" => get_field("graduate_review_how_often_do_you_have_1_2_1s_with_your_management", $id),
"Do you feel comfortable to talk to your management about issues you have within your role?" => get_field("graduate_review_do_you_feel_comfortable_to_talk_to_your_management_about_issues_you_have_within_your_role", $id),
"Do you feel that your management care about your day-to-day activities?" => get_field("graduate_review_do_you_feel_that_your_management_care_about_your_day_to_day_activities", $id),
"Can you think of anything to improve the Management (not including firing any of them!)" => get_field("graduate_review_can_you_think_of_anything_to_improve_the_management_not_including_firing_any_of_them", $id),
"STARTING Salary" => get_field("graduate_review_starting_salary", $id),
"CURRENT Salary" => get_field("graduate_review_current_salary", $id),
"How satisfied are you with your current salary level?" => get_field("graduate_review_how_satisfied_are_you_with_your_current_salary_level", $id),
"Has this company ever offered you a pay rise?" => get_field("graduate_review_has_this_company_ever_offered_you_a_pay_rise", $id),
"How much do you think you are worth?" => get_field("graduate_review_how_much_do_you_think_you_are_worth", $id),
"Can you think of anything to improve the Salary (not including increasing them!)" => get_field("graduate_review_can_you_think_of_anything_to_improve_the_salary_not_including_increasing_them", $id),
"All in all, what one thing could be done to improve the company and job?" => get_field("graduate_review_all_in_all_what_one_thing_could_be_done_to_improve_the_company_and_job", $id),
"Company ID" => get_field("graduate_company_id", $id)	

				);
	
			endwhile;
		endif;
	
		$filename = $x.'_report_'.date("d-m-Y_H-i",time()).'.csv';
		
		#create the CSV into a temp directory
		$output[$x] = fopen(__DIR__ . '/csv/'.$filename , "w");
		foreach ($data as $row) {
			fputcsv($output[$x], $row);
		}
		fclose($output[$x]);	
	
		#reset 
		wp_reset_postdata();	

	

	echo '<hr/';

	sleep(10); # this should halt for 10 seconds for every loop	
	
} #end for loop

# Get real path for our folder
$rootPath = realpath('csv');

# Initialize archive object
$zipname = 'graduate_report_'.date("d-m-Y_H-i",time()).'.zip';
$zip = new ZipArchive();
$zip->open($zipname, ZipArchive::CREATE | ZipArchive::OVERWRITE);

# Initialize empty "delete list"
$filesToDelete = array();

# Create recursive directory iterator
/** @var SplFileInfo[] $files */
$files = new RecursiveIteratorIterator(
    new RecursiveDirectoryIterator($rootPath),
    RecursiveIteratorIterator::LEAVES_ONLY
);

foreach ($files as $name => $file){
	
    # Skip directories (they would be added automatically)
    if (!$file->isDir()){
		
        # Get real and relative path for current file
        $filePath = $file->getRealPath();
        $relativePath = substr($filePath, strlen($rootPath) + 1);

        # Add current file to archive
        $zip->addFile($filePath, $relativePath);
		
     	# Add current file to "delete list"
		$filesToDelete[] = $filePath;        		

    }
}

# Zip archive will be created only after closing object
$zip->close();

header('Content-Type: application/zip');
header('Content-disposition: attachment; filename='.$zipname);
header('Content-Length: ' . filesize($zipname));
#add to correct issue	
ob_clean();
flush();
readfile("$zipname");

print_r($filesToDelete);

# Delete all files from "delete list"
foreach ($filesToDelete as $file){
	unlink($file);
}
# Now remove the ZIP file
unlink($zipname);
?>