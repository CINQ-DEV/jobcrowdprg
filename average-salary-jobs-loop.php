 <?php
# Our include
require_once('../../../wp-load.php');

$company_id = $_POST['cid'];
$career = $_POST['car'];

$find = ["+", "%26"];
$replace   = [" ", "&"];
$clean_career = str_replace($find, $replace, $career);	

#echo '<h3>'.$company_id.'</h3>';
#echo '<h3>'.$clean_career.'</h3>';

$all_meta_for_user = get_user_meta( $company_id );
$company_name = $all_meta_for_user['company_name'][0];
if ($company_name):
	#echo '<h1>'.$company_name.'</h1>';
endif;	

query_posts(array(
	'post_type'		=> 'jobs',	
	'post_status'	=> 'publish',
	'posts_per_page'=> '-1',
	
	'meta_query'	=> array(
		'relation'		=> 'AND',
		array(
			'key'	  	=> 'dynamic_filled_company',
			'value'	  	=> $company_name,
			'compare' 	=> '=',
		),
		array(
			'key'	  	=> 'dynamic_filled_careers',
			'value'	  	=> $clean_career,
			'compare' 	=> 'LIKE',
		),
	),	
));

# our loop
if (have_posts()) :
	
	while (have_posts()):
	the_post();	
	?>
	
	<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?> &raquo;</a><br/>
	
	<?php
	endwhile;

else:

	echo '<p>Unfortunately, no jobs matched your criteria. Please try again.</p>';

endif;
wp_reset_query();		
?>