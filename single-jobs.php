<?php get_header(); ?>
	
<div class="page-header row">

	<div class="medium-5 columns">
		<h2><a href="<?php bloginfo('home'); ?>/jobs">Jobs</a> - </h2> <h1><?php the_title(); ?></h1>
	</div>
	

	<?php get_template_part('template-parts/page-header-search'); ?>

</div><!-- page-header -->


<div class="main-content" data-equalizer="main-content">

<?php  

if ( have_posts() ) : ?>
    
	<?php while ( have_posts() ) : the_post(); 

	$selected = '';
	if(isset($_SESSION['shortlist'])) {
		if ( in_array($id, $_SESSION['shortlist']) ) {
			$selected = 'selected';
		}
	}

?>

	<div id="<?php the_ID(); ?>" class="main-content-main item <?php echo $selected; ?>" data-equalizer-watch="main-content">

				<div class="main-content-main--breadcrumbs" data-equalizer-watch="main-content-headers">
							<?php
							if ( function_exists('yoast_breadcrumb') ) {
							yoast_breadcrumb('
							<p id="breadcrumbs">','</p>
							');
							}
							?>
							
					<div class="search-results-block--savethis save-share">
						<a href="#" class="savethis btn action add" data-action="add">
							<svg class="icon icon-heart"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-heart"></use></svg>
							<svg class="icon icon-heart icon-heart-hidden"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-heart"></use></svg>
							<span class="save-message">Save this</span>
						</a>

						<a href="#" class="btn action remove" data-action="remove">
				        	<i class="fa fa-minus-circle" aria-hidden="true"></i> Remove from shortlist
				        </a>

						<?php get_template_part('template-parts/share-button'); ?>

					</div>

				</div><!-- main-content-main-breadcrumbs -->


		<?php
		global $current_user;
		get_currentuserinfo();
		
		if( isset($_GET['action']) && $_GET['action'] == 'edit' ):
		
			if (is_user_logged_in() && $current_user->ID == $post->post_author) :
			
				#only allow certain fields to be amended, otherwise we can use acf_form();
				
				$options = array(
					'fields' => array(
						#Group 96 :: Job Listing 
						'field_595b5f02d4a72',	#deadline
						'field_59675877b636a',	#deadline other info
						'field_595b5f27d4a75',	#qualifications
						'field_595b5f40d4a76',	#application
						'field_5964b890198cd',	#expiry date
						'field_5966469298f23',	#salary
						'field_5967545659cfe',	#additional salary
													
						#Group 206 :: Job Alert Search Criteria
						#'field_5964f3ad5648a',	#salary - min
						#'field_5964f3ad5650a',	#salary - max
						'field_5964f3ad5658a',	#industry
						'field_5964f3ad5660a',	#type
						'field_596883d81cc60',	#town
						'field_596755eff20e1',	#county
						'field_5964f3ad5668a',	#region
						'field_5a719e9c80f50',  #additional location
						'field_5967933a60835',	#careers	
					
						#Group 96 :: Job Listing 
						'field_5a5e23f440353',	#display application form
						'field_5a5e28d043f0c',	#message
						'field_5a5e241940354',	#email address for form application
						'field_5a719f54ac3e7'	#apply now URL					
					
					)
				);
				acf_form($options);				
						
			else:
			
				echo "<p>You're not the author of this job and therefore not able to make alterations, if you believe you're seeing this message in error, please contact the administrator.</p>";
			
			endif;	
			
		else:
			
			#show the content
			#get the variables
			$job_description			= get_field('job_description');
			$deadline_date				= get_field('deadline_date');
			$deadline_other_info		= get_field('deadline_other_info');
			$salary						= get_field('salary');
			$additional_salary			= get_field('additional_salary');
			$qualifications_required	= get_field('qualifications_required');
			$application_process		= get_field('application_process');
			
			$dynamic_filled_company		= get_field('dynamic_filled_company');
			$dynamic_filled_careers		= get_field('dynamic_filled_careers');
			$dynamic_filled_salary_min	= get_field('dynamic_filled_salary_min');
			$dynamic_filled_salary_max	= get_field('dynamic_filled_salary_max');
			$dynamic_filled_industry	= get_field('dynamic_filled_industry');
			$dynamic_filled_type		= get_field('dynamic_filled_type');
			$dynamic_filled_town		= get_field('dynamic_filled_town');
			$dynamic_filled_county		= get_field('dynamic_filled_county');
			$dynamic_filled_region		= get_field('dynamic_filled_region');
			$additional_locations		= get_field('additional_locations');
			?>
            
	<div class="row padded">
	<?php //the_field('job_description'); ?>


		<div class="medium-8 columns">

			<?php echo $what_makes_you_a_great_company_to_work_for;	?>

			<?php if ($job_description): ?>
				<?php echo $job_description; ?>
			<?php endif; ?> 

			<?php if ($qualifications_required): ?>
				<p><strong>Qualifications Required</strong></p>
				<?php echo $qualifications_required; ?>
			<?php endif; ?> 

			<?php if ($application_process): ?>
				<p><strong>Application Process</strong></p>
				<?php echo $application_process; ?>
			<?php endif; ?>          

			<!-- Nav tabs -->
			<div class="tabs-container">
			<ul class="tabs" data-tabs data-deep-link="true" data-deep-link-smudge="true" data-deep-link-smudge="500" id="job-tabs" data-equalizer-watch="main-content-headers">
				<?php if ($dynamic_filled_careers): ?>
				<li class="tabs-title is-active"><a href="#career" aria-controls="career" role="tab">Career</a></li>
				<?php endif; ?>	
				<?php if ($dynamic_filled_industry): ?>
				<li class="tabs-title"><a href="#industry" aria-controls="industry" role="tab">Industry</a></li>
				<?php endif; ?>	
				<?php if ($dynamic_filled_region && implode(', ', $dynamic_filled_region) != ''): ?>
				<li class="tabs-title"><a href="#region" aria-controls="region" role="tab">Region</a></li>
				<?php endif; ?>	
				<?php if ($additional_locations): ?>
				<li class="tabs-title"><a href="#additional" aria-controls="additional" role="tab">Additional Locations</a></li>
				<?php endif; ?>			
			</ul>
			</div>

			<!-- Tab panes -->
			<div class="tabs-content two-column-list" data-tabs-content="job-tabs">

				<?php if ($dynamic_filled_careers): ?>
				<div class="tabs-panel is-active" id="career">
				<span class=" bolder"><?php #echo implode(',<br/>', $dynamic_filled_careers); ?>
				<?php
				echo '<ul id="double">';
				foreach ($dynamic_filled_careers AS $career):
					echo '<li>'.$career.'</li>';
				endforeach;
				echo '</ul>';
				?>
				</span>
				</div><!-- career tabs panel -->
				<?php endif; ?>			

				<?php if ($dynamic_filled_industry): ?>			
				<div class="tabs-panel" id="industry">
				<ul id="double">
				<?php foreach($dynamic_filled_industry as $industry): ?>
					<li><a href="<?php bloginfo('url'); ?>/industries/?industry=<?php echo $industry; ?>"><?php echo $industry; ?></a></li>
				<?php endforeach; ?>
				</ul>
				</div><!-- industry tabs panel -->	
				<?php endif; ?>				

				<?php if ($dynamic_filled_region && implode(', ', $dynamic_filled_region) != ''): ?>			
				<div class="tabs-panel" id="region">
				<span class="bolder"><?php #echo implode(',<br/>', $dynamic_filled_region); ?>
				<?php
				echo '<ul id="double">';
				foreach ($dynamic_filled_region AS $region):
					echo '<li>'.$region.'</li>';
				endforeach;
				echo '</ul>';
				?>
				</span>		
				</div><!-- region tabs panel -->	
				<?php endif; ?>	

				<?php if ($additional_locations): ?>			
				<div class="tabs-panel" id="additional">

					<ul id="double">
					<?php 
					$additional_locations = explode(',', $additional_locations);
					foreach($additional_locations as $additional): ?>
						<li><?php echo $additional; ?></li>
					<?php endforeach; ?>
					</ul>
					
				</div><!-- additional tabs panel -->	
				<?php endif; ?>	

			</div><!-- /tab panes -->                                                        

		</div><!-- medium-8 columns -->

		<div class="medium-4 columns">

			<?php $apply_now_url = get_field('apply_now_url');
			if($apply_now_url): ?>
				<div class="column-block">
					<a href="<?php echo $apply_now_url; ?>" class="button block blue larger-text" id="button--apply-for-this-job" target="_blank">Click here to apply for this job</a>
				</div>		
			<?php endif; ?>
			
			<?php
			#echo $author_id; 

			#get the main_address
			$what_makes_you_a_great_company_to_work_for = get_field('what_makes_you_a_great_company_to_work_for', 'user_'.$author_id);
			#echo $what_makes_you_a_great_company_to_work_for;
			
			$would_you_like_to_include_our_job_application_form = get_field('would_you_like_to_include_our_job_application_form');
			if($would_you_like_to_include_our_job_application_form): ?>
			<div class="column-block">
				<a href="#" class="button block blue larger-text" id="button--apply-for-this-job">Click here to apply for this job</a>
			</div>
			<?php endif; ?>							

			<?php
			$author_id=$post->post_author; 
			#get the main_address
			$what_makes_you_a_great_company_to_work_for = get_field('what_makes_you_a_great_company_to_work_for', 'user_'.$author_id);
			$company_name = get_field('dynamic_filled_company');
			#echo $company_name;
			//echo $id;

			$args = array(
				'posts_per_page'=> 2,
				'post_type'=> 'graduate_reviews',
				'meta_key'=> 'graduate_review_employer',
				'meta_value'=> $company_name,
			);

			// query
			$the_query = new WP_Query( $args );

			if( $the_query->have_posts() ): ?>

				<div class="column-block">
					<span><strong>What do people say about working for <?php echo $company_name; ?>?</strong></span>
				</div>		

				<div class="background-grey inset-reviews">

				<?php while( $the_query->have_posts() ) : $the_query->the_post(); 
					$author_id=$post->post_author; 
					$overall_rating = get_field('overall_rating', 'user_'.$author_id);
					$overall_rating_new = get_field('star_rating');

					$overall_rating_percent = $overall_rating_new * 20;
					?>

					<div class="inset-review row">

						<a href="<?php the_permalink(); ?>">
							<div class="column">
								<span><strong><?php the_title(); ?></strong></span>
							</div>
						</a>

						<div class="small-6 columns">

							<div class="stars">
								<div class="stars-gold" style="width: <?php echo $overall_rating_percent; ?>%;"> &nbsp; </div>
								<div class="stars-white"> &nbsp; </div>
							</div>

						</div>

						<div class="small-6 columns date-posted">

							<span>Posted <?php the_date('d-m-Y'); ?></span>

						</div>

						<div class="column button-centered">

						<p><?php the_excerpt(); ?></p>

							<a href="<?php the_permalink(); ?>" class="button gold">Find out more</a>

						</div>

					</div> <!-- inset-review-->

				<?php endwhile; wp_reset_query(); ?>

				</div><!-- background-grey inset-reviews-->


			<?php else: // there are no reviews ?>


			<?php endif; // end are there reviews check ?>		
			
		</div><!-- /medium-4 columns -->

	</div><!-- row-->


	<div class="main-content--grey-footer padded" id="individual-job--application-form">
	<?php if( get_field('would_you_like_to_include_our_job_application_form') ): ?>
	
		<div class="padded-half">
			<h3>Apply for this job</h3>
		</div>

		<?php 	
		$email_address = get_field('email_address');

		echo do_shortcode('[gravityform id=10 title=false description=false ajax=false tabindex=99 field_values="company_email_address='.$email_address.'"]');

	endif; ?>			
	</div><!-- main-content-grey-footer -->

	</div><!--main-content-main -->


	<div class="sidebar-left" data-equalizer-watch="main-content">


		<div class="sidebar--header" data-equalizer-watch="main-content-headers">
			<h3>Job Details</h3> <svg class="icon icon-details"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-details"></use></svg>
		</div>

		<?php
		$author_id=$post->post_author; 
		$logo = get_field('logo', 'user_'.$id);
		#$company_name = get_field('company_name', 'user_'.$author_id);
		$hyphenate_company_name = str_replace(" ", "-", $company_name); 
		?>

		<?php if($logo): ?>
			<?php if($hyphenate_company_name && $user->ID): ?>
				<a href="<?php echo esc_url( home_url( '/' ) ).'companies/details/?company='.$hyphenate_company_name.'&cid='.$user->ID; ?>">
			<?php endif; ?>
		<div class="sidebar-left--company-logo">
			<img src="<?php echo $logo['sizes']['medium']; ?>" alt="logo"> 
		</div>
			<?php if($hyphenate_company_name && $user->ID): ?>
				</a>
			<?php endif; ?>

		<?php endif; ?>


		<div class="sidebar-left--company-details row">

			<?php 
			$employer = get_field('dynamic_filled_company', $post_id);


			$blogusers = get_users(array('meta_key' => 'company_name', 'meta_value' => $employer));
			foreach ( $blogusers as $user ) {
				#access the user meta

			}	
			?>

			<?php if ($company_name): ?>
				<div class="small-6 columns">Company</div>
				<div class="small-6 columns bolder">
					<?php if($hyphenate_company_name && $user->ID): ?>
						<a href="<?php echo esc_url( home_url( '/' ) ).'companies/details/?company='.$hyphenate_company_name.'&cid='.$user->ID; ?>">
					<?php endif; ?>
					<?php echo $company_name; ?>
					<?php if($hyphenate_company_name && $user->ID): ?>
						</a>
					<?php endif; ?>
				</div>
			<?php endif; ?>  

			<?php if ($dynamic_filled_type): ?>
			<div class="small-6 columns">Job Type</div>
			<div class="small-6 columns bolder"><?php echo $dynamic_filled_type; ?></div>
			<?php endif; ?>               

			<?php if ($deadline_date): ?>
			<div class="small-6 columns">Deadline Date</div>
			<div class="small-6 columns bolder"><?php echo $deadline_date; ?></div>
			<?php endif; ?>

			<?php if ($deadline_other_info): ?>
			<div class="small-6 columns">Deadline Date</div>
			<div class="small-6 columns bolder"><?php echo $deadline_other_info; ?></div>
			<?php endif; ?>  

			<?php if ($salary): ?>
			<div class="small-6 columns">Salary</div>
			<div class="small-6 columns bolder"><?php #echo number_format($salary,2); ?><?php echo $salary; ?></div>
			<?php endif; ?>   

			<?php if ($additional_salary): ?>
			<div class="small-6 columns">Additional Salary Details</div>
			<div class="small-6 columns bolder"><?php echo $additional_salary; ?></div>
			<?php endif; ?>    

			<?php if ($dynamic_filled_salary_min): ?>
			<div class="small-6 columns">Salary Min</div>
			<div class="small-6 columns bolder"><?php echo $dynamic_filled_salary_min; ?></div>
			<?php endif; ?>  

			<?php if ($dynamic_filled_salary_max): ?>
			<div class="small-6 columns">Salary Max</div>
			<div class="small-6 columns bolder"><?php echo $dynamic_filled_salary_max; ?></div>
			<?php endif; ?>    

			<?php if ($dynamic_filled_town): ?>
			<div class="small-6 columns">Location</div>
			<div class="small-6 columns bolder"><?php echo implode(', ', $dynamic_filled_town); ?></div>
			<?php endif; ?> 

			<?php if ($dynamic_filled_county): ?>
			<div class="small-6 columns">County</div>
			<div class="small-6 columns bolder"><?php echo implode(', ', $dynamic_filled_county); ?></div>
			<?php endif; ?>  

			<?php
			# include this to count how many times this job has been viewed
			setPostViews(get_the_ID());

			# display how many views this job has had
			#echo 'Views: '.getPostViews(get_the_ID());

			# display how many views this job has once over a minimum value
			$job_views = getPostViews(get_the_ID());
			if ($job_views >= 1000):
				echo '<div class="small-6 columns">Views</div><div class="small-6 columns bolder"> '.$job_views.'</div>';
			endif;


			endif; #endif 
			?>

		</div><!-- row -->

		<?php if( have_rows('gallery', 'user_'.$author_id)): ?>

			<div class="sidebar-left--company-media row">

				<?php while( have_rows('gallery', 'user_'.$author_id)): the_row(); 
					$image = get_sub_field('image');
				?>
					<?php if($image): ?>
						<img src="<?php echo $image['sizes']['medium']; ?>" alt="image" /><br/>
					<?php endif; ?>
				<?php endwhile; ?>

			</div><!-- sidebar-left-company-media -->

		<?php endif; ?>

		<div class="sidebar--header">
			<h3>Application Tips</h3> <svg class="icon icon-bulb"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-bulb"></use></svg>
		</div>

		<div class="sidebar-left--application-tips">

			<p>Employers constantly tell us that there is nothing that impresses them more than a graduate who has properly researched the company and really knows why they want to work there - so make sure that you tell them in your application that you have read-up all about them on TheJobCrowd and so you really know what makes that company great and why they are right for you. It will help your application, we promise!</p>

		</div> <!-- sidebar-left-key-information-->


	</div><!--sidebar-left -->


<?php endwhile; endif; ?>

<?php wp_reset_query(); ?>

	<div class="sidebar-right" data-equalizer-watch="main-content">

		<div class="sidebar--header" data-equalizer-watch="main-content-headers">
			<h3>More Jobs Like This</h3>
		</div>

		<?php if($dynamic_filled_industry): 

			foreach($dynamic_filled_industry as $industry): 

				echo '<div class="single-industry-sidebar--header"><h3>'.$industry.'</h3></div>'; ?>

				<?php 
				   $args = array(
				'posts_per_page'   => 4,
				'post_type'         => 'jobs',
				'meta_key'         => 'dynamic_filled_industry',
				'meta_value'      => $industry,
				'meta_compare'=> "LIKE"
				);


				$wp_query = new WP_Query($args);

				if ( $wp_query->have_posts() ) : 

					while ( $wp_query->have_posts() ) : $wp_query->the_post();

						# get the user ID by matching the reviewed company name with an actual company

						$post_id = get_the_ID();
						$employer = get_field('dynamic_filled_company', $post_id);

						$blogusers = get_users(array('meta_key' => 'company_name', 'meta_value' => $employer));
						foreach ( $blogusers as $user ) {
						#access the user meta
						#echo '<span>' . esc_html( $user->ID ) . '</span>'; #debug

						}	
						$author_id=$post->post_author; 
						#get the main_address
						$logo = get_field('logo', 'user_'.$user->ID);
						$company_name = get_field('company_name', 'user_'.$user->ID);
						$industry = get_field('dynamic_filled_industry');

						$overall_rating = get_field('overall_rating', 'user_'.$user->ID);
						//$overall_rating_new = get_field('star_rating');
						$overall_rating_percent = $overall_rating * 20;
						#echo $user->ID;
						?>

						<div class="saved-jobs">
							<div class="saved-jobs--job">
							<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><strong><?php the_title(); ?></strong></a><br/>

							<?php if($company_name): ?>Company: <strong><?php echo $company_name; ?></strong><br/><?php endif; ?>

							<?php if((implode(', ', $industry) != '- Select Industry -') && (implode(', ', $industry) != '')): ?>
								Industry: <strong><?php the_field('dynamic_filled_industry'); ?></strong><br/>
							<?php endif; ?>

							<?php $job_type = get_field('dynamic_filled_type'); 
							if ($job_type == '- Select Job Type -'):
								$job = '';
							else:
								$job = $job_type;
							endif;
							?>

							</div>
						</div>

					<?php endwhile; wp_reset_query();      

				endif; #endif $wp_query

			endforeach; #dynamic_filled_industry

		endif; ?>


	</div><!--sidebar-right -->



</div> <!-- main-content -->


<?php
get_footer();
