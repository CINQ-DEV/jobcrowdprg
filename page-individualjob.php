<?php
/*
Template Name: Individual Job
 */

 get_header(); ?>


<div class="page-header row">

	<div class="medium-5 columns">
		<h1>HR Graduate Scheme at Bakkavor Group</h1>
	</div>
	

	<?php get_template_part('template-parts/page-header-search'); ?>

</div><!-- page-header -->


<div class="main-content" data-equalizer="main-content">






	<div class="main-content-main" data-equalizer-watch="main-content">

				<div class="main-content-main--breadcrumbs" data-equalizer-watch="main-content-headers">
							<?php
							if ( function_exists('yoast_breadcrumb') ) {
							yoast_breadcrumb('
							<p id="breadcrumbs">','</p>
							');
							}
							?>
							
					<div class="save-share">

						<a href="#" class="savethis">
							<svg class="icon icon-heart"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-heart"></use></svg> Save this
						</a>

						<?php get_template_part('template-parts/share-button'); ?>

					</div>

				</div><!-- main-content-main-breadcrumbs -->

				<div class="row padded">
					
					<div class="medium-8 columns">

						<p><strong>We are offering 3 ‘real jobs’ within different business units, giving experience in different product areas and business types. The roles will cover the core skills including recruitment, employee relations, performance management, employment law, pay management, development and succession.</strong></p>

						<p>The HR AMS scheme provides the opportunity to gain practical work experience and relevant training including working towards the CIPD within 3 years.</p>  

						<p>The scheme will offer you the opportunity to undertake three placements in different business units providing an in depth understanding of HR within Bakkavör. This will provide you with the necessary skills and experience to allow you to reach a key HR management role within 5 years of joining.</p>

						<p>This scheme involves interaction with all areas of the business, including manufacturing, commercial, marketing, supply chain and product development. It is essential that you are business focussed and have strong interpersonal and influencing skills. The pace is fast and so excellent organisational skills are essential. As you will be involved in a large blue chip manufacturing business your impact will be visible and significant. It is a fantastic experience!</p>

						<p>Key skills &amp; experience required:<br/>
							• Lorem ipsum dolor sit amet, consectetur adipiscing<br/>
							• Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua<br/>
							• Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut<br/>
							• Aliquip ex ea commodo consequat<br/>
							• Sed do eiusmod tempor incididunt ut labore et dolore</p>

						<p>Please fill in the form below to apply for this position.</p>

					</div><!-- medium-8 columns -->

					
					<div class="medium-4 columns">

							<div class="column-block">
								<a href="#" class="button block blue larger-text" id="button--apply-for-this-job">Click here to apply for this job</a>
							</div>

							<div class="column-block">
								<span><strong>What do people say about working for the Bakkavor Group?</strong></span>
							</div>

							<div class="background-grey inset-reviews">

								<div class="inset-review row">

									<div class="column">
										<span><strong>Marketing Analyst, Desserts at Bakkavor Group</strong></span>
									</div>

									<div class="small-6 columns">

											<div class="stars">
															<div class="stars-gold" style="width: 94%;"> &nbsp; </div>
															<div class="stars-white"> &nbsp; </div>
											</div>

									</div>

									<div class="small-6 columns date-posted">

										<span>Posted 19 Aug 2016</span>

									</div>

									<div class="column button-centered">
										
										<p>“You Get put into a real job, with real responsibilities. 2. Get involved in wide variety of projects. 3. Knowing that once scheme is finished you will have a permanent job. 4. Get experience dealing with customers - not just sat at desk all day...”</p>

										<a href="#" class="button gold">Find out more</a>

									</div>

								</div> <!-- inset-review-->
								

								<div class="inset-review row">

									<div class="column">
										<span><strong>Materials Controller, Materials Planning at Bakkavor Group</strong></span>
									</div>

									<div class="small-6 columns">

											<div class="stars">
															<div class="stars-gold" style="width: 80%;"> &nbsp; </div>
															<div class="stars-white"> &nbsp; </div>
											</div>

									</div>

									<div class="small-6 columns date-posted">

										<span>Posted 19 Aug 2016</span>

									</div>

									<div class="column button-centered">
										
										<p>“You Get put into a real job, with real responsibilities. 2. Get involved in wide variety of projects. 3. Knowing that once scheme is finished you will have a permanent job. 4. Get experience dealing with customers - not just sat at desk all day...”</p>

										<a href="#" class="button gold">Find out more</a>

									</div>

								</div> <!-- inset-review-->
								



							</div><!-- background-grey inset-reviews-->

					</div><!-- medium-4 columns -->

				</div> <!-- row padded -->


				<div class="main-content--grey-footer padded" id="individual-job--application-form">

						<div class="padded-half">
							<h3>Apply for the HR Graduate Scheme at Bakkavor Group</h3>
						</div>

						<?php gravity_form( 1, false, false, false, '', false ); ?>

				</div><!-- main-content-grey-footer -->


			
	</div><!--main-content-main -->










	<div class="sidebar-left" data-equalizer-watch="main-content">


			<div class="sidebar--header" data-equalizer-watch="main-content-headers">
				<h3>Job Details</h3> <svg class="icon icon-details"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-details"></use></svg>
			</div>


			<div class="sidebar-left--company-logo">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/example-logo7.png" alt="anglian water" />
			</div>



			<div class="sidebar-left--company-details row">

				<div class="small-6 columns">Job Title</div>
				<div class="small-6 columns">HR Graduate Scheme</div>
				

				<div class="small-6 columns">Company</div>
				<div class="small-6 columns"><a href="#">Bakkavor</a></div>
				

				<div class="small-6 columns">Location</div>
				<div class="small-6 columns">East Anglia, England</div>
				

				<div class="small-6 columns">Salary</div>
				<div class="small-6 columns">£26k - £29k</div>
				

				<div class="small-6 columns">Date Posted</div>
				<div class="small-6 columns">01 July 2017</div>
				

				<div class="small-6 columns">Closes</div>
				<div class="small-6 columns">16 Aug 2017</div>
				

				<div class="small-6 columns">Industry</div>
				<div class="small-6 columns"><a href="#">HR</a>, <a href="#">Finance</a>, <a href="#">Technical</a>, <a href="#">Manufacturing</a></div>


				<div class="small-6 columns">Job Type</div>
				<div class="small-6 columns">Graduate, Full Time</div>
				
			</div><!-- row -->

			<div class="sidebar-left--company-media row">

				<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/example-pic3.jpg" alt="anglian water" /><br/>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/example-pic4.jpg" alt="anglian water" />
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/example-pic5.jpg" alt="anglian water" />

			</div><!-- sidebar-left-company-media -->






			<div class="sidebar--header">
				<h3>Application Tips</h3> <svg class="icon icon-bulb"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-bulb"></use></svg>
			</div>

			<div class="sidebar-left--application-tips">

					<p>Employers constantly tell us that there is nothing that impresses them more than a graduate who has properly researched the company and really knows why they want to work there - so make sure that you tell them in your application that you have read-up all about them on TheJobCrowd and so you really know what makes that company great and why they are right for you. It will help your application, we promise!</p>

			</div> <!-- sidebar-left-key-information-->




	</div><!--sidebar-left -->








	<div class="sidebar-right" data-equalizer-watch="main-content">

		<div class="sidebar--header" data-equalizer-watch="main-content-headers">
			<h3>More Jobs Like This</h3>
		</div>

		<div class="saved-jobs">
			<div class="saved-jobs--job">
				<strong>Software Architect</strong><br>
				Smith &amp; Williamson<br>
				Full Time (Graduate Job)<br>
				Location:   <strong>Brighton</strong> <br>  
				Salary:       <strong>£36,000</strong>
			</div>
			<div class="saved-jobs--job">
				<strong>Senior Software Developer</strong><br>
				nucleargraduates<br>
				Full Time (Graduate Job)<br>
				Location:   <strong>Brighton</strong> <br>  
				Salary:       <strong>£29,000</strong>
			</div>
			<div class="saved-jobs--job">
				<strong>Software Architect</strong><br>
				Smith &amp; Williamson<br>
				Full Time (Graduate Job)<br>
				Location:   <strong>Brighton</strong> <br>  
				Salary:       <strong>£34,000</strong>
			</div>
		</div>


		
		<div class="sidebar--header">
			<h3>More Jobs in Energy &amp; Utilities</h3>
		</div>

		<div class="saved-jobs">
			<div class="saved-jobs--job">
				<strong>Senior Software Developer</strong><br>
				nucleargraduates<br>
				Full Time (Graduate Job)<br>
				Location:   <strong>Brighton</strong> <br>  
				Salary:       <strong>£29,000</strong>
			</div>
			<div class="saved-jobs--job">
				<strong>Software Architect</strong><br>
				Smith &amp; Williamson<br>
				Full Time (Graduate Job)<br>
				Location:   <strong>Brighton</strong> <br>  
				Salary:       <strong>£34,000</strong>
			</div>
		</div>



		
		<div class="sidebar--header">
			<h3>Energy and Utilities Employers</h3>
		</div>

		<div class="row padded">
				<strong>National Grid</strong>

											<div class="stars">
															<div class="stars-gold" style="width: 80%;"> &nbsp; </div>
															<div class="stars-white"> &nbsp; </div>
											</div>

		</div>

		<div class="row padded">
				<strong>Sellafield Ltd</strong>
											<div class="stars">
															<div class="stars-gold" style="width: 95%;"> &nbsp; </div>
															<div class="stars-white"> &nbsp; </div>
											</div>
		</div>

		<div class="row padded">
				<strong>Thames Water</strong>
											<div class="stars">
															<div class="stars-gold" style="width: 75%;"> &nbsp; </div>
															<div class="stars-white"> &nbsp; </div>
											</div>
		</div>






	</div><!--sidebar-right -->



</div> <!-- main-content -->

 <?php get_footer();
