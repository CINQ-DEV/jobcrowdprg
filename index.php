<?php
/*
Template Name: Blog
 */

 get_header(); ?>


<div class="page-header row">

	<div class="medium-5 columns">
		<h1>Blog</h1>
	</div>
	
	<?php get_template_part('template-parts/page-header-search'); ?>


</div><!-- page-header -->


<div class="main-content" data-equalizer="main-content">



<script src="<?php echo get_stylesheet_directory_uri(); ?>/isotope.js"></script>


	<div class="main-content-main fullwidth">

				<div class="main-content-main--breadcrumbs">
							
							<?php
							if ( function_exists('yoast_breadcrumb') ) {
							yoast_breadcrumb('
							<p id="breadcrumbs">','</p>
							');
							}
							?>
							
					<div class="save-share">

						<!--<a href="#" class="savethis">
							<svg class="icon icon-heart"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-heart"></use></svg> Save this
						</a>-->

				<ul id="filters">
				    <li><a data-filter="*" class="selected">All</a></li>
					<?php 
						$terms = get_terms('category'); // get all categories, but you can use any taxonomy
						$count = count($terms); //How many are they?
						if ( $count > 0 ){  //If there are more than 0 terms
							foreach ( $terms as $term ) {  //for each term:
								echo "<li><a data-filter='.".$term->slug."'>" . $term->name . "</a></li>\n";
								//create a list item with the current term slug for sorting, and name for label
							}
						} 
					?>
				</ul>


					</div>

				</div><!-- main-content-main-breadcrumbs -->


				<div class="blog-posts-container row small-up-1 medium-up-2 large-up-3" data-equalizer="blog-post">

				<?php 
  				$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
				$the_query = new WP_Query( array( 'paged' => $paged, 'post_type' => 'post', 'posts_per_page' => 30) ); //Check the WP_Query docs to see how you can limit which posts to display ?>
					<?php if ( $the_query->have_posts() ) : ?>

							<?php /* Start the Loop */ ?>
							    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); 					

								$termsArray = get_the_terms( $post->ID, 'category' );  //Get the terms for this particular item
								if(!empty($termsArray)) {
								$termsString = ""; //initialize the string that will contain the terms
									foreach ( $termsArray as $term ) { // for each term 
										$termsString .= $term->slug.' '; //create a string that has all the slugs 
									}
								}
							?> 

							
							<div class="<?php echo $termsString; ?> blog-post column"> 

								<a href="<?php the_permalink(); ?>">

									<div class="blog-posts-container--post" style="background-image: url('<?php if(has_post_thumbnail()): the_post_thumbnail_url('large'); else: echo get_stylesheet_directory_uri().'/img/employees-bg.jpg'; endif; ?>');" data-equalizer-watch="blog-ipost">

										<div class="blog-posts-container--details">

											<h2><?php the_title(); ?></h2>

											<!--<span><?php// echo get_the_date( 'd-m-Y' ); ?></span>-->

										</div>


									</div>

								</a>

							</div><!--blog-post-->

					<?php endwhile; ?>

					<?php else : ?>
						<?php get_template_part( 'template-parts/content', 'none' ); ?>

					<?php endif; // End have_posts() check. ?>

				</div><!-- blog-posts-container -->

					<?php
					if ( function_exists( 'foundationpress_pagination' ) ) :
						foundationpress_pagination();
					elseif ( is_paged() ) :
					?>
						<nav id="post-nav">
							<div class="post-previous"><?php next_posts_link( __( '&larr; Older posts', 'foundationpress' ) ); ?></div>
							<div class="post-next"><?php previous_posts_link( __( 'Newer posts &rarr;', 'foundationpress' ) ); ?></div>
						</nav>
					<?php endif; ?>


			
	</div><!--main-content-main -->










</div> <!-- main-content -->

 <?php get_footer();
