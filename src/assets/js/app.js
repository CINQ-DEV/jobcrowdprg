import $ from 'jquery';
import whatInput from 'what-input';

window.$ = $;

import Foundation from 'foundation-sites';
// If you want to pick and choose which modules to include, comment out the above and uncomment
// the line below
//import './lib/foundation-explicit-pieces';

//import './lib/isotope';
import './lib/cookie';
import './lib/slick.min';
import './lib/zscripts';
import './lib/jquery.magnific-popup.min';
import './lib/svgxuse.min';


$(document).foundation();
