$(document).ready(function(){ 

	$(".hero--search input").focus(function() {
		$(this).parent().addClass("has-focus");
	});	


	$(".hero--search input").blur(function() {
		    if( $(this).val().length === 0 ) {
		    	$(this).parent().removeClass("has-focus");
		    }
	});


	$("#button--apply-for-this-job").click(function() {
	    $('html, body').animate({
	        scrollTop: $("#individual-job--application-form").offset().top
	    }, 300);

	});


	$('.image-gallery').magnificPopup({
	  delegate: 'a', // child items selector, by clicking on it popup will open
	  type: 'image',
	      gallery: {
		      enabled: true
		    },

	  // other options
	});


	$(".industries-more").click(function() {
		$(".results-categories--container").toggleClass('show-all-industries');
		$(".icon-downarrow-small").toggleClass('icon-rotated');
	});

	$(".industries-more").click(function() {
		$(".industries-wrapper").toggleClass('show-all-industries');
		$(".more-icon-down-arrow").toggleClass('icon-rotated');
	});


	$(".savethis").click(function() {
		$(this).find(".icon-heart-hidden").addClass("heartpulse");
		$(this).find(".save-message").text("Saved");
		$(this).parent().find(".btn.remove").show();

	});


	$(".selected").find(".save-message").text("Saved");


	$(".btn.remove").click(function() {
		$(this).parent().find(".icon-heart-hidden").removeClass("heartpulse");
		$(this).parent().find(".save-message").text("Save this");
		$(this).hide();
		//$(".saved-jobs").load("https://www.thejobcrowds.com/migrationTest/wp-content/themes/jobcrowdprg/sidebar-saved-jobs.php");
	});

	$(".home-hero--images").slick({
		slidesToShow: 1,
		autoplay: true,
		fade: true,
		speed: 3000,
		autoplaySpeed: 6000,
		pauseOnHover: false
	});

	$(".not-sure-which-toggle").click(function() {
		$(".not-sure-which-review").slideToggle('fast');
	});

	if ($("#sitePopup").length) {
		if ($.cookie("popupcookie") == null) {

			var popupdelay = $("#sitePopup").attr("data-popupdelay");
			function delaypopup(){
			  	$('#sitePopup').foundation('open');
				  $.cookie("popupcookie", 1, {expires: 10, path:'/'});
			}
			setTimeout(delaypopup, popupdelay);
		}
	}

	var enhancedProfileTitle = $(".enhanced-profile-title");
	var enhancedProfile = $(".enhanced-profile");

	enhancedProfileTitle.click(function() {

		enhancedProfile.toggleClass("expanded");
		
  		$(".main-content").foundation();

	});


	$(".page-template-company-details .tabs-title>a").click(function() {
			
			function explode(){
				$(".main-content-main").css("height","auto");
			}
			setTimeout(explode, 100);

		console.log("tab clicked, content resized 2");


	});


});  // document ready function