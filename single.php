<?php get_header(); ?>


<div class="page-header row">

	<div class="medium-5 columns">
		<h1><?php the_title(); ?></h1>
		<!--<span class="search-results-count"><?php// echo get_the_date('Y-m-d'); ?></span>-->
	</div>

	<?php get_template_part('template-parts/page-header-search'); ?>

</div><!-- page-header -->



<div class="main-content no-right-column" data-equalizer="main-content">



		<div class="main-content-main" data-equalizer-watch="main-content">

					<div class="main-content-main--breadcrumbs" data-equalizer-watch="main-content-headers">
							
							<?php
							if ( function_exists('yoast_breadcrumb') ) {
							yoast_breadcrumb('
							<p id="breadcrumbs">','</p>
							');
							}
							?>

						<!--<div class="sort-results">
							<span>Relevance</span> &nbsp; | &nbsp; <a href="#">Newest First</a>
						</div>-->

					</div>

			<?php while ( have_posts() ) : the_post(); ?>
				<article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">

					<div class="entry-content padded">
						<?php the_content(); ?>
						<?php get_template_part('layout'); ?>
						
					<footer>
						<?php
							wp_link_pages(
								array(
									'before' => '<nav id="page-nav"><p>' . __( 'Pages:', 'foundationpress' ),
									'after'  => '</p></nav>',
								)
							);
						?>
						<p><?php the_tags(); ?></p>
					</footer>
					</div>

				</article>
			<?php endwhile;?>

		</div><!-- main-content-main -->


		<div class="sidebar-left" data-equalizer-watch="main-content">


			<div class="sidebar--header" data-equalizer-watch="main-content-headers">
				<h3>Blog Articles</h3>
			</div>

			<div class="padded blog-sidebar">
				
				<?php dynamic_sidebar( 'sidebar-widgets' ); ?>

				<h3>Archive</h3>
		        <?php $month= date('m', strtotime('-1 month')); #echo $month; ?>
		        
		        <ul class="post-categories">
			        
			        <?php #wp_get_archives('type=monthly&limit=6'); ?>
			        <?php wp_get_archives('type=monthly&limit='.$month); ?>
			        <?php wp_get_archives('type=yearly'); ?> 	            	           
		            
		        </ul>
		        
		        <h3>Archive</h3>
		        <?php echo get_the_category_list(); ?>

			</div>


	</div><!--sidebar-left -->




</div>


<?php get_footer();
