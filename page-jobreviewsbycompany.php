<?php
/*
Template Name: Companies
 */

 get_header(); ?>


<div class="page-header row">

	<div class="medium-5 columns">
		<h1><?php the_title(); ?>aa</h1>
	</div>
	
	<?php get_template_part('template-parts/page-header-search'); ?>

</div><!-- page-header -->


<div class="main-content" data-equalizer="main-content">






	<div class="main-content-main" data-equalizer-watch="main-content">

				<div class="main-content-main--breadcrumbs" data-equalizer-watch="main-content-headers">
							<?php
							if ( function_exists('yoast_breadcrumb') ) {
							yoast_breadcrumb('
							<p id="breadcrumbs">','</p>
							');
							}
							?>

				</div><!-- main-content-main-breadcrumbs -->
					
					<?php  
					# get the employers from woocommerce, so target specific roles
					#$users = get_users( [ 'role__in' => [ 'employer', 'subscriber' ] ] ); #remove admin from this list after testing
					#https://codex.wordpress.org/Function_Reference/get_users
					$args = array(
						#'role__in'		=> array('employer', 'subscriber'),
						'role__in'		=> array('customer', 'subscriber'),
						'role__not_in'	=> array('administrator'),
						'exclude'		=> array(1),
						//'orderby'		=> 'user_nicename',
						//'order'			=> 'ASC',
						//'numberposts'   => 5,
						//'posts_per_page'   => 5,
						#'meta_key'		=> 'activate_membership',
						#'meta_value'	=> 'Yes',
						#'meta_compare'	=> '=',
						'meta_key'		=> 'overall_rating',
						'orderby'		=> 'meta_value_num',
						'order'			=> 'DESC'
							
					      );
					$users = get_users($args);
					if ( $users ) : 
						$count = 0; ?>

				<div class="row padded job-reviews-by-company" data-equalizer="job-review-footer">

					<div class="column text-center top-companies-toggle">
						<a class="button active" href="#">Top 100 companies as reviewed by graduates</a>
						<a class="button" href="#">Top 100 companies as reviewed by apprentices</a>
					</div>

	<?php foreach ($users as $user) : 
	#$company_name = get_user_meta( $user->ID, 'billing_company', true );	
	$company_name = get_field('company_name', 'user_'.$user->ID);
	
	$hyphenate_company_name = str_replace(" ", "-", $company_name); 
	
	$average_total_reviews = get_field('average_total_reviews', 'user_'.$user->ID);
	$overall_rating = get_field('overall_rating', 'user_'.$user->ID);
	$overall_rating_percent = $overall_rating * 20;
	$employer_registration_industry = get_field('employer_registration_industry', 'user_'.$user->ID);
	$number_of_employees = get_field('number_of_employees', 'user_'.$user->ID);
	if ($company_name): ?>

		<div class="medium-6 columns job-reviews-by-company--job-review">

		        <a href="<?php echo esc_url( home_url( '/' ) ).'companies/details/?company='.$hyphenate_company_name.'&cid='.$user->ID; ?>" title="View the full details on <?php echo $company_name; ?>">
		        	
									<div class="job-review--header">
										
												<span class="job-review-title"><?php echo $company_name; ?></span>
										
												 <?php if ($average_total_reviews != '0'): ?>
													
												<div class="stars">
													<div class="stars-gold" style="width: <?php echo $overall_rating_percent; ?>%;"> &nbsp; </div>
													<div class="stars-white"> &nbsp; </div>
												</div>
													<?php #$review = '<img src="'.get_template_directory_uri().'/img/stars_'.$overall_rating.'.png" /> <small> based on</small> '.$average_total_reviews.' <small>reviews</small>'; 
													#'<span>'.$average_total_reviews.' reviews</span>'; 
												else:	
													$review = '<small>no reviews</small>'; 
												endif; ?>



									</div><!--job-review-header-->

									<div class="job-review--footer" data-equalizer-watch="job-review-footer">
										<?php if ($employer_registration_industry): ?>
											<span>Industry: <strong><?php echo $employer_registration_industry; ?></strong></span>
										<?php endif; ?>

										<?php if ($number_of_employees): ?>
											<span>Number of employees: <strong><?php echo $number_of_employees; ?></strong></span>
										<?php endif; ?>

										<?php
										#get the main_address
										$main_address = get_field('main_address', 'user_'.$user->ID);
										if($main_address):
											#use geocoding to get the necessary details
											$geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?sensor=false&address=' . urlencode($main_address) . '&key=AIzaSyAj_Xc1gEDKeDqLIMFHgok9hhWgyf16TK0');
											$output = json_decode($geocode);	
											
											#get the address components
											$address_data = $output->results[0]->address_components;
											//$street = str_replace('Dr', 'Drive', $address_data[1]->long_name);
											$town = $address_data[2]->long_name;
											//$county = $address_data[3]->long_name;	
										?>

											<?php if($town): ?>
												<span>
													Location:  <strong><?php echo $town; ?></strong>
												</span>
											<?php endif; // if town?>

										<?php endif; // if has main address ?>

									</div><!--job-review-footer-->
		        	
		        </a>
		        
		</div>
<?php 	$count++;
	if($count > 49) {
		break;
	}
?>
    		
	<?php endif; endforeach; ?>

				</div> <!-- row padded -->

<?php endif; ?>
			
	</div><!--main-content-main -->










	<div class="sidebar-left" data-equalizer-watch="main-content">


			<div class="sidebar--header" data-equalizer-watch="main-content-headers">
				<h3>Filter Reviews</h3> <svg class="icon icon-details"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-details"></use></svg>
			</div>

<form id="job-reviews-by-company" class="padded" action="<?php bloginfo('url');?>/reviews/" method="post">

	<?php
	# we need to get a list of all industries from the options page

	# reset choices
	$field['industries'] = array();
	        
	# get the textarea value from options page without any formatting
	$industries = get_field('industry', 'option', false);
	    
	# explode the value so that each line is a new array piece
	$industries = explode("\n", $industries);
	    
	# remove any unwanted white space
	$industries = array_map('trim', $industries);
	    
	# loop through array and add to field 'choices'
	if( is_array($industries) ) :
		foreach( $industries as $industry ) :
			if($industry === '- Select Industry -') continue; #skip first item    	
			#$industry2 = str_replace("&","&amp;",$industry);		
			$field['industries'][ $industry ] = '<li><label class="container"><input type="checkbox" name="industries[]" value="'.$industry.'"><span class="checkmark"></span> '.$industry.'</label></li>';
	            
		endforeach;
	endif;

	# convert array into comma-separated string
	$industries = implode('', $field['industries'] );
		echo '<h2>Reviews by Industry </h2>';
		echo '<div class="industries-wrapper">';
		echo '<ul>'.$industries.'</ul>';
		echo '</div>';
		//echo '<span class="industries-more">More <svg class="icon icon-downarrow-small"><use xlink:href="'. get_stylesheet_directory_uri() .'/img/icons.svg#icon-downarrow-small"></use></svg></span>';
		echo '<span class="industries-more">More <img src="'. get_stylesheet_directory_uri() .'/img/icon-down-arrow.png" class="more-icon-down-arrow"/></span>';
	?>
	 
	 <hr>
 
<label for="towns"><h2>Reviews by Town</h2></label>
<input id="towns" name="towns" value="" placeholder="E.g. Brighton">

	 <hr>

<h2>Reviews by Rating</h2>
<ul class="reviews-by-rating">
<li>
	<label class="radio-container container">
	<input type="radio" name="ratings" value="0"> 
			<span class="checkmark"></span>
			<div class="stars">
				<div class="stars-gold" style="width: 0%;"> &nbsp; </div>
				<div class="stars-white"> &nbsp; </div>
			</div>
	</label>
</li>

<li>
	<label class="radio-container container">
	<input type="radio" name="ratings" value="1"> 
			<span class="checkmark"></span>
			<div class="stars">
				<div class="stars-gold" style="width: 20%;"> &nbsp; </div>
				<div class="stars-white"> &nbsp; </div>
			</div>
	</label>

</li>

<li>
	<label class="radio-container container">
	<input type="radio" name="ratings" value="2"> 
			<span class="checkmark"></span>
			<div class="stars">
				<div class="stars-gold" style="width: 40%;"> &nbsp; </div>
				<div class="stars-white"> &nbsp; </div>
			</div>
	</label>

</li>

<li>
	<label class="radio-container container">
	<input type="radio" name="ratings" value="3"> 
			<span class="checkmark"></span>
			<div class="stars">
				<div class="stars-gold" style="width: 60%;"> &nbsp; </div>
				<div class="stars-white"> &nbsp; </div>
			</div>
	</label>

</li>

<li>
	<label class="radio-container container">
	<input type="radio" name="ratings" value="4"> 
			<span class="checkmark"></span>
			<div class="stars">
				<div class="stars-gold" style="width: 80%;"> &nbsp; </div>
				<div class="stars-white"> &nbsp; </div>
			</div>
	</label>

</li>

<li>
	<label class="radio-container container">
	<input type="radio" name="ratings" value="5"> 
			<span class="checkmark"></span>
			<div class="stars">
				<div class="stars-gold" style="width: 100%;"> &nbsp; </div>
				<div class="stars-white"> &nbsp; </div>
			</div>
	</label>

</li>

</ul> 

<br/>
<input value="Search Reviews" type="submit" name="submit" class="filter-reviews-button button blue block" /> 

</form> 









	</div><!--sidebar-left -->








	<div class="sidebar-right" data-equalizer-watch="main-content">

		<div class="sidebar--header" data-equalizer-watch="main-content-headers">
			<h3>Your saved jobs</h3>
		</div>

		<div class="saved-jobs">

			<?php get_template_part("sidebar-saved-jobs"); ?>

		</div><!-- saved-jobs -->

	</div><!--sidebar-right -->



</div> <!-- main-content -->


<script type="text/javascript">
	

	$(".search-results-block--savethis .btn.action").click(function() {

		//
		$(".saved-jobs").load("<?php echo get_stylesheet_directory_uri(); ?>/sidebar-saved-jobs.php");


	});


</script>



</div> <!-- main-content -->

 <?php get_footer();
