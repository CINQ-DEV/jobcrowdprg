<?php
/*
Template Name: Top Apprentice Companies
 */

 get_header(); ?>


<div class="page-header row">

	<div class="medium-5 columns">
		<h1><?php the_title(); ?></h1>
	</div>
	
	<?php get_template_part('template-parts/page-header-search'); ?>

</div><!-- page-header -->


<div class="main-content no-right-column" data-equalizer="main-content">


	<div class="main-content-main" data-equalizer-watch="main-content">

				<div class="main-content-main--breadcrumbs" data-equalizer-watch="main-content-headers">
							<?php
							if ( function_exists('yoast_breadcrumb') ) {
							yoast_breadcrumb('
							<p id="breadcrumbs">','</p>
							');
							}
							?>

				</div><!-- main-content-main-breadcrumbs -->
					


				<div class="row top-companies padded job-reviews-by-company" data-equalizer="job-review-footer">

					<div class="column text-center top-companies-toggle">
						<a class="button" href="<?php bloginfo('home'); ?>/companies/top-100-graduate-employers/">Top 100 companies as reviewed by graduates</a>
						<a class="button active" href="#">Top 50 companies as reviewed by apprentices</a>
					</div>
					

					<?php if( have_rows('apprentice_list') ): 
						 $i = 1;
						 while( have_rows('apprentice_list') ): the_row(); 

							// vars
							$company_name = get_sub_field('company_name');
							$logo = get_sub_field('logo');
							$quote = get_sub_field('quote');
							$company_name = str_replace("&amp;","&",$company_name);

							$users = get_users(array('meta_key' => 'company_name', 'meta_value' => $company_name ));
							foreach ( $users as $user ) :
								$cID =  $user->ID;
							endforeach;		

							$hyphenate_company_name = str_replace(" ", "-", $company_name); 			
							?>

							<div class="medium-12 columns job-reviews-by-company--job-review">

								<div class="job-review--header">
									<span class="job-review-title"><?php echo $company_name; ?></span>
								</div><!-- /job-review-header -->

								<div class="job-review--footer" data-equalizer-watch="job-review-footer">

									<div class="logo-container">
									<img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt'] ?>" />
									</div><!-- /logo -->

									<span class="top-companies--number">
										<?php echo $i; ?>
									</span>

									<?php 
									
									if ($quote):
									echo '<p class="company--quote">' . $quote . '</p>'; 
									endif; ?>

									<p><a href="<?php echo esc_url( home_url( '/' ) ).'companies/details/?company='.$hyphenate_company_name.'&cid='.$cID ?>" class="button pink" title="View the full details on <?php echo $company_name; ?>">Overview</a>

									<a href="<?php echo esc_url( home_url( '/' ) ).'companies/details/?company='.$hyphenate_company_name.'&cid='.$cID ?>#graduates" class="button" title="View the full details on <?php echo $company_name; ?>">Reviews</a></p>					

								</div><!-- /job-review-footer -->

							</div><!-- /medium-6 columns -->

						<?php $i++; endwhile; 

					endif; ?>	


				</div> <!-- row padded -->
			
	</div><!--main-content-main -->

	<div class="sidebar-left" data-equalizer-watch="main-content">


            <div class="sidebar--header" data-equalizer-watch="main-content-headers">
                <h3>Top companies by sector</h3> <svg class="icon icon-details"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-details"></use></svg>
            </div>

            <div class="padded">

                <?php
                # we need to get a list of all industries from the options page

                # reset choices
                $field['industries'] = array();
                        
                # get the textarea value from options page without any formatting
                $industries = get_field('industry', 'option', false);
                    
                # explode the value so that each line is a new array piece
                $industries = explode("\n", $industries);
                    
                # remove any unwanted white space
                $industries = array_map('trim', $industries);
                $url = get_site_url();

                # loop through array and add to field 'choices'
                if( is_array($industries) ) :
                    foreach( $industries as $industry ) :
                        if($industry === '- Select Industry -') continue; #skip first item      
                        #$industry2 = str_replace("&","&amp;",$industry);     
				
						$args = array(
							'post_type'  => 'page', 
							posts_per_page => '1',
							'meta_query' => array( 
								array(
									'key'   => 'industry_sector', 
									#'value' => 'Accountancy &amp; Insurance',
									'value' => $industry,	
									'compare' => '='
								)
							)
						);			

						$the_query = new WP_Query( $args );
						if( $the_query->have_posts() ):
							while( $the_query->have_posts() ) : $the_query->the_post(); ?>
								<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>		
							<?php endwhile;			
						endif;				
                            
                    endforeach;
                endif; ?>
            </div>


	</div><!--sidebar-left -->


</div> <!-- main-content -->


</div> <!-- main-content -->

 <?php get_footer();
