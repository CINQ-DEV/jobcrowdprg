<?php
/*
Template Name: Employers Register
 */

 get_header(); ?>


<div class="page-header row">

	<div class="text-center page-header--larger columns">
		<h1>Employers Area > Register</h1>
	</div>
	

</div><!-- page-header -->


<div class="main-content" data-equalizer="main-content">






	<div class="main-content-main" data-equalizer-watch="main-content">

				<div class="main-content-main--breadcrumbs" data-equalizer-watch="main-content-headers">
							<?php
							if ( function_exists('yoast_breadcrumb') ) {
							yoast_breadcrumb('
							<p id="breadcrumbs">','</p>
							');
							}
							?>

				</div><!-- main-content-main-breadcrumbs -->

				<div class="row padded">
					
						<?php the_field('main_content'); ?>

				</div> <!-- row padded -->


				<div class="main-content--grey-footer padded">

			<?php if( have_rows('profile') ):  $profilecount = 0; ?>
				
				<div class="row" data-equalizer="employers-footer">

				<?php while( have_rows('profile') ): the_row();

					// vars
					$title = get_sub_field('title');
					$text = get_sub_field('text');
					$price = get_sub_field('Price');
					$image = get_sub_field('image');
					$colour = get_sub_field('colour');
					$link = get_sub_field('link');
					$button_text = get_sub_field('button_text');
					$profilecount++;

					?>


								<div class="medium-6 columns column-block employer-page--basic-container">
									<a href="<?php echo $link; ?>">

										<div class="employer-page--header" style="background-color: <?php echo $colour; ?>"><?php echo $title; ?></div>

										<div class="employer-page--footer" data-equalizer-watch="employers-footer">

											<?php echo $text; ?>

											<span class="price"><?php echo $price; ?></span>

											<img src="<?php echo $image['sizes']['medium']; ?>" alt="profile">
											<?php if ($button_text): ?>
	
											<button type="button" class="button" style="background-color: <?php echo $colour; ?>"><?php echo $button_text; ?></button>
											<?php endif; ?>
										</div>

								</a>
							
							</div>

						<?php endwhile; ?>

						</div>

					<?php endif; ?>



						<?php// gravity_form( 1, false, false, false, '', false ); ?>


				</div><!-- main-content-grey-footer -->


			
	</div><!--main-content-main -->










	<div class="sidebar-left" data-equalizer-watch="main-content">


			<div class="sidebar--header" data-equalizer-watch="main-content-headers">
				<h3>Benefits of Registering</h3> <svg class="icon icon-tick"><use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons.svg#icon-tick"></use></svg>
			</div>


			<?php if( have_rows('benefits_of_registering') ):  $i = 0;
				while( have_rows('benefits_of_registering') ): the_row();

					// vars
					$benefit_title = get_sub_field('benefit_title');
					$benefit_details = get_sub_field('benefit_details');
					$i++;

					?>

						<div class="sidebar-left--benefits row">

							<div class="benefits-number-container">

								<span class="benefits-number"><?php echo $i; ?></span>

							</div>

							<div class="benefits-details"><span class="title"><?php echo $benefit_title; ?></span>
								<p><?php echo $benefit_details; ?></p>
							</div>

						</div><!-- benefits row -->

						
					<?php endwhile; 
			endif; ?>


	</div><!--sidebar-left -->








	<div class="sidebar-right" data-equalizer-watch="main-content">

				<?php if( have_rows('right_sidebar') ):  $ii = 0;
					while( have_rows('right_sidebar') ): the_row();

						$section_title = get_sub_field('section_title');
						$section_title_icon = get_sub_field('section_title_icon');
						$section_content = get_sub_field('section_content');
						
						$ii++;
					?>

							<div class="sidebar--header" <?php if($ii == 1): ?>data-equalizer-watch="main-content-headers"<?php endif; ?>>
								<h3><?php if($section_title_icon): ?><img src="<?php echo $section_title_icon['url']; ?>" alt="<?php echo $image['alt'] ?>" />&nbsp; &nbsp;  <?php endif; ?><?php echo $section_title; ?></h3>
							</div>

							<div class="padded">

								<?php echo $section_content; ?>

							</div>

					<?php endwhile; endif; ?>

	</div><!--sidebar-right -->



</div> <!-- main-content -->

 <?php get_footer();
