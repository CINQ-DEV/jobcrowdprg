<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 Template Name: Search Reviews
 */

get_header(); ?>
	
<!-- /include the jquery auto complete files -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.12.4.js"></script>
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	
<?php
# we need to get a list of all towns from the options page

# reset choices
$field['towns'] = array();
        
# get the textarea value from options page without any formatting
$towns = get_field('town', 'option', false);
    
# explode the value so that each line is a new array piece
$towns = explode("\n", $towns);
    
# remove any unwanted white space
$towns = array_map('trim', $towns);
    
# loop through array and add to field 'choices'
if( is_array($towns) ) :
        
	foreach( $towns as $town ) :
		if($town === '- Select Town -') continue; #skip first item                
		$field['towns'][ $town ] = '"'.$town.'"';
            
	endforeach;
        
endif;

# convert array into comma-separated string
$towns = implode(',', $field['towns'] );
?>	
<script>
$( function() {
	var availableTowns = [
		<?php echo $towns; #output the towns as a comma-separated list ?>
	];
	$( "#towns" ).autocomplete({
		source: availableTowns
	});
});
</script>

	<section id="primary" class="content-area col-sm-12 col-md-12 col-lg-8">
		<main id="main" class="site-main" role="main">

<h1>Job Search</h1>

<form id="job-reviews-by-company" action="<?php bloginfo('url');?>/search-reviews/" method="post">

<label for="towns">Towns: </label>
<input id="towns" name="towns" value="">


<?php
# we need to get a list of all industries from the options page

# reset choices
$field['industries'] = array();
        
# get the textarea value from options page without any formatting
$industries = get_field('industry', 'option', false);
    
# explode the value so that each line is a new array piece
$industries = explode("\n", $industries);
    
# remove any unwanted white space
$industries = array_map('trim', $industries);
    
# loop through array and add to field 'choices'
if( is_array($industries) ) :
        
	foreach( $industries as $industry ) :
		if($industry === '- Select Industry -') continue; #skip first item    	
		#$industry = str_replace("&","&amp;",$industry);		
		$field['industries'][ $industry ] = '<li><input type="checkbox" name="industries[]" value="'.$industry.'"> '.$industry.'</li>';
            
	endforeach;
        
endif;

# convert array into comma-separated string
$industries = implode('', $field['industries'] );

echo '<p>Industry: <ul>'.$industries.'</ul></p>';
?>
 
 
<p>Rating: <ul>
<li><input type="radio" name="ratings" value="0"> 0 star</li>
<li><input type="radio" name="ratings" value="1"> 1 star</li>
<li><input type="radio" name="ratings" value="2"> 2 star</li>
<li><input type="radio" name="ratings" value="3"> 3 star</li>
<li><input type="radio" name="ratings" value="4"> 4 star</li>
<li><input type="radio" name="ratings" value="5"> 5 star</li>
</ul></p> 

<input value="Search Reviews" type="submit" name="submit"  /> 
</form> 


<?php
# let's handle the form submission
$town 		= $_POST['towns'];
$industries	= $_POST['industries'];
$ratings	= $_POST['ratings'];
$submit		= $_POST['submit'];

# check the form has been submitted
if ($submit):
	
	if ($town):
		echo 'The town you entered is: ' . $town .'<br/>';
	endif;
	if ($industries):	
		echo 'The industries you selected: ' . implode(',', $_POST['industries']) . '<br/>';
	endif;
	if ($ratings):	
		echo 'The rating you selected: ' . $ratings . '<br/>';     
	endif;


	#convert the & otherwise no results will be returned	
	$industry = str_replace("&","&amp;",$industries);		

		
	#https://codex.wordpress.org/Class_Reference/WP_Query#Custom_Field_Parameters
	$args = array(
		'post_type'		=> array('graduate_reviews','apprentice_reviews'), #combine post types as we store them separately
		'post_status'	=> 'publish',
		'orderby'		=> 'post_date',
		'order'			=> 'DESC',				
		'meta_query'	=> array(
			'relation'	=> 'OR', #can use AND

			array(
				'key'	 	=> 'location',
				'value'	  	=> $town,
				'compare' 	=> 'IN',
			),				
				
			array(
				'key'	 	=> 'apprentice_review_industry',
				'value'	  	=> $industry,
				'compare' 	=> 'IN',
			),
			array(
				'key'	 	=> 'graduate_review_industry',
				'value'	  	=> $industry,
				#'value'	  	=> array('IT Development &amp; Consulting'),
				'compare' 	=> 'IN',
			),			
		
			array(
				'key'	  	=> 'star_rating',
				'value'	  	=> array( $ratings, $ratings.'.9' ),
				'type'    	=> 'numeric',
				'compare' 	=> 'BETWEEN',
			),
		),			
			
	);				
	$wp_query = new WP_Query($args);

	$total = $wp_query->found_posts;
	#echo '<p>Results Found: '.$total.'</p>';		
	
	if ( $wp_query->have_posts() ) :	
			
		while ( $wp_query->have_posts() ) : $wp_query->the_post(); 
		
		#need the ascertain the post type, so we can now get the company that's been reviewed
		$post_type = get_post_type( get_the_ID() );	
		if ($post_type == 'graduate_reviews'):
			$industry = get_field('graduate_review_industry', $wp_query->post->ID);
		else:
			$industry = get_field('apprentice_review_industry', $wp_query->post->ID);
		endif;	
		
		#need to exclude any results whereby the industry hasn't been specified
		if ($industry != '- Select Industry -'):
		?>
									
			<ul>
				<li><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></li>
				<li>Post ID: <?php echo $wp_query->post->ID;  ?></li>
				<li>Industry: <?php echo $industry;  ?></li>
			</ul>
			
		<?php		
		endif; #endif 
							
		endwhile; wp_reset_query(); 	
			
	else:	
		
		echo '<h3>No reviews matched your criteria.</h3>';
		
	endif; #endif $users

else:
					
	#the form hasn't been submitted, so show all reviews
	$args = array(
		'post_type'		=> array('graduate_reviews','apprentice_reviews'), #combine post types as we store them separately
		'post_status'	=> 'publish',
		'orderby'		=> 'post_date',
		'order'			=> 'DESC',								
	);				
	$wp_query = new WP_Query($args);
	
	if ( $wp_query->have_posts() ) :	
			
		while ( $wp_query->have_posts() ) : $wp_query->the_post(); 
		?>
									
			<ul>
				<li><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></li>
				<li>Post ID: <?php echo $wp_query->post->ID  ?></li>
			</ul>
			
		<?php 	 							
		endwhile; wp_reset_query(); 	
			
	else:	
		
		echo '<h3>No reviews available at this time.</h3>';
		
	endif; #endif $users					
					
endif; #endif $submit
?>	

		</main><!-- #main -->
	</section><!-- #primary -->

<?php
get_sidebar();
get_footer();